package org.aplikator.shared.rpc.marshaller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.aplikator.utils.IOUtils;
import org.aplikator.client.shared.data.PrimaryKey;
import org.aplikator.client.shared.data.RecordDTO;
import org.aplikator.client.shared.descriptor.RecordsPageDTO;
import org.aplikator.client.shared.rpc.marshaller.PrimaryKeyMarshaller;
import org.aplikator.client.shared.rpc.marshaller.RecordDTOMarshaller;
import org.aplikator.client.shared.rpc.marshaller.RecordsPageDTOMarshaller;
import org.easymock.EasyMock;
import org.jboss.errai.marshalling.client.api.Marshaller;
import org.jboss.errai.marshalling.client.api.MarshallingSession;
import org.jboss.errai.marshalling.client.api.json.EJValue;
import org.jboss.errai.marshalling.client.marshallers.StringMarshaller;
import org.jboss.errai.marshalling.server.JSONDecoder;
import org.junit.Assert;

import junit.framework.TestCase;

public class RecordsPageDTOMarshallerTest extends TestCase {

    public static RecordDTO createRecord() {
        RecordDTO recordDTO = new RecordDTO();
        recordDTO.setPrimaryKey(new PrimaryKey("modul", 2));

        recordDTO.setValue("datumovepole", new Date());
        recordDTO.setValue("stringovepole", "string");
        recordDTO.setValue("integerovepole", 2);
        recordDTO.setValue("doublepole", 3.3);
        recordDTO.setValue("booleanpole", true);
        return recordDTO;
    }

    public void testMarshall() {

        MarshallingSession session = ctx();

        List<RecordDTO> recs = new ArrayList<RecordDTO>();
        {
            recs.add(createRecord());
            recs.add(createRecord());
            recs.add(createRecord());
            recs.add(createRecord());
        }
        RecordsPageDTO page = new RecordsPageDTO();
        page.setRecords(recs);
        page.setOffset(10);
        RecordsPageDTOMarshaller pdtom = new RecordsPageDTOMarshaller();
        String marshalled = pdtom.marshall(page, session);

        RecordsPageDTO demarshalled = pdtom.demarshall(JSONDecoder.decode(marshalled), session);
        Assert.assertTrue(demarshalled.getRecords().size() == 4);
        Assert.assertTrue(demarshalled.getOffset() == 10);
    }

    public void testMarshalled() throws IOException {
        byte[] bytes = IOUtils.readBytes(ViewDTOMarshallerTest.class.getResourceAsStream("recordspagedto.real.txt"), true);
        EJValue decoded = JSONDecoder.decode(new String(bytes));

        MarshallingSession ctx = ctx();
        RecordsPageDTOMarshaller pdtom = new RecordsPageDTOMarshaller();
        RecordsPageDTO demarshalled = pdtom.demarshall(decoded, ctx);
        Assert.assertNotNull(demarshalled);
    }

    @SuppressWarnings("unchecked")
    private MarshallingSession ctx() {
        MarshallingSession session = EasyMock.createMock(MarshallingSession.class);
        PrimaryKeyMarshaller pkmarsh = new PrimaryKeyMarshaller();
        Object marsh = pkmarsh;
        EasyMock.expect(session.getMarshallerInstance(PrimaryKey.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        StringMarshaller smarsh = new StringMarshaller();
        marsh = smarsh;
        EasyMock.expect(session.getMarshallerInstance(String.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        RecordDTOMarshaller recMarsh = new RecordDTOMarshaller();
        marsh = recMarsh;
        EasyMock.expect(session.getMarshallerInstance(RecordDTO.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        EasyMock.replay(session);
        return session;
    }

}
