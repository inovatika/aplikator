/*
 * Copyright 2013 Pavel Stastny <pavel.stastny at gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aplikator.server.processes.impl;

import org.aplikator.server.processes.Work;

/**
 * @author Pavel Stastny <pavel.stastny at gmail.com>
 */
public class WorksChain extends Thread {

    private Work[] works;

    public WorksChain(Work[] works) {
        this.works = works;
    }

    @Override
    public void run() {
        Object value = null;
        for (Work work : works) {
            if (Thread.currentThread().isInterrupted()) {
                return;
            }
            work.setValue(value);
            work.doWork();
            value = work.getValue();
        }
    }
}
