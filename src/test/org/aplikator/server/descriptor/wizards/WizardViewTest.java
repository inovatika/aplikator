/*
 * Copyright 2013 Pavel Stastny <pavel.stastny at gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aplikator.server.descriptor.wizards;

import org.junit.Test;

/**
 * @author Pavel Stastny <pavel.stastny at gmail.com>
 */
public class WizardViewTest {

    /*
    static class _MTestEntity extends Entity {
    
        public _MTestEntity(String name, String tableName, String primaryKeyName) {
            super(name, tableName, primaryKeyName);
        }
        
        public Wizard createWizardView() {
            Wizard vw = new Wizard(this);
            Property<String> vstupniHodnota = this.stringProperty("Vstupni parametr");
            vw.addProperty(vstupniHodnota);
            vw.form(column(
                row(vstupniHodnota)
            ));
            return vw;
        }
    }
    */

    @Test
    public void testWizardView() {
//        Wizard wizard = new _MTestEntity("_mtest", "_mtest","_mtest").createWizardView();
//        Assert.assertEquals("Wizard{properties=[Property:_mtest.Vstupni parametr], entity=_mtest, form=Form{layout=Panel{children=[Panel{children=[TextField{property=Property:_mtest.Vstupni parametr, labelAbove=true, width=null}], frame=false, vertical=false}], frame=false, vertical=true}, properties=[Property:_mtest.Vstupni parametr]}}", wizard.toString());
    }

    @Test
    public void testWizardViewDTO() {
//        Wizard wizard = new _MTestEntity(UUID.randomUUID().toString(), UUID.randomUUID().toString(),UUID.randomUUID().toString()).createWizardView();
//        WizardDTO wdto = wizard.getViewDTO(null);
//        Assert.assertNotNull(wdto);
//        Assert.assertNotNull(wdto.getEntity());
//        Assert.assertNotNull(wdto.getFormDescriptor());
//        Assert.assertNotNull(wdto.getProperties());

    }
}
