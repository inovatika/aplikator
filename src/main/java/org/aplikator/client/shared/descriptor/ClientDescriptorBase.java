package org.aplikator.client.shared.descriptor;

import java.io.Serializable;

@SuppressWarnings("serial")
public abstract class ClientDescriptorBase implements Serializable {

    protected static final char PATH_DELIMITER = '.';


    /**
     * programmer - assigned string identifier (equal to the id of the corresponding server side descriptor item)
     */
    private String id;

    /**
     * localized name obtained from property resource bundle
     */
    private String localizedName;

    /**
     * localized tooltip obtained from property resource bundle
     */
    private String localizedTooltip;

    /**
     * icon from IconType
     */
    private String icon;

    private Access access = Access.READ_WRITE_CREATE_DELETE;

    public ClientDescriptorBase() {
    }

    public ClientDescriptorBase(String id, String localizedName) {
        if (id == null) {
            throw new IllegalArgumentException("Null client descriptor ID");
        }
        this.id = id;
        this.localizedName = localizedName;
    }


    public String getId() {
        return id;
    }


    public String getLocalizedName() {
        return localizedName;
    }

    public void setLocalizedName(String localizedName) {
        this.localizedName = localizedName;
    }

    public void setLocalizedTooltip(String localizedTooltip) {
        this.localizedTooltip = localizedTooltip;
    }

    public String getLocalizedTooltip() {
        return localizedTooltip;
    }

    public void setAccess(Access access) {
        this.access = access;
    }

    public Access getAccess() {
        return this.access;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public void marshallInitialization(String id, String localizedName, String access, String localizedTooltip, String icon) {
        this.id = id;
        this.localizedName = localizedName;
        this.access = (access == null ? Access.READ_WRITE_CREATE_DELETE : Access.valueOf(access));
        this.localizedTooltip = localizedTooltip;
        this.icon = icon;
    }


    @Override
    public String toString() {
        return localizedName;
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof ClientDescriptorBase))
            return false;
        ClientDescriptorBase other = (ClientDescriptorBase) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }
}
