#Aplikator

Aplikator is a Java web application development framework with  declarative approach. All information about the data structure, behaviour and user interface of the application is maintained in one place - the metadata descriptor.

Both the server side runtime module and default web client read the metadata descriptor and dynamically create and configure all application components. The default web client automatically provides standard GUI for basic create-update-delete data processing.
Metadata descriptor can be extended by standard Java classes that control the behaviour of the components beyond the possibilites of declarative structure description (e.g. persistent triggers or business functions).  

Runtime module is supported by standard services that include:

* persistence manager (currently supporting most relational databases through Apache EmpireDb persistence layer and BLOB storage through JCR repository)
* fulltext index and search engine (based on Elastic Search)
* process scheduler
* user and security manager

The default web client  communicates with the server side runtime through REST API using JSON encoded data. The same API can also be used for custom developed application clients.  


![image](AplikatorCore.jpg)