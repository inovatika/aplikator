package org.aplikator.server.descriptor;

public class Reference<R extends Entity> extends JoinableProperty<R> {

    @SuppressWarnings("unchecked")
    public Reference(String id, R referredEntity, Entity ownEntity, boolean required, boolean checkSelfReference) {
        super(id, referredEntity, ownEntity, required);
        if (checkSelfReference && ownEntity.equals(referredEntity)) {
            throw new IllegalStateException("Self reference not supported");
        }
    }

}
