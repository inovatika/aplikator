package org.aplikator.client.shared.rpc;

import org.aplikator.client.local.Aplikator;
import org.aplikator.client.local.widgets.Messages;
import org.jboss.errai.enterprise.client.jaxrs.api.RestErrorCallback;

import com.google.gwt.http.client.Request;

/**
 *
 */
public class AplikatorErrorCallback implements RestErrorCallback {

    final String label;

    final AfterErrorExecutor afterErrorExecutor;

    public AplikatorErrorCallback() {
        this("aplikator.function.error", null);
    }

    public AplikatorErrorCallback(String label) {
        this(label, null);
    }

    public AplikatorErrorCallback(String label, AfterErrorExecutor executor) {
        if (label == null) {
            this.label = "REST Error:";
        } else {
            this.label = Aplikator.application.getConfigString(label);
        }
        afterErrorExecutor = executor;
    }

    @Override
    public boolean error(Request message, Throwable throwable) {
        if (afterErrorExecutor != null) {
            afterErrorExecutor.execute();
        }
        Messages.restError(label, throwable);
        return false;
    }

}
