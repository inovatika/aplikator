package org.aplikator.server.persistence;

import org.aplikator.client.shared.data.PrimaryKey;
import org.aplikator.client.shared.data.RecordDTO;
import org.aplikator.server.data.BinaryData;
import org.aplikator.server.data.Context;
import org.aplikator.server.data.Record;
import org.aplikator.server.descriptor.*;
import org.aplikator.server.query.QueryExpression;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Date;
import java.util.List;

public interface Persister {

    public Connection getJDBCConnection();

    public Transaction beginTransaction();

    public void commitTransaction(Transaction tx);

    public void rollbackTransaction(Transaction tx);

    public void close(Transaction tx);

    public String generateDDL(boolean updateDB, boolean checkDB);

    public String createLocalizedIndex(boolean updateDB);

    public List<PrimaryKey> getEntityPKs(Entity entity, PrimaryKey ownerPrimaryKey, Property<Integer> ownerProperty, Context ctx);

    public RecordIterator getRecordIterator(View view, QueryExpression queryExpression, SortItem[] sortItems, Property[] groupBy, QueryExpression havingExpression, String searchString, Collection ownerProperty, PrimaryKey ownerPrimaryKey, int pageOffset, int pageSize, Context ctx);

    public List<Record> getRecords(View view, QueryExpression queryExpression, SortItem[] sortItems, Property[] groupBy, QueryExpression havingExpression, String searchString, Collection ownerProperty, PrimaryKey ownerPrimaryKey, int pageOffset, int pageSize, Context ctx);

    public int getRecordCount(View view, QueryExpression queryExpression, SortItem[] sortItems, Property[] groupBy, QueryExpression havingExpression, String searchString, Collection ownerProperty, PrimaryKey ownerPrimaryKey, Context ctx);

    public Record getRecord(View view, PrimaryKey primaryKey, Context ctx, boolean limitToView);

    public RecordDTO getCompleteRecord(PrimaryKey primaryKey, int traverseLevel, boolean includeCollections, Context ctx);

    public Record updateRecord(Record record, Context ctx);

    public void deleteRecord(Record record, Context ctx);

    public BinaryData getBlob(Entity entity, BinaryProperty property, int primaryKey, int maxSize);

    public void registerEntity(Entity entity, String tableName);

    public void registerPrimaryKey(Entity entity, Property<Integer> property, String primaryKeyName, String sequenceName);

    public void registerTimestamp(Entity entity, Property<Date> property, String timestampName);

    public <T extends Serializable> void registerProperty(Entity entity, Property<T> property, String columnName);

   /* public void registerReference(Entity entity, Property<Integer> property, String columnName, Entity referredEntity);

    public void registerReference(Entity entity, Property<Integer> property, String columnName, Entity referredEntity, String indexName);
    */
    public void registerReference(Entity entity, Property<Integer> property, String columnName, Entity referredEntity, String indexName, String foreignKeyName);

    public void addInheritanceIndex(Entity entity, Property<String> discriminator);

    public void registerReverseCollection(Collection<? extends Entity> many, Reference<? extends Entity> one);

    @SuppressWarnings("unchecked")
    public void registerIndex(Entity entity, String name, boolean unique, Property<? extends Serializable>... properties);

    public <T extends Serializable> void registerReferencedProperty(Property<T> original, Property<T> clone);

    /**
     * Return true if the application is in the suspended (read - only) state, which is used for system maintenance and backup
     *
     * @return
     */
    public boolean isSuspended();

    /**
     * Attempt to set the suspended (read-only) application state flag
     *
     * @param suspended true tu set the application in the suspended state
     * @return true if the state was changed successfully, false if the state flag already was in the desired state
     */
    public boolean setSuspended(boolean suspended);

    public void shutdown();
}
