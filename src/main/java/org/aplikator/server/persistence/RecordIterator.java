package org.aplikator.server.persistence;

import org.aplikator.server.data.Record;

import java.io.Closeable;

public interface RecordIterator extends Closeable {
    boolean moveNext();
    Record getRecord();
    void close();
}
