package org.aplikator.server.descriptor;


import org.aplikator.server.persistence.PersisterFactory;

import java.io.Serializable;

public abstract class JoinableProperty<R extends Entity> extends Property<Integer> {

    public R referredEntity;


    public JoinableProperty(String id, R referredEntity, Entity ownEntity, boolean required) {
        super(id, Integer.class, 0, required, ownEntity, false, null); // TODO change when PK is more general
        this.referredEntity = referredEntity;
    }


    public <T extends Serializable> Property<T> relate(Property<T> target) {
        Property<T> related;
        try {
            related = target.clone();
        } catch (CloneNotSupportedException e) {
            throw new IllegalStateException(e);
        }
        related.setRefferedThrough(this);
        PersisterFactory.getPersister().registerReferencedProperty(target, related);
        return related;
    }

    //@SuppressWarnings("unchecked")
    public <T extends Entity> JoinableProperty<T> join(JoinableProperty<T> target) {
        return (JoinableProperty<T>) relate(target);
    }
}
