/*
 * Copyright 2013 Pavel Stastny <pavel.stastny at gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aplikator.server.processes.impl.processes.unix;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.aplikator.server.processes.CannotCallStopException;
import org.aplikator.server.processes.RunnableSerializationAware;
import org.aplikator.server.processes.Work;
import org.aplikator.server.processes.impl.processes.AbstractOSProcess;


/**
 * @author Pavel Stastny <pavel.stastny at gmail.com>
 */
public class UnixOSProcess extends AbstractOSProcess {

    public static final Logger LOGGER = Logger.getLogger(UnixOSProcess.class.getName());

    public UnixOSProcess(String identifier, String appname, RunnableSerializationAware runnable, String[] libs, String[] classpath) {
        super(identifier, appname, runnable, libs, classpath);
    }

    public UnixOSProcess(String identifier, String appname, Work[] works, String[] libs, String[] classpath) {
        super(identifier, appname, works, libs, classpath);
    }

    @Override
    protected void stopMeOSDependent() throws CannotCallStopException {
        try {
            LOGGER.fine("Killing process " + getPid());
            // kill -9 <pid>
            List<String> command = new ArrayList<String>();
            command.add("kill");
            command.add("-9");
            command.add("" + getPid());
            ProcessBuilder processBuilder = new ProcessBuilder(command);
            Process startedProcess = processBuilder.start();
            LOGGER.fine("killing command '" + command + "' and exit command "/*+startedProcess.exitValue()*/);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            throw new CannotCallStopException(e);
        }
    }


}
