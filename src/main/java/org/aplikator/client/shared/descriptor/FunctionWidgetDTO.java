package org.aplikator.client.shared.descriptor;

import org.aplikator.client.local.widgets.FormWidget;
import org.aplikator.client.local.widgets.FunctionButtonWidget;
import org.aplikator.client.local.widgets.HasFields;

import com.google.gwt.user.client.ui.Widget;

//@Portable
public class FunctionWidgetDTO extends WidgetDTO {

    public static final String TYPE_KEY = "";

    private FunctionDTO function;

    public FunctionWidgetDTO() {
    }

    public FunctionWidgetDTO(FunctionDTO function) {
        super();
        this.function = function;
    }

    public FunctionDTO getFunction() {
        return function;
    }

    public void setFunction(FunctionDTO function) {
        this.function = function;
    }

    @Override
    public Widget getWidget(HasFields form) {
        FunctionButtonWidget button = new FunctionButtonWidget(this.function, (FormWidget) form, getElementId(), getCustomLabelStyle(), getIcon(), form);
        button.setEnabled(isEnabled());
        return button;
    }

}
