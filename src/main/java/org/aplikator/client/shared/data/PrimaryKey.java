package org.aplikator.client.shared.data;

import java.io.Serializable;

import org.jboss.errai.common.client.api.annotations.Portable;

@SuppressWarnings("serial")
@Portable
public class PrimaryKey implements Serializable {
    public static final String SERIALIZATION_TOKEN = "$";
    private int id;
    private String tempId;
    private String entityId;

    public PrimaryKey() {//TODO custom marshaller
    }

    public String getSerializationString() {
        String pkString = this.getEntityId() + PrimaryKey.SERIALIZATION_TOKEN + this.getId();
        if (tempId == null) {
            return pkString;
        } else {
            return pkString + PrimaryKey.SERIALIZATION_TOKEN + this.tempId;
        }
    }

    public PrimaryKey(String entityId, int id) {
        this.entityId = entityId;
        this.id = id;
    }

    public PrimaryKey(String serializedValue) {
        String[] serializedArray = serializedValue.split("\\" + PrimaryKey.SERIALIZATION_TOKEN);
        if (serializedArray.length == 2) {
            this.entityId = serializedArray[0];
            this.id = Integer.valueOf(serializedArray[1]);
        } else if (serializedArray.length == 3) {
            this.entityId = serializedArray[0];
            this.id = Integer.valueOf(serializedArray[1]);
            this.tempId = serializedArray[2];
        } else {
            throw new IllegalStateException("Invalid primary key format:" + serializedValue);
        }
    }


    public PrimaryKey(String entityId, String tempId) {
        this.entityId = entityId;
        this.tempId = tempId;
        this.id = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTempId() {
        return tempId;
    }

    public void setTempId(String tempId) {
        this.tempId = tempId;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    @Override
    public String toString() {
        return "PrimaryKey [entity=" + entityId + ", id=" + id + (tempId != null ? (", tempId=" + tempId) : "") + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((entityId == null) ? 0 : entityId.hashCode());
        result = prime * result + id;
        result = prime * result + ((tempId == null) ? 0 : tempId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof PrimaryKey))
            return false;
        PrimaryKey other = (PrimaryKey) obj;
        if (entityId == null) {
            if (other.entityId != null)
                return false;
        } else if (!entityId.equals(other.entityId))
            return false;
        if (id != -1 && other.id != -1) {
            return id == other.id;
        }

        if (tempId == null) {
            if (other.tempId != null)
                return false;
        } else if (!tempId.equals(other.tempId))
            return false;
        return true;
    }

}
