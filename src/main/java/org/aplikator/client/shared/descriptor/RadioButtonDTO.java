package org.aplikator.client.shared.descriptor;

import org.aplikator.client.local.widgets.HasFields;
import org.aplikator.client.local.widgets.RadioButtonWidget;

import com.google.gwt.user.client.ui.Widget;

@SuppressWarnings("serial")
//@Portable
public class RadioButtonDTO extends WidgetPropertyDTOBase {


    @SuppressWarnings("unused")
    public RadioButtonDTO() {//TODO custom marshaller
    }

    public RadioButtonDTO(PropertyDTO property) {
        super(property);
    }

    private String switcher;

    public String getSwitcher() {
        return switcher;
    }

    public void setSwitcher(String switcher) {
        this.switcher = switcher;
    }

    @Override
    public Widget getWidget(HasFields form) {
        RadioButtonWidget field = new RadioButtonWidget(getLocalizedName(), getLocalizedTooltip(), property, getSize(), isHorizontal(), getElementId(), getSwitcher(), getCustomLabelStyle(), form);
        field.setEnabled(isEnabled());
        field.setVisible(isVisible());
        field.setDefaultEnabled(isEnabled());
        if (form != null) {
            form.registerDataField(field);
        }
        return field;
    }

}
