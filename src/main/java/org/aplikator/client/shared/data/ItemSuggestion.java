package org.aplikator.client.shared.data;

import java.io.Serializable;

import org.jboss.errai.common.client.api.annotations.Portable;

import com.google.gwt.user.client.ui.SuggestOracle;

/**
 * Created with IntelliJ IDEA.
 * User: vlahoda
 * Date: 13.02.13
 * Time: 9:03
 * To change this template use File | Settings | File Templates.
 */
@SuppressWarnings("serial")
@Portable
public class ItemSuggestion implements Serializable, SuggestOracle.Suggestion {

    private String suggestion;
    private int id;
    private String replacement;
    private RecordDTO recordDTO;

    // Required for IsSerializable to work
    public ItemSuggestion() {
    }

    // Convenience method for creation of a suggestion
    public ItemSuggestion(String suggestion, int id, String replacement, RecordDTO recordDTO) {
        this.suggestion = suggestion;
        this.id = id;
        this.replacement = replacement;
        this.recordDTO = recordDTO;
    }

    public String getDisplayString() {
        return suggestion;
    }

    public String getReplacementString() {
        return replacement;
    }

    public int getId() {
        return id;
    }

    public RecordDTO getRecord() {
        return recordDTO;
    }
}
