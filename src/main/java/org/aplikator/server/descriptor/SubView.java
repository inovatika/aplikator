package org.aplikator.server.descriptor;

import org.aplikator.server.data.ContainerNode;
import org.aplikator.server.data.Context;
import org.aplikator.server.data.PersisterTriggers;
import org.aplikator.server.data.Record;
import org.aplikator.server.query.QueryExpression;

public class SubView extends View {
    private View masterView;
    private Property<String> discriminator;
    private String value;
    private QueryDescriptor subviewQueryDescriptor;
    private QueryExpression subviewQueryExpression;

    public String getValue() {
        return value;
    }

    public SubView(View masterView, Property<String> discriminator, String value) {
        super(masterView, value);
        this.masterView = masterView;
        this.discriminator = discriminator;
        this.value = value;
        this.setPersistersTriggers(new SubPersisterTriggers(discriminator, value));
        subviewQueryExpression = discriminator.EQUAL(value);
        subviewQueryDescriptor = new QueryDescriptor(masterView.getId().substring(masterView.getId().indexOf(TYPE_DELIMITER) + 1) + SUFFIX_DELIMITER + value, value);
        subviewQueryDescriptor.setQueryExpression(subviewQueryExpression);
    }


    @Override
    public QueryDescriptor getQueryDescriptor(String id, Context ctx) {
        //if (id != null && !id.equals("")) {
            try {
                QueryDescriptor masterQueryDescriptor = super.getQueryDescriptor(id, ctx).clone();
                QueryExpression existingExpression = masterQueryDescriptor.getQueryExpression(null, null);
                if (existingExpression != null) {
                    masterQueryDescriptor.setQueryExpression(existingExpression.AND(subviewQueryExpression));
                } else {
                    masterQueryDescriptor.setQueryExpression(subviewQueryExpression);
                }
                return masterQueryDescriptor;
            } catch (CloneNotSupportedException e) {
                throw new RuntimeException(e);
            }
        //}
        //return subviewQueryDescriptor;
    }


    protected class SubPersisterTriggers implements PersisterTriggers {
        private String value;
        Property<String> discriminator;

        protected SubPersisterTriggers(Property<String> discriminator, String value) {
            this.value = value;
            this.discriminator = discriminator;
        }

        @Override
        public void onCreate(ContainerNode node, Context ctx) {
            node.getEdited().setValue(discriminator, value);
            masterView.getPersisterTriggers().onCreate(node, ctx);
        }

        @Override
        public void onUpdate(ContainerNode node, Context ctx) {
            node.getEdited().setValue(discriminator, value);
            masterView.getPersisterTriggers().onUpdate(node, ctx);
        }

        @Override
        public void afterCommit(ContainerNode node, Context ctx) {
            if (node.getEdited() != null) {
                node.getEdited().setValue(discriminator, value);
            }
            masterView.getPersisterTriggers().afterCommit(node, ctx);
        }

        @Override
        public void onDelete(ContainerNode node, Context ctx) {
            node.getOriginal().setValue(discriminator, value);
            masterView.getPersisterTriggers().onDelete(node, ctx);
        }

        @Override
        public void onLoad(Record record, View view, Context ctx) {
            masterView.getPersisterTriggers().onLoad(record, view, ctx);
        }

        @Override
        public QueryExpression addToQueryExpression(QueryExpression queryExpression, View view, Context ctx) {
            return masterView.getPersisterTriggers(). addToQueryExpression(queryExpression, view, ctx);
        }

        @Override
        public void onPrepare(ContainerNode node, boolean isCopy, Context ctx) {
            node.getEdited().setValue(discriminator, value);
            masterView.getPersisterTriggers().onPrepare(node, isCopy, ctx);
        }

        @Override
        public View changeView(View view, Context ctx) {
            return masterView.getPersisterTriggers().changeView(view, ctx);
        }

    }
}
