package org.aplikator.server;

import ch.qos.logback.core.PropertyDefinerBase;
import ch.qos.logback.core.spi.PropertyDefiner;

public class LogbackFolderPropertyDefiner extends PropertyDefinerBase implements PropertyDefiner {
    @Override
    public String getPropertyValue() {
        return Configurator.get().getConfig().getString("aplikator.log.folder");
    }
}
