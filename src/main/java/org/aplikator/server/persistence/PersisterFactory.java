package org.aplikator.server.persistence;

import org.aplikator.server.persistence.empiredb.EmpireDbPersister;

public class PersisterFactory {
    private static class LazyHolder {
        private static Persister instance = new EmpireDbPersister();
    }

    public static Persister getPersister() {
        return LazyHolder.instance;
    }

    public static void shutdownPersister() {
        getPersister().shutdown();
    }
}
