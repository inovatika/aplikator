package org.aplikator.client.shared.rpc.marshaller;

import org.aplikator.client.shared.data.FunctionResult;
import org.aplikator.client.shared.data.FunctionResultStatus;
import org.aplikator.client.shared.data.FunctionResultType;
import org.jboss.errai.marshalling.client.api.MarshallingSession;
import org.jboss.errai.marshalling.client.api.annotations.ClientMarshaller;
import org.jboss.errai.marshalling.client.api.annotations.ServerMarshaller;
import org.jboss.errai.marshalling.client.api.json.EJObject;
import org.jboss.errai.marshalling.client.api.json.EJValue;
import org.jboss.errai.marshalling.client.marshallers.AbstractNullableMarshaller;

@ClientMarshaller(FunctionResult.class)
@ServerMarshaller(FunctionResult.class)
public class FunctionResultMarshaller extends AbstractNullableMarshaller<FunctionResult> {


    @Override
    public FunctionResult[] getEmptyArray() {
        return new FunctionResult[0];
    }

    @Override
    public FunctionResult doNotNullDemarshall(EJValue o, MarshallingSession ctx) {
        if (o.isObject() != null) {
            EJObject ejObject = o.isObject();
            String message = ejObject.get("message").isString().stringValue();
            String status = ejObject.get("status").isString().stringValue();
            String type = ejObject.get("type").isString().stringValue();
            String detail = ejObject.get("detail").isString().stringValue();

            FunctionResult functionResult = new FunctionResult(message, FunctionResultStatus.valueOf(status), FunctionResultType.valueOf(type));
            functionResult.setDetail(detail);

            return functionResult;
        } else
            return null;
    }

    @Override
    public String doNotNullMarshall(FunctionResult o, MarshallingSession ctx) {
        StringBuilder builder = new StringBuilder().append('{');
        MarshallingUtils.string("message", o.getMessage(), builder);
        builder.append(',');
        MarshallingUtils.string("status", o.getStatus().name(), builder);
        builder.append(',');
        MarshallingUtils.string("type", o.getType().name(), builder);
        builder.append(',');
        MarshallingUtils.string("detail", o.getDetail(), builder);

        builder.append('}');
        return builder.toString();
    }
}
