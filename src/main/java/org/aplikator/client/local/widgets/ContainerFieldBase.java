package org.aplikator.client.local.widgets;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Panel;
import org.aplikator.client.shared.descriptor.Access;
import org.gwtbootstrap3.client.ui.Column;
import org.gwtbootstrap3.client.ui.FormLabel;


public abstract class ContainerFieldBase extends Composite {

    protected FormLabel label;
    protected Column wrapper;
    protected Panel dataPanel;

    protected Access defaultAccess;

    protected ContainerFieldBase() {
        super();
    }

    protected void setGridSize(int size) {
        wrapper.setSize(LayoutUtils.size(size));
    }

    public void setDefaultAccess(Access access) {
        defaultAccess = access;
    }

    public Access getDefaultAccess() {
        return defaultAccess;
    }

//    public abstract void setAccess (Access access);
//
//    public abstract Access getAccess();
//
//    public abstract PropertyDTO getProperty();
//
//    public void processAnnotations(RecordDTO sourceRecord){
//        String recordAccessStr = sourceRecord.getAnnotation(AnnotationKeys.ACCESS_ANNOTATION_KEY);
//        String fieldAccessStr = sourceRecord.getPropertyAnnotation(this.getProperty().getId(), AnnotationKeys.ACCESS_ANNOTATION_KEY);
//        if (fieldAccessStr != null) {
//            Access fieldAccess = Access.valueOf(fieldAccessStr);
//            this.setAccess(fieldAccess);
//        } else if (recordAccessStr != null) {
//            Access recordAccess = Access.valueOf(recordAccessStr);
//            this.setAccess(recordAccess);
//        } else {
//            this.setAccess(this.getDefaultAccess());
//        }
//    }

}
