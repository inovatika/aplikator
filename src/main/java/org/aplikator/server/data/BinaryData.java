package org.aplikator.server.data;

import org.aplikator.server.ImageResizer;
import org.aplikator.server.descriptor.BinaryProperty;
import org.aplikator.utils.PDFLoader;

import javax.activation.MimeTypeParseException;
import javax.imageio.IIOException;
import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Wrapper class for holding binary data (byte array) and corresponding
 * properties.
 * Provides also convenience constructors for initialization of the properties
 * based on the filename.
 */
public class BinaryData implements Serializable {
    /**
     * Property key for filename
     */
    public static final String FILENAME_KEY = "filename";
    public static final String MIME_TYPE = "mimetype";
    public static final String IMG_WIDTH = "imgWidth";
    public static final String IMG_HEIGHT = "imgHeight";
    private static final long serialVersionUID = 3328753266953957306L;
    private static final Logger LOG = Logger.getLogger(BinaryData.class.getName());
    /**
     * Binary data placeholder - provide inputStream and size of the data
     */
    public transient InputStream inputStream;
    public long dataLength;
    private String tempFileId;

    private BinaryProperty binaryProperty;

    /**
     * Properties placeholder, initialized upon instance creation
     */
    public Map<String, String> properties = new HashMap<String, String>();

    public byte[] preview = null;
    public byte[] icon = null;

    //when binary data are stored in a JCR DataStore on filesystem, these fields hold record IDs  (MD5 hashes) to the datastore
    public String dataRecordID = null;
    public String previewRecordID = null;
    public String iconRecordID = null;

    /**
     * Default constructor
     */
    public BinaryData(BinaryProperty binaryProperty) {
        this.binaryProperty = binaryProperty;
    }

    /**
     * Convenience constructor, adds the filename to the field properties
     */
    private BinaryData(String filename) {
        properties.put(FILENAME_KEY, filename);
    }

    /**
     * Convenience constructor, adds the filename to the field properties and
     * sets the temporaryFile field.
     */
    public BinaryData(BinaryProperty binaryProperty, String filename, InputStream inputStream, long dataLength, String tempFileId) {
        this(filename);
        this.inputStream = inputStream;
        this.dataLength = dataLength;
        this.tempFileId = tempFileId;
        this.binaryProperty = binaryProperty;
        if (filename != null) {
            javax.activation.MimeType mime;
            try {
                mime = ImageResizer.getMimeType(filename);
            } catch (MimeTypeParseException e) {
                throw new IllegalStateException("Unable to parse mime type of:" + filename, e);
            }
            this.properties.put(BinaryData.MIME_TYPE, mime.getPrimaryType() + "/" + mime.getSubType());
        }
    }

    public void preprocess(int thumbnailSize, int previewSize) {
        String filename = properties.get(BinaryData.FILENAME_KEY);
        if (ImageResizer.isResizeable(binaryProperty, filename)) {
            BufferedImage imageIcon = null;
            try {
                SubsampledImageInfo sii = null;
                if (ImageResizer.isPdf(filename)) {
                    imageIcon = PDFLoader.load(inputStream);
                    sii = new SubsampledImageInfo(imageIcon,imageIcon.getWidth(),imageIcon.getHeight());
                } else {
                    try {
                        imageIcon = ImageIO.read(inputStream);
                        sii = new SubsampledImageInfo(imageIcon, imageIcon.getWidth(), imageIcon.getHeight());
                    } catch (Exception ex) {
                        LOG.log(Level.INFO,"Image resizing failed, trying subsampled. Reason:",ex);
                        try {
                            inputStream.reset();
                            sii = readSubsampled(inputStream, Math.max(thumbnailSize, previewSize));
                            imageIcon = sii.image;
                        } catch (Exception subex) {
                            LOG.log(Level.INFO, "Image subsampling failed. Reason:", subex);
                        }
                    }
                }

                properties.put(BinaryData.IMG_WIDTH, String.valueOf(sii.width));
                properties.put(BinaryData.IMG_HEIGHT, String.valueOf(sii.height));

                imageIcon = ImageResizer.removeAlphaChannel(imageIcon);
                icon = ImageResizer.resize(imageIcon, thumbnailSize);
                preview = ImageResizer.resize(imageIcon, previewSize);
            } catch (Exception e) {
                LOG.log(Level.SEVERE, "Error while resizing image " + filename + ":", e);
            } finally {
                if (imageIcon != null) {
                    imageIcon.flush();
                }
            }
        }
    }

    public String getTempFileId() {
        return tempFileId;
    }

    /**
     * Wrapper for holding BufferedImage together with original image dimensions (subsampled image reports smaller domensions)
     */
    public static class SubsampledImageInfo{
        public final BufferedImage image;
        public final int width;
        public final int height;

        public SubsampledImageInfo(BufferedImage image, int width, int height) {
            this.image = image;
            this.width = width;
            this.height = height;
        }
    }

    /**
     * Method for readsing BufferedImage from input stream, copied from default ImageIO.read, but changed to use SourceSubsampling ImageParams
     * @param input
     * @param requestedSize  dimension (in pixels) to which the loaded BufferedImage should be later resized (used to determine subsampling ratio)
     * @return
     * @throws IOException
     */
    public static SubsampledImageInfo readSubsampled(InputStream input, int requestedSize) throws IOException {
        if (input == null) {
            throw new IllegalArgumentException("input == null!");
        }

        ImageInputStream stream = ImageIO.createImageInputStream(input);
        if (stream == null) {
            throw new IIOException("Can't create an ImageInputStream!");
        }
        SubsampledImageInfo sii = readSubsampled(stream, requestedSize);
        if (sii.image == null) {
            stream.close();
        }
        return sii;
    }

    public static SubsampledImageInfo readSubsampled(ImageInputStream stream, int requestedSize)
            throws IOException {
        if (stream == null) {
            throw new IllegalArgumentException("stream == null!");
        }

        Iterator iter = ImageIO.getImageReaders(stream);
        if (!iter.hasNext()) {
            return null;
        }

        ImageReader reader = (ImageReader) iter.next();
        reader.setInput(stream, true, true);
        int width = reader.getWidth(0);
        int height = reader.getHeight(0);

        int ratio = Math.max(1, (Math.min(width, height) / requestedSize ) / 5);  //lowered the max ratio by 5, otherwise the TIFF resizing did not work properly

        LOG.fine("WIDTH:" + width + " , HEIGHT:" + height + " , REQUESTED:" + requestedSize + " , RATIO:" + ratio);

        ImageReadParam param = reader.getDefaultReadParam();
        param.setSourceSubsampling(ratio, ratio, 0, 0);
        BufferedImage bi;
        try {
            bi = reader.read(0, param);
        } finally {
            reader.dispose();
            stream.close();
        }
        return new SubsampledImageInfo(bi, width, height);
    }
}
