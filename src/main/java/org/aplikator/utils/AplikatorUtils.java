package org.aplikator.utils;

import org.aplikator.client.shared.data.ListItem;
import org.aplikator.client.shared.data.Operation;
import org.aplikator.client.shared.data.PrimaryKey;
import org.aplikator.client.shared.data.RecordDTO;
import org.aplikator.server.data.ContainerNode;
import org.aplikator.server.data.Context;
import org.aplikator.server.data.Record;
import org.aplikator.server.descriptor.Collection;
import org.aplikator.server.descriptor.*;
import org.aplikator.server.security.Account;
import org.aplikator.server.security.SecurityModule;

import java.io.Serializable;
import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

public class AplikatorUtils {
    public static final String NULL_USER_STRING = "----";

    public static Date getDatum(int plusDny, boolean soNe) {
        Calendar cal = new GregorianCalendar();

        cal.add(Calendar.DATE, plusDny);
        if (soNe) {
            while ((cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) || (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)) {
                cal.add(Calendar.DATE, 1);
            }
        }
        return cal.getTime();
    }

    /**
     * Returns true if at least one of the given properties is marked as dirty in the edited record
     *
     * @param properties
     * @return
     */
    @SuppressWarnings("unchecked")
    public static boolean isDirty(ContainerNode node, Property<? extends Serializable>... properties) {
        for (Property<? extends Serializable> property : properties) {
            if (node.getEdited().isDirty(property)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Generates Default ListProvider from array of Strings
     *
     * @param items
     * @return
     */
    public static ListProvider list(String id, String... items) {
        List<ListItem> listItems = new ArrayList<ListItem>(items.length);
        for (String item : items) {
            listItems.add(new ListItem.Default(item));
        }
        return new ListProvider.Default(id, listItems);
    }

    /**
     * Generates Default ListProvider form array of Enums
     *
     * @param items
     * @return
     */
    public static ListProvider list(String id, Enum... items) {
        String[] strings = new String[items.length];
        for (int i = 0; i < items.length; i++) {
            strings[i] = items[i].name();
        }
        return list(id, strings);
    }

    public static String removeEnter(String str) {
        String result = "";
        result = str.replaceAll("\r", "").replaceAll("\n", "");
        return result;
    }

    @SuppressWarnings({"rawtypes", "unchecked"})
    public static void copyRecord(RecordDTO source, RecordDTO target, Property... props) {
        for (Property prop : props) {
            prop.setValue(target, prop.getValue(source));
        }
    }

    public static String getUserLogin(Context ctx, boolean allowNulls) {
        String remoteUser = ctx.getUserLogin();
        if (remoteUser == null && (!allowNulls)) {
            remoteUser = NULL_USER_STRING;
        }
        return remoteUser;
    }

    public static String getUserFullname(Context ctx, boolean allowNulls) {
        String remoteUser = SecurityModule.get().getUserLogin(ctx.getHttpServletRequest());
        String fullname = null;
        if (remoteUser != null) {
            Account account = SecurityModule.get().getAccount(remoteUser, ctx.getHttpServletRequest());
            if (account != null) {
                fullname = account.getFullName();
            }
        }
        if (fullname == null) {
            fullname = remoteUser;
        }
        if (fullname == null && (!allowNulls)) {
            fullname = NULL_USER_STRING;
        }
        return fullname;
    }

    public static java.util.List<ListItem> getStaticList(List<String> sourceList) {
        List<ListItem> listValues = new ArrayList<ListItem>();
        for (String value : sourceList) {
            listValues.add(new ListItem.Default(value, value));
        }
        return listValues;
    }

    public static java.util.List<ListItem> getStaticList(Map<Serializable, String> sourceList) {
        List<ListItem> listValues = new ArrayList<ListItem>();
        for (Serializable value : sourceList.keySet()) {
            listValues.add(new ListItem.Default(value, sourceList.get(value)));
        }
        return listValues;
    }

    public static <T extends Entity> Record getReferenceRecord(Record record, Reference<T> reference, Context ctx) {
        if (record.getValue(reference) == null) {
            return null;
        }
        PrimaryKey pk = new PrimaryKey(reference.referredEntity.getId(), record.getValue(reference));
        Record rec = ctx.getRecord(pk, reference.referredEntity.view());
        return rec;
    }

    public static String getDateAsString(Date datum) {
        return getDateAsString(datum, "dd.MM.yyyy");
    }

    public static String getDateAsString(Date datum, String formatPattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(formatPattern);
        sdf.format(datum);
        return sdf.format(datum);
    }

    public static List<Record> getCollectionRecords(Record ownerRecord, Collection<?> ownerProperty, Context ctx) {
        List<Record> retval = new ArrayList<Record>();
        List<Record> dbRecords = ctx.getRecords(ownerProperty.referredEntity.view()).withOwner(ownerProperty, ownerRecord.getPrimaryKey()).list();
        List<ContainerNode> containerRecords = ctx.getRecordContainer().getRecords();
        for (Record dbRec : dbRecords) {
            boolean changed = false;
            for (ContainerNode node : containerRecords) {
                if (Operation.DELETE.equals(node.getOperation())) {
                    if (dbRec.getPrimaryKey().equals(node.getOriginal().getPrimaryKey())) {
                        changed = true;
                        break;
                    }
                }
                if (Operation.UPDATE.equals(node.getOperation())) {
                    if (dbRec.getPrimaryKey().equals(node.getOriginal().getPrimaryKey())) {
                        changed = true;
                        if ((node.getEdited().getRecordDTO().getProperties().size() == 0)) {
                            retval.add(node.getOriginal());
                        } else {
                            retval.add(node.getEdited());
                        }
                        break;
                    }
                }
            }
            if (!changed) {
                retval.add(dbRec);
            }
        }

        for (ContainerNode node : containerRecords) {
            if (Operation.CREATE.equals(node.getOperation())) {
                if (ownerRecord.getPrimaryKey().equals(node.getEdited().getOwnerPrimaryKey())) {
                    retval.add(node.getEdited());
                }
            }
        }
        return retval;
    }

    public static String unaccent(String str) {
        String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(nfdNormalizedString).replaceAll("");
    }
}
