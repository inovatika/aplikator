package org.aplikator.server.data;

import org.aplikator.client.shared.data.PrimaryKey;
import org.aplikator.server.descriptor.Collection;
import org.aplikator.server.descriptor.Property;
import org.aplikator.server.descriptor.SortItem;
import org.aplikator.server.descriptor.View;
import org.aplikator.server.persistence.RecordIterator;
import org.aplikator.server.query.QueryExpression;

import java.util.List;

/**
 *
 */
public class QueryBuilder {
    private final View view;
    private final Context ctx;
    private QueryExpression queryExpression = null;
    private QueryExpression havingExpresssion = null;
    private SortItem[] sortItems = null;
    private Property[] groupBy = null;
    private String searchString = null;
    private Collection ownerProperty = null;
    private PrimaryKey ownerPrimaryKey = null;
    private int pageOffset = 0;
    private int pageSize = 0;

    QueryBuilder(Context ctx, View view) {
        this.ctx = ctx;
        this.view = view;
    }

    public QueryBuilder withOwner(Collection ownerProperty, PrimaryKey ownerPrimaryKey) {
        this.ownerProperty = ownerProperty;
        this.ownerPrimaryKey = ownerPrimaryKey;
        return this;
    }

    public QueryBuilder withQuery(QueryExpression queryExpression) {
        this.queryExpression = queryExpression;
        return this;
    }

    public QueryBuilder withHaving(QueryExpression queryExpression) {
        this.havingExpresssion = queryExpression;
        return this;
    }

    public QueryBuilder withSort(SortItem... sortItems) {
        this.sortItems = sortItems;
        return this;
    }

    public QueryBuilder withSortFromValue(String searchString, SortItem... sortItems) {
        this.sortItems = sortItems;
        this.searchString = searchString;
        return this;
    }

    public QueryBuilder withDefaultSort() {
        if (this.view.getDefaultSortDescriptor() != null) {
            List<SortItem> viewSort = this.view.getDefaultSortDescriptor().getItems();
            this.sortItems = viewSort.toArray(new SortItem[viewSort.size()]);
        }
        return this;
    }

    public QueryBuilder withGroupBy(Property... groupBy) {
        this.groupBy = groupBy;
        return this;
    }

    public QueryBuilder limitPage(int pageOffset, int pageSize) {
        this.pageOffset = pageOffset;
        this.pageSize = pageSize;
        return this;
    }

    public List<Record> list() {
        return ctx.getAplikatorServiceBackend().getRecords(view, queryExpression, sortItems, groupBy, havingExpresssion, searchString, ownerProperty, ownerPrimaryKey, pageOffset, pageSize, ctx);
    }

    public RecordIterator recordIterator() {
        return ctx.getAplikatorServiceBackend().getRecordIterator(view, queryExpression, sortItems, groupBy, havingExpresssion, searchString, ownerProperty, ownerPrimaryKey, pageOffset, pageSize, ctx);
    }

    public int count() {
        return ctx.getAplikatorServiceBackend().getRecordCount(view, queryExpression, sortItems, groupBy, havingExpresssion, searchString, ownerProperty, ownerPrimaryKey, ctx);
    }

    public QueryExpression getQuery() {
        return this.queryExpression;
    }

}
