package org.aplikator.server.persistence.search;

import java.util.HashMap;

import org.aplikator.client.shared.data.Operation;
import org.aplikator.client.shared.data.PrimaryKey;
import org.aplikator.server.data.Context;

public class ReindexDescriptor {
    private HashMap<PrimaryKey, Operation> getReindexOperations() {
        return reindexOperations;
    }

    public void addDescriptor(PrimaryKey pk, Operation operation) {
        if (!getReindexOperations().containsKey(pk)) {
            getReindexOperations().put(pk, operation);
        }
    }

    private HashMap<PrimaryKey, Operation> reindexOperations = new HashMap<PrimaryKey, Operation>();

    public void processReindex(Context ctx) {
        Search search = SearchFactory.get();
        for (PrimaryKey pk : getReindexOperations().keySet()) {
            switch (getReindexOperations().get(pk)) {
                case CREATE:
                    search.insert(pk, ctx);
                    break;
                case UPDATE:
                    search.update(pk, ctx);
                    break;
                case DELETE:
                    search.delete(pk);
                    break;
            }
        }
        search.finish();
    }
}
