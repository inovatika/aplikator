package org.aplikator.client.shared.rpc.marshaller;

import java.util.List;

import org.aplikator.client.shared.descriptor.EntityDTO;
import org.aplikator.client.shared.descriptor.FormDTO;
import org.aplikator.client.shared.descriptor.FunctionDTO;
import org.aplikator.client.shared.descriptor.PropertyDTO;
import org.aplikator.client.shared.descriptor.QueryDescriptorDTO;
import org.aplikator.client.shared.descriptor.SortDescriptorDTO;
import org.aplikator.client.shared.descriptor.ViewDTO;
import org.jboss.errai.marshalling.client.api.Marshaller;
import org.jboss.errai.marshalling.client.api.MarshallingSession;
import org.jboss.errai.marshalling.client.api.annotations.ClientMarshaller;
import org.jboss.errai.marshalling.client.api.annotations.ServerMarshaller;
import org.jboss.errai.marshalling.client.api.json.EJObject;
import org.jboss.errai.marshalling.client.api.json.EJValue;
import org.jboss.errai.marshalling.client.marshallers.AbstractNullableMarshaller;

@ClientMarshaller(ViewDTO.class)
@ServerMarshaller(ViewDTO.class)
@SuppressWarnings("unchecked")
public class ViewDTOMarshaller extends AbstractNullableMarshaller<ViewDTO> {

	/*@Override
    public Class<ViewDTO> getTypeHandled() {
		return ViewDTO.class;
	}*/

    @Override
    public ViewDTO[] getEmptyArray() {
        return new ViewDTO[0];
    }

    @Override
    public ViewDTO doNotNullDemarshall(EJValue o, MarshallingSession ctx) {
        if (o.isObject() != null) {
            EJObject eObject = o.isObject();
            ViewDTO vdto = new ViewDTO();

            DemarshallingUtils.clientDescriptorBaseUnMarshall(eObject, vdto);
            if (DemarshallingUtils.containsAndNotNull(eObject, "activeFilter")) {
                vdto.setActiveFilter(eObject.get("activeFilter").isString().stringValue());
            }
            if (DemarshallingUtils.containsAndNotNull(eObject, "activeSort")) {
                vdto.setActiveSort(eObject.get("activeSort").isString().stringValue());
            }
            if (DemarshallingUtils.containsAndNotNull(eObject, "pageSize")) {
                vdto.setPageSize(eObject.get("pageSize").isNumber().intValue());
            }


            if (DemarshallingUtils.containsAndNotNull(eObject, "listPanelWidth")) {
                vdto.setListPanelWidth(eObject.get("listPanelWidth").isNumber().intValue());
            }

            if (DemarshallingUtils.containsAndNotNull(eObject, "openAsTable")) {
                vdto.setOpenAsTable(eObject.get("openAsTable").isBoolean().booleanValue());
            }

            if (DemarshallingUtils.containsAndNotNull(eObject, "searchString")) {
                vdto.setSearchString(eObject.get("searchString").isString().stringValue());
            }

            if (DemarshallingUtils.containsAndNotNull(eObject, "properties")) {
                List l = DemarshallingUtils.listUnMarshall(eObject.get("properties"), ctx, PropertyDTO.class.getName());
                vdto.setProperties(l);
            }

            if (DemarshallingUtils.containsAndNotNull(eObject, "filters")) {
                List l = DemarshallingUtils.listUnMarshall(eObject.get("filters"), ctx, QueryDescriptorDTO.class.getName());
                vdto.setQueryDescriptors(l);
            }

            if (DemarshallingUtils.containsAndNotNull(eObject, "sorts")) {
                List l = DemarshallingUtils.listUnMarshall(eObject.get("sorts"), ctx, SortDescriptorDTO.class.getName());
                vdto.setSortDescriptors(l);
            }

            if (DemarshallingUtils.containsAndNotNull(eObject, "functions")) {
                List l = DemarshallingUtils.listUnMarshall(eObject.get("functions"), ctx, FunctionDTO.class.getName());
                vdto.setFunctions(l);
            }

            if (DemarshallingUtils.containsAndNotNull(eObject, "entity")) {
                Marshaller<Object> marshaller = ctx.getMarshallerInstance(EntityDTO.class.getName());
                Object entity = marshaller.demarshall(eObject.get("entity"), ctx);
                vdto.setEntity((EntityDTO) entity);
            }

            if (DemarshallingUtils.containsAndNotNull(eObject, "form")) {
                Marshaller<Object> marshaller = ctx.getMarshallerInstance(FormDTO.class.getName());
                Object fdesc = marshaller.demarshall(eObject.get("form"), ctx);
                vdto.setFormDescriptor((FormDTO) fdesc);
            }

            return vdto;
        } else
            return null;
    }

    @Override
    public String doNotNullMarshall(ViewDTO o, MarshallingSession ctx) {
        StringBuilder builder = new StringBuilder().append('{');
        MarshallingUtils.clientDescriptorBase(builder, o).append(',');
        MarshallingUtils.string("activeFilter", o.getActiveFilter(), builder).append(',');
        MarshallingUtils.string("activeSort", o.getActiveSort(), builder).append(',');
        MarshallingUtils.number("pageSize", o.getPageSize(), builder).append(',');
        MarshallingUtils.number("listPanelWidth", o.getListPanelWidth(), builder).append(',');
        MarshallingUtils.bool("openAsTable", o.isOpenAsTable(), builder).append(',');
        MarshallingUtils.string("searchString", o.getSearchString(), builder).append(',');

        MarshallingUtils.key("properties", builder).append(':');
        MarshallingUtils.listMarshall(builder, o.getProperties(), ctx, PropertyDTO.class.getName()).append(',');


        MarshallingUtils.key("filters", builder).append(':');
        MarshallingUtils.listMarshall(builder, o.getQueryDescriptors(), ctx, QueryDescriptorDTO.class.getName()).append(',');

        MarshallingUtils.key("sorts", builder).append(':');
        MarshallingUtils.listMarshall(builder, o.getSortDescriptors(), ctx, SortDescriptorDTO.class.getName()).append(',');

        MarshallingUtils.key("functions", builder).append(':');
        MarshallingUtils.listMarshall(builder, o.getFunctions(), ctx, FunctionDTO.class.getName()).append(',');

        MarshallingUtils.key("entity", builder).append(':');
        Marshaller<Object> entMarshaller = ctx.getMarshallerInstance(EntityDTO.class.getName());
        builder.append(entMarshaller.marshall(o.getEntity(), ctx)).append(',');

        MarshallingUtils.key("form", builder).append(':');
        Marshaller<Object> fMarshaller = ctx.getMarshallerInstance(FormDTO.class.getName());
        builder.append(fMarshaller.marshall(o.getFormDescriptor(), ctx));

        builder.append('}');
        return builder.toString();
    }
}
