package org.aplikator.server.descriptor;

import org.aplikator.client.shared.descriptor.TextAreaDTO;
import org.aplikator.client.shared.descriptor.WidgetDTO;
import org.aplikator.server.data.Context;

public class TextArea extends WidgetPropertyDescriptorBase<String> {

    private int rows = 0;

    private int height = 0;

    public TextArea(Property<String> property) {
        super(property);
    }

    @Override
    public WidgetDTO getWidgetDescriptor(Context ctx) {
        TextAreaDTO desc = new TextAreaDTO(getProperty().getPropertyDTO(ctx));
        desc.setSize(getSize());
        desc.setRows(rows);
        desc.setHeight(height);
        desc.setFormatPattern(getFormatPattern());
        desc.setLocalizedName(getLocalizedName(ctx));
        desc.setLocalizedTooltip(getLocalizedTooltip(ctx));
        desc.setAccess(getProperty().getAccess(ctx));
        desc.setEnabled(isEnabled(ctx));
        desc.setVisible(isVisible(ctx));
        desc.setHorizontal(isHorizontal());
        desc.setElementId(getElementId());
        desc.setCustomLabelStyle(getCustomLabelStyle());
        return desc;
    }


    public TextArea setRows(int rows) {
        this.rows = rows;
        return this;
    }

    public int getRows() {
        return rows;
    }

    public int getHeight() {
        return height;
    }

    public TextArea setHeight(int height) {
        this.height = height;
        return this;
    }
}
