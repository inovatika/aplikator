package org.aplikator.server.descriptor;

import java.io.Serializable;

/**
 * @author vlahoda
 */
public class SortItem {
    private final Property<? extends Serializable> sortProperty;

    private final boolean sortAscending;

    public static SortItem ascending(Property<? extends Serializable> sortProperty) {
        return new SortItem(sortProperty, true);
    }

    public static SortItem descending(Property<? extends Serializable> sortProperty) {
        return new SortItem(sortProperty, false);
    }

    private SortItem(Property<? extends Serializable> sortProperty, boolean sortAscending) {
        if (sortProperty.isVirtual()) {
            throw new IllegalArgumentException("Virtual property " + sortProperty.getId() + " cannot be sorted.");
        }
        this.sortProperty = sortProperty;
        this.sortAscending = sortAscending;
    }

    public Property<? extends Serializable> getSortProperty() {
        return sortProperty;
    }

    public boolean isSortAscending() {
        return sortAscending;
    }


}
