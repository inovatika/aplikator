package org.aplikator.client.shared.rpc.marshaller;

import org.aplikator.client.shared.data.ClientContext;
import org.aplikator.client.shared.data.RecordDTO;
import org.aplikator.client.shared.descriptor.ViewDTO;
import org.jboss.errai.marshalling.client.api.Marshaller;
import org.jboss.errai.marshalling.client.api.MarshallingSession;
import org.jboss.errai.marshalling.client.api.annotations.ClientMarshaller;
import org.jboss.errai.marshalling.client.api.annotations.ServerMarshaller;
import org.jboss.errai.marshalling.client.api.json.EJObject;
import org.jboss.errai.marshalling.client.api.json.EJValue;
import org.jboss.errai.marshalling.client.marshallers.AbstractNullableMarshaller;

import java.util.Set;

@ClientMarshaller(ClientContext.class)
@ServerMarshaller(ClientContext.class)
public class ClientContextMarshaller extends AbstractNullableMarshaller<ClientContext> {

	/*@Override
    public Class<ClientContext> getTypeHandled() {
		return ClientContext.class;
	}*/

    @Override
    public ClientContext[] getEmptyArray() {
        return new ClientContext[0];
    }

    @Override
    public ClientContext doNotNullDemarshall(EJValue o, MarshallingSession ctx) {
        if (o.isObject() != null) {
            EJObject ejObject = o.isObject();
            ClientContext clientCtx = new ClientContext();
            if (DemarshallingUtils.containsAndNotNull(ejObject, "user")) {
                clientCtx.setUser(ejObject.get("user").isString().stringValue());
            }
            if (DemarshallingUtils.containsAndNotNull(ejObject, "locale")) {
                clientCtx.setLocale(ejObject.get("locale").isString().stringValue());
            }

            if (DemarshallingUtils.containsAndNotNull(ejObject, "view")) {
                Marshaller<Object> marshInstance = ctx.getMarshallerInstance(ViewDTO.class.getName());
                ViewDTO view = (ViewDTO) marshInstance.demarshall(ejObject.get("view"), ctx);
                clientCtx.setView(view);
            }
            if (DemarshallingUtils.containsAndNotNull(ejObject, "currentRecord")) {
                Marshaller<Object> marshallerInstance = ctx.getMarshallerInstance(RecordDTO.class.getName());
                Object demarshalled = marshallerInstance.demarshall(ejObject.get("currentRecord"), ctx);
                clientCtx.setCurrentRecord((RecordDTO) demarshalled);
            }
            if (DemarshallingUtils.containsAndNotNull(ejObject, "selectedRecords")) {
                Set setUnMarshall = DemarshallingUtils.setUnMarshall(ejObject.get("selectedRecords"), ctx, RecordDTO.class.getName());
                clientCtx.setSelectedRecords(setUnMarshall);
            }
            return clientCtx;
        } else
            return null;
    }

    @Override
    public String doNotNullMarshall(ClientContext o, MarshallingSession ctx) {
        StringBuilder builder = new StringBuilder().append('{');
        MarshallingUtils.string("user", o.getUser(), builder).append(',');
        MarshallingUtils.string("locale", o.getLocale(), builder);
        if (o.getView() != null) {
            builder.append(',');
            MarshallingUtils.key("view", builder).append(':');
            Marshaller<Object> marshaller = ctx.getMarshallerInstance(ViewDTO.class.getName());
            builder.append(marshaller.marshall(o.getView(), ctx));
        }
        if (o.getCurrentRecord() != null) {
            builder.append(',');
            MarshallingUtils.key("currentRecord", builder).append(':');
            Marshaller<Object> marshaller = ctx.getMarshallerInstance(RecordDTO.class.getName());
            builder.append(marshaller.marshall(o.getCurrentRecord(), ctx));
        }
        if (o.getSelectedRecords() != null) {
            builder.append(',');
            MarshallingUtils.key("selectedRecords", builder).append(':');
            MarshallingUtils.setMarshall(builder, o.getSelectedRecords(), ctx, RecordDTO.class.getName());
        }
        builder.append('}');
        return builder.toString();
    }

}
