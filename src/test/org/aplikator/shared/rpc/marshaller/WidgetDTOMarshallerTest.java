package org.aplikator.shared.rpc.marshaller;

import junit.framework.TestCase;
import org.aplikator.client.shared.data.ListItem;
import org.aplikator.client.shared.data.PrimaryKey;
import org.aplikator.client.shared.descriptor.*;
import org.aplikator.client.shared.rpc.marshaller.*;
import org.easymock.EasyMock;
import org.jboss.errai.marshalling.client.api.Marshaller;
import org.jboss.errai.marshalling.client.api.MarshallingSession;
import org.jboss.errai.marshalling.client.marshallers.StringMarshaller;
import org.jboss.errai.marshalling.server.JSONDecoder;
import org.junit.Assert;

import java.util.ArrayList;

public class WidgetDTOMarshallerTest extends TestCase {

    public static PropertyDTO prop() {
        PropertyDTO prop = new PropertyDTO();
        prop.marshallInitialization("id", "property", Access.READ_WRITE_CREATE_DELETE.name());

        prop.setType("string");
        prop.setSize(33.3);
        prop.setEditable(true);
        prop.setRequired(false);
        return prop;
    }

    public void testMarshallPanel() {

        MarshallingSession session = ctx();

        PanelDTO pdto = panel();

        WidgetDTOMarshaller marshaller = new WidgetDTOMarshaller();
        String marshall = marshaller.marshall(pdto, session);
        WidgetDTO demarshalled = marshaller.demarshall(JSONDecoder.decode(marshall), session);

        Assert.assertEquals(pdto.isFrame(), ((PanelDTO) demarshalled).isFrame());
        Assert.assertEquals(pdto.isEnabled(), ((PanelDTO) demarshalled).isEnabled());
        Assert.assertEquals(pdto.isHorizontal(), ((PanelDTO) demarshalled).isHorizontal());
        Assert.assertEquals(pdto.getChildren().size(), ((PanelDTO) demarshalled).getChildren().size());

        TextFieldDTO marshalled = (TextFieldDTO) ((PanelDTO) demarshalled).getChildren().get(0);
        TextFieldDTO notMarshalled = (TextFieldDTO) pdto.getChildren().get(0);
        Assert.assertEquals(notMarshalled.getFormatPattern(), marshalled.getFormatPattern());
        Assert.assertEquals(notMarshalled.getSize(), marshalled.getSize());
        Assert.assertEquals(notMarshalled.getProperty(), marshalled.getProperty());
        System.out.println(marshalled.getProperty());

    }

    @SuppressWarnings("unchecked")
    public static PanelDTO panel() {
        PanelDTO pdto = new PanelDTO();
        pdto.setFrame(true);
        pdto.setHorizontal(true);
        pdto.setSize(120);
        pdto.setFormatPattern("formated");

        TextFieldDTO tfdto = new TextFieldDTO();
        tfdto.setProperty(prop());

        TextAreaDTO tadto = new TextAreaDTO();
        tadto.setEnabled(true);
        tadto.setSize(12);
        tadto.setFormatPattern("formated");
        tadto.setRows(30);

        DateFieldDTO df = new DateFieldDTO();
        df.setEnabled(true);
        df.setSize(12);
        df.setFormatPattern("formated");

        ArrayList children = new ArrayList<>();
        children.add(tfdto);
        children.add(tadto);
        children.add(df);
        pdto.setChildren(children);
        return pdto;
    }

    @SuppressWarnings("unchecked")
    private MarshallingSession ctx() {
        MarshallingSession session = EasyMock.createMock(MarshallingSession.class);

        PrimaryKeyMarshaller pkmarsh = new PrimaryKeyMarshaller();
        Object marsh = pkmarsh;
        EasyMock.expect(session.getMarshallerInstance(PrimaryKey.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        StringMarshaller smarsh = new StringMarshaller();
        marsh = smarsh;
        EasyMock.expect(session.getMarshallerInstance(String.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        QueryDescriptorDTOMarshaller m = new QueryDescriptorDTOMarshaller();
        marsh = m;
        EasyMock.expect(session.getMarshallerInstance(QueryDescriptorDTO.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        SortDescriptorDTOMarshaller sort = new SortDescriptorDTOMarshaller();
        marsh = sort;
        EasyMock.expect(session.getMarshallerInstance(SortDescriptorDTO.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        PropertyDTOMarshaller pmarsh = new PropertyDTOMarshaller();
        marsh = pmarsh;
        EasyMock.expect(session.getMarshallerInstance(PropertyDTO.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        ListItemMarshaller lmarsh = new ListItemMarshaller();
        marsh = lmarsh;
        EasyMock.expect(session.getMarshallerInstance(ListItem.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        FunctionDTOMarshaller fmarshaller = new FunctionDTOMarshaller();
        marsh = fmarshaller;
        EasyMock.expect(session.getMarshallerInstance(FunctionDTO.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        FormDTOMarshaller formmarshaller = new FormDTOMarshaller();
        marsh = formmarshaller;
        EasyMock.expect(session.getMarshallerInstance(FormDTO.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        WidgetDTOMarshaller wmarshaller = new WidgetDTOMarshaller();

        EasyMock.replay(session);
        return session;
    }

}

