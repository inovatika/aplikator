package org.aplikator.shared.rpc.marshaller;

import java.util.Arrays;
import java.util.Date;

import org.aplikator.client.shared.data.PrimaryKey;
import org.aplikator.client.shared.data.RecordDTO;
import org.aplikator.client.shared.data.SearchResult;
import org.aplikator.client.shared.rpc.marshaller.PrimaryKeyMarshaller;
import org.aplikator.client.shared.rpc.marshaller.RecordDTOMarshaller;
import org.aplikator.client.shared.rpc.marshaller.SearchResultMarshaller;
import org.easymock.EasyMock;
import org.jboss.errai.marshalling.client.api.Marshaller;
import org.jboss.errai.marshalling.client.api.MarshallingSession;
import org.jboss.errai.marshalling.client.marshallers.StringMarshaller;
import org.jboss.errai.marshalling.server.JSONDecoder;
import org.junit.Assert;

import junit.framework.TestCase;

public class SearchResultMarshallerTest extends TestCase {

    public void testMarshall() {

        RecordDTO recordDTO = new RecordDTO();
        recordDTO.setPrimaryKey(new PrimaryKey("modul", 2));

        recordDTO.setValue("datumovepole", new Date());
        recordDTO.setValue("stringovepole", "string");
        recordDTO.setValue("integerovepole", 2);
        recordDTO.setValue("doublepole", 3.3);
        recordDTO.setValue("booleanpole", true);

        SearchResult search = new SearchResult();
        search.setCount(new Long(3));
        search.setRecords(Arrays.asList(recordDTO));

        MarshallingSession session = ctx();

        SearchResultMarshaller sMarshaller = new SearchResultMarshaller();
        String marshall = sMarshaller.marshall(search, session);
        SearchResult demarshall = sMarshaller.demarshall(JSONDecoder.decode(marshall), session);
        Assert.assertNotNull(demarshall);
        Assert.assertTrue(demarshall.getCount().equals(search.getCount()));

        Assert.assertTrue(demarshall.getRecords().size() == 1);
        Assert.assertTrue(demarshall.getRecords().get(0).getPrimaryKey().equals(recordDTO.getPrimaryKey()));

    }

    @SuppressWarnings("unchecked")
    private MarshallingSession ctx() {
        MarshallingSession session = EasyMock.createMock(MarshallingSession.class);
        PrimaryKeyMarshaller pkmarsh = new PrimaryKeyMarshaller();
        Object marsh = pkmarsh;
        EasyMock.expect(session.getMarshallerInstance(PrimaryKey.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        StringMarshaller smarsh = new StringMarshaller();
        marsh = smarsh;
        EasyMock.expect(session.getMarshallerInstance(String.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        RecordDTOMarshaller rmarsh = new RecordDTOMarshaller();
        marsh = rmarsh;
        EasyMock.expect(session.getMarshallerInstance(RecordDTO.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        EasyMock.replay(session);
        return session;
    }

}
