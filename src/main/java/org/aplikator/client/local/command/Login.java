package org.aplikator.client.local.command;

import org.aplikator.client.local.Aplikator;
import org.aplikator.client.shared.rpc.AplikatorErrorCallback;
import org.aplikator.client.shared.rpc.AplikatorService;
import org.aplikator.client.shared.rpc.AuthenticationService;
import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.FormLabel;
import org.gwtbootstrap3.client.ui.Input;
import org.gwtbootstrap3.client.ui.Legend;
import org.gwtbootstrap3.client.ui.Modal;
import org.gwtbootstrap3.client.ui.ModalBody;
import org.gwtbootstrap3.client.ui.TextBox;
import org.gwtbootstrap3.client.ui.constants.ModalBackdrop;
import org.jboss.errai.common.client.api.RemoteCallback;
import org.jboss.errai.enterprise.client.jaxrs.api.RestClient;
import org.jboss.errai.ui.nav.client.local.PageHidden;
import org.jboss.errai.ui.nav.client.local.PageShowing;
import org.jboss.errai.ui.nav.client.local.PageShown;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;

public class Login extends Modal {

    //public static String SECURITY_REST_API_BASE = "/" + GWT.getModuleName() + "/security";
    public static String SECURITY_REST_API_BASE = Aplikator.getBasePath() + "security";

    private static LoginUiBinder loginUiBinder = GWT.create(LoginUiBinder.class);
    @UiField
    Legend title;
    @UiField
    TextBox username;
    @UiField
    FormLabel usernameLabel;
    @UiField
    Input password;
    @UiField
    FormLabel passwordLabel;
    @UiField
    Button submit;
    @UiField
    Button cancel;
    @UiField
    Legend alternativeTitle;
    @UiField
    Button submitSSO;
    private String redirectToken;
    private Aplikator aplikator;

    public Login(Aplikator aplikator) {
        this.aplikator = aplikator;
        setClosable(false);
        this.add(loginUiBinder.createAndBindUi(this));
        this.setDataBackdrop(ModalBackdrop.STATIC);
        this.setTitle(Aplikator.application.getConfigString("aplikator.login.title"));
        alternativeTitle.setText(Aplikator.application.getConfigString("aplikator.login.alternativeTitle"));
        alternativeTitle.setVisible(Aplikator.application.isShowLoginSSOButton());
        submitSSO.setText(Aplikator.application.getConfigString("aplikator.login.submitSSO"));
        submitSSO.setVisible(Aplikator.application.isShowLoginSSOButton());
        title.setText(Aplikator.application.getConfigString("aplikator.login.standardTitle"));
        title.setVisible(Aplikator.application.isShowLoginSSOButton());
        usernameLabel.setText(Aplikator.application.getConfigString("aplikator.login.username"));
        passwordLabel.setText(Aplikator.application.getConfigString("aplikator.login.password"));
        submit.setText(Aplikator.application.getConfigString("aplikator.login.submit"));
        cancel.setText(Aplikator.application.getConfigString("aplikator.login.cancel"));
         KeyUpHandler onEnter = new KeyUpHandler() {
            @Override
            public void onKeyUp(KeyUpEvent event) {
                event.stopPropagation();
                if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
                    submit();
                }
            }
        };
        username.addKeyUpHandler(onEnter);
        password.addKeyUpHandler(onEnter);
    }

    public String getRedirectToken() {
        return redirectToken;
    }

    public Login setRedirectToken(String redirectToken) {
        this.redirectToken = redirectToken;
        return this;
    }

    @PageShowing
    public void enterLoginPage() {
        aplikator.displayLoginButton(false);
    }

    @PageShown
    public void grabFocus() {

        username.setFocus(true);
        username.setSelectionRange(0, username.getText().length());
    }

    @UiHandler("submit")
    public void submit(ClickEvent event) {
        submit();
    }

    @UiHandler("cancel")
    public void cancel(ClickEvent event) {
        Login.this.hide();
        aplikator.displayLoginButton(true);
    }

    private void submit() {
        RestClient.create(AuthenticationService.class, SECURITY_REST_API_BASE, new RemoteCallback<Void>() {
                    @Override
                    public void callback(Void response) {
                        Login.this.hide();
                        aplikator.reloadApplication(redirectToken);
                   }
                },
                new AplikatorErrorCallback("aplikator.login.error")
        ).login(username.getText(), password.getText());

    }

    @UiHandler("submitSSO")
    public void submitSSO(ClickEvent event) {
        submitSSO();
    }

    private void submitSSO() {
        RestClient.create(AuthenticationService.class, SECURITY_REST_API_BASE, new RemoteCallback<Void>() {
                    @Override
                    public void callback(Void response) {
                        Login.this.hide();
                        aplikator.reloadApplication(redirectToken);
                    }
                },
                new AplikatorErrorCallback("aplikator.login.error")
        ).loginSSO();

    }

    @PageHidden
    public void leaveLoginPage() {
        aplikator.displayLoginButton(true);
    }

    interface LoginUiBinder extends UiBinder<ModalBody, Login> {
    }


}
