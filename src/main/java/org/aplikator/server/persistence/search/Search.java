package org.aplikator.server.persistence.search;

import org.aplikator.client.shared.data.PrimaryKey;
import org.aplikator.client.shared.data.RecordDTO;
import org.aplikator.client.shared.data.SearchResult;
import org.aplikator.client.shared.descriptor.EntityDTO;
import org.aplikator.server.data.Context;
import org.aplikator.server.data.Record;

/**
 * @author eskymo
 *         <p/>
 *         Search interface for providing search services to aplikator
 */

public interface Search {
    public static final String SEARCH_ENABLED = "aplikator.searchEnabled";
    public static final String SEARCH_ENGINE = "aplikator.searchEngine";

    /**
     * getPagefromSearch - searches the records in searchengine and returns a
     * list of Records based on the view supplied - used for a search inside an
     * entity
     *
     * @param vd
     * @param searchArgument
     * @param pageOffset
     * @param pageSize
     * @param ctx
     * @return
     */
    public SearchResult getPagefromSearch(String vdId, String searchArgument,
                                          int pageOffset, int pageSize, Context ctx);

    /**
     * getPagefromSearch - searches the records in searchengine and returns a
     * list of record based on the default view in the entity - used for generic
     * search accross all entities/types
     *
     * @param searchArgument
     * @param pageOffset
     * @param pageSize
     * @param ctx
     * @return
     */
    public SearchResult getPagefromSearch(String searchArgument,
                                          int pageOffset, int pageSize, Context ctx);

    /**
     * index - puts the record into the index into the search engine
     *
     * @param indexName
     * @param record
     */
    public void index(RecordDTO record);

    /**
     * index - puts all records from the entity into the search engine
     *
     * @param indexName
     * @param entityDTO
     */
    public void index(EntityDTO entityDTO);

    /**
     * search - searches the search engine for records matching the
     * searchArgument and type (entity) - records are in non-localized form and
     * without any triggers
     *
     * @param searchArgument
     * @param type
     * @param offset
     * @param size
     * @return
     */
    public SearchResult search(String searchArgument, String type,
                               int offset, int size);


    /**
     * finishes the work with search
     */
    public void finish();

    /**
     * search - searches the search engine for all records matching the
     * searchArgument accross all types - records are in non-localized form and
     * without any triggers
     *
     * @param searchArgument
     * @param offset
     * @param size
     * @return
     */
    public SearchResult search(String searchArgument, int offset,
                               int size);

    /**
     * update method for updating a record in index with given primaryKey
     *
     * @param primaryKey
     */
    public void update(PrimaryKey primaryKey, Context ctx);

    /**
     * insert method for inserting a record into an index
     *
     * @param primaryKey
     */
    public void insert(PrimaryKey primaryKey, Context ctx);

    /**
     * delete method for removing record from index
     *
     * @param primaryKey
     */
    public void delete(PrimaryKey primaryKey);

}
