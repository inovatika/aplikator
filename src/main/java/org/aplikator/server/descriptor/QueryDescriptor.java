package org.aplikator.server.descriptor;

import java.util.ArrayList;
import java.util.List;

import org.aplikator.client.shared.descriptor.QueryDescriptorDTO;
import org.aplikator.server.data.Context;
import org.aplikator.server.query.QueryExpression;

public class QueryDescriptor extends LocalizedServerDescriptorBase implements Cloneable {
    private List<QueryParameter> queryParameters = new ArrayList<QueryParameter>();
    private QueryExpression queryExpression;

    public QueryDescriptor(String id, String label) {
        super(id);
        setLocalizationKey(label);
    }

    public QueryDescriptor setQueryExpression(QueryExpression queryExpression) {
        this.queryExpression = queryExpression;
        return this;
    }

    public QueryDescriptor addQueryParameter(QueryParameter param) {
        queryParameters.add(param);
        return this;
    }

    public List<QueryParameter> getQueryParameters() {
        return queryParameters;
    }


    public QueryExpression getQueryExpression(List<QueryParameter> queryParameters, Context ctx) {
        if (queryExpression != null) {
            queryExpression.bindParameters(queryParameters);
        }
        return queryExpression;
    }

    public QueryDescriptorDTO getQueryDescriptorDTO(Context ctx) {
        QueryDescriptorDTO queryDescriptorDTO = new QueryDescriptorDTO(this.getId(), this.getLocalizedName(ctx));
        for (QueryParameter qp : getQueryParameters()) {
            queryDescriptorDTO.addQueryParameter(qp.getQueryParameterDTO(ctx));
        }
        return queryDescriptorDTO;
    }

    @Override
    public QueryDescriptor clone() throws CloneNotSupportedException {
        return (QueryDescriptor) super.clone();
    }
}
