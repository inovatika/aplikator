package org.aplikator.client.local.widgets;

import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import org.aplikator.client.shared.descriptor.PropertyDTO;
import org.gwtbootstrap3.client.ui.TextBox;

import java.io.Serializable;

public class LabelFieldWidget<T extends Serializable> extends DataFieldBase<T> implements DataField<T> {
    private TextBox box = new TextBox();

    public LabelFieldWidget(String caption, String localizedTooltip, PropertyDTO property, int size, boolean horizontal, String elementId, String customLabelStyle, HasFields form) {
        super(caption, localizedTooltip, property, horizontal, elementId, customLabelStyle, form);
        controlHolder.add(box);
        box.setEnabled(false);
        //box.addStyleName(PaneWidgetResources.INSTANCE.css().formData());

        /*if (property.getRefferedThrough() != null) {
            this.setEnabled(false);
        } */

        this.setGridSize(size);
    }

    public void setValue(T value) {
        box.setTitle(value != null ? value.toString() : "");
    }

    public T getValue() {
        return null;
    }

    public HandlerRegistration addValueChangeHandler(@SuppressWarnings("rawtypes") ValueChangeHandler handler) {
        return null;
    }

    public void setEnabled(boolean enabled) {
    }

    public void setDirty(boolean dirty) {
        super.setDirty(dirty);
    }

}
