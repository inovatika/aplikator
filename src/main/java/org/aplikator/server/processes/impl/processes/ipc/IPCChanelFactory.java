/*
 * Copyright 2013 Pavel Stastny <pavel.stastny at gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aplikator.server.processes.impl.processes.ipc;

/**
 * Creates channel factory
 *
 * @author Pavel Stastny <pavel.stastny at gmail.com>
 */
public abstract class IPCChanelFactory {


    /**
     * Creates IPC channel
     *
     * @param applicationName Application name
     * @return
     */
    public abstract IPCChannel createChannel(String applicationName);

    public static IPCChanelFactory createFactory() throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        // configuration property
        String clz = System.getProperty("ipc.channel.factory", "org.aplikator.server.processes.impl.processes.ipc.sharedfolder.SharedFolderChannelFactory");
        Class<?> classForname = Class.forName(clz);
        Object obj = classForname.newInstance();
        return (IPCChanelFactory) obj;
    }
}
