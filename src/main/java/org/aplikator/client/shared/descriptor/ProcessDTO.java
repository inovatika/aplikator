/*
 * Copyright 2013 Pavel Stastny <pavel.stastny at gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aplikator.client.shared.descriptor;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.jboss.errai.common.client.api.annotations.Portable;

/**
 * Process transfer object
 *
 * @author Pavel Stastny <pavel.stastny at gmail.com>
 */
@Portable
public class ProcessDTO implements Serializable {


    private String identifier;
    private String applicationName;
    private boolean liveProcess;
    private String processType;
    private String processState;

    private String startedDate = null;
    private String stoppedDate = null;


    private HashMap<String, Object> processProperties;

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public boolean isLiveProcess() {
        return liveProcess;
    }

    public void setLiveProcess(boolean liveProcess) {
        this.liveProcess = liveProcess;
    }

    public String getProcessType() {
        return processType;
    }

    public void setProcessType(String processType) {
        this.processType = processType;
    }

    public String getProcessState() {
        return processState;
    }

    public void setProcessState(String processState) {
        this.processState = processState;
    }

    public Map<String, Object> getProcessProperties() {
        return processProperties;
    }

    public void setProcessProperties(Map<String, Object> props) {
        this.processProperties = new HashMap<String, Object>(props);
    }

    public String getStartedDate() {
        return startedDate;
    }

    public void setStartedDate(String startedDate) {
        this.startedDate = startedDate;
    }

    public String getStoppedDate() {
        return stoppedDate;
    }

    public void setStoppedDate(String stoppedDate) {
        this.stoppedDate = stoppedDate;
    }
}
