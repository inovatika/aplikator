package org.aplikator.server.security;

import com.sun.security.auth.UserPrincipal;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigValue;
import org.aplikator.server.Configurator;
import org.aplikator.server.data.Context;
import org.aplikator.server.data.Record;
import org.aplikator.server.descriptor.Application;
import org.aplikator.server.descriptor.Entity;
import waffle.servlet.WindowsPrincipal;
import waffle.windows.auth.IWindowsAuthProvider;
import waffle.windows.auth.IWindowsIdentity;
import waffle.windows.auth.PrincipalFormat;
import waffle.windows.auth.impl.WindowsAuthProviderImpl;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.security.auth.Subject;
import javax.servlet.http.HttpServletRequest;
import java.math.BigInteger;
import java.security.Principal;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 *
 */
public class SecurityModule {

    private static final int ITERATIONS = 1000;
    private static final int KEY_LENGTH = 192;
    private Map<String, Account> accounts = new HashMap<>();
    private boolean databaseRealmLoaded = false;
    private IWindowsAuthProvider waffleAuthenticator = null;

    private static final Logger LOG = Logger.getLogger(SecurityModule.class.getName());

    public static SecurityModule get() {
        return SecurityModule.LazyHolder.instance;
    }

    private static SecurityModule instance() {
        SecurityModule c = new SecurityModule();
        c.loadTextRealm();
        c.loadWaffleRealm();
        return c;
    }

    public static String hashPassword(String password, String salt) {
        char[] passwordChars = password.toCharArray();
        byte[] saltBytes = salt.getBytes();
        PBEKeySpec spec = new PBEKeySpec(passwordChars, saltBytes, ITERATIONS, KEY_LENGTH);
        try {
            SecretKeyFactory key = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
            byte[] hashedPassword = key.generateSecret(spec).getEncoded();
            return String.format("%x", new BigInteger(hashedPassword));
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    private void loadTextRealm() {
        for (Config config : Configurator.get().getConfig().getConfigList("realm")) {
            Account account = new Account(config.getString("username"));
            account.setPasswordHash(hashPassword(config.getString("password"), account.getPrincipal()));
            account.addRoles(config.getStringList("roles").toArray(new String[]{}));
            account.setFullName(config.hasPath("fullname") ? config.getString("fullname") : null);
            account.setEmail(config.hasPath("email") ? config.getString("email") : null);
            this.addAccount(account);
        }
    }

    private void loadDatabaseRealm(Context ctx) {
        if (Application.get().getAccountsEntity() == null) {
            return;
        }
        Entity accountsEntity = Application.get().getAccountsEntity();
        if (accountsEntity.getPersisterTriggers() instanceof AccountsTrigger) {
            List<Record> records = ctx.getRecords(accountsEntity).list();
            for (Record record : records) {
                Account account = ((AccountsTrigger) accountsEntity.getPersisterTriggers()).getAccount(record, ctx);
                addAccount(account);
            }
        } else {
            throw new IllegalStateException("Accounts entity PersisterTriggers must implement AccountsTrigger interface");
        }
    }

    private void loadWaffleRealm() {
        if (Configurator.get().getConfig().hasPath("waffleRealm")) {
            waffleAuthenticator = new WindowsAuthProviderImpl();
            Config realmConfig = Configurator.get().getConfig().getConfig("waffleRealm");

            Map<String, String> groupRolesMap = new HashMap<String, String>();
            for (Map.Entry<String, ConfigValue> groupRole : realmConfig.getConfig("groupRolesMap").entrySet()) {
                groupRolesMap.put(groupRole.getKey().replaceAll("\"", "").replaceAll("\\\\\\\\", "\\\\"), (String) groupRole.getValue().unwrapped());
            }
            SSOGroupMap.setGroupRolesMap(groupRolesMap);
            if (realmConfig.getBoolean("useSSO")) {
                SSOGroupMap.activateSSO();

            }
        }
    }

    public synchronized void ensureDatabaseRealmLoaded(Context ctx) {
        if (databaseRealmLoaded) {
            return;
        }
        loadDatabaseRealm(ctx);
        databaseRealmLoaded = true;
    }

    private void bindPrincipal(Principal principal, HttpServletRequest request) {
        Subject subject = new Subject();
        subject.getPrincipals().add(principal);
        request.getSession(true).setAttribute("javax.security.auth.subject", subject);
    }

    public void login(String username, String password, HttpServletRequest request) {
        boolean loginSuccess = false;
        if (waffleAuthenticator != null) {
            try {
                IWindowsIdentity windowsIdentity = waffleAuthenticator.logonUser(username, password);
                Principal principal = new WindowsPrincipal(windowsIdentity, PrincipalFormat.FQN, PrincipalFormat.FQN);
                bindPrincipal(principal, request);
                loginSuccess = true;
            } catch (Throwable ex) {
                LOG.info("Waffle authenticator attempt failed: " + ex.getMessage());
            }
        }
        if (!loginSuccess) {
            try {
                Account account = getAccount(username, request);
                if (account != null && (
                        (account.getPasswordHash() != null && account.getPasswordHash().equals(hashPassword(password, username))) ||
                                account.getPasswordHash() == null   // HACK - allow login with empty password
                )
                ) {
                    Principal principal = new UserPrincipal(username);
                    bindPrincipal(principal, request);
                    loginSuccess = true;
                }
            } catch (Throwable ex) {
                LOG.info("Account authenticator attempt failed: " + ex.getMessage());
            }

        }
        if (!loginSuccess) {
            throw new RuntimeException("Login failed");
        }
    }

    public void logout(HttpServletRequest request) {
        request.getSession(true).invalidate();//setAttribute("javax.security.auth.subject", null);
    }

    public void addAccount(Account account) {
        if (account == null) return;
        accounts.put(account.getPrincipal().toLowerCase(), account);
    }

    public void removeAccount(String principal) {
        if (principal == null) return;
        accounts.remove(principal.toLowerCase());
    }

    public Account getAccount(String principal, HttpServletRequest request) {
        if (principal == null) return null;
        Account retval = accounts.get(principal.toLowerCase());
        if (retval == null) {
            retval = Application.get().getCustomAccount(principal, request);
        }
        return retval;
    }

    private Principal getSessionPrincipal(HttpServletRequest request) {
        Subject subject = (Subject) request.getSession().getAttribute("javax.security.auth.subject");
        if (subject == null) {
            if (request.getRemoteUser() != null) {
                Principal principal = new UserPrincipal(request.getRemoteUser());
                bindPrincipal(principal, request);
                LOG.fine("getSessionPrincipal- Externally authenticated principal: " + principal.getName());
                return principal;
            }
            if (request.getHeader("Authorization") != null) { //fallback for basic authorization header
                String authHeader = request.getHeader("Authorization");
                if (authHeader != null && authHeader.startsWith("Basic")) {
                    String encodedUsernamePassword = authHeader.substring("Basic ".length()).trim();
                    byte[] decodedBytes = Base64.getDecoder().decode(encodedUsernamePassword);
                    String usernamePassword = new String(decodedBytes);
                    int seperatorIndex = usernamePassword.indexOf(':');

                    String username = usernamePassword.substring(0, seperatorIndex);
                    String password = usernamePassword.substring(seperatorIndex + 1);
                    try {
                        login(username, password, request);
                    } catch (Exception ex) {
                        return null;
                    }
                    return getSessionPrincipal(request);
                } else if (authHeader != null && authHeader.startsWith("Bearer")) {
                    String token = authHeader.substring("Bearer ".length());
                    String username = JWTUtils.getUserLogin(token);
                    if (username == null){
                        return null;
                    }
                    Principal principal = new UserPrincipal(username);
                    bindPrincipal(principal, request);
                    return principal;
                }
            }
            return null;
        }
        if (subject.getPrincipals().isEmpty()) {
            LOG.fine("getSessionPrincipal - boundPrincipal is empty");
            return null;
        }
        Principal retval = subject.getPrincipals().iterator().next();
        //LOG.fine("getSessionPrincipal-bound principal: " + retval.getName() + " (principals:"+subject.getPrincipals()+")");
        return retval;
    }

    public boolean isUserInRole(String role, HttpServletRequest request) {

        Principal principal = getSessionPrincipal(request);
        LOG.fine("isUserInRole:" + role + " Principal:" + (principal != null ? principal.getName() + " " + principal.getClass() : "null"));
        if (principal == null) {
            return false;
        }
        boolean isInWindowsRole = false;
        boolean isInAccountRole = false;
        if (principal instanceof WindowsPrincipal) {
            WindowsPrincipal sessionPrincipal = (WindowsPrincipal) principal;
            isInWindowsRole = SSOGroupMap.isUserInRole(sessionPrincipal, role);

        }
        Account account = getAccount(principal.getName(), request);
        LOG.fine("ACCOUNT:" + account);
        if (account != null) {
            isInAccountRole = account.getRoles().contains(role);
        }
        return isInWindowsRole || isInAccountRole;
    }

    public boolean isAuthenticated(HttpServletRequest request) {
        return getSessionPrincipal(request) != null;
    }

    public String getUserLogin(HttpServletRequest request) {
        Principal principal = getSessionPrincipal(request);
        if (principal == null) {
            return null;
        } else {
            return principal.getName();
        }
    }

    private static class LazyHolder {
        private static SecurityModule instance = instance();
    }
}

/*  original Shiro LDAP impl  //TODO  reimplement natively
    ActiveDirectoryRealm realm = new ActiveDirectoryRealm();
    Config realmConfig = Configurator.get().getConfig().getConfig("activeDirectoryRealm");
            realm.setPrincipalSuffix(realmConfig.getString("principalSuffix"));
                    realm.setUrl(realmConfig.getString("url"));
                    realm.setSystemUsername(realmConfig.getString("systemUsername"));
                    realm.setSystemPassword(realmConfig.getString("systemPassword"));
                    realm.setSearchBase(realmConfig.getString("searchBase"));
                    Map<String, String> groupRolesMap = new HashMap<String, String>();
        for (Map.Entry<String, ConfigValue> groupRole : realmConfig.getConfig("groupRolesMap").entrySet()) {
        groupRolesMap.put(groupRole.getKey().replaceAll("\"", "").replaceAll("\\\\\\\\", "\\\\"), (String) groupRole.getValue().unwrapped());
        }
        realm.setGroupRolesMap(groupRolesMap);
        retval.add(realm);
       */
