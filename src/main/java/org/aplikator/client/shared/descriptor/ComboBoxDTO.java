package org.aplikator.client.shared.descriptor;

import org.aplikator.client.local.widgets.ComboBoxWidget;
import org.aplikator.client.local.widgets.HasFields;

import com.google.gwt.user.client.ui.Widget;

@SuppressWarnings("serial")
//@JsonTypeName("combo")
//@Portable
public class ComboBoxDTO extends WidgetPropertyDTOBase {

    public static final String TYPE_KEY = "combo";

    @SuppressWarnings("unused")
    public ComboBoxDTO() {       //TODO custom marshaller
    }

    public ComboBoxDTO(PropertyDTO property) {
        super(property);
    }

    private String switcher;

    public String getSwitcher() {
        return switcher;
    }

    public void setSwitcher(String switcher) {
        this.switcher = switcher;
    }

    @Override
    public Widget getWidget(HasFields form) {
        ComboBoxWidget field = new ComboBoxWidget(getLocalizedName(), getLocalizedTooltip(), property, getSize(), isHorizontal(), getElementId(), getSwitcher(), getCustomLabelStyle(), form);
        field.setEnabled(isEnabled());
        field.setVisible(isVisible());
        field.setDefaultEnabled(isEnabled());
        if (form != null) {
            form.registerDataField(field);
        }
        return field;
    }
}
