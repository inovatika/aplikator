package org.aplikator.server.persistence.empiredb.oracle;

import java.util.ArrayList;

import org.apache.empire.db.DBColumnExpr;
import org.apache.empire.db.DBCommand;
import org.apache.empire.db.DBDatabase;

/**
 * Class is used to enable run specific SQL query using standard empireDb
 * resources (DBReader,...)
 * SQL query is provided to the class via constructor.
 */
public class EmpireDBCommandExplicit extends DBCommand {
    private static final long serialVersionUID = 1L;
    final String sql;

    public EmpireDBCommandExplicit(String sql, DBDatabase db) {
        super(db);
        this.sql = sql;
        this.select = new ArrayList<DBColumnExpr>();
    }

    @Override
    public synchronized void getSelect(StringBuilder buf) {
        buf.append(this.sql);
    }
}
