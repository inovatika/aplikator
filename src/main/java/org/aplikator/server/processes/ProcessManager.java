/*
 * Copyright 2013 Pavel Stastny <pavel.stastny at gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aplikator.server.processes;

import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.aplikator.server.processes.impl.ProcessManagerImpl;

/**
 * Process manager
 *
 * @author Pavel Stastny <pavel.stastny at gmail.com>
 */
public abstract class ProcessManager {

    public static final Logger LOGGER = Logger.getLogger(ProcessManager.class.getName());

    public static String MANAGER_INSTANCE = "process.manager";

    public static ProcessManager _INSTANCE = null;
    // generated name
    public final static String GENERATED_NAME;

    static {
        GENERATED_NAME = generateAppName();
    }

    public static String generateAppName() {
        Random rand = new Random();
        String name = UUID.randomUUID().toString();
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 10; i++) {
            int randindex = rand.nextInt(name.length());
            builder.append(name.charAt(randindex));
        }
        return builder.toString();
    }


    /**
     * Register new process
     *
     * @param process
     */
    public abstract void registerProcess(Process process);

    /**
     * Deregister process
     *
     * @param process
     */
    public abstract void deregisterProcess(Process process);


    /**
     * Returns all processes
     *
     * @return
     */
    public abstract List<Process> getProcesses();

    /**
     * Find pricess
     *
     * @param ident Process' identification
     * @return
     */
    public abstract Process lookUpProcess(String ident);


    /**
     * Plan process
     *
     * @param process
     */
    public abstract void schedule(Process process, Date schedulingTime);

    /**
     * Start scheduling process
     */
    public abstract void startScheduling();

    /**
     * Stop scheduling
     */
    public abstract void stopScheduling();

    /**
     * Shutdown manager
     */
    public abstract void shutDown();


    /**
     * Initialize manager
     *
     * @param appname
     * @return
     */
    public synchronized static ProcessManager initManager(String appname) {
        if (_INSTANCE == null) {
            LOGGER.info("Initializing process manager with name '" + appname + "'");
            _INSTANCE = new ProcessManagerImpl(appname);
        }
        return _INSTANCE;
    }

    public static ProcessManager getManager() {
        if (_INSTANCE == null) {
            initManager(GENERATED_NAME);
        }
        return _INSTANCE;
    }
}
