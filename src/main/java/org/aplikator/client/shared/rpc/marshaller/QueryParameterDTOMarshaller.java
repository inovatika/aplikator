package org.aplikator.client.shared.rpc.marshaller;

import org.aplikator.client.shared.descriptor.PropertyDTO;
import org.aplikator.client.shared.descriptor.QueryParameterDTO;
import org.jboss.errai.marshalling.client.api.Marshaller;
import org.jboss.errai.marshalling.client.api.MarshallingSession;
import org.jboss.errai.marshalling.client.api.annotations.ClientMarshaller;
import org.jboss.errai.marshalling.client.api.annotations.ServerMarshaller;
import org.jboss.errai.marshalling.client.api.json.EJObject;
import org.jboss.errai.marshalling.client.api.json.EJValue;
import org.jboss.errai.marshalling.client.marshallers.AbstractNullableMarshaller;

@ClientMarshaller(QueryParameterDTO.class)
@ServerMarshaller(QueryParameterDTO.class)
public class QueryParameterDTOMarshaller extends AbstractNullableMarshaller<QueryParameterDTO> {

	/*@Override
    public Class<QueryParameter> getTypeHandled() {
		return QueryParameter.class;
	}*/

    @Override
    public QueryParameterDTO[] getEmptyArray() {
        return new QueryParameterDTO[0];
    }

    @Override
    public QueryParameterDTO doNotNullDemarshall(EJValue o, MarshallingSession ctx) {
        if (o.isObject() != null) {
            EJObject ejObject = o.isObject();
            String name = DemarshallingUtils.safeString(ejObject, "name");
            String value = DemarshallingUtils.safeString(ejObject, "value");
            PropertyDTO propDTO = null;
            if (DemarshallingUtils.containsAndNotNull(ejObject, "property")) {
                Marshaller<Object> marshaller = ctx.getMarshallerInstance(PropertyDTO.class.getName());
                propDTO = (PropertyDTO) marshaller.demarshall(ejObject.get("property"), ctx);
            }
            QueryParameterDTO qp = new QueryParameterDTO(name, propDTO);
            qp.setValue(value);
            return qp;
        } else
            return null;
    }

    @Override
    public String doNotNullMarshall(QueryParameterDTO o, MarshallingSession ctx) {
        StringBuilder builder = new StringBuilder().append('{');
        MarshallingUtils.string("name", o.getName(), builder).append(',');
        MarshallingUtils.string("value", o.getValue(), builder).append(',');
        MarshallingUtils.key("property", builder).append(':');
        Marshaller<Object> entMarshaller = ctx.getMarshallerInstance(PropertyDTO.class.getName());
        builder.append(entMarshaller.marshall(o.getProperty(), ctx));
        builder.append('}');
        return builder.toString();
    }
}
