package org.aplikator.client.local.command;

import com.google.gwt.dom.client.Style;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.*;
import org.aplikator.client.local.Aplikator;
import org.aplikator.client.local.widgets.FormWidget;
import org.aplikator.client.local.widgets.LayoutUtils;
import org.aplikator.client.local.widgets.LoadingLabel;
import org.aplikator.client.shared.data.FunctionResult;
import org.aplikator.client.shared.data.FunctionResultStatus;
import org.aplikator.client.shared.data.PrimaryKey;
import org.aplikator.client.shared.data.RecordContainerDTO;
import org.aplikator.client.shared.descriptor.ViewDTO;
import org.aplikator.client.shared.rpc.AfterErrorExecutor;
import org.aplikator.client.shared.rpc.AplikatorErrorCallback;
import org.aplikator.client.shared.rpc.AplikatorService;
import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.Container;
import org.gwtbootstrap3.client.ui.Navbar;
import org.gwtbootstrap3.client.ui.NavbarForm;
import org.gwtbootstrap3.client.ui.NavbarText;
import org.gwtbootstrap3.client.ui.constants.IconType;
import org.gwtbootstrap3.client.ui.constants.Pull;
import org.gwtbootstrap3.client.ui.html.Div;
import org.gwtbootstrap3.client.ui.html.Strong;
import org.jboss.errai.common.client.api.RemoteCallback;
import org.jboss.errai.enterprise.client.jaxrs.api.RestClient;
import org.jboss.errai.ui.nav.client.local.Page;
import org.jboss.errai.ui.nav.client.local.PageHiding;
import org.jboss.errai.ui.nav.client.local.PageShown;
import org.jboss.errai.ui.nav.client.local.PageState;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import org.jboss.errai.ui.nav.client.local.api.NavigationControl;

import static org.aplikator.client.local.widgets.TableListerWidget.TABLE_NAVBAR_HEIGHT;


@Page(path = "display/{viewID}/{primaryKey}")
public class DisplayRecord extends Composite {
    protected ViewDTO view;
    protected String searchArgument;
    protected HandlerRegistration submitHandlerRegistration;
    FormWidget form;
    SimplePanel holder;
    @PageState
    private String viewID;
    @PageState
    private int primaryKey;
    private Widget widget = null;

    public DisplayRecord() {
        holder = new SimplePanel();
        initWidget(holder);
    }

    @PageShown
    public void start() {
        holder.setWidget(new LoadingLabel(Aplikator.application.getConfigString("aplikator.menu.loading")));
        //Window.alert("ViewID:"+viewID+"-primaryKey:"+ primaryKey);
        if (view == null) {
            RestClient.create(AplikatorService.class, new RemoteCallback<ViewDTO>() {
                        @Override
                        public void callback(ViewDTO response) {
                            DisplayRecord.this.view = response;
                            drawPanel(holder);
                            Window.setTitle( Aplikator.application.getConfigString("aplikator.brand")+ " - "+ response.getLocalizedName());
                        }
                    },
                    new AplikatorErrorCallback("aplikator.displayrecord.error", new AfterErrorExecutor() {
                        @Override
                        public void execute() {
                            holder.clear();
                        }
                    })
            ).getView(viewID);
        } else {
            drawPanel(holder);
        }
    }

    private void drawPanel(AcceptsOneWidget panel) {

        if (widget == null) {
            widget = createForm();
        }
        panel.setWidget(widget);
    }

    private Widget createForm() {
        /*
        <b:Navbar ui:field="navbar" addStyleNames="app-clean-border-top app-clean-mg-bottom">
        <b:Container fluid="true">
        <b:NavbarText addStyleNames="app-clean-mg-left app-color-black">
        <h:Strong ui:field="title"/>
        </b:NavbarText>
        <b:NavbarForm pull="LEFT" addStyleNames="app-clean-pd-left">
         <b:Button ui:field="buttonCancel" icon="BAN"/>
         <b:Button ui:field="buttonSave" icon="SAVE"/>
        </b:NavbarForm>

        <b:Container fluid="true">
                <b:Row ui:field="listContainer">
                    <b:Column ui:field="listColumn" size="1" addStyleNames="app-list-panel"/>
                    <b:Column ui:field="formColumn" size="11" addStyleNames="app-form-panel"/>
                </b:Row>
                <b:Row ui:field="gridContainer">
                    <b:Column ui:field="gridColumn" size="12" addStyleNames="app-grid-panel"/>
                </b:Row>
        </b:Container>
*/
        Div div = new Div();
        Navbar navbar = new Navbar();
        navbar.addStyleName("app-clean-border-top");
        navbar.addStyleName("app-clean-mg-bottom");
        Container navContainer = new Container();
        navContainer.setFluid(true);
        navbar.add(navContainer);
        NavbarText navText = new NavbarText();
        navText.addStyleName("app-clean-mg-left");
        navText.addStyleName("app-color-black");
        navText.add(new Strong(view.getLocalizedName()));
        navContainer.add(navText);
        NavbarForm navForm = new NavbarForm();
        navForm.setPull(Pull.LEFT);
        navForm.addStyleName("app-clean-pd-left");
        navContainer.add(navForm);
        Button buttonCancel = new Button();
        buttonCancel.setIcon(IconType.BAN);
        navForm.add(buttonCancel);
        Button buttonSave = new Button();
        buttonSave.setIcon(IconType.SAVE);
        navForm.add(buttonSave);
        LayoutUtils.addTooltip(buttonCancel, Aplikator.application.getConfigString("aplikator.table.cancel"));
        LayoutUtils.addTooltip(buttonSave, Aplikator.application.getConfigString("aplikator.table.save"));

        buttonCancel.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                form.cancel();
            }
        });

        buttonSave.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                form.recordContainerDTO.setFunctionResult(new FunctionResult("", FunctionResultStatus.SUCCESS));
                form.save(true);
            }
        });

        final int SCROLL_OFFSET = Aplikator.application.isShowNavigation() ? Aplikator.MAINMENUBAR_HEIGHT + TABLE_NAVBAR_HEIGHT : TABLE_NAVBAR_HEIGHT;
        final int scrollHeight =  Window.getClientHeight() - SCROLL_OFFSET;

        SimplePanel formHolder = new SimplePanel();
        formHolder.add(form);

        ScrollPanel formScroller = new ScrollPanel(formHolder);

        formScroller.addStyleName("formscroller");
        formScroller.getElement().getStyle().setHeight(scrollHeight, Style.Unit.PX);

        form = new FormWidget(view, null, null, null, "DisplayRecord");
        formHolder.add(form);
        div.add(navbar);
        div.add(formScroller);
        form.displayRecord(new PrimaryKey(view.getEntity().getId(), primaryKey), new RecordContainerDTO(), null, null, null);

        return div.asWidget();
    }

    @PageHiding
    public void onLeave(NavigationControl control){
        Aplikator.confirmCLose(control);
    }



}
