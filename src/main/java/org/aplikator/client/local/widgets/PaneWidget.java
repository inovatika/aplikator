package org.aplikator.client.local.widgets;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import org.gwtbootstrap3.client.ui.Column;
import org.gwtbootstrap3.client.ui.FormLabel;
import org.gwtbootstrap3.client.ui.Row;
import org.gwtbootstrap3.client.ui.constants.ColumnSize;
import org.gwtbootstrap3.client.ui.html.Div;

public class PaneWidget extends Composite implements DisableableWidget {

    protected Div panel;

    protected FormLabel label;

    protected Div labelHolder;


    public PaneWidget(String label, String localizedTooltip, boolean framed, boolean horizontal, int size, String elementId, String customLabelStyle, HasFields form) {
//        if (elementId != null) {
//            Window.alert("ELEMENTID:"+elementId+" CUSTOM LABEL STYLE:" + customLabelStyle);
//        }
        if (label != null && !"".equals(label) && !"null".equals(label)) {
            this.label = new FormLabel();
            this.label.setText(label);
            this.label.addStyleName("fieldLabel");
            if (customLabelStyle != null){
                this.label.addStyleName(customLabelStyle);
            }
            if (horizontal) {
                labelHolder = new Row();
            } else {
                labelHolder = new Column(LayoutUtils.size(size));
            }
            labelHolder.addStyleName("app-clean-mg-left");
            labelHolder.addStyleName("app-clean-mg-right");
            labelHolder.add(this.label);
        }
        if (horizontal) {
            panel = new Row();
        } else {
            if (labelHolder == null) {
                panel = new Column(LayoutUtils.size(size));
            } else {
                panel = new Column(ColumnSize.XS_12);
            }
        }

        if (horizontal && size > 0 && size < 12) {

            Column sizerColumn = new Column(LayoutUtils.size(size));
            sizerColumn.add(panel);
            if (labelHolder == null) {
                Row wrapper = new Row();
                wrapper.add(sizerColumn);
                initPane(wrapper, elementId, form);
            } else {
                labelHolder.add(sizerColumn);
                initPane(labelHolder, elementId, form);
            }
        } else {
            if (labelHolder == null) {
                initPane(panel, elementId, form);
            } else {
                labelHolder.add(panel);
                initPane(labelHolder, elementId, form);
            }
        }

        if (framed) {
            panel.addStyleName("panelBorder");
        }
        if (localizedTooltip != null && !localizedTooltip.isEmpty() && !"null".equals(localizedTooltip)) {
            panel.getElement().setAttribute("title", localizedTooltip);
            if (labelHolder != null) {
                labelHolder.getElement().setAttribute("title", localizedTooltip);
            }
        }

    }

    private void initPane(Widget widget, String elementId, HasFields form){
        if (elementId != null && form != null) {
            widget.getElement().setId(form.getPrefix()+"-"+elementId);
        }
        initWidget(widget);
    }

    public void add(Widget widget) {
        panel.add(widget);
    }

    @Override
    public void setEnabled(boolean enabled) {
        for (int i = 0; i < panel.getWidgetCount(); i++) {
            Widget child = panel.getWidget(i);
            if (child instanceof  DisableableWidget) {
                ((DisableableWidget) child).setEnabled(enabled);
            }
        }
    }
}
