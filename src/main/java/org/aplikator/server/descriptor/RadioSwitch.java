package org.aplikator.server.descriptor;

import org.aplikator.client.shared.descriptor.RadioSwitchDTO;
import org.aplikator.client.shared.descriptor.WidgetDTO;
import org.aplikator.server.data.Context;

import java.io.Serializable;

public class RadioSwitch<T extends Serializable> extends WidgetPropertyDescriptorBase<T> {


    public RadioSwitch(Property<T> property) {
        super(property);
        setHorizontal(false);
        if (property.getListProvider() == null) {
            throw new IllegalStateException("Property " + property.getId() + " must have assigned listProvider");
        }
    }

    private String switcher;

    public RadioSwitch setSwitcher(String switcher) {
        this.switcher = switcher;
        return this;
    }

    @Override
    public WidgetDTO getWidgetDescriptor(Context ctx) {
        RadioSwitchDTO desc = new RadioSwitchDTO(getProperty().getPropertyDTO(ctx));
        desc.setSize(getSize());
        desc.setAccess(getProperty().getAccess(ctx));
        desc.setEnabled(isEnabled(ctx));
        desc.setVisible(isVisible(ctx));
        desc.setLocalizedName(getLocalizedName(ctx));
        desc.setLocalizedTooltip(getLocalizedTooltip(ctx));
        desc.setHorizontal(isHorizontal());
        desc.setElementId(getElementId());
        desc.setCustomLabelStyle(getCustomLabelStyle());
        desc.setSwitcher(switcher);
        return desc;
    }


}
