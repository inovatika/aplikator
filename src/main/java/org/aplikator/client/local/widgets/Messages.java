package org.aplikator.client.local.widgets;


import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.RootPanel;
import org.aplikator.client.local.Aplikator;
import org.aplikator.client.shared.data.FunctionResultStatus;
import org.aplikator.client.shared.rpc.AplikatorException;
import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.Modal;
import org.gwtbootstrap3.client.ui.ModalBody;
import org.gwtbootstrap3.client.ui.ModalSize;
import org.gwtbootstrap3.client.ui.constants.ButtonSize;
import org.gwtbootstrap3.client.ui.constants.ModalBackdrop;
import org.gwtbootstrap3.client.ui.html.Div;
import org.gwtbootstrap3.client.ui.html.Span;
import org.gwtbootstrap3.extras.notify.client.constants.NotifyType;
import org.gwtbootstrap3.extras.notify.client.ui.Notify;
import org.gwtbootstrap3.extras.notify.client.ui.NotifySettings;
import org.jboss.errai.enterprise.client.jaxrs.api.ResponseException;

import java.util.ArrayList;
import java.util.List;

/**
 * @author vlahoda
 */
public class Messages {

    private static final int Z_INDEX = 99999;
    private static final int ERROR_TOP = 20;
    private static final int ERROR_OFFSET = 96;

    public static void success(String text) {
        NotifySettings go = NotifySettings.newSettings();
        go.setType(NotifyType.SUCCESS);
        go.setAllowDismiss(false);
        go.setZIndex(Z_INDEX);
        Notify.notify(text, go);
    }

    public static void error(String text) {
        NotifySettings go = NotifySettings.newSettings();
        go.setType(NotifyType.DANGER);
        go.setAllowDismiss(true);
        go.setDelay(0);
        go.setPauseOnMouseOver(true);
        go.setZIndex(Z_INDEX);
        Notify.notify(text, go);
    }

    public static void info(String text) {
        NotifySettings go = NotifySettings.newSettings();
        go.setType(NotifyType.INFO);
        go.setAllowDismiss(true);
        go.setDelay(10000);
        go.setZIndex(Z_INDEX);
        Notify.notify(text, go);
    }

    public static void warning(String text) {
        NotifySettings go = NotifySettings.newSettings();
        go.setType(NotifyType.WARNING);
        go.setDelay(10000);
        go.setPauseOnMouseOver(true);
        go.setAllowDismiss(false);
        go.setZIndex(Z_INDEX);
        Notify.notify(text, go);
    }

    public static void cleanAll() {
        Notify.hideAll();
    }

    public static void restError(String text, Throwable throwable) {
        if (throwable instanceof AplikatorException) {
            messageWithDetail(text, throwable.getMessage(), FunctionResultStatus.ERROR);
        } else {
            try {
                throw throwable;
            } catch (ResponseException e) {
                Response response = e.getResponse();
                messageWithDetail(text, response.getText(), FunctionResultStatus.ERROR);
            } catch (RuntimeException e) {
                Messages.error(text);
            } catch (Throwable t) {
                Messages.error(text + " " + t);
            }
        }
    }


    /**
     * Simulate Notify behaviour for error messages, but with additional details button
     */
    private static List<Div> errorDivs = new ArrayList<>();

    public static void messageWithDetail(final String message, final String detail, final FunctionResultStatus status) {
        String localizedMessage = Aplikator.application.getConfigString(message);
        final Div msg = new Div();
        msg.addStyleName("col-xs-11");
        msg.addStyleName("col-sm-4");
        msg.addStyleName("alert");
        if (status.equals(FunctionResultStatus.ERROR)) {
            msg.addStyleName("alert-danger");
        } else if (status.equals(FunctionResultStatus.WARNING)) {
            msg.addStyleName("alert-warning");
        } else {
            msg.addStyleName("alert-info");
        }
        msg.addStyleName("animated");
        msg.addStyleName("fadeInDown");
        msg.getElement().setAttribute("data-notify", "container");
        msg.getElement().setAttribute("data-notify-position", "top-right");
        msg.getElement().getStyle().setZIndex(Z_INDEX-10);
        msg.getElement().getStyle().setDisplay(Style.Display.INLINE_BLOCK);
        msg.getElement().getStyle().setMargin(0, Style.Unit.PX);
        msg.getElement().getStyle().setPosition(Style.Position.FIXED);
        msg.getElement().getStyle().setTop(20, Style.Unit.PX);
        msg.getElement().getStyle().setRight(20, Style.Unit.PX);


        Button close = new Button("x");
        close.getElement().setAttribute("data-notify", "dissmiss");
        close.getElement().setAttribute("type", "button");
        close.getElement().setAttribute("aria-hidden", "true");
        close.addStyleName("close");
        close.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                removeErrorDiv(msg);
            }
        });

        Button details = new Button(Aplikator.application.getConfigString("aplikator.error.details"));
        details.getElement().setAttribute("data-notify", "dissmiss");
        details.getElement().setAttribute("type", "button");
        details.getElement().setAttribute("aria-hidden", "true");
        details.getElement().getStyle().setDisplay(Style.Display.BLOCK);
        details.setSize(ButtonSize.EXTRA_SMALL);
        details.setMarginTop(5);
        details.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(ClickEvent event) {
                Modal dm = new Modal();
                dm.setSize(ModalSize.LARGE);
                dm.setTitle(localizedMessage);
                dm.getElement().getStyle().setZIndex(Z_INDEX + 10);
                dm.setClosable(true);
                dm.setDataBackdrop(ModalBackdrop.STATIC);
                ModalBody dmb = new ModalBody();
                dmb.add(new HTML(detail.replaceAll("\n", "<br>")));
                dm.add(dmb);
                removeErrorDiv(msg);
                dm.show();
            }
        });
        msg.add(new Span(localizedMessage));
        msg.add(close);
        msg.add(details);
        errorDivs.add(msg);
        repositionErrorDivs();
        Notify.hideAll();
        RootPanel.get().add(msg);


    }

    private static void removeErrorDiv(Div msg) {
        RootPanel.get().remove(msg);
        errorDivs.remove(msg);
        repositionErrorDivs();

    }

    private static void repositionErrorDivs() {
        for (int i = 0; i < errorDivs.size(); i++) {
            Div msg = errorDivs.get(i);
            msg.getElement().getStyle().setTop(ERROR_TOP + i * ERROR_OFFSET, Style.Unit.PX);
        }
    }

}
