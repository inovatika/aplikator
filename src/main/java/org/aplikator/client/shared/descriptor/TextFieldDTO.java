package org.aplikator.client.shared.descriptor;

import org.aplikator.client.local.widgets.HasFields;
import org.aplikator.client.local.widgets.TextFieldWidget;

import com.google.gwt.user.client.ui.Widget;

@SuppressWarnings("serial")
//@JsonTypeName("textfield")
//@Portable
public class TextFieldDTO extends WidgetPropertyDTOBase {

    public static final String TYPE_KEY = "textfield";

    @SuppressWarnings("unused")
    public TextFieldDTO() {//TODO custom marshaller
    }

    public TextFieldDTO(PropertyDTO property) {
        super(property);
    }

    @Override
    public Widget getWidget(HasFields form) {
        TextFieldWidget field = new TextFieldWidget(getLocalizedName(), getLocalizedTooltip(), property, getSize(), getFormatPattern(), isHorizontal(), getElementId(), getCustomLabelStyle(), form);
        field.setEnabled(isEnabled());
        field.setVisible(isVisible());
        field.setDefaultEnabled(isEnabled());
        if (form != null) {
            form.registerDataField(field);
        }

        return field;
    }

}
