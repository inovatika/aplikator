package org.aplikator.server.descriptor;

import org.aplikator.server.data.Context;

import java.io.Serializable;

/**
 * User: vlahoda
 */
public abstract class WidgetPropertyDescriptorBase<T extends Serializable> extends WidgetDescriptorBase implements HasProperty<T> {

    private Property<T> property;

    protected WidgetPropertyDescriptorBase(Property<T> property) {
        this.property = property;
        this.setEnabled(property.isEditable());
        this.setFormatPattern(property.getFormatPattern());
        this.setLocalizationKey(property.getLocalizationKey());
        this.setLocalizationTooltipKey(property.getLocalizationTooltipKey());
    }

    public Property<T> getProperty() {
        return property;
    }

    public void registerProperties(Form form) {
        form.addProperty(getProperty());
    }

    @Override
    public boolean isEnabled(Context ctx) {
        if (super.isEnabled(ctx)) {
            return !property.getAccess(ctx).isReadOnly();
        } else {
            return false;
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public Widget cloneWithReference(Reference<? extends Entity> referencingProperty) {
        WidgetPropertyDescriptorBase retval;
        try {
            retval = (WidgetPropertyDescriptorBase) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new UnsupportedOperationException(e);
        }

        retval.property = getPropertyWithReference(referencingProperty);
        return retval;
    }

    @SuppressWarnings("unchecked")
    private Property<T> getPropertyWithReference(Reference<? extends Entity> referencingProperty) {
        if (referencingProperty != null) {
            Property rootProperty = getProperty();
            if (rootProperty.isVirtual()) {
                return rootProperty;
            }
            Property lastRelatedProperty = null;
            while (rootProperty.getRefferedThrough() != null) {
                lastRelatedProperty = rootProperty;
                rootProperty = rootProperty.getRefferedThrough();
                lastRelatedProperty = ((Reference) rootProperty).relate(lastRelatedProperty);
            }
            Property referencedProperty = referencingProperty.relate(rootProperty);
            if (lastRelatedProperty == null) {
                return referencedProperty;
            } else {
                //lastRelatedProperty.setRefferedThrough((Reference) referencedProperty);
                return ((Reference) referencedProperty).relate(lastRelatedProperty);
                //return getProperty();
            }
        } else {
            throw new IllegalArgumentException("Referencing property cannot be null -" + this);
            //return getProperty();
        }
    }

}
