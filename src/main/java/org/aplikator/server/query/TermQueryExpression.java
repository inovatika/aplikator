package org.aplikator.server.query;

import org.aplikator.server.descriptor.Property;
import org.aplikator.server.descriptor.QueryParameter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class TermQueryExpression<T extends Serializable> extends QueryExpression {

    private final Property<T> property;
    private final TermQueryOperator operator;
    private T value;
    private final QueryParameterReference parameterReference;
    private Property<T> compareProperty;

    public TermQueryExpression(Property<T> property, TermQueryOperator operator, T value) {
        super();
        if (property.isVirtual() && !property.equals(Property.COUNT)) {
            throw new IllegalArgumentException("Virtual property " + property.getId() + " cannot be queried.");
        }
        this.property = property;
        this.operator = operator;
        this.value = value;
        this.parameterReference = null;
        this.compareProperty = null;
    }

    public TermQueryExpression(Property<T> property, TermQueryOperator operator, QueryParameterReference parameterReference) {
        super();
        if (property.isVirtual()) {
            throw new IllegalArgumentException("Virtual property " + property.getId() + " cannot be queried.");
        }
        this.property = property;
        this.operator = operator;
        this.value = null;
        this.parameterReference = parameterReference;
        this.compareProperty = null;
    }

    public TermQueryExpression(Property<T> property, TermQueryOperator operator, Property<T> compareProperty) {
        super();
        if (property.isVirtual()) {
            throw new IllegalArgumentException("Virtual property " + property.getId() + " cannot be queried.");
        }
        this.property = property;
        this.operator = operator;
        this.value = null;
        this.parameterReference = null;
        if (compareProperty.isVirtual()) {
            throw new IllegalArgumentException("Virtual property " + compareProperty.getId() + " cannot be queried.");
        }
        this.compareProperty = compareProperty;
    }

    public Property<T> getProperty() {
        return property;
    }

    public TermQueryOperator getOperator() {
        return operator;
    }

    public T getValue() {
        return value;
    }

    public Property<T> getCompareProperty() {
        return compareProperty;
    }

    @SuppressWarnings({"unchecked"})
    @Override
    public void bindParameters(List<QueryParameter> parameters) {
        if (parameterReference != null && parameters != null) {
            String stringValue = parameters.get(parameterReference.getParameterIndex()).getValue();
            if (property.getEncoder() != null) {
                value = (T) property.getEncoder().encode(stringValue);
            } else {
                if (property.getType().equals(java.util.Date.class)) {
                    try {
                        long parseLong = Long.parseLong(stringValue);
                        value = (T) new Date(parseLong);
                    } catch (NumberFormatException e) {
                        value = null;
                    }

                } else {
                    value = (T) stringValue;
                }
            }
        }
    }

}
