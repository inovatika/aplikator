package org.aplikator.server;

import org.apache.commons.fileupload.util.Streams;
import org.apache.commons.io.IOUtils;
import org.aplikator.client.local.widgets.BinaryFieldWidget;
import org.aplikator.client.shared.data.PrimaryKey;
import org.aplikator.client.shared.descriptor.Access;
import org.aplikator.server.data.AplikatorServiceBackend;
import org.aplikator.server.data.BinaryData;
import org.aplikator.server.data.Context;
import org.aplikator.server.descriptor.BinaryProperty;
import org.aplikator.server.descriptor.Entity;
import org.aplikator.server.persistence.PersisterFactory;
import org.aplikator.server.persistence.tempstore.Tempstore;
import org.aplikator.server.persistence.tempstore.TempstoreFactory;
import org.aplikator.utils.PDFLoader;

import javax.activation.MimeTypeParseException;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.aplikator.server.data.BinaryData.readSubsampled;

@WebServlet(
        name = "FileDownloadServlet",
        urlPatterns = {"/download"})
public class FileDownloadServlet extends HttpServlet {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private static final Logger LOG = Logger.getLogger(FileDownloadServlet.class.getName());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setCharacterEncoding("UTF-8");
        if (req.getParameter(BinaryFieldWidget.TEMPFILE_ID) != null) {
            processTempFile(req, resp);
        } else {

            BinaryData blob = null;
            String entityId = req.getParameter(BinaryFieldWidget.ENTITY_ID);
            String propertyId = req.getParameter(BinaryFieldWidget.PROPERTY_ID);
            try (Context ctx = new Context(req, resp, new AplikatorServiceBackend(), null)){
                int primaryKey = Integer.parseInt(req.getParameter(BinaryFieldWidget.PRIMARY_KEY_ID));
                int maxSize = Integer.parseInt(req.getParameter(BinaryFieldWidget.MAXSIZE_ID));
                long startTime = System.currentTimeMillis();
                boolean attachment = false;
                String attachmentPar = req.getParameter(BinaryFieldWidget.ATTACHMENT_ID);
                if (attachmentPar != null) {
                    attachment = Boolean.parseBoolean(attachmentPar);
                }
                Entity entity = (Entity) DescriptorRegistry.get().getDescriptionItem(entityId);
                @SuppressWarnings("unchecked")
                BinaryProperty property = (BinaryProperty) DescriptorRegistry.get().getDescriptionItem(propertyId);


                PrimaryKey pk = new PrimaryKey(entityId, primaryKey);
                Access access = property.getRecordAccess(ctx, pk, maxSize);
                if (maxSize >= BinaryFieldWidget.FULL_SIZE_ATTACHMENT_CODE || maxSize == BinaryFieldWidget.FULL_SIZE_IMAGE_CODE) {  //full size or custom size
                    if (!(access.equals(Access.READ_WRITE_CREATE_DELETE) || access.equals(Access.READ_WRITE_CREATE) || access.equals(Access.READ_WRITE) || access.equals(Access.READ))) {
                        resp.sendError(HttpServletResponse.SC_FORBIDDEN, "Access denied");
                        return;
                    }
                } else if (maxSize >= BinaryFieldWidget.PREVIEW_SIZE_CODE) { //preview
                    if (!(access.equals(Access.READ_WRITE_CREATE_DELETE) || access.equals(Access.READ_WRITE_CREATE) || access.equals(Access.READ_WRITE) || access.equals(Access.READ) || access.equals(Access.READ_PREVIEW))) {
                        resp.sendError(HttpServletResponse.SC_FORBIDDEN, "Access denied");
                        return;
                    }
                } else {//thumbnail
                    if (!(access.equals(Access.READ_WRITE_CREATE_DELETE) || access.equals(Access.READ_WRITE_CREATE) || access.equals(Access.READ_WRITE) || access.equals(Access.READ) || access.equals(Access.READ_PREVIEW) || access.equals(Access.READ_THUMBNAIL))) {
                        resp.sendError(HttpServletResponse.SC_FORBIDDEN, "Access denied");
                        return;
                    }
                }

                blob = PersisterFactory.getPersister().getBlob(entity, property, primaryKey, maxSize);
                long blobTime = System.currentTimeMillis();

                if (maxSize == BinaryFieldWidget.FULL_SIZE_ATTACHMENT_CODE) {
                    attachment = true;
                    resp.setContentType("application/octet-stream");
                } else if (maxSize == BinaryFieldWidget.FULL_SIZE_IMAGE_CODE) {
                    try {
                        resp.setContentType(ImageResizer.getMimeType(blob.properties.get("filename")).toString());
                    } catch (MimeTypeParseException e) {
                        resp.setContentType("application/octet-stream");
                    }
                } else {
                    resp.setContentType("image/jpeg");
                }
                resp.setHeader("Cache-Control", "no-cache");
                resp.setHeader("Pragma", "no-cache");
                if (attachment || maxSize != BinaryFieldWidget.THUMBNAIL_SIZE_CODE) {
                    String filename = "unnamed";
                    try {
                        String rawfilename = blob.properties.get("filename");
                        if (property.getTriggers() != null) {
                            rawfilename = property.getTriggers().processFileName(rawfilename, maxSize, new PrimaryKey(entityId, primaryKey), property, ctx);
                        }
                        if (rawfilename != null) {
                            String encodedfilename = URLEncoder.encode(rawfilename, "UTF-8");
                            filename = encodedfilename.replace("+", " ");
                        }
                    } catch (Exception e) {
                        LOG.log(Level.WARNING, "Error encoding filename ", e);
                    }
                    if (attachment) {
                        resp.setHeader("Content-disposition", "attachment; filename*=UTF-8''" + filename);
                    } else {
                        resp.setHeader("Content-Disposition", "inline; filename*=UTF-8''" + filename);
                    }
                }
                if (blob.inputStream != null && (maxSize == BinaryFieldWidget.FULL_SIZE_ATTACHMENT_CODE || maxSize == BinaryFieldWidget.FULL_SIZE_IMAGE_CODE || !property.isDisplayAsIcon())) {
                    OutputStream os = resp.getOutputStream();

                    Streams.copy(blob.inputStream, os, true);
                    os.flush();
                } else if (blob.properties.isEmpty()) {
                    OutputStream os = resp.getOutputStream();
                    Streams.copy(IOUtils.toInputStream("EMPTY", "UTF-8"), os, true);
                    os.flush();
                } else {
                    resp.setContentType("image/png");
                    String rawfilename = blob.properties.get("filename");
                    String iconFile = mapFilenameToIcon(rawfilename);
                    OutputStream os = resp.getOutputStream();
                    InputStream icon = this.getClass().getResourceAsStream(iconFile);
                    Streams.copy(icon, os, true);
                    os.flush();
                }
                long endTime = System.currentTimeMillis();
                //LOG.log(Level.INFO, "Downloaded file: propertyId=" + propertyId + " primaryKey=" + primaryKey + " maxSize=" + maxSize + " blobTime=" + (blobTime - startTime) + " endTime=" + (endTime - blobTime));
            } catch (Throwable th) {
                LOG.log(Level.SEVERE, "Error downloading file: propertyId=" + propertyId + " primaryKey=" + req.getParameter(BinaryFieldWidget.PRIMARY_KEY_ID) + " maxSize=" + req.getParameter(BinaryFieldWidget.MAXSIZE_ID), th);
                if (!resp.isCommitted()) {
                    if (th instanceof IllegalStateException) {
                        resp.sendError(HttpServletResponse.SC_NOT_FOUND, "File not found: " + propertyId + ":" + req.getParameter(BinaryFieldWidget.PRIMARY_KEY_ID));
                    } else if (th instanceof NumberFormatException) {
                        resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid argument: " + th.getMessage());
                    } else {
                        resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, th.getMessage());
                    }
                } else {
                    LOG.severe("Could not set the response error code, response already committed.");
                }
            } finally {
                if (blob != null) {
                    org.aplikator.utils.IOUtils.tryClose(blob.inputStream);
                }
            }
        }
    }

    private String mapFilenameToIcon(String filename) {
        if (filename != null) {
            filename = filename.toLowerCase().substring(filename.lastIndexOf(".") + 1);
            switch (filename) {
                case "ai":
                    return "/fileicons/AI.png";
                case "bmp":
                    return "/fileicons/BMP.png";
                case "css":
                    return "/fileicons/CSS.png";
                case "doc":
                    return "/fileicons/DOC.png";
                case "docx":
                    return "/fileicons/DOC.png";
                case "htm":
                    return "/fileicons/HTML.png";
                case "html":
                    return "/fileicons/HTML.png";
                case "jpg":
                    return "/fileicons/JPG.png";
                case "jpeg":
                    return "/fileicons/JPG.png";
                case "mp3":
                    return "/fileicons/MP3.png";
                case "mp4":
                    return "/fileicons/MP4.png";
                case "mov":
                    return "/fileicons/MOV.png";
                case "pdf":
                    return "/fileicons/PDF.png";
                case "php":
                    return "/fileicons/PHP.png";
                case "png":
                    return "/fileicons/PNG.png";
                case "psd":
                    return "/fileicons/PSD.png";
                case "qt":
                    return "/fileicons/MOV.png";
                case "xls":
                    return "/fileicons/XLS.png";
                case "xlsx":
                    return "/fileicons/XLS.png";
                case "wmv":
                    return "/fileicons/MP4.png";
                case "zip":
                    return "/fileicons/ZIP.png";
            }
        }
        return "/fileicons/genericFileIcon.png";
    }

    private void processTempFile(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        String tempFileId = req.getParameter(BinaryFieldWidget.TEMPFILE_ID);
        int maxSize = Integer.parseInt(req.getParameter(BinaryFieldWidget.MAXSIZE_ID));

        Tempstore ts = TempstoreFactory.getTempstore();
        InputStream inputStream = ts.load(tempFileId);
        try {
            byte[] resized = resize(maxSize, ts.getFilename(tempFileId), inputStream);
            resp.setContentType("image/jpeg");
            resp.setHeader("Cache-Control", "no-cache");
            resp.setHeader("Pragma", "no-cache");
            OutputStream os = resp.getOutputStream();

            Streams.copy(new ByteArrayInputStream(resized), os, true);
            os.flush();
        } finally {
            org.aplikator.utils.IOUtils.tryClose(inputStream);
        }

    }

    /*
    Modified logic from BinaryData.preprocess method, independent of Binary property
     */
    private byte[] resize(int thumbnailSize, String filename, InputStream inputStream) {
        if (ImageResizer.isResizeable(null, filename)) {
            BufferedImage imageIcon = null;
            try {
                BinaryData.SubsampledImageInfo sii = null;
                if (ImageResizer.isPdf(filename)) {
                    imageIcon = PDFLoader.load(inputStream);
                    sii = new BinaryData.SubsampledImageInfo(imageIcon, imageIcon.getWidth(), imageIcon.getHeight());
                } else {
                    try {
                        imageIcon = ImageIO.read(inputStream);
                        sii = new BinaryData.SubsampledImageInfo(imageIcon, imageIcon.getWidth(), imageIcon.getHeight());
                    } catch (Exception ex) {
                        LOG.log(Level.INFO, "Image resizing failed, trying subsampled. Reason:", ex);
                        inputStream.reset();
                        sii = readSubsampled(inputStream, thumbnailSize);
                        imageIcon = sii.image;
                    }
                }

                imageIcon = ImageResizer.removeAlphaChannel(imageIcon);
                return ImageResizer.resize(imageIcon, thumbnailSize);
            } catch (Exception e) {
                LOG.log(Level.SEVERE, "Error while resizing image " + filename + ":", e);
            } finally {
                if (imageIcon != null) {
                    imageIcon.flush();
                }
            }
        }
        return new byte[0];
    }

}
