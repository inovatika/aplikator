/*
 * Copyright 2013 Pavel Stastny <pavel.stastny at gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aplikator.server.descriptor;

import java.io.File;

import org.junit.Test;

/**
 * @author Pavel Stastny <pavel.stastny at gmail.com>
 */

public class FunctionTest {

    /*
    public static class _MTestEntity extends Entity {

        public _MTestEntity(String name, String tableName, String primaryKeyName) {
            super(name, tableName, primaryKeyName);
        }
        Function function = new Function("identifikator", "jmeno", new Executable() {
            @Override
            public FunctionResult execute(FunctionParameters parameters, Context context) {
                // EXECUTING
                return null;
            }

        });

        {
            function.registerWizard("first", createWizardView());
        }

        public Wizard createWizardView() {
            Wizard vw = new Wizard(this);
            Property<String> vstupniHodnota = this.stringProperty("Vstupni parametr");
            vw.addProperty(vstupniHodnota);
            vw.form(column(
                    row(vstupniHodnota)));
            return vw;
        }
    }
    */
    @Test
    public void testFunction() {
        /*
        FunctionTest._MTestEntity ent = new  FunctionTest._MTestEntity(UUID.randomUUID().toString(), UUID.randomUUID().toString(), UUID.randomUUID().toString());
        FunctionDTO functionDTO = ent.function.getFunctionDTO(null);
        Assert.assertNotNull(functionDTO);
        Assert.assertNotNull(functionDTO.getRegisteredWizardDTO("first"));
        WizardDTO viewDTO = functionDTO.getRegisteredWizardDTO("first");
        FormDTO fdto = viewDTO.getFormDescriptor();
        Assert.assertNotNull(fdto);
        Assert.assertNotNull(fdto.getLayout());
        */
    }
}
