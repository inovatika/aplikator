package org.aplikator.client.local.widgets;

import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.Composite;
import org.aplikator.client.shared.data.RecordDTO;
import org.aplikator.client.shared.descriptor.Access;
import org.aplikator.client.shared.descriptor.AnnotationKeys;
import org.aplikator.client.shared.descriptor.PropertyDTO;
import org.gwtbootstrap3.client.ui.Column;
import org.gwtbootstrap3.client.ui.FormLabel;
import org.gwtbootstrap3.client.ui.constants.ColumnSize;
import org.gwtbootstrap3.client.ui.html.Div;

import java.io.Serializable;
import java.util.logging.Logger;

public abstract class DataFieldBase<I extends Serializable> extends Composite implements DataField<I> {

    private static Logger LOG = Logger.getLogger(DataFieldBase.class.getName());

    protected PropertyDTO property;
    protected FormLabel label;
    //private FormGroup wrapper;
    protected Div controlHolder;

    protected boolean dirty;
    protected boolean defaultEnabled;
    protected final boolean horizontal;

    public DataFieldBase(String label, String tooltip, PropertyDTO property, boolean horizontal, String elementId, String customLabelStyle, HasFields form) {
        //wrapper = new FormGroup();

        this.horizontal = horizontal;

        controlHolder = new Column(ColumnSize.XS_1);

        if (elementId != null && form != null) {
            controlHolder.getElement().setId(form.getPrefix()+"-"+elementId);
        }
        //wrapper.addStyleName("app-clean-pd-right");
        //wrapper.addStyleName("app-clean-pd-left");

        if (label != null) {
            this.label = new FormLabel();
            this.label.addStyleName("fieldLabel");
            if (customLabelStyle != null){
                this.label.addStyleName(customLabelStyle);
            }
            if (property.isRequired()) {
                this.label.setText(label + " \u2757");
            } else {
                this.label.setText(label);
            }
        }
        //wrapper.add(controlHolder);
        initWidget(controlHolder);
        if (label != null && !"".equals(label) && !"null".equals(label)) {
            controlHolder.add(this.label);
        }
        if (tooltip != null && !tooltip.isEmpty() && !"null".equals(tooltip)) {
            controlHolder.getElement().setAttribute("title", tooltip);
        }

        this.property = property;
    }

    public String getElementId() {
        return controlHolder.getElement().getId();
    }



    public boolean isHorizontal() {
        return horizontal;
    }

    public PropertyDTO getProperty() {
        return property;
    }


    public void setDirty(boolean dirty) {
        this.dirty = dirty;
        if (dirty) {
            //wrapper.setValidationState(ValidationState.SUCCESS);//TODO resolve dirty styling
        } else {
            //wrapper.setValidationState(ValidationState.NONE);
        }

    }


    public boolean isDirty() {
        return dirty;
    }

    protected void setGridSize(int size) {
        if (controlHolder instanceof Column) {
            ((Column) controlHolder).setSize(LayoutUtils.size(size));
        }
        //wrapper.addStyleName(LayoutUtils.size(size).getCssName());
    }


    public void grabFocus() {
    }


    public void addEnterHandler(Command command) {
    }


    public void setDefaultEnabled(boolean enabled) {
        defaultEnabled = enabled;
    }


    public boolean isDefaultEnabled() {
        return defaultEnabled;
    }

    public void processAnnotations(RecordDTO sourceRecord) {
        String recordAccessStr = sourceRecord.getAnnotation(AnnotationKeys.ACCESS_ANNOTATION_KEY);
        String fieldAccessStr = sourceRecord.getPropertyAnnotation(this.getProperty().getId(), AnnotationKeys.ACCESS_ANNOTATION_KEY);
        if (fieldAccessStr != null) {
            Access fieldAccess = Access.valueOf(fieldAccessStr);
            this.setEnabled(!fieldAccess.isReadOnly());
        } else if (recordAccessStr != null) {
            Access recordAccess = Access.valueOf(recordAccessStr);
            this.setEnabled(!recordAccess.isReadOnly());
        } else {
            this.setEnabled(this.isDefaultEnabled());
        }

        String annotationStyleStr = sourceRecord.getPropertyAnnotation(this.getProperty().getId(), AnnotationKeys.STYLE_ANNOTATION_KEY);
        if (annotationStyleStr != null) {
            controlHolder.getElement().setAttribute(AnnotationKeys.STYLE_CSS_ATTRIBUTE, annotationStyleStr);
        } else {
            controlHolder.getElement().removeAttribute(AnnotationKeys.STYLE_CSS_ATTRIBUTE);
        }
    }
}
