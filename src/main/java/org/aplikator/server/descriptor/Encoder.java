package org.aplikator.server.descriptor;

import java.io.Serializable;

public interface Encoder<T extends Serializable> {
    T encode(String plainValue);
}
