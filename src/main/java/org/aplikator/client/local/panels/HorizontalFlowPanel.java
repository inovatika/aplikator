package org.aplikator.client.local.panels;

import com.google.gwt.dom.client.Style;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

public class HorizontalFlowPanel extends HorizontalPanel {
    private Label strut = null;

    public HorizontalFlowPanel() {
        super();
        getElement().getStyle().setBorderStyle(Style.BorderStyle.SOLID);
        // addCleaner();
        // setWidth("100%");
    }

    @Override
    public void add(Widget widget) {
        /*
         * widget.getElement().getStyle().setProperty("verticalAlign",
         * "middle"); SimplePanel holder = new SimplePanel();
         * holder.getElement().getStyle().setProperty("cssFloat", "left");
         * holder.setWidget(widget); insert(holder,getWidgetCount()-1);
         */
        if (strut == null) {
            super.add(widget);
        } else {
            insert(widget, getWidgetIndex(strut));
        }
    }

    public void addRight(Widget widget) {
        /*
         * widget.getElement().getStyle().setProperty("verticalAlign",
         * "middle"); SimplePanel holder = new SimplePanel();
         * holder.getElement().getStyle().setProperty("cssFloat", "right");
         * holder.setWidget(widget); insert(holder, getWidgetCount()-1);
         */
        if (strut == null) {
            strut = new Label("");
            super.add(strut);
            setCellWidth(strut, "100%");
        }
        insert(widget, getWidgetIndex(strut) + 1);
    }

    /*
     * private void addCleaner(){ HTML cleaner = new HTML("<hr>");
     * cleaner.getElement().getStyle().setProperty("clear", "both");
     * cleaner.getElement().getStyle().setProperty("visibility", "hidden");
     * cleaner.setHeight("0px"); super.add(cleaner); }
     */

}
