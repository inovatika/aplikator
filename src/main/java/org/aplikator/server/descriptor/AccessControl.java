package org.aplikator.server.descriptor;

import org.aplikator.client.shared.data.PrimaryKey;
import org.aplikator.client.shared.descriptor.Access;
import org.aplikator.server.data.Context;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.SetMultimap;

/**
 *
 */
public interface AccessControl {

    Access getAccess(Context ctx);

    Access getRecordAccess(Context ctx, PrimaryKey primaryKey, int maxSize);

    public static class Default implements AccessControl {
        private SetMultimap<Access, String> mappings = HashMultimap.create();
        private Access defaultAccess = Access.READ_WRITE_CREATE_DELETE;
        private Access guestAccess = Access.NONE;

        public static Default authenticatedFullAccess() {
            return new Default(Access.READ_WRITE_CREATE_DELETE);
        }

        public static Default authenticated(Access defaultAccess) {
            return new Default(defaultAccess);
        }

        private Default(Access access) {
            defaultAccess = access;
        }

        public Default guest(Access guestAccess) {
            this.guestAccess = guestAccess;
            return this;
        }

        public Default role(String role, Access access) {
            mappings.put(access, role);
            return this;
        }

        @Override
        public Access getAccess(Context ctx) {
            if (checkAccess(Access.READ_WRITE_CREATE_DELETE, ctx)) {
                return Access.READ_WRITE_CREATE_DELETE;
            }
            if (checkAccess(Access.READ_WRITE_CREATE, ctx)) {
                return Access.READ_WRITE_CREATE;
            }
            if (checkAccess(Access.READ_WRITE, ctx)) {
                return Access.READ_WRITE;
            }
            if (checkAccess(Access.READ, ctx)) {
                return Access.READ;
            }
            if (checkAccess(Access.READ_PREVIEW, ctx)) {
                return Access.READ_PREVIEW;
            }
            if (checkAccess(Access.READ_THUMBNAIL, ctx)) {
                return Access.READ_THUMBNAIL;
            }
            return guestAccess;
        }

        @Override
        public Access getRecordAccess(Context ctx, PrimaryKey primaryKey, int maxSize){
            return getAccess(ctx);
        }

        private boolean checkAccess(Access access, Context ctx) {
            if (ctx.isAuthenticated()) {
                if (access == defaultAccess) {
                    return true;
                }
                for (String role : mappings.get(access)) {
                    if (ctx.isUserInRole(role)) {
                        return true;
                    }
                }
                return false;
            } else {
                return (access == guestAccess);
            }
        }
    }
}
