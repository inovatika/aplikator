package org.aplikator.server.data;


import org.aplikator.client.shared.data.Operation;
import org.aplikator.client.shared.data.PrimaryKey;
import org.aplikator.server.Configurator;
import org.aplikator.server.DescriptorRegistry;
import org.aplikator.server.descriptor.Collection;
import org.aplikator.server.descriptor.Entity;
import org.aplikator.server.descriptor.View;
import org.aplikator.server.persistence.Transaction;
import org.aplikator.server.security.Account;
import org.aplikator.server.security.SecurityModule;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.util.*;
import java.util.function.Predicate;

public class Context implements AutoCloseable {

    private final HttpServletRequest httpServletRequest;
    private final HttpServletResponse httpServletResponse;
    private final AplikatorServiceBackend aplikatorServiceBackend;
    private RecordContainer recordContainer;
    private Transaction transaction;

    private Locale locale;
    private String login;


    public Context(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AplikatorServiceBackend aplikatorServiceBackend, RecordContainer rc) {
        super();
        this.httpServletRequest = httpServletRequest;
        this.httpServletResponse = httpServletResponse;
        this.aplikatorServiceBackend = aplikatorServiceBackend;
        transaction = aplikatorServiceBackend.persister.beginTransaction();
        this.recordContainer = rc;
        //System.out.println("CONTEXT:"+httpServletRequest + " LENGTH:"+httpServletRequest.getContentLength());
        //System.out.println("BODY:"+extractPostRequestBody(httpServletRequest));
    }

    Context asyncClone(){
        Context asyncClone = new Context(null, null, new AplikatorServiceBackend(), recordContainer);
        asyncClone.locale = getUserLocale();
        asyncClone.login = getUserLogin();
        return asyncClone;
    }

    static String extractPostRequestBody(HttpServletRequest request) {
        if ("POST".equalsIgnoreCase(request.getMethod())) {
            Scanner s = null;
            try {
                s = new Scanner(request.getInputStream(), "UTF-8").useDelimiter("\\A");
            } catch (IOException e) {
                e.printStackTrace();
            }
            return s.hasNext() ? s.next() : "";
        }
        return "";
    }


    public HttpServletRequest getHttpServletRequest() {
        return this.httpServletRequest;
    }

    public HttpServletResponse getHttpServletResponse() {
        return this.httpServletResponse;
    }

    public Object getSessionAttribute(String attr){
        if (getHttpServletRequest() != null && getHttpServletRequest().getSession() != null) {
            return getHttpServletRequest().getSession().getAttribute(attr);
        }
        return null;
    }

    public void setSessionAttribute(String attr, Object value){
        if (getHttpServletRequest() != null && getHttpServletRequest().getSession() != null) {
            getHttpServletRequest().getSession().setAttribute(attr, value);
        }
    }

    public Locale getUserLocale() {
        if (this.httpServletRequest == null){
            if (locale != null){
                return locale;
            }else {
                return Locale.forLanguageTag(Configurator.get().getConfig().getString(Configurator.DEFAULT_LOCALE));
            }
        } else {
            return this.httpServletRequest.getLocale();
        }
    }

    AplikatorServiceBackend getAplikatorServiceBackend() {
        return aplikatorServiceBackend;
    }

    public RecordContainer getRecordContainer() {
        if (recordContainer == null) {
            recordContainer = new RecordContainer();
        }
        return recordContainer;
    }

    public void clearRecordContainer() {
        getRecordContainer().clear();
    }

    public void addNewRecordToContainer(Record record) {
        View view = ((Entity) DescriptorRegistry.get().getDescriptionItem(record.getPrimaryKey().getEntityId())).view();
        getRecordContainer().addRecord(view, null, record, Operation.CREATE);
    }

    public void addUpdatedRecordToContainer(Record originalRecord, Record updatedRecord) {
        View view = ((Entity) DescriptorRegistry.get().getDescriptionItem(originalRecord.getPrimaryKey().getEntityId())).view();
        getRecordContainer().addRecord(view, originalRecord, updatedRecord, Operation.UPDATE);
    }

    public void addDeletedRecordToContainer(Record record) {
        Entity entity = (Entity) DescriptorRegistry.get().getDescriptionItem(record.getPrimaryKey().getEntityId());
       /* for (Property<? extends Serializable> property : entity.getProperties()) {
            if (property instanceof Collection){
                List<Record> nestedRecords = record.getCollectionRecords((Collection) property, this);
                for (Record nestedRecord : nestedRecords) {
                    this.addDeletedRecordToContainer(nestedRecord);
                }
            }
        }*/
        View view = entity.view();
        getRecordContainer().addRecord(view, record, record, Operation.DELETE);
    }

    public void prepareDuplicateCollection(ContainerNode node, Collection collection, View collectionView) {
        for (Record record : node.getOriginal().getCollectionRecords(collection, collectionView, this)) {
            Record duplicatedRecord = record.clone();
            duplicatedRecord.setPrimaryKey(new PrimaryKey(record.getPrimaryKey().getEntityId(), UUID.randomUUID().toString()));
            duplicatedRecord.setOwnerPrimaryKey(node.getEdited().getPrimaryKey());
            duplicatedRecord.setOwnerProperty(collection);
            this.getRecordContainer().addRecord(node.getView(), null, duplicatedRecord, Operation.PREPARE);
        }
    }

    public void processRecordContainer() {
        recordContainer = getAplikatorServiceBackend().processRecords(getRecordContainer(), this, true);
    }

    public RecordContainer processRecordContainer(RecordContainer rc) {
        return getAplikatorServiceBackend().processRecords(rc, this, true);
    }

    public RecordContainer processRecordContainer(RecordContainer rc, boolean runTriggers) {
        return getAplikatorServiceBackend().processRecords(rc, this, runTriggers);
    }

    public QueryBuilder getRecords(View view) {
        return new QueryBuilder(this, view);
    }

    public QueryBuilder getRecords(Entity entity) {
        return new QueryBuilder(this, entity.view());
    }

    public Record getRecord(PrimaryKey primaryKey, View view, boolean limitToView, boolean ignoreRecordContainer) {
        Record edited = null;
        if (! ignoreRecordContainer) {
            edited = getRecordContainer().findLastUpdated(primaryKey);
        }
        if (edited != null) {
            return edited;
        } else if (primaryKey.getId()>=0){
            return getAplikatorServiceBackend().getRecord(primaryKey, view, this, limitToView);
        }
        return null;
    }

    public Record getRecord(PrimaryKey primaryKey, View view, boolean limitToView){
        return getRecord(primaryKey, view, limitToView, false);
    }

    public Record getRecord(PrimaryKey primaryKey, View view) {
        return getRecord(primaryKey, view, false, false);
    }

    public Transaction getTransaction() {
        return transaction;
    }

    @Override
    public void close() throws IOException {
        aplikatorServiceBackend.persister.close(transaction);
    }

    public boolean isUserInRole(String role) {
        if (httpServletRequest == null){
            return true;
        } else {
            return SecurityModule.get().isUserInRole(role, getHttpServletRequest());
        }
    }

    public boolean isAuthenticated() {
        if (httpServletRequest == null){
            return true;
        } else {
            return SecurityModule.get().isAuthenticated(getHttpServletRequest());
        }
    }

    public String getUserLogin() {
        if (httpServletRequest == null){
            return login;
        } else {
            return SecurityModule.get().getUserLogin(getHttpServletRequest());
        }
    }

    public Connection getJDBCConnection() {
        return getAplikatorServiceBackend().persister.getJDBCConnection();
    }

    public Record getCollectionRepresentationRecord(Record ownerRecord, Collection<?> collectionProperty, Predicate<Record> mainflagPredicate) {
        Set<Record> editedCollectionRecords = new HashSet<>();
        for (ContainerNode containerNode : this.getRecordContainer().getRecords()) {
            if (containerNode.getEdited().getOwnerPrimaryKey() != null && containerNode.getEdited().getOwnerPrimaryKey().equals(ownerRecord.getPrimaryKey())
                    && collectionProperty.getId().equals(containerNode.getEdited().getOwnerPropertyId())) {
                editedCollectionRecords.add(containerNode.getMerged());
            }
        }
        List<Record> existingCollectionRecords = ownerRecord.getCollectionRecords(collectionProperty, this);
        editedCollectionRecords.addAll(existingCollectionRecords);
        Record mainCollectionRecord = null;
        for (Record editedCollectionRecord : editedCollectionRecords) {
            if (mainCollectionRecord == null) {
                mainCollectionRecord = editedCollectionRecord;
            }
            if (mainflagPredicate.test(editedCollectionRecord)) {
                mainCollectionRecord = editedCollectionRecord;
                break;
            }
        }
        return mainCollectionRecord;
    }

    public Account getUserAccount(){
        return SecurityModule.get().getAccount(getUserLogin(), httpServletRequest);
    }

}
