package org.aplikator.utils;


import org.apache.commons.io.IOUtils;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.*;

public class MultiReadHttpServletRequest extends HttpServletRequestWrapper {

    private byte[] body;

    public MultiReadHttpServletRequest(HttpServletRequest httpServletRequest) {
        super(httpServletRequest);
        if (!httpServletRequest.getRequestURI().endsWith("/upload")) {
            // Read the request body and save it as a byte array
            try {
                InputStream is = super.getInputStream();
                body = IOUtils.toByteArray(is);
            } catch (IOException ioe) {
                throw new RuntimeException(ioe);
            }
        }
    }

    @Override
    public ServletInputStream getInputStream() throws IOException {
        if (body != null) {
            return new ServletInputStreamImpl(new ByteArrayInputStream(body));
        }else{
            return super.getInputStream();
        }
    }

    @Override
    public BufferedReader getReader() throws IOException {
        String enc = getCharacterEncoding();
        if (enc == null)
            enc = "UTF-8";
        return new BufferedReader(new InputStreamReader(getInputStream(), enc));
    }

    private class ServletInputStreamImpl extends ServletInputStream {

        private InputStream is;

        public ServletInputStreamImpl(InputStream is) {
            this.is = is;
        }

        public int read() throws IOException {
            return is.read();
        }

        public boolean markSupported() {
            return false;
        }

        public synchronized void mark(int i) {
            throw new RuntimeException(new IOException("mark/reset not supported"));
        }

        public synchronized void reset() throws IOException {
            throw new IOException("mark/reset not supported");
        }

        //@Override
        public boolean isFinished() {
            try {
                return is.available() == 0;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        //@Override
        public boolean isReady() {
            return true;
        }

        //@Override
        public void setReadListener(ReadListener listener) {
            throw new RuntimeException("Not implemented");
        }
    }

}