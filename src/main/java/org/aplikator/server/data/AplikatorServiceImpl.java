package org.aplikator.server.data;

import org.aplikator.client.shared.data.*;
import org.aplikator.client.shared.descriptor.*;
import org.aplikator.client.shared.rpc.AplikatorService;
import org.aplikator.server.Configurator;
import org.aplikator.server.DescriptorRegistry;
import org.aplikator.server.ListRegistry;
import org.aplikator.server.descriptor.*;
import org.aplikator.server.persistence.Persister;
import org.aplikator.server.persistence.PersisterFactory;
import org.aplikator.server.persistence.search.SearchFactory;
import org.aplikator.server.processes.CannotCallStopException;
import org.aplikator.server.processes.Process;
import org.aplikator.server.processes.ProcessManager;
import org.aplikator.server.query.QueryExpression;
import org.jboss.errai.bus.server.annotations.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The server side implementation of the RPC service.
 */
@Service
public class AplikatorServiceImpl implements AplikatorService {
    private static final Logger LOG = Logger.getLogger(AplikatorServiceImpl.class.getName());
    @javax.ws.rs.core.Context
    HttpServletRequest httpServletRequest;
    @javax.ws.rs.core.Context
    HttpServletResponse httpServletResponse;
    private AplikatorServiceBackend aplikatorServiceBackend = new AplikatorServiceBackend();
    private Persister persister = PersisterFactory.getPersister();


    private Context ctx(RecordContainer rc) {
        return new Context(httpServletRequest, httpServletResponse, aplikatorServiceBackend, rc);
    }


    private Context ctx() {
        return new Context(httpServletRequest, httpServletResponse, aplikatorServiceBackend, null);
    }


    @Override
    public ApplicationDTO getApplication() {
        try (Context ctx = ctx()) {
            int timeout = Configurator.get().getConfig().getInt("aplikator.sessiontimeout");
            ctx.getHttpServletRequest().getSession().setMaxInactiveInterval(timeout * 60);
            LOG.info("Session timeout set: " + timeout + " minutes");
            return Application.get().getApplicationDTO(ctx);
        } catch (Throwable t) {
            LOG.log(Level.SEVERE, "Error in getApplication: " + t.getMessage(), t);
            throw new RuntimeException("Error in getApplication: " + t.getMessage(), t);
        }
    }

    @Override
    public List<ListItem> getList(String listName) {
        try (Context ctx = ctx()) {
            ListProvider listProvider = ListRegistry.get().getListProvider(listName);
            if (listProvider != null) {
                return listProvider.getListValues(ctx);
            }
            return null;
        } catch (Throwable t) {
            LOG.log(Level.SEVERE, "Error in getList: " + t.getMessage(), t);
            throw new RuntimeException("Error in getList: " + t.getMessage(), t);
        }
    }

    @Override
    public ViewDTO getView(String id) {
        try (Context ctx = ctx()) {
            View view = (View) DescriptorRegistry.get().getDescriptionItem(id);
            return view.getViewDTO(ctx);
        } catch (Throwable t) {
            LOG.log(Level.SEVERE, "Error in getView: " + t.getMessage(), t);
            throw new RuntimeException("Error in getView: " + t.getMessage(), t);
        }
    }


    @Override
    public RecordContainerDTO processRecords(RecordContainerDTO recordContainerDTO) {
        RecordContainer recordContainer = new RecordContainer(recordContainerDTO);
        try (Context ctx = ctx(recordContainer)) {
            return aplikatorServiceBackend.processRecords(recordContainer, ctx, true).getRecordContainerDTO();
        } catch (Throwable t) {
            if (FunctionResultStatus.ERROR.equals(recordContainer.getFunctionResult().getStatus())) {
                return recordContainer.getRecordContainerDTO();
            }
            String errorID = UUID.randomUUID().toString();
            LOG.log(Level.SEVERE, "Error in processRecords (ERROR_ID: " + errorID + "):" + t.getMessage(), t);
            FunctionResult result = recordContainerDTO.getFunctionResult();
            if (result == null) {
                result = new FunctionResult("aplikator.table.saveerror", FunctionResultStatus.ERROR);
                result.setDetail(t.getMessage() + "\nError ID: " + errorID);
            } else if (!result.getStatus().equals(FunctionResultStatus.ERROR)) {
                result.setStatus(FunctionResultStatus.ERROR);
                result.setMessage("aplikator.table.saveerror");
                result.addDetail(t.getMessage() + "\nError ID: " + errorID);
            }
            recordContainerDTO.setFunctionResult(result);
            return recordContainerDTO;
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public SuggestionsDTO getSuggestions(String viewid, String activeSort, String searchString) {
        try (Context ctx = ctx()) {
            View view = (View) DescriptorRegistry.get().getDescriptionItem(viewid);
            SortDescriptor activeSortDescriptor = view.getSortDescriptor(activeSort);
            Property primarySortProperty = activeSortDescriptor.getPrimarySortProperty();
            SortItem[] sortItems = activeSortDescriptor.getItems().toArray(new SortItem[activeSortDescriptor.getItems().size()]);
            List<Record> records = persister.getRecords(view, null, sortItems, null, null, searchString, null, null, 0, view.getPageSize(), ctx);
            List<ItemSuggestion> suggestions = new ArrayList<ItemSuggestion>(records.size());
            for (Record rec : records) {
                int pk = rec.getPrimaryKey().getId();
                Object val = rec.getValue(primarySortProperty);
                String repl = val != null ? val.toString() : "";
                suggestions.add(new ItemSuggestion(rec.getPreview(), pk, repl, rec.getRecordDTO()));
            }
            SuggestionsDTO sug = new SuggestionsDTO();
            sug.setSuggestions(suggestions);
            return sug;
        } catch (Throwable t) {
            LOG.log(Level.SEVERE, "Error in getSuggestions: " + t.getMessage(), t);
            throw new RuntimeException("Error in getSuggestions: " + t.getMessage(), t);
        }
    }

    @Override
    public RecordDTO getRecord(String primaryKey, String viewId) {
        try (Context ctx = ctx()) {
            View view = (View) DescriptorRegistry.get().getDescriptionItem(viewId);
            return aplikatorServiceBackend.getRecord(new PrimaryKey(primaryKey), view, ctx, false).getRecordDTO();
        } catch (Throwable t) {
            LOG.log(Level.SEVERE, "Error in getRecord: " + t.getMessage(), t);
            throw new RuntimeException("Error in getRecord: " + t.getMessage(), t);
        }
    }

    @Override
    public RecordsPageDTO getRecords(String viewId, String activeFilter, String activeSort, QueryDescriptorDTO queryDescriptor, String searchString, String ownerPropertyId, String ownerPrimaryKey, int pageOffset, int pageSize) {
        try (Context ctx = ctx()) {
            RecordsPageDTO page = new RecordsPageDTO();
            Collection ownerProperty = null;
            if (ownerPropertyId != null && !"".equals(ownerPropertyId)) {
                ownerProperty = (Collection) DescriptorRegistry.get().getDescriptionItem(ownerPropertyId);
            }
            View view = (View) DescriptorRegistry.get().getDescriptionItem(viewId);
            List<QueryParameter> queryParameters = new ArrayList<QueryParameter>(queryDescriptor.getQueryParameters().size());
            for (QueryParameterDTO queryParameterDTO : queryDescriptor.getQueryParameters()) {
                queryParameters.add(new QueryParameter(queryParameterDTO));
            }
            QueryExpression queryExpression = view.getQueryDescriptor(activeFilter, ctx).getQueryExpression(queryParameters, ctx);
            SortItem[] sortItems = null;
            if (activeSort != null && !activeSort.equals("")) {
                SortDescriptor sortDescriptor = view.getSortDescriptor(activeSort);
                sortItems = sortDescriptor.getItems().toArray(new SortItem[sortDescriptor.getItems().size()]);
            }
            List<Record> loadedRecords = persister.getRecords(view, queryExpression, sortItems, null, null, searchString, ownerProperty, (ownerPrimaryKey == null || ownerPrimaryKey.trim().length() == 0) ? null : new PrimaryKey(ownerPrimaryKey), pageOffset, pageSize, ctx);
            List<RecordDTO> dtos = new ArrayList<RecordDTO>(loadedRecords.size());
            for (Record loadedRecord : loadedRecords) {
                dtos.add(loadedRecord.getRecordDTO());
            }
            page.setRecords(dtos);
            page.setOffset(pageOffset);
            return page;
        } catch (Throwable t) {
            LOG.log(Level.SEVERE, "Error in getPage: " + t.getMessage(), t);
            throw new RuntimeException("Error in getPage: " + t.getMessage(), t);
        }
    }

    @Override
    public SearchResult getPageSearch(String viewId, String searchArgument, int pageOffset, int pageSize) {
        try (Context ctx = ctx()) {
            return SearchFactory.get().getPagefromSearch(viewId, searchArgument, pageOffset, pageSize, ctx);
        } catch (Throwable t) {
            LOG.log(Level.SEVERE, "Error in getPageSearch: " + t.getMessage(), t);
            throw new RuntimeException("Error in getPageSearch: " + t.getMessage(), t);
        }
    }

    @Override
    public int getRecordCount(String viewId, String activeFilter, String activeSort, QueryDescriptorDTO queryDescriptor, String searchString, String ownerPropertyId, String ownerPrimaryKeyString) {
        try (Context ctx = ctx()) {
            Collection ownerProperty = null;
            PrimaryKey ownerPrimaryKey = null;
            if (ownerPropertyId != null && !"".equals(ownerPropertyId)) {
                ownerProperty = (Collection) DescriptorRegistry.get().getDescriptionItem(ownerPropertyId);
                ownerPrimaryKey = (ownerPrimaryKeyString == null || ownerPrimaryKeyString.trim().length() == 0) ? null : new PrimaryKey(ownerPrimaryKeyString);
            }
            View view = (View) DescriptorRegistry.get().getDescriptionItem(viewId);
            List<QueryParameter> queryParameters = new ArrayList<QueryParameter>(queryDescriptor.getQueryParameters().size());
            for (QueryParameterDTO queryParameterDTO : queryDescriptor.getQueryParameters()) {
                queryParameters.add(new QueryParameter(queryParameterDTO));
            }
            QueryExpression queryExpression = view.getQueryDescriptor(activeFilter, ctx).getQueryExpression(queryParameters, ctx);
            SortItem[] sortItems = null;
            if (activeSort != null && !activeSort.equals("")) {
                SortDescriptor sortDescriptor = view.getSortDescriptor(activeSort);
                sortItems = sortDescriptor.getItems().toArray(new SortItem[sortDescriptor.getItems().size()]);
            }
            return aplikatorServiceBackend.getRecordCount(view, queryExpression, sortItems, null, null, searchString, ownerProperty, ownerPrimaryKey, ctx);
        } catch (Throwable t) {
            LOG.log(Level.SEVERE, "Error in getRecordCount: " + t.getMessage(), t);
            throw new RuntimeException("Error in getRecordCount: " + t.getMessage(), t);
        }
    }

    @Override
    public FunctionResult runFunction(FunctionParameters functionParameters) {
        aplikatorServiceBackend.checkSuspendedState();
        try (Context ctx = ctx()) {
            Function func = (Function) DescriptorRegistry.get().getDescriptionItem(functionParameters.getFunctionId());
            LOG.info("Running function: " + functionParameters.getFunctionId() + ", user: " + ctx.getUserLogin());
            if (func.isAsync()) {
                final Context context = ctx.asyncClone();
                LOG.info("Function started asynchronously in the background.");
                CompletableFuture<FunctionResult> future = CompletableFuture.supplyAsync(() -> {
                    return func.getExecutable().execute(new Record(functionParameters.getClientContext().getCurrentRecord()),
                            new Record(functionParameters.getClientParameters()), functionParameters.getClientContext(), context);
                });
                future.whenComplete((result, exception) -> {
                    try {
                        context.close();
                    } catch (Exception e) {
                        LOG.log(Level.SEVERE, "Error closing async context", e);
                    }
                    if (exception != null) {
                        LOG.log(Level.SEVERE, "Async function call for " + functionParameters.getFunctionId() + " failed.", exception);
                    } else {
                        if (FunctionResultStatus.SUCCESS.equals(result.getStatus())) {
                            LOG.info("Async function call for " + functionParameters.getFunctionId() + " completed successfully with result: " + result);
                        } else if (FunctionResultStatus.WARNING.equals(result.getStatus())) {
                            LOG.warning("Async function call for " + functionParameters.getFunctionId() + " completed with warning: " + result);
                        } else {
                            LOG.severe("Async function call for " + functionParameters.getFunctionId() + " completed with error: " + result);
                        }
                    }
                });
                return new FunctionResult(Configurator.get().getLocalizedString("aplikator.function.async", ctx.getUserLocale()), FunctionResultStatus.SUCCESS);
            } else {
                FunctionResult result = func.getExecutable().execute(new Record(functionParameters.getClientContext().getCurrentRecord()),
                        new Record(functionParameters.getClientParameters()), functionParameters.getClientContext(), ctx);
                LOG.info("Function call for " + functionParameters.getFunctionId() + " completed successfully with result: " + result);
                return result;
            }
        } catch (Throwable t) {
            LOG.log(Level.SEVERE, "Error in runFunction: " + t.getMessage(), t);
            throw new RuntimeException("Error in runFunction: " + t.getMessage(), t);
        }
    }

    @Override
    public FunctionDTO getFunction(String id) {
        try (Context ctx = ctx()) {
            Function function = (Function) DescriptorRegistry.get().getDescriptionItem(id);
            return function.getFunctionDTO(ctx);
        } catch (Throwable t) {
            LOG.log(Level.SEVERE, "Error in getFunction: " + t.getMessage(), t);
            throw new RuntimeException("Error in getFunction: " + t.getMessage(), t);
        }
    }

    @Override
    public WizardPageDTO getWizardPage(String curPage, boolean forwardFlag, FunctionParameters functionParameters) {
        aplikatorServiceBackend.checkSuspendedState();
        try (Context ctx = ctx()) {
            Function func = (Function) DescriptorRegistry.get().getDescriptionItem(functionParameters.getFunctionId());
            Record record = new Record(functionParameters.getClientContext().getCurrentRecord());
            Record clientRecordProperties = new Record(functionParameters.getClientParameters());
            WizardPage wizPage = func.getExecutable().getWizardPage(curPage, forwardFlag, record, clientRecordProperties, functionParameters.getClientContext(), ctx);
            return wizPage == null ? null : wizPage.getViewDTO(ctx);
        } catch (Throwable t) {
            LOG.log(Level.SEVERE, "Error in getWizardPage: " + t.getMessage(), t);
            throw new RuntimeException("Error in getWizardPage: " + t.getMessage(), t);
        }
    }


    @Override
    public RecordDTO getCompleteRecord(String primaryKey, int traverseLevel, boolean includeCollections) {
        try (Context ctx = ctx()) {
            RecordDTO completeRecord = aplikatorServiceBackend.getCompleteRecord(primaryKey, traverseLevel, includeCollections, ctx);
            return completeRecord;
        } catch (Throwable t) {
            LOG.log(Level.SEVERE, "Error in getCompleteRecord: " + t.getMessage(), t);
            throw new RuntimeException("Error in getCompleteRecord: " + t.getMessage(), t);
        }
    }


    @Override
    public RecordContainerDTO prepareRecord(String primaryKey, String viewId, String ownerPropertyId, String ownerPrimaryKey) {
        try (Context ctx = ctx()) {
            PrimaryKey pk = null;
            if (primaryKey != null && !"".equals(primaryKey)) {
                pk = new PrimaryKey(primaryKey);
            }
            View view = (View) DescriptorRegistry.get().getDescriptionItem(viewId);
            Collection ownerProperty = null;
            PrimaryKey ownerPK = null;
            if (ownerPropertyId != null && !"".equals(ownerPropertyId)) {
                ownerProperty = (Collection) DescriptorRegistry.get().getDescriptionItem(ownerPropertyId);
                ownerPK = new PrimaryKey(ownerPrimaryKey);
            }
            RecordContainer rc = aplikatorServiceBackend.prepareRecord(pk, view, ownerProperty, ownerPK, ctx);
            return rc.getRecordContainerDTO();
        } catch (Throwable t) {
            RecordContainerDTO recordContainerDTO = new RecordContainerDTO();
            String errorID = UUID.randomUUID().toString();
            LOG.log(Level.SEVERE, "Error in prepareRecord (" + errorID + "):" + t.getMessage(), t);
            FunctionResult result = new FunctionResult("aplikator.table.saveerror", FunctionResultStatus.ERROR);
            result.setDetail(t.getMessage() + "\nErrorID: " + errorID);
            recordContainerDTO.setFunctionResult(result);
            return recordContainerDTO;
        }
    }

    @Override
    public List<ProcessDTO> getProcesses() {
        try {
            ProcessManager pm = ProcessManager.getManager();
            List<Process> prs = pm.getProcesses();
            List<ProcessDTO> pdtos = new ArrayList<ProcessDTO>();
            for (Process ps : prs) {
                pdtos.add(ps.toProcessDTO());
            }
            return pdtos;
        } catch (Throwable t) {
            LOG.log(Level.SEVERE, "Error in getProcesses: " + t.getMessage(), t);
            throw new RuntimeException("Error in getProcesses: " + t.getMessage(), t);
        }
    }

    @Override
    public ProcessDTO getProcess(String procId) {
        try {
            ProcessManager pm = ProcessManager.getManager();
            Process p = pm.lookUpProcess(procId);
            return p != null ? p.toProcessDTO() : null;
        } catch (Throwable t) {
            LOG.log(Level.SEVERE, "Error in getProcess: " + t.getMessage(), t);
            throw new RuntimeException("Error in getProcess: " + t.getMessage(), t);
        }
    }

    @Override
    public ProcessDTO stopProcess(String procId, String stop) {
        try {
            ProcessManager pm = ProcessManager.getManager();
            Process p = pm.lookUpProcess(procId);
            if (p != null) {
                try {
                    p.stopMe();
                } catch (CannotCallStopException ex) {
                    Logger.getLogger(AplikatorServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
                return p.toProcessDTO();
            } else {
                return null;
            }
        } catch (Throwable t) {
            LOG.log(Level.SEVERE, "Error in stopProcess: " + t.getMessage(), t);
            throw new RuntimeException("Error in stopProcess: " + t.getMessage(), t);
        }
    }

    @Override
    public ProcessDTO deleteProcess(String procId) {
        try {
            ProcessManager pm = ProcessManager.getManager();
            Process p = pm.lookUpProcess(procId);
            pm.deregisterProcess(p);
            return p.toProcessDTO();
        } catch (Throwable t) {
            LOG.log(Level.SEVERE, "Error in deleteProcess: " + t.getMessage(), t);
            throw new RuntimeException("Error in deleteProcess: " + t.getMessage(), t);
        }
    }

    @Override
    public boolean isSuspended() {
        return persister.isSuspended();
    }

    @Override
    public boolean setSuspended(boolean suspended) {
        return persister.setSuspended(suspended);
    }

    @Override
    public String getStatus() {
        LOG.fine("CALLED GET_STATUS");
        return "ALIVE. SESSION TIMEOUT: " + Configurator.get().getConfig().getInt("aplikator.sessiontimeout") + " minutes";
    }

}
