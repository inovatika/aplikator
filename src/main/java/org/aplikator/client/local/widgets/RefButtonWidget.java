package org.aplikator.client.local.widgets;

import org.aplikator.client.shared.data.RecordDTO;
import org.aplikator.client.shared.descriptor.Access;
import org.aplikator.client.shared.descriptor.AnnotationKeys;
import org.aplikator.client.shared.descriptor.PropertyDTO;
import org.aplikator.client.shared.descriptor.ViewDTO;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class RefButtonWidget extends Composite implements DataField<Integer> {
    private Button button = new Button();
    private Integer value;// TODO change to pk
    private PropertyDTO property;
    private ViewDTO view;
    private TableInterface table;
    private DialogBox dialogBox;
    private boolean dirty;
    private boolean defaultEnabled = true;

    public RefButtonWidget(String caption, ViewDTO view) {
        HorizontalPanel panel = new HorizontalPanel();
        this.view = view;
        panel.add(button);
        button.setText(caption);
        button.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                if (dialogBox == null) {
                    dialogBox = createDialogBox();
                    // dialogBox.setAnimationEnabled(true);
                    // dialogBox.center();
                }
                dialogBox.show();
                table.reload();
            }
        });
        initWidget(panel);
    }

    public PropertyDTO getProperty() {
        return property;
    }

    public void setProperty(PropertyDTO property) {
        this.property = property;

    }

    public void setValue(Integer value) {
        setValue(value, false);
    }

    public void setValue(Integer value, boolean fireEvents) {
        Integer oldValue = this.value;
        this.value = value;
        if (fireEvents) {
            setDirty(true);
            ValueChangeEvent.fireIfNotEqual(this, oldValue, value);
        }
    }

    public Integer getValue() {
        return value;
    }

    public HandlerRegistration addValueChangeHandler(ValueChangeHandler<Integer> handler) {
        return this.addHandler(handler, ValueChangeEvent.getType());
    }

    private DialogBox createDialogBox() {
        // Create a dialog box and set the caption text

        // Create a table to layout the content
        VerticalPanel dialogContents = new VerticalPanel();
        final DialogBox dialogBox = new DialogBox();
        dialogBox.setText("Select reference - " + view.getLocalizedName());
        dialogBox.setWidget(dialogContents);
        dialogContents.setSpacing(4);
        dialogBox.setWidget(dialogContents);
        table = TableFactory.create(view, null, null, 450);
        table.asWidget().setSize("100%", "450px");
        dialogContents.add(table);
        // Add a close button at the bottom of the dialog
        Button OKButton = new Button("OK", new ClickHandler() {
            public void onClick(ClickEvent event) {
                dialogBox.hide();
                setValue(table.getSelectedPrimaryKey().getId(), true);
            }
        });
        dialogContents.add(OKButton);

        // dialogContents.setCellHorizontalAlignment(closeButton,
        // HasHorizontalAlignment.ALIGN_RIGHT);

        Button closeButton = new Button("Cancel", new ClickHandler() {
            public void onClick(ClickEvent event) {
                dialogBox.hide();

            }
        });
        dialogContents.add(closeButton);

        // Return the dialog box
        return dialogBox;
    }

    public RecordDTO getSelectedRecord() {
        if (table != null) {
            return table.getSelectedRecord();
        }
        return null;
    }

    public void setDirty(boolean dirty) {
        this.dirty = dirty;
        if (dirty) {
            button.addStyleName("dirty");
        } else {
            button.removeStyleName("dirty");
        }
    }

    public boolean isDirty() {
        return dirty;
    }

    public void setEnabled(boolean enabled) {
        button.setEnabled(enabled);
    }

    @Override
    public void setDefaultEnabled(boolean enabled) {
        defaultEnabled = enabled;
    }

    @Override
    public boolean isDefaultEnabled() {
        return defaultEnabled;
    }

    @Override
    public void grabFocus() {
    }

    @Override
    public void addEnterHandler(Command command) {
    }

    public void processAnnotations(RecordDTO sourceRecord){
        String recordAccessStr = sourceRecord.getAnnotation(AnnotationKeys.ACCESS_ANNOTATION_KEY);
        String fieldAccessStr = sourceRecord.getPropertyAnnotation(this.getProperty().getId(), AnnotationKeys.ACCESS_ANNOTATION_KEY);
        if (fieldAccessStr != null) {
            Access fieldAccess = Access.valueOf(fieldAccessStr);
            this.setEnabled(!fieldAccess.isReadOnly());
        } else if (recordAccessStr != null) {
            Access recordAccess = Access.valueOf(recordAccessStr);
            this.setEnabled(!recordAccess.isReadOnly());
        } else {
            this.setEnabled(this.isDefaultEnabled());
        }

        String annotationStyleStr = sourceRecord.getPropertyAnnotation(this.getProperty().getId(), AnnotationKeys.STYLE_ANNOTATION_KEY);
        if (annotationStyleStr != null){
            this.getElement().setAttribute(AnnotationKeys.STYLE_CSS_ATTRIBUTE, annotationStyleStr);
        } else {
            this.getElement().removeAttribute(AnnotationKeys.STYLE_CSS_ATTRIBUTE);
        }
    }
}
