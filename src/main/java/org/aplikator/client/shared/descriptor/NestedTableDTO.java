package org.aplikator.client.shared.descriptor;

import com.google.gwt.user.client.ui.Widget;
import org.aplikator.client.local.widgets.HasFields;
import org.aplikator.client.local.widgets.NestedTableWidget;

@SuppressWarnings("serial")
//@Portable
public class NestedTableDTO extends WidgetPropertyDTOBase {

    private ViewDTO view;

    private boolean defaultSortId = false;

    @SuppressWarnings("unused")
    public NestedTableDTO() {//TODO custom marshaller
    }

    public NestedTableDTO(PropertyDTO property, ViewDTO view) {
        super(property);
        this.view = view;
    }

    @Override
    public Widget getWidget(HasFields form) {
        NestedTableWidget repeatedForm = new NestedTableWidget(view, property, getLocalizedName(), form, getAccess(), getSize(), defaultSortId, getCustomLabelStyle());
        if (form != null) {
            form.registerNestedCollection(repeatedForm);
        }
        repeatedForm.setVisible(isVisible());
        return repeatedForm;

    }

    public ViewDTO getView() {
        return this.view;
    }

    public void setView(ViewDTO view) {
        this.view = view;
    }

    public boolean getDefaultSortId() {
        return defaultSortId;
    }

    public void setDefaultSortId(boolean defaultSortId) {
        this.defaultSortId = defaultSortId;
    }
}
