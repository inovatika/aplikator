package org.aplikator.server.persistence.tempstore;

import java.io.InputStream;

/**
 * @author vlahoda
 */
public interface Tempstore {
    /**
     * Store contents of inputstream into temporary staore and return its UUID identifier
     *
     * @param data Contents of the file to store
     * @return UUID identifier of stored file
     */
    public String store(String filename, InputStream data, boolean closeInput);

    public String storeString(String filename, String data, String encoding);

    public String getFilename(String id);

    public long getFileLength(String id);

    public InputStream load(String id);

    /**
     * Remove the file for the given UUID
     *
     * @param id UUID of the file to remove
     */
    public void remove(String id);
}
