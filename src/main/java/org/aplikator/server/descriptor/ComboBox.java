package org.aplikator.server.descriptor;

import org.aplikator.client.shared.descriptor.ComboBoxDTO;
import org.aplikator.client.shared.descriptor.WidgetDTO;
import org.aplikator.server.data.Context;

import java.io.Serializable;

public class ComboBox<T extends Serializable> extends WidgetPropertyDescriptorBase<T> {


    public ComboBox(Property<T> property) {
        super(property);
        if (property.getListProvider() == null) {
            throw new IllegalStateException("Property " + property.getId() + " must have assigned listProvider");
        }
    }

    private String switcher;

    public ComboBox setSwitcher(String switcher) {
        this.switcher = switcher;
        return this;
    }

    @Override
    public WidgetDTO getWidgetDescriptor(Context ctx) {
        ComboBoxDTO desc = new ComboBoxDTO(getProperty().getPropertyDTO(ctx));
        desc.setSize(getSize());
        desc.setAccess(getProperty().getAccess(ctx));
        desc.setEnabled(isEnabled(ctx));
        desc.setVisible(isVisible(ctx));
        desc.setHorizontal(isHorizontal());
        desc.setElementId(getElementId());
        desc.setCustomLabelStyle(getCustomLabelStyle());
        desc.setLocalizedName(getLocalizedName(ctx));
        desc.setLocalizedTooltip(getLocalizedTooltip(ctx));
        desc.setSwitcher(switcher);
        return desc;
    }

}
