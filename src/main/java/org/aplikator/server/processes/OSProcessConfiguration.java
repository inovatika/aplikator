/*
 * Copyright 2013 Pavel Stastny <pavel.stastny at gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aplikator.server.processes;

import javax.servlet.ServletContext;
import java.util.logging.Logger;

/**
 * Configuration for os processes
 *
 * @author Pavel Stastny <pavel.stastny at gmail.com>
 */
public class OSProcessConfiguration extends ProcessConfiguration {

    public static final Logger LOGGER = Logger.getLogger(OSProcessConfiguration.class.getName());

    public final static String CLASSPATH_LIB = "jvm.classpathlib";
    public final static String JVM_ARG_KEY = "jvm.args";
    public final static String CLASSPATH = "jvm.classpath";

    //public final static String String REDIRECT_FILE = "os.redirect";
    public final static String STD_OUT_FILE = "os.redirect";
    public final static String STERR_OUT_FILE = "err.redirect";

    public OSProcessConfiguration jvmargs(String... jvmargs) {
        this.configurations.put(JVM_ARG_KEY, jvmargs);
        return this;
    }

    public OSProcessConfiguration classpath(String... classpath) {
        this.configurations.put(CLASSPATH, classpath);
        return this;
    }

    public OSProcessConfiguration classpathlib(String... lib) {
        this.configurations.put(CLASSPATH_LIB, lib);
        return this;
    }


    public OSProcessConfiguration redirectStdOut(String filePath) {
        this.configurations.put(STD_OUT_FILE, filePath);
        return this;
    }

    public OSProcessConfiguration redirectErrOut(String filePath) {
        this.configurations.put(STERR_OUT_FILE, filePath);
        return this;
    }

    private String getWebAppClasspathJars(ServletContext context) {
        String appLibPath = context.getRealPath("WEB-INF/lib");
        LOGGER.info("app lib path = " + appLibPath);
        return appLibPath;
    }

    private String getWebAppClasspathClasses(ServletContext context) {
        String classes = context.getRealPath("WEB-INF/classes");
        LOGGER.info("app classes path = " + classes);
        return classes;
    }

    public OSProcessConfiguration defaultClasspath(ServletContext servletContext) {
        return classpathlib(getWebAppClasspathJars(servletContext)).classpath(getWebAppClasspathClasses(servletContext));
    }
}
