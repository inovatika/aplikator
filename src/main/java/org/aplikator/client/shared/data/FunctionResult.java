package org.aplikator.client.shared.data;

public class FunctionResult{
    private  String message = "";
    private  String detail = "";
    private  FunctionResultStatus status;


    private final FunctionResultType type;

    public FunctionResult(String message, FunctionResultStatus status, FunctionResultType type) {
        this.message = message != null ? message : "";
        this.status = status;
        this.type = type;
    }

    public FunctionResult(String message, FunctionResultStatus status) {
        this(message != null ? message : "", status, FunctionResultType.MESSAGE);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message != null ? message : "";
    }

    public FunctionResultStatus getStatus() {
        return status;
    }

    public void setStatus(FunctionResultStatus status) {
        if (this.status.equals(FunctionResultStatus.ERROR)){
            return;
        }
        if (this.status.equals(FunctionResultStatus.WARNING) && status.equals(FunctionResultStatus.ERROR)) {
            this.status = status;
            return;
        }
        if (this.status.equals(FunctionResultStatus.SUCCESS)) {
            this.status = status;
            return;
        }
    }

    public FunctionResultType getType() {
        return type;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public void addDetail(String detail){
        if (!this.detail.isEmpty()){
            this.detail = this.detail + "\n";
        }
        this.detail = this.detail + detail;
    }

    @Override
    public String toString() {
        return "FunctionResult{" +
                "status=" + status + '\'' +
                ", message='" + message + '\'' +
                ", detail='" + detail + '\'' +
                '}';
    }
}
