package org.aplikator.server.descriptor;

import java.util.Locale;

import org.aplikator.server.Configurator;
import org.aplikator.server.data.Context;

/**
 *
 */
public class LocalizedServerDescriptorBase extends ServerDescriptorBase {

    protected LocalizedServerDescriptorBase(String id) {
        super(id);
    }

    /**
     * key for obtaining localized name from property resource bundle
     */
    private String localizationKey;
    private String localizationTooltipKey;

    public String getLocalizedName(Context ctx) {
        return Configurator.get().getLocalizedString(localizationKey, ctx != null ? ctx.getUserLocale() : Locale.getDefault());
    }

    public String getLocalizationKey() {
        return localizationKey;
    }

    public void setLocalizationKey(String key) {
        this.localizationKey = key;
    }

    public String getLocalizationTooltipKey() {
        return localizationTooltipKey;
    }

    public void setLocalizationTooltipKey(String key) {
        this.localizationTooltipKey = key;
    }

    public static void setLocalizationKeyRoot(String root, LocalizedServerDescriptorBase... descriptors) {
        for (LocalizedServerDescriptorBase desc : descriptors) {
            String oldKey = desc.getLocalizationKey();
            if (oldKey.contains(".")) {
                desc.setLocalizationKey(root + oldKey.substring(oldKey.indexOf(".")));
            }

        }
    }
}
