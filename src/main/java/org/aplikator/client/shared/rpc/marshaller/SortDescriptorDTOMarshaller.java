package org.aplikator.client.shared.rpc.marshaller;

import org.aplikator.client.shared.descriptor.PropertyDTO;
import org.aplikator.client.shared.descriptor.SortDescriptorDTO;
import org.jboss.errai.marshalling.client.api.Marshaller;
import org.jboss.errai.marshalling.client.api.MarshallingSession;
import org.jboss.errai.marshalling.client.api.annotations.ClientMarshaller;
import org.jboss.errai.marshalling.client.api.annotations.ServerMarshaller;
import org.jboss.errai.marshalling.client.api.json.EJObject;
import org.jboss.errai.marshalling.client.api.json.EJValue;
import org.jboss.errai.marshalling.client.marshallers.AbstractNullableMarshaller;

@ClientMarshaller(SortDescriptorDTO.class)
@ServerMarshaller(SortDescriptorDTO.class)
public class SortDescriptorDTOMarshaller extends AbstractNullableMarshaller<SortDescriptorDTO> {

	/*@Override
    public Class<SortDescriptorDTO> getTypeHandled() {
		return SortDescriptorDTO.class;
	}*/

    @Override
    public SortDescriptorDTO[] getEmptyArray() {
        return new SortDescriptorDTO[0];
    }

    @Override
    public SortDescriptorDTO doNotNullDemarshall(EJValue o,
                                                 MarshallingSession ctx) {
        if (o.isObject() != null) {
            EJObject ejObject = o.isObject();
            SortDescriptorDTO sdto = new SortDescriptorDTO();
            DemarshallingUtils.clientDescriptorBaseUnMarshall(ejObject, sdto);
            if (ejObject.containsKey("primaryAscending")) {
                sdto.setPrimaryAscending(ejObject.get("primaryAscending").isBoolean().booleanValue());
            }
            EJValue ejPrimarySortProperty = ejObject.get("primarySortProperty");
            Marshaller<Object> marsh = ctx.getMarshallerInstance(PropertyDTO.class.getName());
            sdto.setPrimarySortProperty((PropertyDTO) marsh.demarshall(ejPrimarySortProperty, ctx));
            return sdto;
        } else
            return null;
    }

    @Override
    public String doNotNullMarshall(SortDescriptorDTO o, MarshallingSession ctx) {
        StringBuilder builder = new StringBuilder().append('{');
        MarshallingUtils.clientDescriptorBase(builder, o).append(',');
        MarshallingUtils.bool("primaryAscending", o.isPrimaryAscending(), builder).append(',');
        MarshallingUtils.key("primarySortProperty", builder).append(':');
        Marshaller<Object> marsh = ctx.getMarshallerInstance(PropertyDTO.class.getName());
        builder.append(marsh.marshall(o.getPrimarySortProperty(), ctx));
        builder.append('}');
        return builder.toString();
    }


}
