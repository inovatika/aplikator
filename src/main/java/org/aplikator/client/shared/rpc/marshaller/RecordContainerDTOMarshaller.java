package org.aplikator.client.shared.rpc.marshaller;

import java.util.ArrayList;
import java.util.List;

import org.aplikator.client.shared.data.ContainerNodeDTO;
import org.aplikator.client.shared.data.FunctionResult;
import org.aplikator.client.shared.data.RecordContainerDTO;
import org.aplikator.client.shared.descriptor.FormDTO;
import org.jboss.errai.marshalling.client.api.Marshaller;
import org.jboss.errai.marshalling.client.api.MarshallingSession;
import org.jboss.errai.marshalling.client.api.annotations.ClientMarshaller;
import org.jboss.errai.marshalling.client.api.annotations.ServerMarshaller;
import org.jboss.errai.marshalling.client.api.json.EJObject;
import org.jboss.errai.marshalling.client.api.json.EJValue;
import org.jboss.errai.marshalling.client.marshallers.AbstractNullableMarshaller;

@ClientMarshaller(RecordContainerDTO.class)
@ServerMarshaller(RecordContainerDTO.class)
@SuppressWarnings("unchecked")
public class RecordContainerDTOMarshaller extends AbstractNullableMarshaller<RecordContainerDTO> {

	/*@Override
    public Class<RecordContainer> getTypeHandled() {
		return RecordContainer.class;
	}*/

    @Override
    public RecordContainerDTO[] getEmptyArray() {
        return new RecordContainerDTO[0];
    }

    @Override
    public RecordContainerDTO doNotNullDemarshall(EJValue o, MarshallingSession ctx) {
        if (o.isObject() != null) {
            EJObject ejObject = o.isObject();
            List<ContainerNodeDTO> cnodes = new ArrayList<ContainerNodeDTO>();
            if (DemarshallingUtils.containsAndNotNull(ejObject, "nodes")) {
                cnodes = DemarshallingUtils.listUnMarshall(ejObject.get("nodes"), ctx, ContainerNodeDTO.class.getName());
            }
            RecordContainerDTO container = new RecordContainerDTO();
            container.setContainerNodes(cnodes);

            if (DemarshallingUtils.containsAndNotNull(ejObject, "functionResult")) {
                Marshaller<Object> dem = ctx.getMarshallerInstance(FunctionResult.class.getName());
                Object functionResult = dem.demarshall(ejObject.get("functionResult"), ctx);
                container.setFunctionResult((FunctionResult) functionResult);
            }

            return container;
        } else
            return null;
    }

    @Override
    public String doNotNullMarshall(RecordContainerDTO o, MarshallingSession ctx) {
        StringBuilder builder = new StringBuilder().append('{');
        MarshallingUtils.key("nodes", builder).append(':');
        MarshallingUtils.listMarshall(builder, o.getRecords(), ctx, ContainerNodeDTO.class.getName());

        if (o.getFunctionResult() != null) {
            builder.append(',');
            MarshallingUtils.key("functionResult", builder).append(':');
            Marshaller<Object> dem = ctx.getMarshallerInstance(FunctionResult.class.getName());
            builder.append(dem.marshall(o.getFunctionResult(), ctx));
        }


        builder.append('}');
        return builder.toString();
    }
}
