package org.aplikator.client.shared.rpc.marshaller;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.aplikator.client.shared.data.ItemSuggestion;
import org.aplikator.client.shared.data.RecordDTO;
import org.aplikator.client.shared.descriptor.SuggestionsDTO;
import org.jboss.errai.marshalling.client.api.MarshallingSession;
import org.jboss.errai.marshalling.client.api.annotations.ClientMarshaller;
import org.jboss.errai.marshalling.client.api.annotations.ServerMarshaller;
import org.jboss.errai.marshalling.client.api.json.EJValue;
import org.jboss.errai.marshalling.client.marshallers.AbstractNullableMarshaller;

@ClientMarshaller(SuggestionsDTO.class)
@ServerMarshaller(SuggestionsDTO.class)
@SuppressWarnings("unchecked")
public class SuggestionsDTOMarshaller extends AbstractNullableMarshaller<SuggestionsDTO> {

	/*@Override
    public Class<SuggestionsDTO> getTypeHandled() {
		return SuggestionsDTO.class;
	}*/

    @Override
    public SuggestionsDTO[] getEmptyArray() {
        return new SuggestionsDTO[0];
    }

    @Override
    public SuggestionsDTO doNotNullDemarshall(EJValue o, MarshallingSession ctx) {
        if (o.isObject() != null) {
            SuggestionsDTO dto = new SuggestionsDTO();
            EJValue sugg = o.isObject().get("suggestions");
            List l = DemarshallingUtils.listUnMarshall(sugg, ctx, ItemSuggestion.class.getName());
            dto.setSuggestions(l);
            return dto;
        } else
            return null;
    }

    @Override
    public String doNotNullMarshall(SuggestionsDTO o, MarshallingSession ctx) {


        Set<RecordDTO> recordDTOSet = new HashSet<RecordDTO>();
        List<ItemSuggestion> sugs = o.getSuggestions();
        for (ItemSuggestion isug : sugs) {
        }

        StringBuilder builder = new StringBuilder("{");
        MarshallingUtils.key("suggestions", builder).append(':');
        MarshallingUtils.listMarshall(builder, o.getSuggestions(), ctx, ItemSuggestion.class.getName());
        builder.append("}");
        return builder.toString();

    }
}
