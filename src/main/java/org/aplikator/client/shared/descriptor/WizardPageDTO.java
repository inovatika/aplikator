/*
 * Copyright 2013 Pavel Stastny <pavel.stastny at gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aplikator.client.shared.descriptor;

import java.util.ArrayList;
import java.util.List;

import org.aplikator.client.shared.data.RecordDTO;
import org.jboss.errai.common.client.api.annotations.Portable;

/**
 * @author Pavel Stastny <pavel.stastny at gmail.com>
 */
@SuppressWarnings("serial")
@Portable
public class WizardPageDTO extends ClientDescriptorBase implements Cloneable {

    private List<PropertyDTO> properties = new ArrayList<PropertyDTO>();

    private FormDTO formDescriptor = null;
    private RecordDTO clientRecordDTO;

    private boolean hasNext = false;
    private boolean hasPrevious = false;
    private boolean hasExecute = false;

    private boolean handleEnter = false;


    private String pageId;


    @SuppressWarnings("unused")
    public WizardPageDTO() {
    }

    public WizardPageDTO(String id, String localizedName) {
        super(id, localizedName);
    }

    public List<PropertyDTO> getProperties() {
        return properties;
    }

    public void setProperties(List<PropertyDTO> properties) {
        this.properties = properties;
    }


    public WizardPageDTO addProperty(PropertyDTO property) {
        properties.add(property);
        return this;
    }

    public FormDTO getFormDescriptor() {
        return this.formDescriptor;
    }


    public void setFormDescriptor(FormDTO fdto) {
        this.formDescriptor = fdto;
    }

    public WizardPageDTO clone() {
        WizardPageDTO retval = new WizardPageDTO(this.getId(), this.getLocalizedName());
        retval.setLocalizedName(this.getLocalizedName());
        retval.formDescriptor = this.formDescriptor;
        retval.properties = this.properties;
        retval.hasExecute = this.hasExecute;
        retval.hasNext = this.hasNext;
        retval.hasPrevious = this.hasPrevious;
        retval.handleEnter = this.handleEnter;
        return retval;
    }

    public boolean isHasNext() {
        return hasNext;
    }

    public void setHasNext(boolean hasNext) {
        this.hasNext = hasNext;
    }

    public boolean isHasPrevious() {
        return hasPrevious;
    }

    public void setHasPrevious(boolean hasPrevious) {
        this.hasPrevious = hasPrevious;
    }

    public boolean isHasExecute() {
        return hasExecute;
    }

    public void setHasExecute(boolean hasExecute) {
        this.hasExecute = hasExecute;
    }

    public boolean isHandleEnter() {
        return handleEnter;
    }

    public void setHandleEnter(boolean handleEnter) {
        this.handleEnter = handleEnter;
    }

    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    public RecordDTO getClientRecord() {
        return clientRecordDTO;
    }

    public void setClientRecord(RecordDTO clientRecordDTO) {
        this.clientRecordDTO = clientRecordDTO;
    }


}
