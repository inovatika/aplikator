package org.aplikator.client.shared.rpc.marshaller;

import org.aplikator.client.shared.data.PrimaryKey;
import org.aplikator.client.shared.data.RecordDTO;
import org.aplikator.client.shared.rpc.marshaller.DataMarshallingUtils.DataType;
import org.jboss.errai.marshalling.client.api.MarshallingSession;
import org.jboss.errai.marshalling.client.api.annotations.ClientMarshaller;
import org.jboss.errai.marshalling.client.api.annotations.ServerMarshaller;
import org.jboss.errai.marshalling.client.api.json.EJObject;
import org.jboss.errai.marshalling.client.api.json.EJValue;
import org.jboss.errai.marshalling.client.marshallers.AbstractNullableMarshaller;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

@ClientMarshaller(RecordDTO.class)
@ServerMarshaller(RecordDTO.class)
public class RecordDTOMarshaller extends AbstractNullableMarshaller<RecordDTO> {

	/*@Override
    public Class<Record> getTypeHandled() {
		return Record.class;
	}*/

    @Override
    public RecordDTO[] getEmptyArray() {
        return new RecordDTO[0];
    }


    @SuppressWarnings("rawtypes")
    @Override
    public RecordDTO doNotNullDemarshall(EJValue o, MarshallingSession ctx) {
        if (o.isObject() != null) {
            RecordDTO recordDTO = null;
            EJObject object = o.isObject();
            if (DemarshallingUtils.containsAndNotNull(object, "primaryKey")) {
                recordDTO = new RecordDTO(new PrimaryKey(object.get("primaryKey").isString().stringValue()));
            } else {
                recordDTO = new RecordDTO();
            }

            if (DemarshallingUtils.containsAndNotNull(object, "data")) {
                EJValue data = object.get("data");
                if (data.isObject() != null) {
                    EJObject dataObject = data.isObject();
                    Set<String> keySet = dataObject.keySet();
                    for (String key : keySet) {
                        EJValue val = dataObject.get(key);
                        DataType dt = DataMarshallingUtils.DataType.lookup(val);
                        if (dt != null) {
                            Object value = dt.demarshall(val, ctx);
                            recordDTO.setValue(key, value);
                        }
                    }
                }
            }

            if (DemarshallingUtils.containsAndNotNull(object, "annotations")) {
                EJValue data = object.get("annotations");
                if (data.isObject() != null) {
                    EJObject dataObject = data.isObject();
                    Set<String> keySet = dataObject.keySet();
                    for (String key : keySet) {
                        EJValue val = dataObject.get(key);
                        recordDTO.putAnnotation(key, val.isString().stringValue());
                    }
                }
            }

            if (DemarshallingUtils.containsAndNotNull(object, "propertyannotations")) {
                EJValue data = object.get("propertyannotations");
                if (data.isObject() != null) {
                    EJObject dataObject = data.isObject();
                    Set<String> keySet = dataObject.keySet();
                    for (String key : keySet) {
                        EJValue val = dataObject.get(key);
                        if (val.isObject() != null) {
                            EJObject valObject = val.isObject();
                            if (DemarshallingUtils.containsAndNotNull(valObject, "annotations")) {
                                EJValue anotdata = valObject.get("annotations");
                                if (anotdata.isObject() != null) {
                                    EJObject anotObject = anotdata.isObject();
                                    Set<String> keyValSet = anotObject.keySet();
                                    for (String keyVal : keyValSet) {
                                        EJValue anotVal = anotObject.get(keyVal);
                                        recordDTO.putPropertyAnnotation(key, keyVal, anotVal.isString().stringValue());
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (DemarshallingUtils.containsAndNotNull(object, "dirty")) {
                List l = DemarshallingUtils.listUnMarshall(object.get("dirty"), ctx, String.class.getName());
                recordDTO.resetDirtyFlags();
                for (Object k : l) {
                    recordDTO.setDirty((String) k);
                }
            }
            if (DemarshallingUtils.containsAndNotNull(object, "primaryKey")) {
                PrimaryKey pk = new PrimaryKey(object.get("primaryKey").isString().stringValue());
                recordDTO.setPrimaryKey(pk);
            }
            if (DemarshallingUtils.containsAndNotNull(object, "ownerPrimaryKey")) {
                PrimaryKey pk = new PrimaryKey(object.get("ownerPrimaryKey").isString().stringValue());
                recordDTO.setOwnerPrimaryKey(pk);
            }
            if (DemarshallingUtils.containsAndNotNull(object, "ownerPropertyId")) {
                recordDTO.setOwnerPropertyId(object.get("ownerPropertyId").isString().stringValue());
            }

            if (DemarshallingUtils.containsAndNotNull(object, "preview")) {
                recordDTO.setPreviewProperty(object.get("preview").isString().stringValue());
            }
            return recordDTO;
        } else
            return null;
    }

    @Override
    public String doNotNullMarshall(RecordDTO o, MarshallingSession ctx) {
        StringBuilder builder = new StringBuilder();
        builder.append('{');
        if (o.getPrimaryKey() != null) {
            MarshallingUtils.key("primaryKey", builder).append(':');
            builder.append("\"" + o.getPrimaryKey().getSerializationString() + "\"").append(',');
        }
        if (o.getOwnerPrimaryKey() != null) {
            MarshallingUtils.key("ownerPrimaryKey", builder).append(':');
            builder.append("\"" + o.getOwnerPrimaryKey().getSerializationString() + "\"").append(',');
        }

        if (!o.getDirtyFields().isEmpty()) {
            MarshallingUtils.key("dirty", builder).append(':');
            MarshallingUtils.listMarshall(builder, o.getDirtyFields(), ctx, String.class.getName());
            builder.append(',');
        }

        MarshallingUtils.key("data", builder).append(':');
        builder.append('{');
        Set<String> properties = o.getProperties();
        boolean first = true;
        for (String k : properties) {
            if (!first)
                builder.append(',');
            MarshallingUtils.key(k, builder).append(':');
            Object val = o.getValue(k);
            DataType dt = DataMarshallingUtils.DataType.lookup((Serializable) val);
            if (dt != null)
                dt.marshall(builder, (Serializable) val, ctx);
            else
                throw new UnsupportedOperationException("unsuported type " + val.getClass().getName());
            first = false;
        }
        builder.append('}').append(',');

        MarshallingUtils.key("annotations", builder).append(':');
        builder.append('{');
        Set<String> annotations = o.getAnnotations();
        first = true;
        for (String k : annotations) {
            if (!first)
                builder.append(',');
            String val = o.getAnnotation(k);
            MarshallingUtils.string(k,val,builder);
            first = false;
        }
        builder.append('}').append(',');

        MarshallingUtils.key("propertyannotations", builder).append(':');
        builder.append('{');
        Set<String> propertyannotations = o.getPropertyAnnotations();
        first = true;
        for (String pa : propertyannotations) {
            if (!first)
                builder.append(',');
            MarshallingUtils.key(pa, builder).append(':').append('{');
            MarshallingUtils.key("annotations", builder).append(':');
            builder.append('{');
            Set<String> keys = o.propertyAnnotations.get(pa).keySet();
            first = true;
            for (String k : keys) {
                if (!first)
                    builder.append(',');
                String val = o.propertyAnnotations.get(pa).get(k);
                MarshallingUtils.string(k,val,builder);
                first = false;
            }
            builder.append('}').append('}');
            first = false;
        }
        builder.append('}').append(',');


        MarshallingUtils.string("ownerPropertyId", o.getOwnerPropertyId(), builder);
        if (o.getPreviewProperty() != null) {
            builder.append(',');
            MarshallingUtils.string("preview", o.getPreviewProperty(), builder);
        }

        builder.append('}');
        return builder.toString();
    }
}
