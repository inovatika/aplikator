/*
 * Copyright 2013 Pavel Stastny <pavel.stastny at gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aplikator.client.shared.descriptor;

import org.aplikator.client.local.widgets.HasFields;
import org.aplikator.client.local.widgets.HtmlFieldWidget;

import com.google.gwt.user.client.ui.Widget;

/**
 *
 */
//@Portable
public class HtmlFieldDTO extends WidgetPropertyDTOBase {


    @SuppressWarnings("unused")
    public HtmlFieldDTO() {//TODO custom marshaller
    }

    public HtmlFieldDTO(PropertyDTO property) {
        super(property);
    }


    @Override
    public Widget getWidget(HasFields form) {
        String label = getLocalizedName();
        HtmlFieldWidget simpleWidget = new HtmlFieldWidget("null".equalsIgnoreCase(label) ? null : label, getLocalizedTooltip(), property, getSize(), isHorizontal(), getElementId(), getCustomLabelStyle(), form);
        simpleWidget.setVisible(isVisible());
        if (form != null) {
            form.registerDataField(simpleWidget);
        }
        return simpleWidget;
    }
}
