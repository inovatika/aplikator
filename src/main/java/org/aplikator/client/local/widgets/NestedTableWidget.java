package org.aplikator.client.local.widgets;

import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.dom.client.*;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.HasKeyboardPagingPolicy;
import com.google.gwt.user.cellview.client.HasKeyboardSelectionPolicy;
import com.google.gwt.user.client.Window;
import com.google.gwt.view.client.*;
import org.aplikator.client.local.Aplikator;
import org.aplikator.client.shared.data.*;
import org.aplikator.client.shared.descriptor.Access;
import org.aplikator.client.shared.descriptor.PropertyDTO;
import org.aplikator.client.shared.descriptor.RecordsPageDTO;
import org.aplikator.client.shared.descriptor.ViewDTO;
import org.aplikator.client.shared.rpc.AplikatorErrorCallback;
import org.aplikator.client.shared.rpc.AplikatorService;
import org.gwtbootstrap3.client.ui.*;
import org.gwtbootstrap3.client.ui.constants.ButtonSize;
import org.gwtbootstrap3.client.ui.constants.ColumnSize;
import org.gwtbootstrap3.client.ui.constants.IconType;
import org.gwtbootstrap3.client.ui.constants.ModalBackdrop;
import org.gwtbootstrap3.client.ui.html.Span;
import org.jboss.errai.common.client.api.RemoteCallback;
import org.jboss.errai.enterprise.client.jaxrs.api.RestClient;

import java.util.*;
import java.util.stream.Collectors;

public class NestedTableWidget extends ContainerFieldBase implements NestedCollectionWidget {

    private static final int BUTTON_HOLDER_WIDTH = 40;
    private static final int MAX_TABLE_STRING_LENGTH = 80;

    RecordContainerDTO recordContainerDTO;
    CellTable<RecordDTO> grid;
    Modal formPopup;
    FormWidget formForGrid;
    Row formPopupHolder;
    private MultiSelectionModel<RecordDTO> gridSelectionModel;
    private RecordDTO gridSelectedRecordDTO = null;
    ListDataProvider<RecordDTO> dataProvider;
    boolean editingNewRecord = false;


    private Map<PrimaryKey, FormWidget> nestedForms = new HashMap<>();

    private Button buttonCreate;
    private Button buttonDelete;
    private ViewDTO view;
    private List<RecordDTO> pageData;
    private PrimaryKey ownerPrimaryKey;
    private PropertyDTO ownerProperty;
    private HasFields ownerForm;
    private Access access = Access.READ_WRITE_CREATE_DELETE;
    private boolean defaultSortId = false;

    public NestedTableWidget(ViewDTO view, PropertyDTO ownerProperty, String localizedName, HasFields ownerForm, Access access, int size, boolean defaultSortId, String customLabelStyle) {
        super();
        this.pageData = new ArrayList<RecordDTO>();
        this.view = view;
        this.ownerProperty = ownerProperty;
        this.ownerForm = ownerForm;
        this.access = access;
        this.setDefaultAccess(access);
        if (ownerProperty.getRefferedThrough() != null) {
            this.access = Access.READ;
        }
        this.defaultSortId = defaultSortId;
        Well well = new Well();
        well.getElement().getStyle().setPosition(Style.Position.RELATIVE);
        well.removeStyleName("well");
        well.addStyleName("well-nested");
        well.addStyleName("clearfix");


        well.getElement().getStyle().setPaddingLeft(0, Style.Unit.PX);
        well.getElement().getStyle().setPaddingRight(0, Style.Unit.PX);
        wrapper = new Column(ColumnSize.XS_1);
        this.label = new FormLabel();
        this.label.setText(localizedName);
        this.label.addStyleName("fieldLabel");
        if (customLabelStyle != null){
            this.label.addStyleName(customLabelStyle);
        }
        initWidget(wrapper);

        wrapper.add(this.label);
        wrapper.add(well);
        setGridSize(size);
        if (this.view.getSortDescriptors().size() > 0) {
            this.view.setActiveSort(this.view.getSortDescriptors().get(0).getId());
        }

        // forms container
        dataPanel = new Column(LayoutUtils.size(12));
        dataPanel.getElement().getStyle().setPaddingLeft(0, Style.Unit.PX);
        dataPanel.getElement().getStyle().setPaddingRight(0, Style.Unit.PX);

        buttonCreate = new Button("", IconType.PLUS, new ClickHandler() {
            public void onClick(ClickEvent event) {
                NestedTableWidget.this.buttonCreateClicked();
            }
        });
        buttonCreate.setSize(ButtonSize.DEFAULT);
        buttonCreate.getElement().getStyle().setMarginTop(5, Style.Unit.PX);
        buttonCreate.getElement().getStyle().setMarginLeft(5, Style.Unit.PX);
        buttonCreate.setEnabled(this.access.canCreate());

        buttonDelete = new Button("", IconType.TRASH_O, new ClickHandler() {
            public void onClick(ClickEvent event) {
                NestedTableWidget.this.buttonDeleteClicked();
            }
        });
        buttonDelete.setSize(ButtonSize.DEFAULT);
        buttonDelete.getElement().getStyle().setMarginTop(5, Style.Unit.PX);
        buttonDelete.getElement().getStyle().setMarginLeft(15, Style.Unit.PX);
        buttonDelete.setEnabled(this.access.canDelete());

        well.add(dataPanel);
        initGrid();
        dataPanel.add(grid);
        well.add(LayoutUtils.addTooltip(buttonCreate, Aplikator.application.getConfigString("aplikator.repeated.create")));
        well.add(LayoutUtils.addTooltip(buttonDelete, Aplikator.application.getConfigString("aplikator.table.delete")));
    }

    private void initGrid() {
        formPopup = createFormPopup();
        grid = new CellTable<RecordDTO>(0, TableListerWidget.DataGridResources.INSTANCE, RecordDTO.KEY_PROVIDER, new LoadingLabel());
        //grid = new CellTable<RecordDTO>(pageSize, RecordDTO.KEY_PROVIDER);
        grid.setWidth("100%");

        dataProvider = new ListDataProvider<>();
        dataProvider.addDataDisplay(grid);

        // Do not refresh the headers and footers every time the data is updated.
        grid.setAutoHeaderRefreshDisabled(true);
        grid.setAutoFooterRefreshDisabled(true);
        //gridSelectionModel = new SingleSelectionModel<RecordDTO>(RecordDTO.KEY_PROVIDER);

        gridSelectionModel = new MultiSelectionModel<RecordDTO>(RecordDTO.KEY_PROVIDER);
        grid.setSelectionModel(gridSelectionModel, DefaultSelectionEventManager.createCustomManager(createGridTranslator()));
        //grid.setSelectionModel(gridSelectionModel, DefaultSelectionEventManager.<RecordDTO> createCheckboxManager(0));

        grid.setKeyboardPagingPolicy(HasKeyboardPagingPolicy.KeyboardPagingPolicy.CHANGE_PAGE);
        grid.setKeyboardSelectionPolicy(HasKeyboardSelectionPolicy.KeyboardSelectionPolicy.ENABLED);
        //grid.setHover(true);
        //grid.setLoadingIndicator(new LoadingLabel());
        grid.setEmptyTableWidget(new EmptyLabel());
        gridSelectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
            public void onSelectionChange(SelectionChangeEvent event) {
                if (gridSelectionModel.getSelectedSet() != null && !gridSelectionModel.getSelectedSet().isEmpty()) {
                    gridSelectedRecordDTO = gridSelectionModel.getSelectedSet().iterator().next();
                } else {
                    gridSelectedRecordDTO = null;
                }
            }
        });

        // Checkbox column. This table will uses a checkbox column for selection.
        // Alternatively, you can call cellTable.setSelectionEnabled(true) to enable
        // mouse selection.
        com.google.gwt.user.cellview.client.Column<RecordDTO, Boolean> checkColumn = new com.google.gwt.user.cellview.client.Column<RecordDTO, Boolean>(
                new CheckboxCell(true, false)) {
            @Override
            public Boolean getValue(RecordDTO object) {
                // Get the value from the selection model.
                return gridSelectionModel.isSelected(object);
            }
        };
        grid.addColumn(checkColumn, SafeHtmlUtils.fromSafeConstant("<br/>"));
        grid.setColumnWidth(checkColumn, 30, Style.Unit.PX);

        for (final PropertyDTO propertyDTO : view.getProperties()) {
            grid.addColumn(new com.google.gwt.user.cellview.client.Column<RecordDTO, String>(new TextCell()) {
                @Override
                public String getValue(RecordDTO row) {
                    String retval = propertyDTO.getStringValue(row);
                    if (retval.length() <= MAX_TABLE_STRING_LENGTH) {
                        return retval;
                    } else {
                        return retval.substring(0, MAX_TABLE_STRING_LENGTH) + "...";
                    }
                }
            }, propertyDTO.getLocalizedName());
        }

        grid.addCellPreviewHandler(new CellPreviewEvent.Handler<RecordDTO>() {
            long lastClick = -1000;

            @Override
            public void onCellPreview(CellPreviewEvent<RecordDTO> event) {
                long clictAt = System.currentTimeMillis();
                if (BrowserEvents.CLICK.equalsIgnoreCase(event.getNativeEvent().getType())) {
                    if (clictAt - lastClick < 300) { // dblclick on 2 clicks detected within 300 ms
                        showFormPopup(event.getValue().getPrimaryKey());
                    }
                    lastClick = System.currentTimeMillis();
                }
                if (BrowserEvents.KEYUP.equalsIgnoreCase(event.getNativeEvent().getType())) {
                    //event.getNativeEvent().stopPropagation();
                    if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_ENTER) {
                        showFormPopup(event.getValue().getPrimaryKey());
                    }
                }
            }
        });


    }

    private DefaultSelectionEventManager.EventTranslator<RecordDTO> createGridTranslator() {
        DefaultSelectionEventManager.EventTranslator<RecordDTO> translator = new DefaultSelectionEventManager.EventTranslator<RecordDTO>() {

            @Override
            public boolean clearCurrentSelection(CellPreviewEvent<RecordDTO> event) {
                NativeEvent nativeEvent = event.getNativeEvent();
                //boolean ctrlOrMeta = nativeEvent.getCtrlKey() || nativeEvent.getMetaKey();
                //return !ctrlOrMeta;
                if (BrowserEvents.KEYUP.equals(nativeEvent.getType())) {
                    return true;
                }
                return (event.getColumn() != 0);
            }

            @Override
            public DefaultSelectionEventManager.SelectAction translateSelectionEvent(CellPreviewEvent<RecordDTO> event) {
                // Handle the event.
                NativeEvent nativeEvent = event.getNativeEvent();
                if (BrowserEvents.CLICK.equals(nativeEvent.getType())) {
                    // Ignore if the event didn't occur in the correct column.
                    if (event.getColumn() != 0) {
                        return DefaultSelectionEventManager.SelectAction.TOGGLE;
                    }
                    // Determine if we clicked on a checkbox.
                    Element target = nativeEvent.getEventTarget().cast();
                    if ("input".equals(target.getTagName().toLowerCase(Locale.ROOT))) {
                        final InputElement input = target.cast();
                        if ("checkbox".equals(input.getType().toLowerCase(Locale.ROOT))) {

                            // Synchronize the checkbox with the current selection state.
                            input.setChecked(event.getDisplay().getSelectionModel().isSelected(
                                    event.getValue()));
                            return DefaultSelectionEventManager.SelectAction.TOGGLE;
                        }
                    }
                    return DefaultSelectionEventManager.SelectAction.TOGGLE;
                }

                // For keyboard events, do the default action.
                return DefaultSelectionEventManager.SelectAction.DEFAULT;
            }


        };
        return translator;
    }

    private void showFormPopup(PrimaryKey primaryKey) {

        formForGrid = nestedForms.get(primaryKey);
        formPopupHolder.clear();
        formPopupHolder.add(formForGrid);
        formPopup.setPixelSize(Window.getClientWidth() - 40, Window.getClientHeight());
        formPopup.show();
        //formForGrid.displayRecord(primaryKey, recordContainerDTO, this.ownerProperty != null ? this.ownerProperty.getId() : null, ownerPrimaryKey, null);

    }

    public void reopenFormPopup() {

        formPopup.setPixelSize(Window.getClientWidth() - 40, Window.getClientHeight());
        formPopup.show();
        //formForGrid.displayRecord(primaryKey, recordContainerDTO, TableListerWidget.this.ownerProperty != null ? TableListerWidget.this.ownerProperty.getId(): null, ownerPrimaryKey, null);

    }

    private Modal createFormPopup() {
        ModalBody formPopupContents = new ModalBody();
        formPopupContents.addStyleName("formscroller");
        final Modal formPopup = new Modal();
        formPopup.getElement().getStyle().setOverflowY(Style.Overflow.AUTO);//fix against hidding scrollbar in modal stack
        formPopup.setDataBackdrop(ModalBackdrop.STATIC);
        formForGrid = new FormWidget(this.view, null, this, this.ownerForm, (this.ownerForm != null?this.ownerForm.getPrefix():"")+"/"+this.view.getId()+"-NestedTableForm");

        ModalHeader buttonPanel = new ModalHeader();
        buttonPanel.setClosable(false);

//        Button buttonCancelPopup = new Button("", IconType.BAN, new ClickHandler() {
//
//            public void onClick(ClickEvent event) {
//                //formForGrid.cancel();
//                if (editingNewRecord) {
//                    nestedForms.remove(pageData.get(pageData.size() - 1).getPrimaryKey());
//                    pageData.remove(pageData.size() - 1);
//                    dataProvider.getList().remove(dataProvider.getList().size() - 1);
//                }
//                editingNewRecord = false;
//                formPopup.hide();
//                grid.redraw();
//                focusGrid();
//            }
//
//        });
//        buttonPanel.add(buttonCancelPopup);

        final Button buttonSavePopup = new Button("", IconType.CHEVRON_LEFT, new ClickHandler() {

            public void onClick(ClickEvent event) {
                formPopup.hide();
                RecordDTO edited = null;
                if (formForGrid != null) {
                    edited = formForGrid.save(false);
                }
                if (edited != null) {
                    for (RecordDTO visibleItem : grid.getVisibleItems()) {
                        if (visibleItem.getPrimaryKey().equals(edited.getPrimaryKey())) {
                            visibleItem.setPreviewProperty(edited.getPreviewProperty());
                            for (String s : edited.getProperties()) {
                                visibleItem.setValue(s, edited.getValue(s));
                            }
                            grid.redraw();
                            break;
                        }
                    }
                }
                //grid.redraw();
                focusGrid();
            }

        });
        buttonPanel.add(buttonSavePopup);

        //LayoutUtils.addTooltip(buttonCancelPopup, Aplikator.application.getConfigString("aplikator.table.cancel"));
        LayoutUtils.addTooltip(buttonSavePopup, Aplikator.application.getConfigString("aplikator.table.close"));

        formPopup.add(buttonPanel);
        formPopup.add(formPopupContents);

        formPopupHolder = new Row();
        formPopupHolder.add(formForGrid);
        formPopupContents.add(formPopupHolder);

        return formPopup;
    }

    private Modal createDeleteConfirmDialogBox() {
        // Create a dialog box and set the caption text
        int numberOfRecords = gridSelectionModel.getSelectedSet().size();

        // Create a table to layout the content
        final Modal dialogBox = new Modal();
        dialogBox.setDataBackdrop(ModalBackdrop.STATIC);
        dialogBox.setTitle(Aplikator.application.getConfigString("aplikator.table.confirmation"));
        ModalBody contents = new ModalBody();
        contents.add(new Span(Aplikator.application.getConfigString("aplikator.table.confirmdelete") + " (" + numberOfRecords + ")"));
        dialogBox.add(contents);

        // Add a close button at the bottom of the dialog
        Button okButton = new Button(Aplikator.application.getConfigString("aplikator.table.ok"), new ClickHandler() {
            @SuppressWarnings("unchecked")
            public void onClick(ClickEvent event) {
                dialogBox.hide();

//                for (int i = 0; i < nestedForms.size(); i++) {
//                    if (form == nestedForms.get(i)) {
//                        RecordDTO orig = pageData.get(i);
//                        if (orig == null) {
//                            return;
//                        }
//                        nestedForms.remove(i);
//                        ((Column) dataPanel).remove(i);
//
//                        recordContainerDTO.addRecord(view.getId(), orig, orig, Operation.DELETE);
//                        pageData.remove(i);
//                        if (ownerForm != null) {
//                            ownerForm.setDirty(true);
//                        }
//                        break;
//                    }
//                }





                for (RecordDTO toDelete : gridSelectionModel.getSelectedSet()) {
                    recordContainerDTO.addRecord(view.getId(), toDelete, toDelete, Operation.DELETE);
                    nestedForms.remove(toDelete.getPrimaryKey());
                    pageData = pageData.stream().filter(pageRec -> !pageRec.getPrimaryKey().equals(toDelete.getPrimaryKey())).collect(Collectors.toList());
                }
                updateGridWithPageData();
                if (ownerForm != null) {
                    ownerForm.setDirty(true);
                }

//                try {
//                    RestClient.create(AplikatorService.class,
//                            new RemoteCallback<RecordContainerDTO>() {
//                                @Override
//                                public void callback(RecordContainerDTO records) {
//                                    if (records.getFunctionResult() == null) {
//                                        Messages.success(Aplikator.application.getConfigString("aplikator.table.deletedok"));
//                                    } else {
//                                        FunctionResult functionResult = records.getFunctionResult();
//                                        if (functionResult.getStatus().equals(FunctionResultStatus.SUCCESS)) {
//                                            Messages.success(functionResult.getMessage());
//                                        } else if (functionResult.getStatus().equals(FunctionResultStatus.WARNING)) {
//                                            Messages.messageWithDetail(records.getFunctionResult().getMessage(), records.getFunctionResult().getDetail(), FunctionResultStatus.WARNING);
//                                        } else {
//                                            Messages.messageWithDetail(records.getFunctionResult().getMessage(), records.getFunctionResult().getDetail(), FunctionResultStatus.ERROR);
//                                        }
//
//                                    }
//                                    reload();
//                                }
//                            },
//                            new AplikatorErrorCallback("aplikator.table.deletederror")
//                    ).processRecords(newContainer);
//                } catch (AplikatorException e) {
//                    Window.alert("CHYBA: " + e);
//                }


            }
        });

        Button cancelButton = new Button(Aplikator.application.getConfigString("aplikator.table.cancel"), new ClickHandler() {
            @SuppressWarnings("unchecked")
            public void onClick(ClickEvent event) {
                dialogBox.hide();

            }
        });
        ModalFooter buttonPanel = new ModalFooter();
        dialogBox.add(buttonPanel);
        buttonPanel.add(cancelButton);
        buttonPanel.add(okButton);


        // Return the dialog box
        return dialogBox;
    }

    private void focusGrid() {
        if (ownerProperty == null) {
            Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
                @Override
                public void execute() {
                    grid.setFocus(true);
                }
            });
        }
    }


    @Override
    public void reload() {
        //dataPanel.clear();
        //dataPanel.add(new LoadingLabel());
        pageData = new ArrayList<RecordDTO>();
        RestClient.create(AplikatorService.class, new RemoteCallback<RecordsPageDTO>() {
                    @Override
                    public void callback(RecordsPageDTO resp) {
                        NestedTableWidget.this.pageData = resp.getRecords();
                        redrawPage(null);
                    }
                },
                new AplikatorErrorCallback("aplikator.table.loaderror")
        ).getRecords(view.getId(), null, defaultSortId ? view.getActiveSort() : null, null, null, ownerProperty.getId(), (ownerPrimaryKey == null) ? null : ownerPrimaryKey.getSerializationString(), 0, -1);

    }

    @Override
    public void initFromContainer(RecordContainerDTO initializingRecords) {
        pageData = new ArrayList<RecordDTO>();
        for (ContainerNodeDTO containerNode : initializingRecords.getRecords()) {
            if (ownerProperty.getId().equals(containerNode.getEdited().getOwnerPropertyId())) {
                pageData.add(containerNode.getEdited());
                recordContainerDTO.addRecord(view.getId(), containerNode.getEdited(), containerNode.getEdited(), Operation.CREATE);
            }
        }
        redrawPage(initializingRecords);
    }

    @Override
    public void setAccess(Access access) {
        this.access = access;
        buttonCreate.setEnabled(this.access.canCreate());
        buttonDelete.setEnabled(this.access.canDelete());
        redrawPage(null);
    }

    @Override
    public Access getAccess() {
        return this.access;
    }

    @Override
    public void clear() {
//        if (recordContainerDTO != null){
//            recordContainerDTO.clear();
//        }
        if (pageData != null){
            pageData.clear();
        }
//        if (dataPanel != null) {
//            dataPanel.clear();
//        }

        nestedForms.clear();
        updateGridWithPageData();
    }



    private void redrawPage(RecordContainerDTO initializingRecords) {
        nestedForms.clear();
        for (int i = 1; i <= pageData.size(); i++) {
            FormWidget form = new FormWidget(this.view, null, this, ownerForm, (ownerForm != null ? ownerForm.getPrefix():"")+"/"+this.view.getId()+"-"+i);
            form.setEnabled(!access.isReadOnly());
            form.displayRecord(pageData.get(i - 1).getPrimaryKey(), recordContainerDTO, ownerProperty.getId(), ownerPrimaryKey, initializingRecords);
            nestedForms.put(pageData.get(i - 1).getPrimaryKey(), form);
        }
        updateGridWithPageData();
    }

    private void updateGridWithPageData(){
        gridSelectionModel.clear();
        List<RecordDTO> list = dataProvider.getList();
        list.clear();
        list.addAll(pageData);
        grid.setPageSize(pageData.size());
    }

    private void buttonCreateClicked() {
        FormWidget form = new FormWidget(this.view, null, this, ownerForm, (ownerForm!= null?ownerForm.getPrefix():"")+"/"+this.view.getId()+"-"+(pageData.size()+1));
        formForGrid = form;
        formPopupHolder.clear();
        formPopupHolder.add(formForGrid);

        formPopup.setPixelSize(Window.getClientWidth() - 40, Window.getClientHeight());
        formPopup.show();
        formForGrid.addRecord(recordContainerDTO, ownerProperty != null ? ownerProperty.getId() : null, ownerPrimaryKey, /*recordToCopy*/null, new AddRecordCallback() {
            @Override
            public void recordAdded(RecordDTO recordDTO) {
                nestedForms.put(recordDTO.getPrimaryKey(), form);
                recordDTO.setPreviewProperty(Aplikator.application.getConfigString("aplikator.table.newRecordLabel"));
                editingNewRecord = true;
                gridSelectionModel.clear();
                pageData.add(recordDTO);
                dataProvider.getList().add(recordDTO);
                grid.setPageSize(pageData.size());
                gridSelectionModel.setSelected(recordDTO, true);
            }
        });
    }

    private void buttonDeleteClicked() {
        createDeleteConfirmDialogBox().show();
    }


    public PrimaryKey getOwnerPrimaryKey() {
        return ownerPrimaryKey;
    }

    public void setOwnerPrimaryKey(PrimaryKey value) {
        ownerPrimaryKey = value;
    }

    public PropertyDTO getOwnerProperty() {
        return ownerProperty;
    }

    public void setRecordContainerDTO(RecordContainerDTO recordContainerDTO) {
        this.recordContainerDTO = recordContainerDTO;
    }

    @Override
    public void save() {
        for (FormWidget nestedForm : nestedForms.values()) {
            if (nestedForm.isDirty()) {
                nestedForm.save(false);
            }
        }
    }

    public void setDirty(boolean dirty) {
        for (FormWidget nestedForm : nestedForms.values()) {
            nestedForm.setNestedDirty(dirty);
        }
    }

}
