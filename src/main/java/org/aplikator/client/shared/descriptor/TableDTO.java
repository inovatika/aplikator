package org.aplikator.client.shared.descriptor;

import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;
import org.aplikator.client.local.widgets.HasFields;
import org.aplikator.client.local.widgets.LayoutUtils;
import org.aplikator.client.local.widgets.TableFactory;
import org.aplikator.client.local.widgets.TableInterface;
import org.gwtbootstrap3.client.ui.Column;

@SuppressWarnings("serial")
//@JsonTypeName("table")
//@Portable
public class TableDTO extends WidgetPropertyDTOBase {

    public static final String TYPE_KEY = "table";

    private ViewDTO view;
    private int height = 0;

    @SuppressWarnings("unused")
    public TableDTO() { //TODO custom marshaller
    }

    public TableDTO(PropertyDTO property, ViewDTO view) {
        super(property);
        this.view = view;
    }


    public ViewDTO getView() {
        return view;
    }

    public void setView(ViewDTO view) {
        this.view = view;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public Widget getWidget(HasFields form) {
        Column frame = new Column(LayoutUtils.size(getSize()));
        TableInterface table = TableFactory.create(view, property, form, height > 0 ? height : calculateFormHeight());
        table.setAccess(getAccess());
        table.setDefaultAccess(getAccess());
        if (form != null) {
            form.registerNestedCollection(table);
        }
        Widget tableWidget = table.asWidget();
        tableWidget.addStyleName("nested-table-frame");
        frame.add(tableWidget);
        return frame;
    }

    private int calculateFormHeight() {
        Widget dummyform = view.getFormDescriptor().getLayout().getWidget(null);
        RootPanel.get().add(dummyform);
        int height = dummyform.getElement().getClientHeight();
        dummyform.removeFromParent();
        return height;
    }

}
