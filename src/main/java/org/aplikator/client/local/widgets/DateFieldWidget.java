package org.aplikator.client.local.widgets;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import org.aplikator.client.local.Aplikator;
import org.aplikator.client.shared.descriptor.PropertyDTO;
import org.gwtbootstrap3.extras.datetimepicker.client.ui.DateTimePicker;
import org.gwtbootstrap3.extras.datetimepicker.client.ui.base.constants.DateTimePickerFormatViewType;
import org.gwtbootstrap3.extras.datetimepicker.client.ui.base.constants.DateTimePickerLanguage;
import org.gwtbootstrap3.extras.datetimepicker.client.ui.base.constants.DateTimePickerView;
import org.gwtbootstrap3.extras.datetimepicker.client.ui.base.events.ChangeDateEvent;
import org.gwtbootstrap3.extras.datetimepicker.client.ui.base.events.ChangeDateHandler;

import java.util.Date;


/**
 * Internal date format - must be converted from GWT DateTimeFormat. Only M-m and H-h are handled now
 * String. Default: 'dd.mm.yyyy'
 * <p>
 * The date format, combination of p, P, h, hh, i, ii, s, ss, d, dd, m, mm, M, MM, yy, yyyy.
 * <p>
 * p : meridian in lower case ('am' or 'pm') - according to locale file
 * P : meridian in upper case ('AM' or 'PM') - according to locale file
 * s : seconds without leading zeros
 * ss : seconds, 2 digits with leading zeros
 * i : minutes without leading zeros
 * ii : minutes, 2 digits with leading zeros
 * h : hour without leading zeros - 24-hour format
 * hh : hour, 2 digits with leading zeros - 24-hour format
 * H : hour without leading zeros - 12-hour format
 * HH : hour, 2 digits with leading zeros - 12-hour format
 * d : day of the month without leading zeros
 * dd : day of the month, 2 digits with leading zeros
 * m : numeric representation of month without leading zeros
 * mm : numeric representation of the month, 2 digits with leading zeros
 * M : short textual representation of a month, three letters
 * MM : full textual representation of a month, such as January or March
 * yy : two digit representation of a year
 * yyyy : full numeric representation of a year, 4 digits
 */
@SuppressWarnings( "deprecation" )
public class DateFieldWidget extends DataFieldBase<Date> implements DataField<Date> {

    private DateTimePicker box = new DateTimePicker();
    private String gwtFormat;

    public static DateTimePickerLanguage mapLanguageToAplikatorLocale() {
        String locale = Aplikator.application.getLocale();
        if (locale != null && locale.startsWith("cs")) {
            return DateTimePickerLanguage.CS;
        }
        return DateTimePickerLanguage.EN;
    }

    public DateFieldWidget(String caption, String localizedTooltip, PropertyDTO property, int size, String formatPattern, boolean horizontal, String elementId, String customLabelStyle, HasFields form) {
        super(caption, localizedTooltip, property, horizontal, elementId, customLabelStyle, form);
        controlHolder.add(box);
        if (formatPattern != null) {
            gwtFormat = formatPattern;
            box.setFormat(formatPattern.replace("m", "i").replace("M", "m").replace("h", "o").replace("H", "h").replace("o", "H"));
            box.setLanguage(mapLanguageToAplikatorLocale());
            if (!formatPattern.contains("h") && !formatPattern.contains("H")) { //display date picker only
                box.setMinView(DateTimePickerView.MONTH);
            } else if (!formatPattern.contains("d")) {//display time picker only
                box.setStartView(DateTimePickerView.DAY);
                box.setMinView(DateTimePickerView.HOUR);
                box.setMaxView(DateTimePickerView.DAY);
                box.setFormatViewType(DateTimePickerFormatViewType.TIME);
            }
        }
        box.addValueChangeHandler(new ValueChangeHandler<Date>() {
            public void onValueChange(ValueChangeEvent<Date> event) {
                //Window.alert("VALUECHANGE");
                setDirty(true);
            }
        });
        box.addChangeDateHandler(new ChangeDateHandler() {
            @Override
            public void onChangeDate(ChangeDateEvent evt) {
                //Window.alert("DATECHANGE");
                setDirty(true);
                ValueChangeEvent.fire(DateFieldWidget.this, null);
            }
        });
        box.addDomHandler(new KeyUpHandler() {
            @Override
            public void onKeyUp(KeyUpEvent event) {
                ValueChangeEvent.fire(box, null);
            }
        }, KeyUpEvent.getType());
        //box.setLanguage(DateTimePickerLanguage.CS);//TODO get from applicationDTO
        setGridSize(size);

    }

    @SuppressWarnings( "deprecation" )
    public void setValue(Date value) {
        if (value != null && value.getYear() <70 && !gwtFormat.contains("h") && !gwtFormat.contains("H")){
            int offset = value.getTimezoneOffset();
            Date newDate = new Date(value.getTime() - offset*MILLIS_IN_MINUTE);
            //Window.alert("Date:"+ newDate+" millis:" + newDate.getTime() + " offset:" + newDate.getTimezoneOffset()+ " year:"+newDate.getYear());
            box.setValue(newDate);
        } else {
            box.setValue(value);
        }
    }

    private static final long MILLIS_IN_MINUTE = 60000;
    private static final String EUROPE_PRAGUE = "{\"id\": \"Europe/Prague\", \"transitions\": [81049, 60, 85417, 0, 89953, 60, 94153, 0, 98521, 60, 102889, 0, 107257, 60, 111625, 0, 115993, 60, 120361, 0, 124729, 60, 129265, 0, 133633, 60, 138001, 0, 142369, 60, 146737, 0, 151105, 60, 155473, 0, 159841, 60, 164209, 0, 168577, 60, 172945, 0, 177313, 60, 181849, 0, 186217, 60, 190585, 0, 194953, 60, 199321, 0, 203689, 60, 208057, 0, 212425, 60, 216793, 0, 221161, 60, 225529, 0, 230065, 60, 235105, 0, 238801, 60, 243841, 0, 247537, 60, 252577, 0, 256273, 60, 261481, 0, 265009, 60, 270217, 0, 273745, 60, 278953, 0, 282649, 60, 287689, 0, 291385, 60, 296425, 0, 300121, 60, 305329, 0, 308857, 60, 314065, 0, 317593, 60, 322801, 0, 326329, 60, 331537, 0, 335233, 60, 340273, 0, 343969, 60, 349009, 0, 352705, 60, 357913, 0, 361441, 60, 366649, 0, 370177, 60, 375385, 0, 379081, 60, 384121, 0, 387817, 60, 392857, 0, 396553, 60, 401593, 0, 405289, 60, 410497, 0, 414025, 60, 419233, 0, 422761, 60, 427969, 0, 431665, 60, 436705, 0, 440401, 60, 445441, 0, 449137, 60, 454345, 0, 457873, 60, 463081, 0, 466609, 60, 471817, 0, 475513, 60, 480553, 0, 484249, 60, 489289, 0, 492985, 60, 498025, 0, 501721, 60, 506929, 0, 510457, 60, 515665, 0, 519193, 60, 524401, 0, 528097, 60, 533137, 0, 536833, 60, 541873, 0, 545569, 60, 550777, 0, 554305, 60, 559513, 0, 563041, 60, 568249, 0, 571777, 60, 576985, 0, 580681, 60, 585721, 0, 589417, 60, 594457, 0], \"names\": [\"CET\", \"Central European Standard Time\", \"CEST\", \"Central European Summer Time\"], \"std_offset\": 60}";

    @SuppressWarnings( "deprecation" )
    public Date getValue() {
        Date retval= box.getValue();
       if (retval != null && retval.getYear() <70&& !gwtFormat.contains("h") && !gwtFormat.contains("H")){
            retval = new Date(Date.UTC(retval.getYear(),retval.getMonth(),retval.getDate(),0,0,0));
            //Window.alert("Date:"+ retval+" millis:" + retval.getTime() + " offset:" + retval.getTimezoneOffset() + " year:"+retval.getYear());
        }
        return retval;
    }

    public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<Date> handler) {
        return addHandler(handler, ValueChangeEvent.getType());
        //return box.addValueChangeHandler(handler);
    }

    public void setEnabled(boolean enabled) {
        if (property.getRefferedThrough() != null) {
            enabled = false;
        }
        box.setEnabled(enabled);
    }

    public void setDirty(boolean dirty) {
        super.setDirty(dirty);
        if (dirty) {
            box.addStyleName("dirty");
        } else {
            box.removeStyleName("dirty");
        }
    }

    @Override
    public void grabFocus() {
        Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
            @Override
            public void execute() {
                box.onShow(null);
            }
        });
    }

}
