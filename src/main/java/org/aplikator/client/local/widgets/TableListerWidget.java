package org.aplikator.client.local.widgets;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.cell.client.CheckboxCell;
import com.google.gwt.cell.client.TextCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.dom.client.*;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.cellview.client.*;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DeckPanel;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.*;
import org.aplikator.client.local.Aplikator;
import org.aplikator.client.local.SettingsStorage;
import org.aplikator.client.local.wizards.WizardSupport;
import org.aplikator.client.local.wizards.WizardSupportCallbacks;
import org.aplikator.client.shared.data.*;
import org.aplikator.client.shared.descriptor.*;
import org.aplikator.client.shared.rpc.AplikatorErrorCallback;
import org.aplikator.client.shared.rpc.AplikatorException;
import org.aplikator.client.shared.rpc.AplikatorService;
import org.gwtbootstrap3.client.ui.Column;
import org.gwtbootstrap3.client.ui.*;
import org.gwtbootstrap3.client.ui.constants.FormType;
import org.gwtbootstrap3.client.ui.constants.IconType;
import org.gwtbootstrap3.client.ui.constants.ModalBackdrop;
import org.gwtbootstrap3.client.ui.html.Div;
import org.gwtbootstrap3.client.ui.html.Span;
import org.gwtbootstrap3.client.ui.html.Strong;
import org.gwtbootstrap3.extras.datetimepicker.client.ui.DateTimePicker;
import org.gwtbootstrap3.extras.datetimepicker.client.ui.base.constants.DateTimePickerFormatViewType;
import org.gwtbootstrap3.extras.datetimepicker.client.ui.base.constants.DateTimePickerView;
import org.gwtbootstrap3.extras.datetimepicker.client.ui.base.events.ChangeDateEvent;
import org.gwtbootstrap3.extras.datetimepicker.client.ui.base.events.ChangeDateHandler;
import org.jboss.errai.common.client.api.RemoteCallback;
import org.jboss.errai.enterprise.client.jaxrs.api.RestClient;

import java.util.*;
import java.util.logging.Logger;

import static org.aplikator.client.local.widgets.DateFieldWidget.mapLanguageToAplikatorLocale;

//import org.gwtbootstrap3.client.ui.gwt.CellTable;

public class TableListerWidget extends Composite implements TableInterface {

    public static final int TABLE_NAVBAR_HEIGHT = 52;
    private static final int MAX_TABLE_STRING_LENGTH = 80;

    private static Logger LOG = Logger.getLogger(TableListerWidget.class.getName());
    private static TableListerWidgetUiBinder uiBinder = GWT.create(TableListerWidgetUiBinder.class);

    static {
        TableListerWidgetResources.INSTANCE.cellListStyle().ensureInjected();
        DataGridResources.INSTANCE.cellTableStyle().ensureInjected();
    }

    private final CellList<RecordDTO> cellList;
    private final SingleSelectionModel<RecordDTO> listSelectionModel;
    private final ShowMorePagerPanel listPagerPanel;
    private final ScrollPanel listScroller;
    private final ScrollPanel formScroller;
    private final AplikatorDataProvider listDataProvider;
    private final MultiSelectionModel<RecordDTO> gridSelectionModel;
    private final ScrollPanel gridScroller;
    private final GridDataProvider gridDataProvider;
    private final ViewDTO view;
    private final PropertyDTO ownerProperty;
    @UiField
    Container tableBody;
    @UiField
    Container showMoreMenu;
    @UiField
    Strong showMoreTitle;
    @UiField
    Button buttonShowMore;
    @UiField
    Container fullMenu;
    @UiField
    Navbar navbar;
    @UiField
    Strong title;
    @UiField
    ButtonGroup switchTableList;
    @UiField
    RadioButton buttonList;
    @UiField
    RadioButton buttonTable;
    @UiField
    Row listContainer;
    @UiField
    Row gridContainer;
    @UiField
    NavbarForm queryControls;
    @UiField
    ListBox querySelector;
    @UiField
    FormLabel queryLabel;
    @UiField
    NavbarForm sortControls;
    @UiField
    ListBox sortSelector;
    @UiField
    FormLabel sortLabel;
    @UiField
    NavbarForm queryParameters;
    @UiField
    TextBox queryTextField;
    @UiField
    ListBox queryListField;
    @UiField
    DateTimePicker queryDateField;
    @UiField
    CheckBox queryCheckboxField;
    @UiField
    Button buttonQuery;
    @UiField
    Button buttonQueryEdit;
    @UiField
    TextBox searchField;
    @UiField
    Button buttonCreate;
    @UiField
    Button buttonDelete;
    @UiField
    Button buttonReload;
    @UiField
    Button buttonCancel;
    @UiField
    Button buttonSave;
    @UiField
    Button buttonFunction;
    @UiField
    ListDropDown menuFunction;
    @UiField
    DropDownMenu menuFunctionItems;


    @UiField
    NavbarText rangeLabelHolder;
    RangeLabelPager listPager;
    SimplePager gridPager;
    @UiField
    Column listColumn;
    @UiField
    Column formColumn;
    @UiField
    Column gridColumn;
    CellTable<RecordDTO> grid;
    Modal formPopup;
    FormWidget formForGrid;
    Button buttonSavePopup;
    RecordContainerDTO recordContainerDTO;
    private NestedEditorNotification nestedEditorNotification = null;
    private FormWidget form;
    private DeckPanel formHolder;
    private Label noForm;
    private boolean editingNewRecord = false;
    private ListMode listMode = ListMode.STANDARD;
    private DisplayMode displayMode = DisplayMode.LIST;
    private int pageSize = 0;
    private String indexSearchArgument;
    private RecordDTO listSelectedRecordDTO = null;
    private RecordDTO gridSelectedRecordDTO = null;
    private int recordCount = -1;
    private PrimaryKey ownerPrimaryKey;
    private HasFields ownerForm;


    private QueryParamDialogBox queryParamDialogBox;

    private boolean initialized = false;

    @SuppressWarnings("GWTStyleCheck")
    public TableListerWidget(ViewDTO view, PropertyDTO ownerProperty, HasFields ownerForm, int availableHeight) {
        super();
        this.view = view;
        this.pageSize = view.getPageSize();
        this.ownerProperty = ownerProperty;
        this.ownerForm = ownerForm;
        recordContainerDTO = new RecordContainerDTO();

        initWidget(uiBinder.createAndBindUi(this));

        //top navigation panel
        if (ownerProperty != null) {
            navbar.addStyleName("nestedTableNavbar");
            showMoreMenu.setVisible(true);
            fullMenu.setVisible(false);
            tableBody.setVisible(false);
            buttonShowMore.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    showMoreMenu.setVisible(false);
                    fullMenu.setVisible(true);
                    tableBody.setVisible(true);
                }
            });
        } else {
            showMoreMenu.setVisible(false);
            fullMenu.setVisible(true);
        }

        //title
        title.setText(this.view.getLocalizedName());
        showMoreTitle.setText(this.view.getLocalizedName());
        //buttons
        LayoutUtils.addTooltip(buttonList, Aplikator.application.getConfigString("aplikator.table.showList"));
        LayoutUtils.addTooltip(buttonTable, Aplikator.application.getConfigString("aplikator.table.showGrid"));

        LayoutUtils.addTooltip(buttonReload, Aplikator.application.getConfigString("aplikator.table.reload"));
        LayoutUtils.addTooltip(buttonCreate, Aplikator.application.getConfigString("aplikator.table.create"));
        LayoutUtils.addTooltip(buttonDelete, Aplikator.application.getConfigString("aplikator.table.delete"));

        LayoutUtils.addTooltip(buttonCancel, Aplikator.application.getConfigString("aplikator.table.cancel"));
        LayoutUtils.addTooltip(buttonSave, Aplikator.application.getConfigString("aplikator.table.save"));

        //sort selector
        for (SortDescriptorDTO sd : this.view.getSortDescriptors()) {
            String sortLabel = sd.getLocalizedName();
            if (sd.isPrimaryAscending()) {
                sortLabel = "\u2191 " + sortLabel;
            } else {
                sortLabel = "\u2193 " + sortLabel;
            }
            sortSelector.addItem(sortLabel, sd.getId());
        }
        final String searchPopupTitlePrefix = Aplikator.application.getConfigString("aplikator.table.searchPopupPrefix");
        sortSelector.addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent event) {
                searchField.setVisible(isSelectedSortScrollable());
                SortDescriptorDTO activeSort = getSelectedSort();
                TableListerWidget.this.view.setActiveSort(activeSort.getId());
                SettingsStorage.storeSetting(TableListerWidget.this.view.getId() + SettingsStorage.ACTIVE_SORT, activeSort.getId());
                reload();
            }
        });
        if (this.view.getSortDescriptors().size() > 0) {
            String storedSort = SettingsStorage.getSetting(TableListerWidget.this.view.getId() + SettingsStorage.ACTIVE_SORT);
            int storedSortIndex = TableListerWidget.this.view.getSortIndex(storedSort);
            sortSelector.setSelectedIndex(-1);
            sortSelector.setSelectedIndex(storedSortIndex);
            TableListerWidget.this.view.setActiveSort(TableListerWidget.this.view.getSortDescriptors().get(storedSortIndex).getId());
        }
        sortLabel.setText(Aplikator.application.getConfigString("aplikator.table.sort"));
        sortControls.setVisible(this.view.getSortDescriptors().size() > 1);

        //search field and button
        searchField.setVisible(isSelectedSortScrollable());
        searchField.addKeyUpHandler(new KeyUpHandler() {
            @Override
            public void onKeyUp(KeyUpEvent event) {
                event.stopPropagation();
                if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
                    TableListerWidget.this.sqlSearch();
                }
            }
        });

        //filter selector

        queryDateField.setFormat("dd.MM.yyyy");
        queryDateField.setMinView(DateTimePickerView.MONTH);
        queryDateField.setAutoClose(true);

        for (QueryDescriptorDTO qd : this.view.getQueryDescriptors()) {
            querySelector.addItem(qd.getLocalizedName(), qd.getId());
        }
        querySelector.addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent event) {
                enableQueryParameterFields(true);
            }
        });
        buttonQuery.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                reload();
            }
        });
        buttonQueryEdit.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                editQueryParameters();
            }
        });
        queryTextField.addKeyUpHandler(new KeyUpHandler() {
            @Override
            public void onKeyUp(KeyUpEvent event) {
                event.stopPropagation();
                if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
                    reload();
                }
            }
        });
        queryListField.addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent event) {
                event.stopPropagation();
                reload();
            }
        });
        queryDateField.addChangeDateHandler(new ChangeDateHandler() {
            @Override
            public void onChangeDate(ChangeDateEvent evt) {
                reload();
            }
        });

        queryCheckboxField.addChangeHandler(new ChangeHandler() {
            @Override
            public void onChange(ChangeEvent event) {
                event.stopPropagation();
                reload();
            }
        });
        if (this.view.getQueryDescriptors().size() > 0) {
            String storedFilter = SettingsStorage.getSetting(TableListerWidget.this.view.getId() + SettingsStorage.ACTIVE_FILTER);
            int storedFilterIndex = TableListerWidget.this.view.getQueryIndex(storedFilter);
            querySelector.setSelectedIndex(-1);

            querySelector.setSelectedIndex(storedFilterIndex);
            //querySelector.setItemSelected(querySelector.getSelectedIndex(), false);
            TableListerWidget.this.view.setActiveFilter(TableListerWidget.this.view.getQueryDescriptors().get(storedFilterIndex).getId());
        }
        queryLabel.setText(Aplikator.application.getConfigString("aplikator.table.filter"));
        queryControls.setVisible(this.view.getQueryDescriptors().size() > 1);
        queryParameters.setVisible(TableListerWidget.this.view.getQueryDescriptors().get(querySelector.getSelectedIndex()).getQueryParameters().size() == 1);

        if (view.getFunctions().size() == 1) {
            buttonFunction.setVisible(true);
            buttonFunction.setEnabled(true);
            buttonFunction.setText(view.getFunctions().get(0).getLocalizedName());
            ClientContext clientContext = new ClientContext();
            clientContext.setUser(Aplikator.application.getUsername());
            clientContext.setLocale(Aplikator.application.getLocale());
            clientContext.setView(view);
            final WizardSupport wizardSupport = new WizardSupport(view.getFunctions().get(0), clientContext, new WizardSupportCallbacks() {
                @Override
                public void wizardPageCallback(WizardPageDTO response, boolean forw) {

                }

                @Override
                public void runBeforeFunctionCallback() {
                    buttonFunction.setEnabled(false);
                }

                @Override
                public void runAfterFunctionCallback() {
                    buttonFunction.setEnabled(true);
                    refresh();
                }
            });
//            buttonFunction.setIcon(IconType.fromStyleName(view.getFunctions().get(0).getIcon()));
            buttonFunction.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    if (displayMode.equals(DisplayMode.LIST)) {
                        wizardSupport.getClientContext().setCurrentRecord(listSelectedRecordDTO);
                        Set<RecordDTO> singleset = new HashSet<>();
                        singleset.add(listSelectedRecordDTO);
                        wizardSupport.getClientContext().setSelectedRecords(singleset);
                    } else {
                        wizardSupport.getClientContext().setCurrentRecord(gridSelectedRecordDTO);
                        wizardSupport.getClientContext().setSelectedRecords(gridSelectionModel.getSelectedSet());
                    }
                    wizardSupport.changePageRequest(true);
                }
            });
            LayoutUtils.addTooltip(buttonFunction, view.getFunctions().get(0).getLocalizedTooltip());

        } else if (view.getFunctions().size() > 1) {
            menuFunction.setVisible(true);
            for (FunctionDTO functionDTO : view.getFunctions()) {
                final AnchorListItem functionHandle = new AnchorListItem(functionDTO.getLocalizedName());
                menuFunctionItems.add(functionHandle);
                ClientContext clientContext = new ClientContext();
                clientContext.setUser(Aplikator.application.getUsername());
                clientContext.setLocale(Aplikator.application.getLocale());
                clientContext.setView(view);
                final WizardSupport wizardSupport = new WizardSupport(functionDTO, clientContext, new WizardSupportCallbacks() {
                    @Override
                    public void wizardPageCallback(WizardPageDTO response, boolean forw) {

                    }

                    @Override
                    public void runBeforeFunctionCallback() {
                        functionHandle.setEnabled(false);
                    }

                    @Override
                    public void runAfterFunctionCallback() {
                        functionHandle.setEnabled(true);
                        refresh();
                    }
                });
//                functionHandle.setIcon(IconType.fromStyleName(functionDTO.getIcon()));
                functionHandle.addClickHandler(new ClickHandler() {
                    @Override
                    public void onClick(ClickEvent event) {
                        if (displayMode.equals(DisplayMode.LIST)) {
                            wizardSupport.getClientContext().setCurrentRecord(listSelectedRecordDTO);
                            Set<RecordDTO> singleset = new HashSet<>();
                            singleset.add(listSelectedRecordDTO);
                            wizardSupport.getClientContext().setSelectedRecords(singleset);
                        } else {
                            wizardSupport.getClientContext().setCurrentRecord(gridSelectedRecordDTO);
                            wizardSupport.getClientContext().setSelectedRecords(gridSelectionModel.getSelectedSet());
                        }
                        wizardSupport.changePageRequest(true);
                    }
                });
                LayoutUtils.addTooltip(functionHandle, functionDTO.getLocalizedTooltip());

            }

        }


        listPager = new RangeLabelPager();
        gridPager = new SimplePager(SimplePager.TextLocation.CENTER, false, false) {
            @Override
            protected String createText() {
                // Default text is 1 based.
                NumberFormat formatter = NumberFormat.getFormat("#,###");
                String decimalSeparator = Aplikator.application.getConfigString("aplikator.table.rangeSeparator");
                HasRows display = getDisplay();
                Range range = display.getVisibleRange();
                int pageStart = range.getStart() + 1;
                int pageSize = range.getLength();
                int dataSize = display.getRowCount();
                int endIndex = Math.min(dataSize, pageStart + pageSize - 1);
                endIndex = Math.max(pageStart, endIndex);
                boolean exact = display.isRowCountExact();
                return formatter.format(pageStart).replace(",", decimalSeparator)
                        + " - "
                        + formatter.format(endIndex).replace(",", decimalSeparator)
                        + (exact ? " : " : " :~ ")
                        + formatter.format(dataSize).replace(",", decimalSeparator);
            }
        };
        rangeLabelHolder.add(listPager);
        rangeLabelHolder.add(gridPager);


        // scrollable cellList
        cellList = new CellList<RecordDTO>(new RecordCell(), TableListerWidgetResources.INSTANCE, RecordDTO.KEY_PROVIDER);
        cellList.setPageSize(pageSize);
        listSelectionModel = new SingleSelectionModel<RecordDTO>(RecordDTO.KEY_PROVIDER);
        cellList.setSelectionModel(listSelectionModel, DefaultSelectionEventManager.createCustomManager(createVetoTranslator()));
        cellList.setKeyboardPagingPolicy(HasKeyboardPagingPolicy.KeyboardPagingPolicy.CHANGE_PAGE);
        cellList.setKeyboardSelectionPolicy(HasKeyboardSelectionPolicy.KeyboardSelectionPolicy.ENABLED);

        cellList.setLoadingIndicator(new LoadingLabel());
        cellList.setEmptyListWidget(new EmptyLabel());


        listSelectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
            public void onSelectionChange(SelectionChangeEvent event) {
                if (listSelectionModel.getSelectedObject() != null) {
                    listSelectedRecordDTO = listSelectionModel.getSelectedObject();
                    formHolder.showWidget(1);
                    if (!editingNewRecord) {
                        openForm();
                    }
                } else {
                    listSelectedRecordDTO = null;
                    formHolder.showWidget(0);
                }
            }
        });


        final int SCROLL_OFFSET = Aplikator.application.isShowNavigation() ? Aplikator.MAINMENUBAR_HEIGHT + TABLE_NAVBAR_HEIGHT : TABLE_NAVBAR_HEIGHT;
        final int scrollHeight = (availableHeight > 0 ? availableHeight + TABLE_NAVBAR_HEIGHT + SCROLL_OFFSET : Window.getClientHeight()) - SCROLL_OFFSET;
        listScroller = new ScrollPanel();
        listScroller.getElement().getStyle().setHeight(scrollHeight, Unit.PX);

        listPagerPanel = new ShowMorePagerPanel(listScroller);
        listPagerPanel.setIncrementSize(pageSize);
        listPagerPanel.setDisplay(cellList);
        listPager.setDisplay(cellList);

        listDataProvider = new AplikatorDataProvider(listScroller, pageSize);
        listDataProvider.addDataDisplay(cellList);


        form = new FormWidget(this.view, this, null, this.ownerForm, (this.ownerForm!= null?this.ownerForm.getPrefix():"")+"TableForm");
        form.addStyleName("app-mg-top-5");
        form.addStyleName("app-row-divider");
        noForm = new Label(Aplikator.application.getConfigString("aplikator.table.noselect"));
        formHolder = new DeckPanel();
        formHolder.add(noForm);
        formHolder.add(form);
        formHolder.showWidget(0);

        formScroller = new ScrollPanel(formHolder);

        formScroller.addStyleName("formscroller");
        formScroller.getElement().getStyle().setHeight(scrollHeight, Unit.PX);


        formPopup = createFormPopup();
        grid = new CellTable<RecordDTO>(pageSize, DataGridResources.INSTANCE, RecordDTO.KEY_PROVIDER, new LoadingLabel());
        //grid = new CellTable<RecordDTO>(pageSize, RecordDTO.KEY_PROVIDER);
        grid.setWidth("100%");
        // Do not refresh the headers and footers every time the data is updated.
        grid.setAutoHeaderRefreshDisabled(true);
        grid.setAutoFooterRefreshDisabled(true);
        //gridSelectionModel = new SingleSelectionModel<RecordDTO>(RecordDTO.KEY_PROVIDER);

        gridSelectionModel = new MultiSelectionModel<RecordDTO>(RecordDTO.KEY_PROVIDER);
        grid.setSelectionModel(gridSelectionModel, DefaultSelectionEventManager.createCustomManager(createGridTranslator()));
        //grid.setSelectionModel(gridSelectionModel, DefaultSelectionEventManager.<RecordDTO> createCheckboxManager(0));

        grid.setKeyboardPagingPolicy(HasKeyboardPagingPolicy.KeyboardPagingPolicy.CHANGE_PAGE);
        grid.setKeyboardSelectionPolicy(HasKeyboardSelectionPolicy.KeyboardSelectionPolicy.ENABLED);
        //grid.setHover(true);
        //grid.setLoadingIndicator(new LoadingLabel());
        grid.setEmptyTableWidget(new EmptyLabel());
        gridSelectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
            public void onSelectionChange(SelectionChangeEvent event) {
                if (gridSelectionModel.getSelectedSet() != null && !gridSelectionModel.getSelectedSet().isEmpty()) {
                    gridSelectedRecordDTO = gridSelectionModel.getSelectedSet().iterator().next();
                } else {
                    gridSelectedRecordDTO = null;
                }
            }
        });

        // Checkbox column. This table will uses a checkbox column for selection.
        // Alternatively, you can call cellTable.setSelectionEnabled(true) to enable
        // mouse selection.
        com.google.gwt.user.cellview.client.Column<RecordDTO, Boolean> checkColumn = new com.google.gwt.user.cellview.client.Column<RecordDTO, Boolean>(
                new CheckboxCell(true, false)) {
            @Override
            public Boolean getValue(RecordDTO object) {
                // Get the value from the selection model.
                return gridSelectionModel.isSelected(object);
            }
        };
        grid.addColumn(checkColumn, SafeHtmlUtils.fromSafeConstant("<br/>"));
        grid.setColumnWidth(checkColumn, 30, Unit.PX);

        for (final PropertyDTO propertyDTO : view.getProperties()) {
            grid.addColumn(new com.google.gwt.user.cellview.client.Column<RecordDTO, String>(new TextCell()) {
                @Override
                public String getValue(RecordDTO row) {
                    String retval = propertyDTO.getStringValue(row);
                    if (retval.length() <= MAX_TABLE_STRING_LENGTH) {
                        return retval;
                    } else {
                        return retval.substring(0, MAX_TABLE_STRING_LENGTH) + "...";
                    }
                }
            }, propertyDTO.getLocalizedName());
        }

        grid.addCellPreviewHandler(new CellPreviewEvent.Handler<RecordDTO>() {
            long lastClick = -1000;

            @Override
            public void onCellPreview(CellPreviewEvent<RecordDTO> event) {
                long clictAt = System.currentTimeMillis();
                if (BrowserEvents.CLICK.equalsIgnoreCase(event.getNativeEvent().getType())) {
                    if (clictAt - lastClick < 300) { // dblclick on 2 clicks detected within 300 ms
                        showFormPopup(event.getValue().getPrimaryKey());
                    }
                    lastClick = System.currentTimeMillis();
                }
                if (BrowserEvents.KEYUP.equalsIgnoreCase(event.getNativeEvent().getType())) {
                    //event.getNativeEvent().stopPropagation();
                    if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_ENTER) {
                        showFormPopup(event.getValue().getPrimaryKey());
                    }
                }
            }
        });


        gridScroller = new ScrollPanel();
        gridScroller.getElement().getStyle().setHeight(scrollHeight, Unit.PX);

        // gridPagerPanel = new ShowMorePagerPanel(gridScroller);
        // gridPagerPanel.setIncrementSize(pageSize);
        // gridPagerPanel.setDisplay(grid);

        gridPager.setDisplay(grid);
        gridDataProvider = new GridDataProvider();
        gridDataProvider.addDataDisplay(grid);
        if (ownerProperty == null) {
            gridScroller.add(grid);
        }


        if (availableHeight <= 0) {
            Window.addResizeHandler(new ResizeHandler() {
                @Override
                public void onResize(ResizeEvent event) {
                    listScroller.getElement().getStyle().setHeight(event.getHeight() - SCROLL_OFFSET, Unit.PX);
                    formScroller.getElement().getStyle().setHeight(event.getHeight() - SCROLL_OFFSET, Unit.PX);
                    gridScroller.getElement().getStyle().setHeight(event.getHeight() - SCROLL_OFFSET, Unit.PX);
                    formPopup.setPixelSize(Window.getClientWidth() - 40, Window.getClientHeight());
                }
            });
        }

        listColumn.setSize(LayoutUtils.size(this.view.getListPanelWidth()));
        listColumn.add(listPagerPanel);

        formColumn.setSize(LayoutUtils.size(12 - this.view.getListPanelWidth()));
        formColumn.add(formScroller);

        gridScroller.getElement().getStyle().setHeight(scrollHeight, Unit.PX);
        if (ownerProperty == null) {
            gridColumn.add(gridScroller);
        } else {
            gridColumn.add(grid);
        }
        initialized = true;

        boolean openAsTable = view.isOpenAsTable();
        String storedtableModeString = SettingsStorage.getSetting(TableListerWidget.this.view.getId() + SettingsStorage.TABLE_MODE);
        if (storedtableModeString != null) {
            openAsTable = Boolean.parseBoolean(storedtableModeString);
        }
        if (ownerProperty != null) {
            openAsTable = true;
            buttonList.setVisible(false);
            buttonTable.setVisible(false);
        }
        if (openAsTable) {
            showTable(null);
        } else {
            showList(null);
        }
        this.form.setEnabled(!view.getAccess().isReadOnly());
        this.formForGrid.setEnabled(!view.getAccess().isReadOnly());
        enableNavigationControls();
        enableQueryParameterFields(false);

    }

    private DefaultSelectionEventManager.EventTranslator<RecordDTO> createGridTranslator() {
        DefaultSelectionEventManager.EventTranslator<RecordDTO> translator = new DefaultSelectionEventManager.EventTranslator<RecordDTO>() {

            @Override
            public boolean clearCurrentSelection(CellPreviewEvent<RecordDTO> event) {
                NativeEvent nativeEvent = event.getNativeEvent();
                //boolean ctrlOrMeta = nativeEvent.getCtrlKey() || nativeEvent.getMetaKey();
                //return !ctrlOrMeta;
                if (BrowserEvents.KEYUP.equals(nativeEvent.getType())) {
                    return true;
                }
                return (event.getColumn() != 0);
            }

            @Override
            public DefaultSelectionEventManager.SelectAction translateSelectionEvent(CellPreviewEvent<RecordDTO> event) {
                // Handle the event.
                NativeEvent nativeEvent = event.getNativeEvent();
                if (BrowserEvents.CLICK.equals(nativeEvent.getType())) {
                    // Ignore if the event didn't occur in the correct column.
                    if (event.getColumn() != 0) {
                        return DefaultSelectionEventManager.SelectAction.TOGGLE;
                    }
                    // Determine if we clicked on a checkbox.
                    Element target = nativeEvent.getEventTarget().cast();
                    if ("input".equals(target.getTagName().toLowerCase(Locale.ROOT))) {
                        final InputElement input = target.cast();
                        if ("checkbox".equals(input.getType().toLowerCase(Locale.ROOT))) {

                            // Synchronize the checkbox with the current selection state.
                            input.setChecked(event.getDisplay().getSelectionModel().isSelected(
                                    event.getValue()));
                            return DefaultSelectionEventManager.SelectAction.TOGGLE;
                        }
                    }
                    return DefaultSelectionEventManager.SelectAction.TOGGLE;
                }

                // For keyboard events, do the default action.
                return DefaultSelectionEventManager.SelectAction.DEFAULT;
            }


        };
        return translator;
    }

    private void adjustFormatPattern(DateTimePicker queryDateField, String formatPattern) {
        queryDateField.setFormat(formatPattern.replace("m", "i").replace("M", "m").replace("h", "o").replace("H", "h").replace("o", "H"));
        queryDateField.setLanguage(mapLanguageToAplikatorLocale());
        if (!formatPattern.contains("h") && !formatPattern.contains("H")) { //display date picker only
            queryDateField.setMinView(DateTimePickerView.MONTH);
        } else if (!formatPattern.contains("d")) {//display time picker only
            queryDateField.setStartView(DateTimePickerView.DAY);
            queryDateField.setMinView(DateTimePickerView.HOUR);
            queryDateField.setMaxView(DateTimePickerView.DAY);
            queryDateField.setFormatViewType(DateTimePickerFormatViewType.TIME);
        }
    }

    private void enableQueryParameterFields(boolean fireReload) {
        QueryDescriptorDTO activeFilter = TableListerWidget.this.view.getQueryDescriptors().get(querySelector.getSelectedIndex());
        TableListerWidget.this.view.setActiveFilter(activeFilter.getId());
        SettingsStorage.storeSetting(TableListerWidget.this.view.getId() + SettingsStorage.ACTIVE_FILTER, activeFilter.getId());
        if (activeFilter.getQueryParameters().size() == 1) {
            queryParameters.setVisible(true);
            buttonQueryEdit.setVisible(false);
            if ("java.util.Date".equals(TableListerWidget.this.view.getActiveQueryDescriptor().getQueryParameters().get(0).getProperty().getType())) {

                String formatPattern = TableListerWidget.this.view.getActiveQueryDescriptor().getQueryParameters().get(0).getProperty().getFormatPattern();
                adjustFormatPattern(queryDateField, formatPattern);

                queryDateField.setVisible(true);
                queryListField.setVisible(false);
                queryTextField.setVisible(false);
                queryCheckboxField.setVisible(false);
                queryDateField.setValue(null);
            } else if (TableListerWidget.this.view.getActiveQueryDescriptor().getQueryParameters().get(0).getProperty().getListValues() != null) {
                queryDateField.setVisible(false);
                queryListField.setVisible(true);
                queryTextField.setVisible(false);
                queryCheckboxField.setVisible(false);
                queryListField.clear();
                for (org.aplikator.client.shared.data.ListItem item : TableListerWidget.this.view.getActiveQueryDescriptor().getQueryParameters().get(0).getProperty().getListValues()) {
                    queryListField.addItem(item.getName(), item.getValue().toString());
                }
                queryListField.setSelectedIndex(0);
                queryListField.setFocus(true);
                reload();
            } else if ("java.lang.Boolean".equals(TableListerWidget.this.view.getActiveQueryDescriptor().getQueryParameters().get(0).getProperty().getType())) {


                queryDateField.setVisible(false);
                queryListField.setVisible(false);
                queryTextField.setVisible(false);
                queryCheckboxField.setVisible(true);
                queryDateField.setValue(null);
            } else {
                queryDateField.setVisible(false);
                queryListField.setVisible(false);
                queryCheckboxField.setVisible(false);
                queryTextField.setVisible(true);
                queryTextField.setText("");
                queryTextField.setFocus(true);
            }
        } else {
            if (activeFilter.getQueryParameters().size() > 1) {
                buttonQueryEdit.setVisible(true);
            } else {
                buttonQueryEdit.setVisible(false);
            }
            queryParameters.setVisible(false);
            if (fireReload) {
                reload();
            }
        }

    }

    private void showFormPopup(PrimaryKey primaryKey) {
        if (displayMode.equals(DisplayMode.GRID)) {
            formPopup.setPixelSize(Window.getClientWidth() - 40, Window.getClientHeight());
            formPopup.show();
            formForGrid.displayRecord(primaryKey, recordContainerDTO, TableListerWidget.this.ownerProperty != null ? TableListerWidget.this.ownerProperty.getId() : null, ownerPrimaryKey, null);
        }
    }

    public void reopenFormPopup() {
        if (displayMode.equals(DisplayMode.GRID)) {
            formPopup.setPixelSize(Window.getClientWidth() - 40, Window.getClientHeight());
            formPopup.show();
            //formForGrid.displayRecord(primaryKey, recordContainerDTO, TableListerWidget.this.ownerProperty != null ? TableListerWidget.this.ownerProperty.getId(): null, ownerPrimaryKey, null);
        }
    }

    private Modal createFormPopup() {
        ModalBody formPopupContents = new ModalBody();
        formPopupContents.addStyleName("formscroller");
        final Modal formPopup = new Modal();
        formPopup.getElement().getStyle().setOverflowY(Style.Overflow.AUTO);//fix against hidding scrollbar in modal stack
        formPopup.setDataBackdrop(ModalBackdrop.STATIC);
        formForGrid = new FormWidget(this.view, this, null, this.ownerForm, (this.ownerForm!= null?this.ownerForm.getPrefix():"")+"GridForm");

        ModalHeader buttonPanel = new ModalHeader();
        buttonPanel.setClosable(false);

        Button buttonCancelPopup = new Button("", IconType.BAN, new ClickHandler() {

            public void onClick(ClickEvent event) {
                formForGrid.cancel();
                if (editingNewRecord) {
                    gridDataProvider.removeFirstRecord();
                }
                editingNewRecord = false;
                formPopup.hide();
                focusGrid();
            }

        });
        buttonPanel.add(buttonCancelPopup);

        buttonSavePopup = new Button("", IconType.SAVE, new ClickHandler() {

            public void onClick(ClickEvent event) {
                formPopup.hide();
                if (formForGrid != null) {
                    formForGrid.recordContainerDTO.setFunctionResult(new FunctionResult("", FunctionResultStatus.SUCCESS));
                    formForGrid.save(true);
                }
                focusGrid();
            }

        });
        buttonPanel.add(buttonSavePopup);

        LayoutUtils.addTooltip(buttonCancelPopup, Aplikator.application.getConfigString("aplikator.table.cancel"));
        LayoutUtils.addTooltip(buttonSavePopup, Aplikator.application.getConfigString("aplikator.table.save"));

        formPopup.add(buttonPanel);
        formPopup.add(formPopupContents);

        Row formPopupHolder = new Row();
        formPopupHolder.add(formForGrid);
        formPopupContents.add(formPopupHolder);

        return formPopup;
    }


    @UiHandler("buttonList")
    void showList(ClickEvent e) {
        if (!buttonList.isEnabled()) {
            buttonTable.setActive(true);
            buttonList.setActive(false);
            return;
        }
        gridContainer.setVisible(false);
        listContainer.setVisible(true);
        gridPager.setVisible(false);
        listPager.setVisible(true);
        rangeLabelHolder.getElement().getStyle().setMarginTop(15, Unit.PX);
        rangeLabelHolder.getElement().getStyle().setMarginBottom(15, Unit.PX);
        displayMode = DisplayMode.LIST;
        buttonCancel.setVisible(true);
        buttonSave.setVisible(true);
        buttonList.setActive(true);
        buttonTable.setActive(false);
        listDataProvider.refreshListAndExtendScroll(0, null, cellList);
        focusList();
        SettingsStorage.storeSetting(TableListerWidget.this.view.getId() + SettingsStorage.TABLE_MODE, "false");
    }


    @UiHandler("buttonTable")
    void showTable(ClickEvent e) {
        if (!buttonTable.isEnabled()) {
            buttonTable.setActive(false);
            buttonList.setActive(true);
            return;
        }
        if (listMode.equals(ListMode.INSERT)) {
            refresh();
        }
        listContainer.setVisible(false);
        gridContainer.setVisible(true);
        listPager.setVisible(false);
        gridPager.setVisible(true);
        rangeLabelHolder.getElement().getStyle().setMarginTop(11, Unit.PX);
        rangeLabelHolder.getElement().getStyle().setMarginBottom(11, Unit.PX);
        displayMode = DisplayMode.GRID;
        buttonCancel.setVisible(false);
        buttonSave.setVisible(false);
        buttonList.setActive(false);
        buttonTable.setActive(true);
        focusGrid();
        SettingsStorage.storeSetting(TableListerWidget.this.view.getId() + SettingsStorage.TABLE_MODE, "true");
    }

    private void focusList() {
        if (ownerProperty == null && displayMode.equals(DisplayMode.LIST)) {
            Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
                @Override
                public void execute() {
                    cellList.setFocus(true);
                }
            });
        }
    }

    private void focusGrid() {
        if (ownerProperty == null && displayMode.equals(DisplayMode.GRID)) {
            Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
                @Override
                public void execute() {
                    grid.setFocus(true);
                }
            });
        }
    }

    public SingleSelectionModel<RecordDTO> getListSelectionModel() {
        return listSelectionModel;
    }

    private boolean canEnableEmbedded() {
        if (ownerPrimaryKey == null) {
            return true;
        } else {
            return ownerPrimaryKey.getId() != -1;
        }
    }

    void enableEditingControls() {
        searchField.setEnabled(false);
        buttonCreate.setEnabled(false);
        buttonDelete.setEnabled(false);
        buttonReload.setEnabled(false);

        sortSelector.setEnabled(false);
        querySelector.setEnabled(false);
        queryTextField.setEnabled(false);
        queryListField.setEnabled(false);
        queryCheckboxField.setEnabled(false);
        queryDateField.setEnabled(false);
        buttonQuery.setEnabled(false);
        buttonQueryEdit.setEnabled(false);

        boolean canEnable = canEnableEmbedded();
        buttonCancel.setEnabled(canEnable && view.getAccess().canWrite());
        buttonSave.setEnabled(canEnable && view.getAccess().canWrite());
        buttonSavePopup.setEnabled(canEnable && view.getAccess().canWrite());
        buttonList.setEnabled(false);
        buttonTable.setEnabled(false);

        if (nestedEditorNotification != null) {
            nestedEditorNotification.nestedEditorStarted();
        }

        cellList.setKeyboardSelectionPolicy(HasKeyboardSelectionPolicy.KeyboardSelectionPolicy.DISABLED);
        cellList.setKeyboardPagingPolicy(HasKeyboardPagingPolicy.KeyboardPagingPolicy.INCREASE_RANGE);
    }

    void enableNavigationControls() {
        boolean canEnable = canEnableEmbedded();
        searchField.setEnabled(canEnable);
        buttonCreate.setEnabled(canEnable && view.getAccess().canCreate());
        buttonDelete.setEnabled(canEnable && view.getAccess().canDelete());
        buttonReload.setEnabled(canEnable);

        sortSelector.setEnabled(canEnable);
        querySelector.setEnabled(canEnable);
        queryTextField.setEnabled(canEnable);
        queryListField.setEnabled(canEnable);
        queryCheckboxField.setEnabled(canEnable);
        queryDateField.setEnabled(canEnable);
        buttonQuery.setEnabled(canEnable);
        buttonQueryEdit.setEnabled(canEnable);

        buttonCancel.setEnabled(false);
        buttonSave.setEnabled(false);
        buttonSavePopup.setEnabled(false);
        buttonList.setEnabled(canEnable);
        buttonTable.setEnabled(canEnable);


        if (nestedEditorNotification != null) {
            nestedEditorNotification.nestedEditorFinished();
        }

        cellList.setKeyboardSelectionPolicy(HasKeyboardSelectionPolicy.KeyboardSelectionPolicy.BOUND_TO_SELECTION);
        cellList.setKeyboardPagingPolicy(HasKeyboardPagingPolicy.KeyboardPagingPolicy.INCREASE_RANGE);
    }

    private boolean isSelectedSortScrollable() {
        SortDescriptorDTO sort = getSelectedSort();
        if (sort.getPrimarySortProperty().getType().equals("java.lang.String")) {//accept all strings
            return true;
        }
        if (sort.getPrimarySortProperty().getType().equals("java.lang.Integer") //accept all int except primary key
                && !sort.getPrimarySortProperty().getId().equals(view.getEntity().getPrimaryKey().getId())) {
            return true;
        }
        if (sort.getPrimarySortProperty().getType().equals("java.math.BigDecimal")) {//accept all numbers
            return true;
        }
        return false;
    }

    private SortDescriptorDTO getSelectedSort() {
        return TableListerWidget.this.view.getSortDescriptors().get(sortSelector.getSelectedIndex());
    }

    private DefaultSelectionEventManager.EventTranslator<RecordDTO> createVetoTranslator() {
        DefaultSelectionEventManager.EventTranslator<RecordDTO> translator = new DefaultSelectionEventManager.EventTranslator<RecordDTO>() {

            @Override
            public boolean clearCurrentSelection(CellPreviewEvent<RecordDTO> event) {
                NativeEvent nativeEvent = event.getNativeEvent();
                boolean ctrlOrMeta = nativeEvent.getCtrlKey() || nativeEvent.getMetaKey();
                return !ctrlOrMeta;
            }

            @Override
            public DefaultSelectionEventManager.SelectAction translateSelectionEvent(CellPreviewEvent<RecordDTO> event) {
                if (form.isDirty()) {
                    return DefaultSelectionEventManager.SelectAction.IGNORE;
                }
                return DefaultSelectionEventManager.SelectAction.DEFAULT;
            }
        };
        return translator;
    }

    public void reload(String searchArgument) {
        this.indexSearchArgument = searchArgument;
        refresh();
    }

    @Override
    public void initFromContainer(RecordContainerDTO initializingRecords) {
        //Not supported for table yet, just reload
        reload();
    }

    private Access defaultAccess = Access.READ_WRITE_CREATE_DELETE;

    @Override
    public void setDefaultAccess(Access access) {
        defaultAccess = access;
    }

    @Override
    public Access getDefaultAccess() {
        return defaultAccess;
    }

    @Override
    public void reload() {
        this.indexSearchArgument = null;
        searchField.setText("");
        view.setSearchString("");

        if (view.getActiveQueryDescriptor() != null && view.getActiveQueryDescriptor().getQueryParameters().size() > 1) {
            queryParamDialogBox = createQueryParamDialogBox();
            queryParamDialogBox.show();
        } else if (view.getActiveQueryDescriptor() != null && view.getActiveQueryDescriptor().getQueryParameters().size() == 1) {
            view.getActiveQueryDescriptor().getQueryParameters().get(0).setValue(getQueryFieldValue());
            refresh();
        } else {
            refresh();
        }
    }

    private void editQueryParameters() {
        queryParamDialogBox.show();
    }

    private String getQueryFieldValue() {
        if ("java.util.Date".equals(view.getActiveQueryDescriptor().getQueryParameters().get(0).getProperty().getType())) {
            if (queryDateField.getValue() != null) {
                return Long.toString(queryDateField.getValue().getTime());
            } else {
                return null;
            }
        } else if ("java.lang.Boolean".equals(view.getActiveQueryDescriptor().getQueryParameters().get(0).getProperty().getType())) {
            if (queryCheckboxField.getValue() != null) {
                return Boolean.toString(queryCheckboxField.getValue());
            } else {
                return null;
            }
        } else if (view.getActiveQueryDescriptor().getQueryParameters().get(0).getProperty().getListValues() != null) {
            return queryListField.getSelectedValue();
        }
        return queryTextField.getText();
    }

    @Override
    public void refresh() {
        editingNewRecord = false;
        listSelectionModel.clear();
        gridSelectionModel.clear();
        if (ownerPrimaryKey == null) {
            recordContainerDTO.clear();
        }

        if (indexSearchArgument != null) {
            //loadDataSearch();
            listMode = ListMode.INDEX;
            recordCount = 0;
            resetDataProviders();
        } else {
            listMode = ListMode.STANDARD;
            recordCount = 0;
            //resetDataProviders();
            listDataProvider.resetCache();
            cellList.setRowCount(0);
            cellList.setVisibleRange(0, 0);
            gridDataProvider.resetCache();
            grid.setRowCount(0);
            grid.setVisibleRange(0, 0);
            RestClient.create(AplikatorService.class, new RemoteCallback<Integer>() {
                        @Override
                        public void callback(Integer resp) {
                            recordCount = resp;
                            resetDataProviders();
                        }
                    },
                    new AplikatorErrorCallback("aplikator.table.loaderror")
            ).getRecordCount(view.getId(), view.getActiveFilter(), view.getActiveSort(), view.getActiveQueryDescriptor(), view.getSearchString(), ownerProperty != null ? ownerProperty.getId() : null, (ownerPrimaryKey == null) ? null : ownerPrimaryKey.getSerializationString());
        }
        enableNavigationControls();
    }

    private void resetDataProviders() {
        if (ownerProperty != null) {
            if (recordCount > 0) {
                showMoreMenu.setVisible(false);
                fullMenu.setVisible(true);
                tableBody.setVisible(true);
            } else {
                showMoreMenu.setVisible(true);
                fullMenu.setVisible(false);
                tableBody.setVisible(false);
            }
        }
        listDataProvider.resetCache();
        listDataProvider.updateRowCount(recordCount, false);
        cellList.setVisibleRange(0, pageSize);

        gridDataProvider.resetCache();
        gridDataProvider.updateRowCount(recordCount, true);
        grid.setVisibleRange(0, pageSize);
    }

    private QueryParamDialogBox createQueryParamDialogBox() {
        Container dialogWrapper = new Container();
        dialogWrapper.setFluid(true);
        Form dialogContents = new Form();
        dialogContents.setType(FormType.HORIZONTAL);
        final QueryParamDialogBox dialogBox = new QueryParamDialogBox();
        dialogBox.setTitle(Aplikator.application.getConfigString("aplikator.table.queryparams"));
        ModalBody contents = new ModalBody();
        dialogWrapper.add(dialogContents);
        contents.add(dialogWrapper);
        dialogBox.add(contents);

        final int paramCount = view.getActiveQueryDescriptor().getQueryParameters().size();
        final ParamValues[] paramValues = new ParamValues[paramCount];

        final Button closeButton = new Button(Aplikator.application.getConfigString("aplikator.table.ok"), new ClickHandler() {
            @SuppressWarnings("unchecked")
            public void onClick(ClickEvent event) {
                queryParamDialogBoxClosed(dialogBox, paramCount, paramValues);
            }
        });


        for (int i = 0; i < paramCount; i++) {
            FormGroup paramPane = new FormGroup();
            FormLabel paramLabel = new FormLabel();
            paramLabel.setText(view.getActiveQueryDescriptor().getQueryParameters().get(i).getName());
            paramPane.add(paramLabel);

            paramValues[i] = new ParamValues(view.getActiveQueryDescriptor().getQueryParameters().get(i), dialogBox, paramCount, paramValues);
            if (i == 0) {
                dialogBox.firstField = paramValues[i];
            }

            paramPane.add(paramValues[i]);
            dialogContents.add(paramPane);
        }
        ModalFooter buttonPanel = new ModalFooter();
        dialogBox.add(buttonPanel);
        buttonPanel.add(closeButton);
        return dialogBox;
    }

    private void queryParamDialogBoxClosed(Modal dialogBox, int paramCount, ParamValues[] paramValues) {
        dialogBox.hide();
        for (int i = 0; i < paramCount; i++) {
            view.getActiveQueryDescriptor().getQueryParameters().get(i).setValue(paramValues[i].getQueryParamValue());
        }
        refresh();
    }

    public PrimaryKey getSelectedPrimaryKey() {
        RecordDTO rec = getSelectedRecord();
        return rec == null ? null : rec.getPrimaryKey();
    }

    public RecordDTO getSelectedRecord() {
        if (displayMode.equals(DisplayMode.GRID)) {
            return this.gridSelectedRecordDTO;
        } else {
            return this.listSelectedRecordDTO;
        }
    }

    private void sqlSearch() {
        view.setSearchString(searchField.getText());
        indexSearchArgument = null;
        refresh();
    }

    private void indexSearch() {
        indexSearchArgument = (searchField.getText());
        refresh();
    }

    @UiHandler("buttonCreate")
    void buttonCreateClicked(ClickEvent event) {
        RecordDTO recordToCopy = null;
        NativeEvent nEv = event.getNativeEvent();
        if (nEv.getAltKey()) {
            recordToCopy = getSelectedRecord();
        }
        listMode = ListMode.INSERT;
        if (displayMode.equals(DisplayMode.GRID)) {
            formPopup.setPixelSize(Window.getClientWidth() - 40, Window.getClientHeight());
            formPopup.show();
            formForGrid.addRecord(recordContainerDTO, ownerProperty != null ? ownerProperty.getId() : null, ownerPrimaryKey, recordToCopy, new AddRecordCallback() {
                @Override
                public void recordAdded(RecordDTO recordDTO) {
                    recordDTO.setPreviewProperty(Aplikator.application.getConfigString("aplikator.table.newRecordLabel"));
                    editingNewRecord = true;
                    gridSelectionModel.clear();
                    gridDataProvider.insertRecord(recordDTO);
                    gridSelectionModel.setSelected(recordDTO, true);
                }
            });

        } else {
            formHolder.showWidget(1);

            form.addRecord(recordContainerDTO, ownerProperty != null ? ownerProperty.getId() : null, ownerPrimaryKey, recordToCopy, new AddRecordCallback() {
                @Override
                public void recordAdded(RecordDTO recordDTO) {
                    recordDTO.setPreviewProperty(Aplikator.application.getConfigString("aplikator.table.newRecordLabel"));
                    editingNewRecord = true;
                    listSelectionModel.clear();
                    listDataProvider.insertRecord(recordDTO);
                    listSelectionModel.setSelected(recordDTO, true);
                }
            });
            formScroller.setVerticalScrollPosition(0);

        }
    }

    @UiHandler("buttonCancel")
    void buttonCancelClicked(ClickEvent event) {
        buttonSave.setEnabled(false);
        buttonCancel.setEnabled(false);
        form.cancel();
        if (editingNewRecord) {
            listDataProvider.removeFirstRecord();
        }
        editingNewRecord = false;

    }

    private void openForm() {
        form.displayRecord(getSelectedPrimaryKey(), recordContainerDTO, ownerProperty != null ? ownerProperty.getId() : null, ownerPrimaryKey, null);
        formScroller.setVerticalScrollPosition(0);
    }

    @UiHandler("buttonSave")
    void buttonSaveClicked(ClickEvent event) {
        buttonSave.setEnabled(false);
        buttonCancel.setEnabled(false);
        form.recordContainerDTO.setFunctionResult(new FunctionResult("", FunctionResultStatus.SUCCESS));
        form.save(true);
    }

    public void updateEditedRecord(RecordDTO savedRecordDTO) {
        if (displayMode.equals(DisplayMode.LIST)) {
            if (editingNewRecord) {
                if (listSelectedRecordDTO != null && listSelectedRecordDTO.getPrimaryKey().equals(savedRecordDTO.getPrimaryKey())) {
                    listSelectionModel.setSelected(listSelectedRecordDTO, false);//remove temporary record from selection before changing ID and reselect afterwards
                    listSelectedRecordDTO.getPrimaryKey().setId(savedRecordDTO.getPrimaryKey().getId());
                    listSelectionModel.setSelected(listSelectedRecordDTO, true);
                } else {
                    Messages.warning("Inconsistent state: selected pk:" + listSelectedRecordDTO.getPrimaryKey() + "\nsaved pk:" + savedRecordDTO.getPrimaryKey());
                }
                editingNewRecord = false;
            }

            if (listSelectedRecordDTO != null && listSelectedRecordDTO.getPrimaryKey().equals(savedRecordDTO.getPrimaryKey())) {
                LOG.fine("UPDATE EDITED RECORD:" + listSelectedRecordDTO.getPrimaryKey() + " - " + savedRecordDTO.getPreview());
                listSelectedRecordDTO.setPreviewProperty(savedRecordDTO.getPreviewProperty());
                for (String s : savedRecordDTO.getProperties()) {
                    listSelectedRecordDTO.setValue(s, savedRecordDTO.getValue(s));
                }
                cellList.redraw();
            }
        } else {
            if (editingNewRecord) {
                if (gridSelectedRecordDTO != null && gridSelectedRecordDTO.getPrimaryKey().equals(savedRecordDTO.getPrimaryKey())) {
                    gridSelectionModel.setSelected(gridSelectedRecordDTO, false);//remove temporary record from selection before changing ID and reselect afterwards
                    gridSelectedRecordDTO.getPrimaryKey().setId(savedRecordDTO.getPrimaryKey().getId());
                    gridSelectionModel.setSelected(gridSelectedRecordDTO, true);
                } else {
                    Messages.warning("Inconsistent state: selected pk:" + gridSelectedRecordDTO.getPrimaryKey() + "\nsaved pk:" + savedRecordDTO.getPrimaryKey());
                }
                editingNewRecord = false;
            }
            for (RecordDTO visibleItem : grid.getVisibleItems()) {
                if (visibleItem.getPrimaryKey().equals(savedRecordDTO.getPrimaryKey())) {
                    visibleItem.setPreviewProperty(savedRecordDTO.getPreviewProperty());
                    for (String s : savedRecordDTO.getProperties()) {
                        visibleItem.setValue(s, savedRecordDTO.getValue(s));
                    }
                    grid.redraw();
                    break;
                }
            }
        }

    }

    @Override
    public void save() {
        //Window.alert("SAVE "+this.view.getId()+" DIRTY:" + form.isDirty());
        if (form != null && ownerProperty == null) {
            form.recordContainerDTO.setFunctionResult(new FunctionResult("", FunctionResultStatus.SUCCESS));
            form.save(true);
        }
    }

    @UiHandler("buttonDelete")
    void buttonDeleteClicked(ClickEvent event) {
        createDeleteConfirmDialogBox().show();
    }

    private Modal createDeleteConfirmDialogBox() {
        // Create a dialog box and set the caption text
        int numberOfRecords = 0;
        if (displayMode.equals(DisplayMode.LIST)) {
            numberOfRecords = 1;
        } else {
            numberOfRecords = gridSelectionModel.getSelectedSet().size();

        }

        // Create a table to layout the content
        final Modal dialogBox = new Modal();
        dialogBox.setDataBackdrop(ModalBackdrop.STATIC);
        dialogBox.setTitle(Aplikator.application.getConfigString("aplikator.table.confirmation"));
        ModalBody contents = new ModalBody();
        contents.add(new Span(Aplikator.application.getConfigString("aplikator.table.confirmdelete") + " (" + numberOfRecords + ")"));
        dialogBox.add(contents);

        // Add a close button at the bottom of the dialog
        Button okButton = new Button(Aplikator.application.getConfigString("aplikator.table.ok"), new ClickHandler() {
            @SuppressWarnings("unchecked")
            public void onClick(ClickEvent event) {
                dialogBox.hide();
                //if (ownerProperty != null) {
                // recordContainer.addRecord(view, getSelectedRecord(), null, Operation.DELETE);
                // pageData.remove(getSelectedRecord());
                // redrawPage();
                //} else {
                RecordContainerDTO newContainer = new RecordContainerDTO();
                if (displayMode.equals(DisplayMode.LIST)) {
                    //for (Record toDelete : listSelectionModel.getSelectedList()) {
                    newContainer.addRecord(view.getId(), listSelectionModel.getSelectedObject(), listSelectionModel.getSelectedObject(), Operation.DELETE);
                    //}
                } else {
                    for (RecordDTO toDelete : gridSelectionModel.getSelectedSet()) {
                        newContainer.addRecord(view.getId(), toDelete, toDelete, Operation.DELETE);
                    }
                }
                try {
                    RestClient.create(AplikatorService.class,
                            new RemoteCallback<RecordContainerDTO>() {
                                @Override
                                public void callback(RecordContainerDTO records) {
                                    if (records.getFunctionResult() == null) {
                                        Messages.success(Aplikator.application.getConfigString("aplikator.table.deletedok"));
                                    } else {
                                        FunctionResult functionResult = records.getFunctionResult();
                                        if (functionResult.getStatus().equals(FunctionResultStatus.SUCCESS)) {
                                            Messages.success(functionResult.getMessage().isEmpty() ? Aplikator.application.getConfigString("aplikator.table.deletedok") : functionResult.getMessage());
                                        } else if (functionResult.getStatus().equals(FunctionResultStatus.WARNING)) {
                                            Messages.messageWithDetail(records.getFunctionResult().getMessage(), records.getFunctionResult().getDetail(), FunctionResultStatus.WARNING);
                                        } else {
                                            Messages.messageWithDetail(records.getFunctionResult().getMessage(), records.getFunctionResult().getDetail(), FunctionResultStatus.ERROR);
                                        }

                                    }
                                    if (records != null) {
                                        records.clear();
                                    }
                                    if (form != null) {
                                        form.clear();
                                    }
                                    if (formForGrid != null) {
                                        formForGrid.clear();
                                    }
                                    refresh();
                                }
                            },
                            new AplikatorErrorCallback("aplikator.table.deletederror")
                    ).processRecords(newContainer);
                } catch (AplikatorException e) {
                    Window.alert("CHYBA: " + e);
                }
                //}

            }
        });

        Button cancelButton = new Button(Aplikator.application.getConfigString("aplikator.table.cancel"), new ClickHandler() {
            @SuppressWarnings("unchecked")
            public void onClick(ClickEvent event) {
                dialogBox.hide();

            }
        });
        ModalFooter buttonPanel = new ModalFooter();
        dialogBox.add(buttonPanel);
        buttonPanel.add(cancelButton);
        buttonPanel.add(okButton);


        // Return the dialog box
        return dialogBox;
    }

    @UiHandler("buttonReload")
    void buttonReloadClicked(ClickEvent event) {
        reload();
    }

    public PropertyDTO getOwnerProperty() {
        return ownerProperty;
    }

    public PrimaryKey getOwnerPrimaryKey() {
        return ownerPrimaryKey;
    }

    public void setOwnerPrimaryKey(PrimaryKey value) {
        ownerPrimaryKey = value;
    }

    public void setRecordContainerDTO(RecordContainerDTO recordContainerDTO) {
        this.recordContainerDTO = recordContainerDTO;
    }

    public void setDirty(boolean dirty) {
        // for (Form nestedForm:nestedForms){
        // nestedForm.setDirty(dirty);
        // }
    }

    @Override
    public void setAccess(Access access) {
        this.view.setAccess(access);
        this.form.setEnabled(!access.isReadOnly());
        this.formForGrid.setEnabled(!access.isReadOnly());
    }

    @Override
    public Access getAccess() {
        return this.view.getAccess();
    }

    @Override
    public void clear() {

    }


    void setListAccess(boolean active) {
        if (!active) {
            cellList.getElement().addClassName("inactive");
        } else {
            cellList.getElement().removeClassName("inactive");
        }
    }

    public void addNestedEditorNotification(NestedEditorNotification nestedEditorNotification) {
        this.nestedEditorNotification = nestedEditorNotification;
    }

    private enum ListMode {STANDARD, INSERT, INDEX}

    private enum DisplayMode {LIST, GRID}

    interface TableListerWidgetUiBinder extends UiBinder<Div, TableListerWidget> {
    }

    public interface TableListerWidgetResources extends CellList.Resources {
        public static final TableListerWidgetResources INSTANCE = GWT.create(TableListerWidgetResources.class);


        @Source("TableListerCellList.css")
        CellList.Style cellListStyle();


    }

    public interface DataGridResources extends CellTable.Resources {
        public static final DataGridResources INSTANCE = GWT.create(DataGridResources.class);


        @Source("TableListerCellTable.css")
        CellTable.Style cellTableStyle();
    }

    private static class QueryParamDialogBox extends Modal {
        ParamValues firstField;

        @Override
        public void show() {
            super.show();
            if (firstField != null) {
                firstField.setFocus();
            }
        }
    }

    private static class RecordCell extends AbstractCell<RecordDTO> {
        @Override
        public void render(Context ctx, RecordDTO value, SafeHtmlBuilder sb) {
            if (value != null) {
                String preview = value.getPreview();
                if (preview == null || "".equals(preview)) {
                    preview = "&nbsp;";
                }
                sb.appendHtmlConstant(preview);
            }
        }
    }

    private class ParamValues extends Composite {
        TextBox queryTextField;
        ListBox queryListField;
        CheckBox queryCheckboxField;
        DateTimePicker queryDateField;
        QueryParameterDTO queryParameter;
        Div holder;

        ParamValues(QueryParameterDTO queryParameter, final Modal dialogBox, final int paramCount, final ParamValues[] paramValues) {
            this.queryParameter = queryParameter;
            holder = new Div();
            initWidget(holder);
            if ("java.util.Date".equals(queryParameter.getProperty().getType())) {
                queryDateField = new DateTimePicker();
                String formatPattern = queryParameter.getProperty().getFormatPattern();
                adjustFormatPattern(queryDateField, formatPattern);
                queryDateField.setAutoClose(true);
                queryDateField.setValue(null);
                holder.add(queryDateField);
            } else if ("java.lang.Boolean".equals(queryParameter.getProperty().getType())) {
                queryCheckboxField = new CheckBox();
                queryCheckboxField.setValue(false);
                holder.add(queryCheckboxField);
            } else if (queryParameter.getProperty().getListValues() != null) {
                queryListField = new ListBox();
                for (org.aplikator.client.shared.data.ListItem item : queryParameter.getProperty().getListValues()) {
                    queryListField.addItem(item.getName(), item.getValue().toString());
                }
                queryListField.setSelectedIndex(0);
                holder.add(queryListField);

            } else {
                queryTextField = new TextBox();
                queryTextField.setText("");
                holder.add(queryTextField);
                queryTextField.addKeyUpHandler(new KeyUpHandler() {

                    @Override
                    public void onKeyUp(KeyUpEvent event) {
                        event.stopPropagation();
                        if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
                            queryParamDialogBoxClosed(dialogBox, paramCount, paramValues);
                        }
                    }
                });
            }
        }

        String getQueryParamValue() {
            if ("java.util.Date".equals(queryParameter.getProperty().getType())) {
                if (queryDateField.getValue() != null) {
                    return Long.toString(queryDateField.getValue().getTime());
                } else {
                    return null;
                }
            } else if ("java.lang.Boolean".equals(queryParameter.getProperty().getType())) {
                if (queryCheckboxField.getValue() != null) {
                    return Boolean.toString(queryCheckboxField.getValue());
                } else {
                    return null;
                }
            } else if (queryParameter.getProperty().getListValues() != null) {
                return queryListField.getSelectedValue();
            } else {
                return queryTextField.getValue();
            }
        }

        void setFocus() {
            if (queryTextField != null) {
                queryTextField.setFocus(true);
            } else if (queryListField != null) {
                queryListField.setFocus(true);
            }
        }
    }

    /**
     * A scrolling pager that automatically increases the range every time the
     * scroll bar reaches the bottom.
     */
    class ShowMorePagerPanel extends AbstractPager {

        /**
         * The default increment size.
         */
        private static final int DEFAULT_INCREMENT = 20;
        /**
         * The scrollable panel.
         */
        private final ScrollPanel scrollable;
        /**
         * The increment size.
         */
        private int incrementSize = DEFAULT_INCREMENT;
        /**
         * The last scroll position.
         */
        private int lastScrollPos = 0;

        /**
         * Construct a new {@link ShowMorePagerPanel}.
         */
        public ShowMorePagerPanel(ScrollPanel scrollPanel) {
            scrollable = scrollPanel;
            initWidget(scrollable);

            // Do not let the scrollable take tab focus.
            scrollable.getElement().setTabIndex(-1);

            // Handle scroll events.
            scrollable.addScrollHandler(new ScrollHandler() {
                @Override
                public void onScroll(ScrollEvent event) {

                    HasRows display = getDisplay();
                    if (display == null) {
                        return;
                    }
                    // If scrolling up, ignore the event.
                    int oldScrollPos = lastScrollPos;
                    lastScrollPos = scrollable.getVerticalScrollPosition();
                    //LOG.info(TableListerWidget.this.view.getLocalizedName()+" OldScrollPos:" + oldScrollPos + " LastScrollPos:" + lastScrollPos);
                    if (oldScrollPos >= lastScrollPos) {
                        return;
                    }


                    int maxScrollTop = scrollable.getWidget().getOffsetHeight()
                            - scrollable.getOffsetHeight();
                    //LOG.info(TableListerWidget.this.view.getLocalizedName()+" Lastscrollpos:"+lastScrollPos+ " maxScrollTop:"+maxScrollTop);
                    //LOG.info(TableListerWidget.this.view.getLocalizedName()+" WidgetOffset:" + scrollable.getWidget().getOffsetHeight() + " panelOffset:" + scrollable.getOffsetHeight());
                    if (lastScrollPos >= maxScrollTop - 10) {
                        // We are near the end, so increase the page size.

                        int newPageSize = Math.min(
                                display.getVisibleRange().getLength() + incrementSize,
                                display.getRowCount());
                        //LOG.info(TableListerWidget.this.view.getLocalizedName()+" Extending scroll down. Size:" + newPageSize);

                        display.setVisibleRange(0, newPageSize);
                    }
                }
            });
        }

        /**
         * Get the number of rows by which the range is increased when the scrollbar
         * reaches the bottom.
         *
         * @return the increment size
         */
        public int getIncrementSize() {
            return incrementSize;
        }

        /**
         * Set the number of rows by which the range is increased when the scrollbar
         * reaches the bottom.
         *
         * @param incrementSize the incremental number of rows
         */
        public void setIncrementSize(int incrementSize) {
            this.incrementSize = incrementSize;
        }

        @Override
        public void setDisplay(HasRows display) {
            assert display instanceof Widget : "display must extend Widget";
            scrollable.setWidget((Widget) display);
            super.setDisplay(display);
        }

        @Override
        protected void onRangeOrRowCountChanged() {

        }
    }

    private class RangeLabelPager extends AbstractPager {

        /**
         * The label that shows the current range.
         */
        private final Span label = new Span();

        /**
         * Construct a new {@link RangeLabelPager}.
         */
        public RangeLabelPager() {
            initWidget(label);
            //label.addStyleName("control-label");
        }

        @Override
        protected void onRangeOrRowCountChanged() {
            if (listMode.equals(ListMode.STANDARD) || listMode.equals(ListMode.INDEX)) {
                NumberFormat formatter = NumberFormat.getFormat("#,###");
                String decimalSeparator = Aplikator.application.getConfigString("aplikator.table.rangeSeparator");
                HasRows display = getDisplay();
                Range range = display.getVisibleRange();
                int start = range.getStart();
                int end = Math.min(start + range.getLength(), display.getRowCount());
                label.setText(formatter.format(start + 1).replace(",", decimalSeparator)
                        + " - "
                        + formatter.format(end).replace(",", decimalSeparator)
                        + " : "
                        + formatter.format(display.getRowCount()).replace(",", decimalSeparator));
            } else if (listMode.equals(ListMode.INSERT)) {
                label.setText(Aplikator.application.getConfigString("aplikator.table.insertMode"));
            }
        }
    }

    private class AplikatorDataProvider extends AsyncDataProvider<RecordDTO> {

        private final ScrollPanel scrollable;
        private final int incrementSize;
        private final List<RecordDTO> recordCache = new ArrayList<RecordDTO>();
        private final List<RecordDTO> insertList = new ArrayList<RecordDTO>();
        private boolean waitingForNextPage = false;

        public AplikatorDataProvider(ScrollPanel scrollPanel, int inc) {
            scrollable = scrollPanel;
            incrementSize = inc;
        }

        /**
         * {@link #onRangeChanged(HasData)} is called when the table requests a new
         * range of data. You can push data back to the displays using
         * {@link #updateRowData(int, List)}.
         */
        @Override
        protected void onRangeChanged(final HasData<RecordDTO> display) {
            if (listMode.equals(ListMode.STANDARD)) {
                // Get the new range.
                final Range range = display.getVisibleRange();
                final int start = range.getStart();
                final int length = range.getLength();
                if (recordCount == 0 || length == 0) {
                    display.setRowCount(0);
                    return;
                }
                if (length > recordCache.size() && !waitingForNextPage) {
                    if (ownerProperty != null && (ownerPrimaryKey == null || ownerPrimaryKey.getId() == -1)) {
                        return;
                    }
                    final int pageStart = recordCache.size();
                    final int pageSize = length - pageStart;
                    //LOG.fine("LOADING PAGE:"+pageStart+"-"+length);
                    if (initialized) {
                        waitingForNextPage = true;
                        RestClient.create(AplikatorService.class,
                                new RemoteCallback<RecordsPageDTO>() {
                                    @Override
                                    public void callback(RecordsPageDTO resp) {
                                        waitingForNextPage = false;
                                        if (resp.getOffset() == recordCache.size()) {
                                            processPageCallback(resp.getRecords(), start, display);
                                        }
                                    }
                                },
                                new AplikatorErrorCallback("aplikator.table.loaderror")
                        ).getRecords(view.getId(), view.getActiveFilter(), view.getActiveSort(), view.getActiveQueryDescriptor(), view.getSearchString(), ownerProperty != null ? ownerProperty.getId() : null, (ownerPrimaryKey == null) ? null : ownerPrimaryKey.getSerializationString(), pageStart, pageSize);
                    }
                }
            } else if (listMode.equals(ListMode.INSERT)) {
                updateRowCount(insertList.size(), true);
                updateRowData(0, insertList);
                if ((listSelectionModel.getSelectedObject() == null /*|| listSelectionModel.getSelectedList().size() == 0*/) && insertList.size() > 0) {
                    listSelectionModel.setSelected(insertList.get(0), true);
                }
            } else if (listMode.equals(ListMode.INDEX)) {
                // Get the new range.
                final Range range = display.getVisibleRange();
                final int start = range.getStart();
                final int length = range.getLength();
                //if (recordCount == 0 || length == 0)
                //    return;
                if (length > recordCache.size() && !waitingForNextPage) {
                    final int pageStart = recordCache.size();
                    final int pageSize = length - pageStart;
                    waitingForNextPage = true;
                    RestClient.create(AplikatorService.class,
                            new RemoteCallback<SearchResult>() {
                                @Override
                                public void callback(SearchResult resp) {
                                    recordCount = resp.getCount().intValue();
                                    listDataProvider.updateRowCount(recordCount, true);
                                    processPageCallback(resp.getRecords(), start, display);
                                }
                            },
                            new AplikatorErrorCallback("aplikator.table.loaderror")
                    ).getPageSearch(view.getId(), indexSearchArgument, pageStart, pageSize);
                }
            }
        }

        private void processPageCallback(List<RecordDTO> newPage, int start, HasData<RecordDTO> display) {
            int oldListEnd = recordCache.size();
            recordCache.addAll(newPage);
            waitingForNextPage = false;
            refreshListAndExtendScroll(oldListEnd, newPage, display);
            if ((listSelectionModel.getSelectedObject() == null /* || listSelectionModel.getSelectedList().size() == 0 */) && newPage.size() > 0) {
                listSelectionModel.setSelected(newPage.get(0), true);
                if (ownerProperty == null) {
                    cellList.setKeyboardSelectedRow(0, true);
                }
            }
        }

        public void refreshListAndExtendScroll(int start, List<RecordDTO> newPage, HasData<RecordDTO> display) {

            if (cellList.getKeyboardSelectedRow() >= 0 && cellList.getKeyboardSelectedRow() <= (start - pageSize - 1)) {
                //Window.alert("SELECTED:"+cellList.getKeyboardSelectedRow()+" START:"+start+" pageSize:"+pageSize);
                scrollable.getElement().focus();
            }
            if (newPage == null && start == 0) {
                updateRowData(start, recordCache);
            } else {
                updateRowData(start, newPage);
            }
            int scrollpos = scrollable.getMaximumVerticalScrollPosition();
            if (TableListerWidget.this.ownerProperty == null && scrollpos <= 0 && scrollable.getElement().getClientHeight() > Window.getClientHeight()) {
                return;  //Safeguard against unreliable scroll.getMaximumVerticalScrollPosition() (but only for top level tables
            }
            int rangestart = display.getVisibleRange().getStart();
            int rangelength = display.getVisibleRange().getLength();
            int rowcount = display.getRowCount();
            int newPageSize = Math.min(
                    display.getVisibleRange().getLength() + incrementSize,
                    display.getRowCount());
            //LOG.fine("???Display "+TableListerWidget.this.view.getLocalizedName()+" VISIBLE:"+scrollable.isVisible()+"!!!Size:" + newPageSize + " scrolpos:" + scrollpos + " rangestart:" + rangestart + " rangelength:" + rangelength + " rowcount:" + rowcount);
            if (scrollpos <= 0 && (rangestart + rangelength < rowcount)) {
                //LOG.fine("!!!Display"+TableListerWidget.this.view.getLocalizedName()+" Extending scroll down automatically.-" + (rangestart+rangelength)+"<"+rowcount);
                if (displayMode.equals(DisplayMode.LIST)) {
                    display.setVisibleRange(0, newPageSize);
                }
            }

        }

        public void resetCache() {
            recordCache.clear();
            waitingForNextPage = false;
            insertList.clear();
        }

        public void insertRecord(RecordDTO rec) {
            insertList.add(0, rec);
            updateRowCount(insertList.size(), true);
            updateRowData(0, insertList);
            cellList.redraw();
        }

        public void removeFirstRecord() {
            if (insertList.size() > 0) {
                listSelectionModel.setSelected(insertList.get(0), false);
                insertList.remove(0);
            }
            updateRowCount(insertList.size(), true);
            updateRowData(0, insertList);
            cellList.redraw();
        }
    }

    private class GridDataProvider extends AsyncDataProvider<RecordDTO> {
        private final List<RecordDTO> insertList = new ArrayList<RecordDTO>();


        public GridDataProvider() {
        }

        /**
         * {@link #onRangeChanged(HasData)} is called when the table requests a new
         * range of data. You can push data back to the displays using
         * {@link #updateRowData(int, List)}.
         */
        @Override
        protected void onRangeChanged(final HasData<RecordDTO> display) {
            if (listMode.equals(ListMode.STANDARD)) {

                // Get the new range.
                final Range range = display.getVisibleRange();
                final int start = range.getStart();
                final int length = range.getLength();
                if (recordCount == 0 || length == 0) {
                    display.setRowCount(0);
                    return;
                }

                if (initialized) {
                    RestClient.create(AplikatorService.class,
                            new RemoteCallback<RecordsPageDTO>() {
                                @Override
                                public void callback(RecordsPageDTO resp) {
                                    updateRowData(start, resp.getRecords());
                                    if ((gridSelectionModel.getSelectedSet().size() == 0) && resp.getRecords().size() > 0) {
                                        //gridSelectionModel.setSelected(resp.getRecords().get(0), true);
                                        if (ownerProperty == null) {
                                            grid.setKeyboardSelectedRow(0, true);
                                            focusGrid();
                                        }
                                    }
                                }
                            },
                            new AplikatorErrorCallback("aplikator.table.loaderror")
                    ).getRecords(view.getId(), view.getActiveFilter(), view.getActiveSort(), view.getActiveQueryDescriptor(), view.getSearchString(), ownerProperty != null ? ownerProperty.getId() : null, (ownerPrimaryKey == null) ? null : ownerPrimaryKey.getSerializationString(), start, length);
                }
            } else if (listMode.equals(ListMode.INSERT)) {
                updateRowCount(insertList.size(), true);
                updateRowData(0, insertList);
                if ((gridSelectionModel.getSelectedSet().size() == 0) && insertList.size() > 0) {
                    gridSelectionModel.setSelected(insertList.get(0), true);
                }
            }
        }

        public void resetCache() {
            insertList.clear();
        }

        public void insertRecord(RecordDTO rec) {
            insertList.add(0, rec);
            updateRowCount(insertList.size(), true);
            updateRowData(0, insertList);
            grid.redraw();
        }

        public void removeFirstRecord() {
            if (insertList.size() > 0) {
                gridSelectionModel.setSelected(insertList.get(0), false);
                insertList.remove(0);
            }
            updateRowCount(insertList.size(), true);
            updateRowData(0, insertList);
            grid.redraw();
        }

    }


}
