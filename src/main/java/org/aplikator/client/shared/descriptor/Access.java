package org.aplikator.client.shared.descriptor;

import org.jboss.errai.common.client.api.annotations.Portable;

/**
 *
 */
@Portable
public enum Access {
    NONE, READ_THUMBNAIL, READ_PREVIEW, READ, READ_WRITE, READ_WRITE_CREATE, READ_WRITE_CREATE_DELETE;

    public boolean isReadOnly() {
        return this.equals(READ) || this.equals(READ_PREVIEW) || this.equals(READ_THUMBNAIL) || this.equals(NONE);
    }

    public boolean canWrite() {
        return this.equals(READ_WRITE) || this.equals(READ_WRITE_CREATE) || this.equals(READ_WRITE_CREATE_DELETE) ;
    }

    public boolean canCreate() {
        return this.equals(READ_WRITE_CREATE) || this.equals(READ_WRITE_CREATE_DELETE) ;
    }

    public boolean canDelete() {
        return  this.equals(READ_WRITE_CREATE_DELETE) ;
    }

}
