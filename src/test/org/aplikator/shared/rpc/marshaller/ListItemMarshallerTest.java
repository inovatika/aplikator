package org.aplikator.shared.rpc.marshaller;

import org.aplikator.client.shared.data.ListItem;
import org.aplikator.client.shared.rpc.marshaller.ListItemMarshaller;
import org.jboss.errai.marshalling.server.JSONDecoder;
import org.junit.Assert;

import junit.framework.TestCase;

public class ListItemMarshallerTest extends TestCase {

    public void testMarshall() {
        ListItemMarshaller lm = new ListItemMarshaller();
        ListItem.Default val1 = new ListItem.Default("stringvalue", "string");

        ListItem demarshalled1 = lm.demarshall(
                JSONDecoder.decode(lm.marshall(val1, null)), null);
        Assert.assertEquals(val1.getName(), demarshalled1.getName());
        Assert.assertEquals(val1.getValue(), demarshalled1.getValue());

        ListItem.Default val2 = new ListItem.Default(2, "string");
        ListItem demarshalled2 = lm.demarshall(
                JSONDecoder.decode(lm.marshall(val2, null)), null);
        Assert.assertEquals(val2.getName(), demarshalled2.getName());
        Assert.assertEquals(val2.getValue(), demarshalled2.getValue());

        ListItem.Default val3 = new ListItem.Default(true, "string");
        ListItem demarshalled3 = lm.demarshall(
                JSONDecoder.decode(lm.marshall(val3, null)), null);
        Assert.assertEquals(val3.getName(), demarshalled3.getName());
        Assert.assertEquals(val3.getValue(), demarshalled3.getValue());

    }
}
