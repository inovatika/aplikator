package org.aplikator.server.security;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import waffle.servlet.WindowsPrincipal;

/**
 *
 */
public class SSOGroupMap {

    private SSOGroupMap(){}

    private static final String ROLE_NAMES_DELIMETER = ",";

    /** The group roles map. */
    private static final Map<String, String> groupRolesMap = new HashMap<>();

    /**
     * Sets the translation from group names to role names. If not set, the map is empty, resulting in no users getting
     * roles.
     *
     * @param value
     *            the group roles map to set
     */
    public static void setGroupRolesMap(final Map<String, String> value) {
        groupRolesMap.clear();
        if (value != null) {
            groupRolesMap.putAll(value);
        }
    }

    static Collection<String> getRoleNamesForGroups(final Collection<String> groupNames) {
        final Set<String> roleNames = new HashSet<>();
        for (final String groupName : groupNames) {
            final String strRoleNames = groupRolesMap.get(groupName);


            if (strRoleNames != null) {
                for (String roleName : strRoleNames.split(ROLE_NAMES_DELIMETER)) {


                    roleNames.add(roleName);

                }
            }
        }
        return roleNames;
    }


    private static HashMap<WindowsPrincipal, Collection<String>> roleCache = new HashMap<>();

    public static boolean isUserInRole(WindowsPrincipal principal, String role){
        Collection<String> roleNamesForGroups = roleCache.get(principal);
        if (roleNamesForGroups == null) {
            roleNamesForGroups = getRoleNamesForGroups(principal.getGroups().keySet());
            roleCache.put(principal, roleNamesForGroups);
        }
        return roleNamesForGroups.contains(role);
    }

    private static boolean useSSO = false;

    public static void activateSSO (){
        useSSO = true;
    }

    public static boolean useSSO(){
        return useSSO;
    }

}
