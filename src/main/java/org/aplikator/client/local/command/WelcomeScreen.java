package org.aplikator.client.local.command;

import org.aplikator.client.shared.descriptor.ViewDTO;
import org.gwtbootstrap3.client.ui.Container;
import org.gwtbootstrap3.client.ui.TextBox;
import org.gwtbootstrap3.client.ui.constants.ColumnSize;
import org.jboss.errai.ui.nav.client.local.DefaultPage;
import org.jboss.errai.ui.nav.client.local.Page;

import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;

@Page(role = DefaultPage.class, path = "home")
public class WelcomeScreen extends Composite {


    public WelcomeScreen() {
        initWidget(createTable());
    }

    protected ViewDTO view;

    private static TextBox search = new TextBox();


    private void drawPanel(AcceptsOneWidget panel) {
        if (widget == null) {
            widget = createTable();
        }
        panel.setWidget(widget);


    }


    private Widget widget = null;

    private Widget createTable() {
        //search.setPlaceholder(Aplikator.application.getConfigString("aplikator.menu.search"));
        search.addStyleName(ColumnSize.XS_1.getCssName());
        //boolean isSearchEnabled = Boolean.parseBoolean( Aplikator.application.getConfigString(Search.SEARCH_ENABLED));
//        if (view.getEntity().isIndexed()&&isSearchEnabled) {
//            KeyDownHandler keyDownHandler = new KeyDownHandler() {
//
//                @Override
//                public void onKeyDown(KeyDownEvent event) {
//                    event.stopPropagation();
//                    if (event.getNativeKeyCode()== KeyCodes.KEY_ENTER) {
//                        String  input = search.getValue();
//                        if (input.trim().length()>0) {
//                            table.reload(input);
//                        } else {
//                            table.reload();
//                        }
//                    }
//                }
//            };
//            Aplikator.getSearch().setEnabled(true);
//            submitHandlerRegistration = Aplikator.getSearch().addKeyDownHandler(keyDownHandler);
//        } else {
//            Aplikator.getSearch().setEnabled(false);
//        }

        //search.addStyleName(NavbarConstants.NAVBAR_SEARCH);
        //search.addStyleName(Bootstrap.search_query);
        //search.getElement().getStyle().setMarginRight(10, Unit.PX);

        Container container = new Container();
        //container.add(search);
        container.setFluid(true);
        return container.asWidget();

    }


}
