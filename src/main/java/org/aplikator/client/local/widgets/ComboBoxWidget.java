package org.aplikator.client.local.widgets;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import org.aplikator.client.shared.data.ListItem;
import org.aplikator.client.shared.data.RecordDTO;
import org.aplikator.client.shared.descriptor.AnnotationKeys;
import org.aplikator.client.shared.descriptor.PropertyDTO;
import org.aplikator.client.shared.rpc.marshaller.DemarshallingUtils;
import org.gwtbootstrap3.client.ui.ListBox;

import java.io.Serializable;
import java.util.List;

public class ComboBoxWidget<T extends Serializable> extends DataFieldBase<T> implements DataField<T> {
    private ListBox box = new ListBox();

    public ComboBoxWidget(String caption, String localizedTooltip, PropertyDTO property, int size, boolean horizontal, String elementId, String switcherInit, String customLabelStyle, HasFields form) {
        super(caption, localizedTooltip, property, horizontal, elementId, customLabelStyle, form);
        resetItemList();
        controlHolder.add(box);
        box.addChangeHandler(new ChangeHandler() {
            public void onChange(ChangeEvent event) {
                setDirty(true);
                ValueChangeEvent.fire(ComboBoxWidget.this, null);
            }
        });
        if (switcherInit != null) {
            switcher = new VisibilitySwitcher(switcherInit, property.getType(), form);
            //Window.alert("SWITCHER:"+switcher);
            box.addChangeHandler(new ChangeHandler() {
                public void onChange(ChangeEvent event) {
                    switcher.apply(getValue());
                }
            });
        }

        setGridSize(size);

    }

    private VisibilitySwitcher<T> switcher;

    private List<ListItem> annotationListItems = null;

    @Override
    public void processAnnotations(RecordDTO sourceRecord){
        super.processAnnotations(sourceRecord);
        String annotationListItemsStr = sourceRecord.getPropertyAnnotation(this.getProperty().getId(), AnnotationKeys.LIST_ANNOTATION_KEY);
        if (annotationListItemsStr == null){
            annotationListItems = null;
        } else {
            annotationListItems = DemarshallingUtils.listItemsUnMarshall(annotationListItemsStr);
        }
    }

    private void resetItemList() {
        box.clear();
        if (annotationListItems == null) {
            for (ListItem item : property.getListValues()) {
                box.addItem(item.getName(), item.getValue() != null ? item.getValue().toString() : "");
            }
        } else {
            for (ListItem item : annotationListItems) {
                box.addItem(item.getName(), item.getValue() != null ? item.getValue().toString() : "");
            }
        }
    }

    public void setValue(Serializable value) {
        if (switcher != null){
            switcher.apply((T)value);
        }
        resetItemList();
        box.setSelectedIndex(-1);
        if (annotationListItems == null) {
            for (int i = 0; i < getProperty().getListValues().size(); i++) {
                Serializable listValue = ((ListItem) getProperty().getListValues().get(i)).getValue();
                if (listValue != null && listValue.equals(value)) {
                    box.setSelectedIndex(i);
                    return;
                }
            }
        }else{
            for (int i = 0; i < annotationListItems.size(); i++) {
                Serializable listValue = ((ListItem) annotationListItems.get(i)).getValue();
                if (listValue != null && listValue.equals(value)) {
                    box.setSelectedIndex(i);
                    return;
                }
            }
        }
        if (value != null) {
            box.addItem("! " + value.toString(), value.toString());
            if (annotationListItems == null) {
                box.setSelectedIndex(getProperty().getListValues().size());
            } else {
                box.setSelectedIndex(annotationListItems.size());
            }
        }
    }

    @SuppressWarnings("unchecked")
    public T getValue() {
        int index = box.getSelectedIndex();
        if (index == -1) {
            return null;
        }
        if (annotationListItems == null) {
            return (T) ((ListItem) getProperty().getListValues().get(index)).getValue();
        } else {
            return (T) ((ListItem) annotationListItems.get(index)).getValue();
        }
    }

    public HandlerRegistration addValueChangeHandler(ValueChangeHandler handler) {
        return addHandler(handler, ValueChangeEvent.getType());
    }

    public void setEnabled(boolean enabled) {
        if (property.getRefferedThrough() != null) {
            enabled = false;
        }
        box.setEnabled(enabled);
    }

}
