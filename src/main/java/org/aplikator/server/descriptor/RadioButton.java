package org.aplikator.server.descriptor;

import org.aplikator.client.shared.descriptor.RadioButtonDTO;
import org.aplikator.client.shared.descriptor.WidgetDTO;
import org.aplikator.server.data.Context;

import java.io.Serializable;

public class RadioButton<T extends Serializable> extends WidgetPropertyDescriptorBase<T> {


    public RadioButton(Property<T> property) {
        super(property);
        setHorizontal(true);
        if (property.getListProvider() == null) {
            throw new IllegalStateException("Property " + property.getId() + " must have assigned listProvider");
        }
    }

    private String switcher;

    public RadioButton setSwitcher(String switcher) {
        this.switcher = switcher;
        return this;
    }

    @Override
    public WidgetDTO getWidgetDescriptor(Context ctx) {
        RadioButtonDTO desc = new RadioButtonDTO(getProperty().getPropertyDTO(ctx));
        desc.setSize(getSize());
        desc.setAccess(getProperty().getAccess(ctx));
        desc.setEnabled(isEnabled(ctx));
        desc.setVisible(isVisible(ctx));
        desc.setLocalizedName(getLocalizedName(ctx));
        desc.setLocalizedTooltip(getLocalizedTooltip(ctx));
        desc.setHorizontal(isHorizontal());
        desc.setElementId(getElementId());
        desc.setCustomLabelStyle(getCustomLabelStyle());
        desc.setSwitcher(switcher);
        return desc;
    }


}
