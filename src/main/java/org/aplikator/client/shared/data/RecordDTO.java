package org.aplikator.client.shared.data;

import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.view.client.ProvidesKey;
import org.aplikator.client.shared.descriptor.PropertyDTO;
import org.aplikator.client.shared.descriptor.ViewDTO;
import org.jboss.errai.common.client.api.annotations.Portable;

import java.io.Serializable;
import java.util.*;

@SuppressWarnings("serial")
@Portable
public class RecordDTO implements Serializable {

    /**
     * The key provider that provides the unique ID of a record.
     */
    public static final transient ProvidesKey<RecordDTO> KEY_PROVIDER = new ProvidesKey<RecordDTO>() {
        public Object getKey(RecordDTO item) {
            return item == null ? null : item.getPrimaryKey().getId();
        }
    };

    protected PrimaryKey primaryKey;

    protected HashMap<String, Object> data = new HashMap<>();
    protected Set<String> dirty = new HashSet<String>();

    private PrimaryKey ownerPrimaryKey;

    private String ownerPropertyId;

    private String preview;

    protected HashMap<String, String> recordAnnotations = new HashMap<>();

    public HashMap<String, HashMap<String,String>> propertyAnnotations = new HashMap<>();

    @SuppressWarnings("unused")
    public RecordDTO() {//TODO custom marshaller
    }

    public RecordDTO(PrimaryKey primaryKey) {
        this.primaryKey = primaryKey;
    }

    public void setValue(String propertyId, Object value) {
        data.put(propertyId, value);
        dirty.add(propertyId);
    }

    public Object getValue(String propertyId) {
        return data.get(propertyId);
    }

    public Set<String> getProperties() {
        return data.keySet();
    }

    public PrimaryKey getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(PrimaryKey pk) {
        primaryKey = pk;
    }

    public PrimaryKey getOwnerPrimaryKey() {
        return ownerPrimaryKey;
    }

    public void setOwnerPrimaryKey(PrimaryKey ownerPrimaryKey) {
        this.ownerPrimaryKey = ownerPrimaryKey;
    }

    public String getOwnerPropertyId() {
        return ownerPropertyId;
    }

    public void setOwnerPropertyId(String ownerPropertyId) {
        this.ownerPropertyId = ownerPropertyId;
    }

    public RecordDTO clone() {
        RecordDTO retval = new RecordDTO(new PrimaryKey(primaryKey.getSerializationString()));
        retval.ownerPrimaryKey = ownerPrimaryKey;
        retval.ownerPropertyId = ownerPropertyId;
        for (String propId : data.keySet()) {
            retval.data.put(propId, data.get(propId));
        }
        for (String s : dirty) {
            retval.dirty.add(s);
        }
        for (String annotKey : this.recordAnnotations.keySet()) {
            retval.putAnnotation(annotKey, this.recordAnnotations.get(annotKey));
        }
        for (String annotKey : this.propertyAnnotations.keySet()) {
            retval.propertyAnnotations.put(annotKey, this.propertyAnnotations.get(annotKey));
        }
        return retval;
    }

    @Override
    public String toString() {
        return data.toString();
    }

    public String toHTML(ViewDTO view) {
        return toSafeHTML(view).asString();
    }

    public SafeHtml toSafeHTML(ViewDTO view) {
        SafeHtmlBuilder sb = new SafeHtmlBuilder();
        boolean dataAdded = false;
        if (view != null) {
            for (PropertyDTO prop : view.getProperties()) {
                Object value = data.get(prop.getId());
                appendValueForProperty(sb, value);
                dataAdded = true;
            }
        } else {
            for (String propId : data.keySet()) {
                Object value = data.get(propId);
                appendValueForProperty(sb, value);
                dataAdded = true;
            }
        }
        if (!dataAdded){
            sb.appendHtmlConstant("&nbsp;");
        }
        return sb.toSafeHtml();
    }

    private void appendValueForProperty(SafeHtmlBuilder sb, Object value) {
        //sb.appendEscaped(propId);
        //sb.appendHtmlConstant(":<b>");
        sb.appendEscaped(value != null ? value.toString() : "");
        //sb.appendHtmlConstant("</b>");
        sb.appendHtmlConstant("<br/>");
    }

    public String getPreview() {
        return preview != null ? preview : toHTML(null);
    }

    public void setPreview(SafeHtml safePreview) {
        this.preview = safePreview.asString();
    }

    /**
     * used only for custom marshallers
     *
     * @return
     */
    public String getPreviewProperty() {
        return this.preview;
    }

    /**
     * used only for custom marshallers
     *
     * @param preview
     */
    public void setPreviewProperty(String preview) {
        this.preview = preview;
    }


    public boolean isDirty(String propertyId) {
        return dirty.contains(propertyId);
    }


    public void resetDirtyFlags() {
        dirty.clear();
    }

    public void putAnnotation(String key, String value) {
        this.recordAnnotations.put(key, value);
    }

    public void removeAnnotation(String key) {
        this.recordAnnotations.remove(key);
    }

    public String getAnnotation(String key) {
        return this.recordAnnotations.get(key);
    }

    public Set<String> getAnnotations() {
        return recordAnnotations.keySet();
    }

    public boolean hasAnnotation(String key) {
        return this.recordAnnotations.containsKey(key);
    }

    public void putPropertyAnnotation(String property, String key, String value) {
        if( ! this.propertyAnnotations.containsKey(property)){
            this.propertyAnnotations.put(property, new HashMap<>());
        }
        this.propertyAnnotations.get(property).put(key, value);
    }

    public void removePropertyAnnotation(String property, String key) {
        if( this.propertyAnnotations.containsKey(property)){
            this.propertyAnnotations.get(property).remove(key);
        }
    }

    public String getPropertyAnnotation(String property, String key) {
        if( this.propertyAnnotations.containsKey(property)){
            return this.propertyAnnotations.get(property).get(key);
        } else {
            return null;
        }
    }

    public Set<String> getPropertyAnnotations() {
        return propertyAnnotations.keySet();
    }

    public boolean hasPropertyAnnotation(String property, String key) {
        if( this.propertyAnnotations.containsKey(property)){
            return this.propertyAnnotations.get(property).containsKey(key);
        } else {
            return false;
        }
    }

    public void setDirty(String k) {
        if (!this.dirty.contains(k)) {
            this.dirty.add(k);
        }
    }

    public List<String> getDirtyFields() {
        return new ArrayList<String>(this.dirty);
    }

}
