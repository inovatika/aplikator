package org.aplikator.client.shared.descriptor;

import com.google.gwt.user.client.ui.Widget;
import org.aplikator.client.local.widgets.HasFields;
import org.aplikator.client.local.widgets.RadioSwitchWidget;

@SuppressWarnings("serial")
//@Portable
public class RadioSwitchDTO extends WidgetPropertyDTOBase {


    @SuppressWarnings("unused")
    public RadioSwitchDTO() {//TODO custom marshaller
    }

    public RadioSwitchDTO(PropertyDTO property) {
        super(property);
    }

    private String switcher;

    public String getSwitcher() {
        return switcher;
    }

    public void setSwitcher(String switcher) {
        this.switcher = switcher;
    }

    @Override
    public Widget getWidget(HasFields form) {
        RadioSwitchWidget field = new RadioSwitchWidget(getLocalizedName(), getLocalizedTooltip(), property, getSize(), isHorizontal(), getElementId(), getSwitcher(), getCustomLabelStyle(), form);
        field.setEnabled(isEnabled());
        field.setVisible(isVisible());
        field.setDefaultEnabled(isEnabled());
        if (form != null) {
            form.registerDataField(field);
        }
        return field;
    }

}
