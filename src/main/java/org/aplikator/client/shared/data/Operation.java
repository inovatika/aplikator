package org.aplikator.client.shared.data;

import org.jboss.errai.common.client.api.annotations.Portable;

@Portable
public enum Operation {
    CREATE, UPDATE, DELETE, PREPARE;
}
