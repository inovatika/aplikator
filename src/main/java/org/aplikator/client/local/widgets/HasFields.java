package org.aplikator.client.local.widgets;

import com.google.gwt.event.logical.shared.ValueChangeHandler;
import org.aplikator.client.shared.data.RecordDTO;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: vlahoda
 * Date: 09.04.13
 * Time: 10:52
 * To change this template use File | Settings | File Templates.
 */
public interface HasFields extends ValueChangeHandler {
    void registerDataField(DataField<?> field);

    public List<DataField<?>> getDataFields();

    void registerNestedCollection(NestedCollectionWidget collection);


    void registerBinaryField(BinaryFieldWidget binaryField);

    void registerFunctionButton(FunctionButtonWidget functionButton);

    void setDirty(boolean dirty);

    RecordDTO getOriginal();

    String getPrefix();

    void setPrefix(String prefix);
}
