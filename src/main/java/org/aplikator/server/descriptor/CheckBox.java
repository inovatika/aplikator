package org.aplikator.server.descriptor;

import org.aplikator.client.shared.descriptor.CheckBoxDTO;
import org.aplikator.client.shared.descriptor.WidgetDTO;
import org.aplikator.server.data.Context;

public class CheckBox extends WidgetPropertyDescriptorBase<Boolean> {

    public CheckBox(Property<Boolean> property) {
        super(property);
    }

    private String switcher;

    public CheckBox setSwitcher(String switcher) {
        this.switcher = switcher;
        return this;
    }

    @Override
    public WidgetDTO getWidgetDescriptor(Context ctx) {
        CheckBoxDTO desc = new CheckBoxDTO(getProperty().getPropertyDTO(ctx));
        desc.setSize(getSize());
        desc.setAccess(getProperty().getAccess(ctx));
        desc.setEnabled(isEnabled(ctx));
        desc.setVisible(isVisible(ctx));
        desc.setHorizontal(isHorizontal());
        desc.setElementId(getElementId());
        desc.setCustomLabelStyle(getCustomLabelStyle());
        desc.setLocalizedName(getLocalizedName(ctx));
        desc.setLocalizedTooltip(getLocalizedTooltip(ctx));
        desc.setSwitcher(switcher);
        return desc;
    }

}
