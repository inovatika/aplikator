package org.aplikator.client.shared.descriptor;

/**
 * Created with IntelliJ IDEA.
 * User: vlahoda
 * Date: 02.04.13
 * Time: 15:20
 * To change this template use File | Settings | File Templates.
 */
public abstract class WidgetPropertyDTOBase extends WidgetDTO {

    protected PropertyDTO property;

    protected WidgetPropertyDTOBase() {
    }

    protected WidgetPropertyDTOBase(PropertyDTO property) {
        this.property = property;
    }

    public PropertyDTO getProperty() {
        return this.property;
    }

    public void setProperty(PropertyDTO property) {
        this.property = property;
    }


}
