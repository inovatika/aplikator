/*
 * Copyright 2013 Pavel Stastny <pavel.stastny at gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aplikator.server.descriptor;

import org.aplikator.client.shared.descriptor.HtmlFieldDTO;
import org.aplikator.client.shared.descriptor.WidgetDTO;
import org.aplikator.server.data.Context;

/**
 *
 */
public class HtmlField extends WidgetPropertyDescriptorBase<String> {


    public HtmlField(Property<String> property) {
        super(property);
    }

    @Override
    public WidgetDTO getWidgetDescriptor(Context ctx) {
        HtmlFieldDTO simpleLabelDTO = new HtmlFieldDTO(this.getProperty().getPropertyDTO(ctx));
        simpleLabelDTO.setSize(getSize());
        simpleLabelDTO.setLocalizedName(getLocalizedName(ctx));
        simpleLabelDTO.setLocalizedTooltip(getLocalizedTooltip(ctx));
        simpleLabelDTO.setVisible(isVisible(ctx));
        simpleLabelDTO.setAccess(getProperty().getAccess(ctx));
        simpleLabelDTO.setHorizontal(isHorizontal());
        simpleLabelDTO.setElementId(getElementId());
        simpleLabelDTO.setCustomLabelStyle(getCustomLabelStyle());
        return simpleLabelDTO;
    }
}
