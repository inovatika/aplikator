package org.aplikator.server.persistence.tempstore;

public class TempstoreFactory {
    private static class LazyHolder {
        private static Tempstore instance = new FileTempstore();
    }

    public static Tempstore getTempstore() {
        return LazyHolder.instance;
    }
}
