package org.aplikator.server.descriptor;

import org.aplikator.client.shared.descriptor.LabelFieldDTO;
import org.aplikator.client.shared.descriptor.WidgetDTO;
import org.aplikator.server.data.Context;

import java.io.Serializable;

public class LabelField<T extends Serializable> extends WidgetPropertyDescriptorBase<T> {


    public LabelField(Property<T> property) {
        super(property);
    }

    @Override
    public WidgetDTO getWidgetDescriptor(Context ctx) {
        LabelFieldDTO desc = new LabelFieldDTO(getProperty().getPropertyDTO(ctx));
        desc.setSize(getSize());
        desc.setFormatPattern(getFormatPattern());
        desc.setLocalizedName(getLocalizedName(ctx));
        desc.setLocalizedTooltip(getLocalizedTooltip(ctx));
        desc.setAccess(getProperty().getAccess(ctx));
        desc.setEnabled(isEnabled(ctx));
        desc.setVisible(isVisible(ctx));
        desc.setHorizontal(isHorizontal());
        desc.setElementId(getElementId());
        desc.setCustomLabelStyle(getCustomLabelStyle());
        return desc;
    }


}
