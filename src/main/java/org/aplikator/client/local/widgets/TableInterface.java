package org.aplikator.client.local.widgets;

import org.aplikator.client.shared.data.PrimaryKey;
import org.aplikator.client.shared.data.RecordDTO;

import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.view.client.SingleSelectionModel;

public interface TableInterface extends NestedCollectionWidget, IsWidget {


    public void reload(String searchArgument);

    public void refresh();

    public PrimaryKey getSelectedPrimaryKey();

    public RecordDTO getSelectedRecord();

    public SingleSelectionModel<RecordDTO> getListSelectionModel();
}
