package org.aplikator.client.local.command;

import org.aplikator.client.local.Aplikator;
import org.aplikator.client.local.wizards.WizardSupport;
import org.aplikator.client.local.wizards.WizardSupportCallbacks;
import org.aplikator.client.shared.data.ClientContext;
import org.aplikator.client.shared.descriptor.FunctionDTO;
import org.aplikator.client.shared.descriptor.WizardPageDTO;
import org.aplikator.client.shared.rpc.AplikatorErrorCallback;
import org.aplikator.client.shared.rpc.AplikatorService;
import org.jboss.errai.common.client.api.RemoteCallback;
import org.jboss.errai.enterprise.client.jaxrs.api.RestClient;
import org.jboss.errai.ui.nav.client.local.Page;
import org.jboss.errai.ui.nav.client.local.PageShown;
import org.jboss.errai.ui.nav.client.local.PageState;

import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.SimplePanel;

@Page(path = "execute/{functionID}")
public class ExecuteFunction extends Composite implements WizardSupportCallbacks {

    @PageState
    private String functionID;

    private SimplePanel dummy = new SimplePanel();
    private FunctionDTO function;
    private WizardSupport wizardSupport;

    public ExecuteFunction() {
        initWidget(dummy);
    }

    @PageShown
    public void start() {
        if (function == null) {
            RestClient.create(AplikatorService.class,
                    new RemoteCallback<FunctionDTO>() {
                        @Override
                        public void callback(FunctionDTO response) {
                            ExecuteFunction.this.function = response;
                            drawPanel();
                        }
                    },
                    new AplikatorErrorCallback("aplikator.function.error")
            ).getFunction(functionID);
        } else {
            drawPanel();
        }

    }

    private void drawPanel() {
        ClientContext clientContext = new ClientContext();
        clientContext.setUser(Aplikator.application.getUsername());
        clientContext.setLocale(Aplikator.application.getLocale());
        this.wizardSupport = new WizardSupport(function, clientContext, this);
        this.wizardSupport.changePageRequest(true);
        History.back();
    }

    @Override
    public void wizardPageCallback(WizardPageDTO response, boolean forw) {
    }

    @Override
    public void runBeforeFunctionCallback() {
    }

    @Override
    public void runAfterFunctionCallback() {
    }


}
