package org.aplikator.server.descriptor;


import org.aplikator.server.data.Record;
import org.aplikator.server.data.RecordUtils;

public class Collection<R extends Entity> extends JoinableProperty<R> {

    public Collection(String id, R referredEntity, Entity ownEntity) {
        super(id, referredEntity, ownEntity, false);
    }



    public Record createCollectionRecord(Record ownerRecord) {
        return RecordUtils.newSubrecord(ownerRecord.getPrimaryKey(), this);
    }
}
