package org.aplikator.client.local.widgets;

import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.FocusEvent;
import com.google.gwt.event.dom.client.FocusHandler;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.SuggestOracle;
import com.google.gwt.user.client.ui.SuggestOracle.Suggestion;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;
import org.aplikator.client.local.Aplikator;
import org.aplikator.client.shared.data.ItemSuggestion;
import org.aplikator.client.shared.data.PrimaryKey;
import org.aplikator.client.shared.data.RecordContainerDTO;
import org.aplikator.client.shared.data.RecordDTO;
import org.aplikator.client.shared.descriptor.*;
import org.aplikator.client.shared.rpc.AplikatorService;
import org.gwtbootstrap3.client.ui.*;
import org.gwtbootstrap3.client.ui.constants.ColumnSize;
import org.gwtbootstrap3.client.ui.constants.IconType;
import org.gwtbootstrap3.client.ui.constants.ModalBackdrop;
import org.gwtbootstrap3.client.ui.constants.Pull;
import org.gwtbootstrap3.client.ui.html.Div;
import org.gwtbootstrap3.client.ui.html.Span;
import org.jboss.errai.common.client.api.ErrorCallback;
import org.jboss.errai.common.client.api.RemoteCallback;
import org.jboss.errai.enterprise.client.jaxrs.api.RestClient;
import org.jboss.errai.enterprise.client.jaxrs.api.RestErrorCallback;


public class ReferenceFieldWidget extends ContainerFieldBase implements DataField<Integer> {

    Row searchPanel;
    Row formPanel;

    private Button buttonSearch;
    private Button buttonDelete;

    private final Button buttonView;

    private SuggestBox typeahead;
    private TextBox searchBox;
    private boolean searchBoxActive = false;

    private Integer value;// TODO change to pk
    private PropertyDTO property;
    private ViewDTO view;
    private TableListerWidget table;
    private Modal searchDialogBox;

    private Modal detailDialogBox;
    FormWidget detailForm;


    private RecordDTO selectedRecordDTO = null;
    private boolean dirty;

    private boolean defaultEnabled = true;

    public ReferenceFieldWidget(String caption, ViewDTO view, Widget child, int size, String customLabelStyle, HasFields form) {
        super();
        this.view = view;

        wrapper = new Column(ColumnSize.XS_1);
        initWidget(wrapper);
        this.label = new FormLabel();
        this.label.addStyleName("fieldLabel");
        this.label.setText(caption);
        if (customLabelStyle != null){
            this.label.addStyleName(customLabelStyle);
        }
        if (caption != null && !"".equals(caption) && !"null".equals(caption)) {
            wrapper.add(this.label);
        }
        setGridSize(size);

        // forms container
        dataPanel = new Row();
        //
        if (child instanceof  DisableableWidget) {
            ((DisableableWidget) child).setEnabled(false);
        }
        dataPanel.add(child);


        buttonSearch = new Button("", IconType.SEARCH, new ClickHandler() {
            public void onClick(ClickEvent event) {
                if (searchDialogBox == null) {
                    searchDialogBox = createSearchDialogBox();
                }
                searchDialogBox.setPixelSize(Window.getClientWidth() - 40, Window.getClientHeight());
                searchDialogBox.show();

                if (ReferenceFieldWidget.this.view.getActivePrimarySortProperty() != null && searchBoxActive) {
                    ReferenceFieldWidget.this.view.setSearchString(searchBox.getText());
                }
                table.refresh();
            }
        });
        Widget buttonSearchWrapper = LayoutUtils.addTooltip(buttonSearch, Aplikator.application.getConfigString("aplikator.reference.search"));
        //buttonSearchWrapper.getElement().getStyle().setDisplay(Style.Display.INLINE_BLOCK);
        buttonSearch.getElement().getStyle().setDisplay(Style.Display.TABLE_CELL);
        buttonSearch.getElement().getStyle().setProperty("width", "auto");
        buttonSearch.getElement().getStyle().setVerticalAlign(Style.VerticalAlign.BASELINE);

        buttonDelete = new Button("", IconType.UNLINK , new ClickHandler() {
            public void onClick(ClickEvent event) {
                selectedRecordDTO = null;
                resetSearchBox();
                setValue(-1, true);
                buttonDelete.setVisible(false);
            }
        });
        buttonDelete.getElement().getStyle().setMarginLeft(0, Style.Unit.PX);
        buttonDelete.getElement().getStyle().setMarginBottom(10, Style.Unit.PX);
        buttonDelete.getElement().getStyle().setMarginRight(0, Style.Unit.PX);
        buttonDelete.setVisible(false);

        searchPanel = new Row();
        searchPanel.getElement().getStyle().setMarginLeft(0, Style.Unit.PX);
        searchPanel.getElement().getStyle().setMarginRight(0, Style.Unit.PX);

        Div searchBlock = new Div();
        searchBlock.getElement().getStyle().setMarginTop(0, Style.Unit.PX);
        searchBlock.getElement().getStyle().setMarginBottom(0, Style.Unit.PX);
        searchBlock.getElement().getStyle().setPaddingLeft(15, Style.Unit.PX);
        searchBlock.setPull(Pull.LEFT);
        searchBlock.getElement().getStyle().setProperty("width", "calc( 100% - 40px )");

        searchBlock.getElement().getStyle().setDisplay(Style.Display.TABLE);

        if (view.getActivePrimarySortProperty() != null) {
            ItemSuggestOracle oracle = new ItemSuggestOracle();

            searchBox = new TextBox();
            searchBox.getElement().getStyle().setDisplay(Style.Display.TABLE_CELL);
            searchBox.getElement().getStyle().setProperty("width", "auto");

            typeahead = new SuggestBox(oracle, searchBox);
            resetSearchBox();
            searchBox.addFocusHandler(new FocusHandler() {
                public void onFocus(FocusEvent event) {
                    searchBox.setText("");
                    searchBoxActive = true;
                }
            });
            typeahead.addSelectionHandler(new SelectionHandler<Suggestion>() {
                @Override
                public void onSelection(SelectionEvent<Suggestion> se) {
                    ItemSuggestion selectedSuggestion = (ItemSuggestion) se.getSelectedItem();
                    int id = selectedSuggestion.getId();
                    selectedRecordDTO = ((ItemSuggestion) selectedSuggestion).getRecord();
                    setValue(id, true);
                }
            });
            typeahead.getElement().getStyle().setDisplay(Style.Display.TABLE_CELL);
            Span prepend = new Span();
            prepend.setText(Aplikator.application.getConfigString("aplikator.reference.searchby"));
            prepend.getElement().getStyle().setMarginRight(5, Style.Unit.PX);
            searchBlock.add(prepend);
            searchBlock.add(typeahead);
            searchBlock.add(buttonSearch);


        } else {
            searchBlock.add(buttonSearch);

        }

        searchPanel.add(searchBlock);

        buttonView = new Button("");
        buttonView.setIcon(IconType.EYE);
        buttonView.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                if (detailDialogBox == null) {
                    detailDialogBox = createDisplayDialogBox();
                }
                detailDialogBox.setPixelSize(Window.getClientWidth() - 40, Window.getClientHeight());

                detailDialogBox.show();
                detailForm.displayRecord(new PrimaryKey(view.getEntity().getId(), value), new RecordContainerDTO(), null, null, null);

            }
        });
        buttonView.getElement().getStyle().setMarginLeft(0, Style.Unit.PX);
        buttonView.getElement().getStyle().setMarginBottom(10, Style.Unit.PX);
        buttonView.getElement().getStyle().setMarginRight(0, Style.Unit.PX);


        formPanel = new Row();
        formPanel.getElement().getStyle().setMarginLeft(0, Style.Unit.PX);
        formPanel.getElement().getStyle().setMarginRight(0, Style.Unit.PX);
        Column formHolder = new Column(LayoutUtils.size(11), dataPanel);
        formHolder.getElement().getStyle().setProperty("width", "calc( 100% - 45px )");
        Column buttonHolder = new Column(LayoutUtils.size(1), LayoutUtils.addTooltip(buttonDelete, Aplikator.application.getConfigString("aplikator.reference.delete")));
        buttonHolder.add(LayoutUtils.addTooltip(buttonView, Aplikator.application.getConfigString("aplikator.reference.view")));
        buttonHolder.getElement().getStyle().setWidth(40, Style.Unit.PX);
        buttonHolder.getElement().getStyle().setFloat(Style.Float.RIGHT);
        buttonHolder.getElement().getStyle().setPaddingLeft(0, Style.Unit.PX);
        buttonHolder.getElement().getStyle().setPaddingRight(0, Style.Unit.PX);
        formPanel.add(formHolder);
        formPanel.add(buttonHolder);

        searchPanel.setVisible(true);
        formPanel.setVisible(false);

        Well well = new Well();
        wrapper.add(well);
        well.add(searchPanel);
        well.add(formPanel);
    }

    private void resetSearchBox() {
        if (view.getActivePrimarySortProperty() != null) {
            searchBox.setText(view.getActivePrimarySortProperty().getLocalizedName() + ":");
            searchBoxActive = false;
        }
    }

    public PropertyDTO getProperty() {
        return property;
    }

    public void setProperty(PropertyDTO property) {
        this.property = property;

    }

    public void setValue(Integer value) {
        setValue(value, false);
        selectedRecordDTO = null;
        resetSearchBox();
    }

    public void setValue(Integer value, boolean fireEvents) {
        Integer oldValue = this.value;
        this.value = value;
        if (fireEvents) {
            setDirty(true);
            ValueChangeEvent.fire(this, value);
        }
        if (value != null && value.intValue() > 0) {
            searchPanel.setVisible(false);
            formPanel.setVisible(true);
            buttonDelete.setVisible(true);
        } else {
            searchPanel.setVisible(true);
            searchPanel.removeStyleName("refHeader");
            formPanel.setVisible(false);
        }
    }

    public Integer getValue() {
        return value;
    }

    public void setEnabled(boolean enabled) {
        buttonSearch.setEnabled(enabled);
        buttonDelete.setEnabled(enabled);
        if (searchBox != null) {
            searchBox.setEnabled(enabled);
        }
    }

    public HandlerRegistration addValueChangeHandler(ValueChangeHandler<Integer> handler) {
        return this.addHandler(handler, ValueChangeEvent.getType());
    }

    private Modal createSearchDialogBox() {
        ModalBody searchDialogContents = new ModalBody();
        final Modal dialogBox = new Modal();
        dialogBox.getElement().getStyle().setOverflowY(Style.Overflow.AUTO);//fix against hiding scrollbar in modal stack
        dialogBox.setDataBackdrop(ModalBackdrop.STATIC);


        ModalHeader buttonPanel = new ModalHeader();
        buttonPanel.setTitle(Aplikator.application.getConfigString("aplikator.reference.dialogtitle") + " - " + view.getLocalizedName());
        buttonPanel.addStyleName("refButtonPanel");

        Button closeButton = new Button(Aplikator.application.getConfigString("aplikator.reference.cancel"), IconType.BAN, new ClickHandler() {

            public void onClick(ClickEvent event) {
                dialogBox.hide();
            }

        });
        closeButton.addStyleName("refSelectButton");
        buttonPanel.add(closeButton);
        buttonPanel.setClosable(false);

        final Button OKButton = new Button(Aplikator.application.getConfigString("aplikator.reference.select"), IconType.CHECK, new ClickHandler() {

            public void onClick(ClickEvent event) {
                dialogBox.hide();
                selectedRecordDTO = table.getSelectedRecord();
                setValue(table.getSelectedPrimaryKey().getId(), true);
            }

        });
        OKButton.setEnabled(false);
        OKButton.addStyleName("refSelectButton");
        buttonPanel.add(OKButton);

        dialogBox.add(buttonPanel);
        dialogBox.add(searchDialogContents);

        table = (TableListerWidget) TableFactory.create(view, null, null, 450);
        table.addStyleName("refTablePanel");
        searchDialogContents.add(table);
        table.addNestedEditorNotification(new NestedEditorNotification() {
            @Override
            public void nestedEditorStarted() {
                OKButton.setEnabled(false);
            }

            @Override
            public void nestedEditorFinished() {
                OKButton.setEnabled(true);
            }
        });
        final SingleSelectionModel<RecordDTO> selectionModel = table.getListSelectionModel();
        selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
            public void onSelectionChange(SelectionChangeEvent event) {
                if (selectionModel.getSelectedObject() != null) {
                    OKButton.setEnabled(true);
                } else {
                    OKButton.setEnabled(false);
                }

            }
        });

        return dialogBox;
    }


    private Modal createDisplayDialogBox() {
        ModalBody modalBody = new ModalBody();
        final Modal dialogBox = new Modal();
        dialogBox.getElement().getStyle().setOverflowY(Style.Overflow.AUTO);//fix against hiding scrollbar in modal stack
        dialogBox.setDataBackdrop(ModalBackdrop.STATIC);


        ModalHeader buttonPanel = new ModalHeader();
        buttonPanel.setTitle(Aplikator.application.getConfigString("aplikator.reference.detail") + " - " + view.getLocalizedName());
        buttonPanel.addStyleName("refButtonPanel");

        final Button OKButton = new Button(Aplikator.application.getConfigString("aplikator.reference.selectOther"), IconType.SEARCH, new ClickHandler() {

            public void onClick(ClickEvent event) {
                dialogBox.hide();
                if (searchDialogBox == null) {
                    searchDialogBox = createSearchDialogBox();
                }
                searchDialogBox.setPixelSize(Window.getClientWidth() - 40, Window.getClientHeight());
                searchDialogBox.show();

                if (ReferenceFieldWidget.this.view.getActivePrimarySortProperty() != null && searchBoxActive) {
                    ReferenceFieldWidget.this.view.setSearchString(searchBox.getText());
                }
                table.refresh();
            }

        });
        OKButton.setEnabled(true);
        OKButton.addStyleName("refSelectButton");
        OKButton.getElement().getStyle().setMarginRight(12, Style.Unit.PX);
        buttonPanel.add(OKButton);

        dialogBox.add(buttonPanel);


        detailForm = new FormWidget(view, null, null, null, "ReferencedForm");
        detailForm.setEnabled(false);
        Row formPopupHolder = new Row();
        formPopupHolder.add(detailForm);
        modalBody.add(formPopupHolder);

        dialogBox.add(modalBody);

        return dialogBox;
    }

    public RecordDTO getSelectedRecord() {
        return selectedRecordDTO;
    }

    @Override
    public void setDirty(boolean dirty) {
        this.dirty = dirty;
    }

    @Override
    public boolean isDirty() {
        return dirty;
    }

    @Override
    public void grabFocus() {
    }

    @Override
    public void addEnterHandler(Command command) {
    }


    @Override
    public void setDefaultEnabled(boolean enabled) {
        this.defaultEnabled = enabled;
    }

    @Override
    public boolean isDefaultEnabled() {
        return this.defaultEnabled;
    }

    public void processAnnotations(RecordDTO sourceRecord){
        String recordAccessStr = sourceRecord.getAnnotation(AnnotationKeys.ACCESS_ANNOTATION_KEY);
        String fieldAccessStr = sourceRecord.getPropertyAnnotation(this.getProperty().getId(), AnnotationKeys.ACCESS_ANNOTATION_KEY);
        if (fieldAccessStr != null) {
            Access fieldAccess = Access.valueOf(fieldAccessStr);
            this.setEnabled(!fieldAccess.isReadOnly());
        } else if (recordAccessStr != null) {
            Access recordAccess = Access.valueOf(recordAccessStr);
            this.setEnabled(!recordAccess.isReadOnly());
        } else {
            this.setEnabled(this.isDefaultEnabled());
        }

        String annotationStyleStr = sourceRecord.getPropertyAnnotation(this.getProperty().getId(), AnnotationKeys.STYLE_ANNOTATION_KEY);
        if (annotationStyleStr != null){
            wrapper.getElement().setAttribute(AnnotationKeys.STYLE_CSS_ATTRIBUTE, annotationStyleStr);
        } else {
            wrapper.getElement().removeAttribute(AnnotationKeys.STYLE_CSS_ATTRIBUTE);
        }
    }

    public class ItemSuggestOracle extends SuggestOracle {

        public boolean isDisplayStringHTML() {
            return true;
        }

        public void requestSuggestions(SuggestOracle.Request req, SuggestOracle.Callback callback) {
            RestClient.create(AplikatorService.class,
                    new ItemSuggestCallback(req, callback),
                    new RestErrorCallback() {
                        @Override
                        public boolean error(com.google.gwt.http.client.Request message, Throwable throwable) {
                            return false;
                        }
                    }
            ).getSuggestions(view.getId(), view.getActiveSort(), req.getQuery());
        }

        class ItemSuggestCallback implements RemoteCallback<SuggestionsDTO>, ErrorCallback {

            private SuggestOracle.Request req;
            private SuggestOracle.Callback callback;

            public ItemSuggestCallback(SuggestOracle.Request r, SuggestOracle.Callback c) {
                req = r;
                callback = c;
            }


            @Override
            public boolean error(Object message, java.lang.Throwable throwable) {
                callback.onSuggestionsReady(req, new SuggestOracle.Response());
                return false;
            }

            @Override
            public void callback(SuggestionsDTO dto) {
                callback.onSuggestionsReady(req, new SuggestOracle.Response(dto.getSuggestions()));
            }
        }
    }

}
