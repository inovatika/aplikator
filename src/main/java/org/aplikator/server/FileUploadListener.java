package org.aplikator.server;

import org.apache.commons.fileupload.ProgressListener;

public class FileUploadListener implements ProgressListener {

    private long kiloBytes = -1;

    private long bytesRead = -1;
    private long contentLength = -1;
    private int items = -1;

    public void update(long bytesRead, long contentLength, int items) {
        long kBytes = bytesRead / 1024;
        if (kiloBytes == kBytes) {
            return;
        }
        kiloBytes = kBytes;

        this.bytesRead = bytesRead;
        this.contentLength = contentLength;
        this.items = items;
    }

    public long getBytesRead() {
        return bytesRead;
    }

    public long getContentLength() {
        return contentLength;
    }

    public int getItems() {
        return items;
    }

}
