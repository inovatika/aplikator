package org.aplikator.shared.rpc.marshaller;

import junit.framework.TestCase;
import org.aplikator.client.shared.data.PrimaryKey;
import org.aplikator.client.shared.data.RecordDTO;
import org.aplikator.client.shared.descriptor.Access;
import org.aplikator.client.shared.descriptor.AnnotationKeys;
import org.aplikator.client.shared.rpc.marshaller.PrimaryKeyMarshaller;
import org.aplikator.client.shared.rpc.marshaller.RecordDTOMarshaller;
import org.easymock.EasyMock;
import org.jboss.errai.marshalling.client.api.Marshaller;
import org.jboss.errai.marshalling.client.api.MarshallingSession;
import org.jboss.errai.marshalling.client.api.json.EJValue;
import org.jboss.errai.marshalling.client.marshallers.StringMarshaller;
import org.jboss.errai.marshalling.server.JSONDecoder;
import org.junit.Assert;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

public class RecordDTOMarshallTest extends TestCase {

    public void testMarshall() {
        RecordDTO recordDTO = new RecordDTO();
        recordDTO.setPrimaryKey(new PrimaryKey("modul", 2));

        recordDTO.setValue("datumovepole", new Date(100000L));
        recordDTO.setValue("stringovepole", "string");
        recordDTO.setValue("integerovepole", 2);
        recordDTO.setValue("doublepole", 3.3);
        recordDTO.setValue("booleanpole", true);
        recordDTO.setValue("bigdecimal", new BigDecimal("33.2223445"));

        RecordDTO subrecord1 = new RecordDTO();
        subrecord1.setPrimaryKey(new PrimaryKey("submodul", 2));
        subrecord1.setOwnerPrimaryKey(new PrimaryKey("modul", 2));

        RecordDTO subrecord2 = new RecordDTO();
        subrecord2.setPrimaryKey(new PrimaryKey("submodul", 2));
        subrecord2.setOwnerPrimaryKey(new PrimaryKey("modul", 2));
        recordDTO.setValue("kolekce", new ArrayList<RecordDTO>(Arrays.asList(subrecord1, subrecord2)));

        recordDTO.setValue("reference", subrecord1);

        recordDTO.putAnnotation(AnnotationKeys.ACCESS_ANNOTATION_KEY, Access.NONE.name());
        recordDTO.putAnnotation("color", "black");

        recordDTO.putPropertyAnnotation("stringovepole", AnnotationKeys.ACCESS_ANNOTATION_KEY, Access.NONE.name());
        recordDTO.putPropertyAnnotation("stringovepole","color", "red");
        recordDTO.putPropertyAnnotation("doublepole","creator", "blb");

        MarshallingSession session = ctx();

        RecordDTOMarshaller recMarshaller = new RecordDTOMarshaller();
        String marshalled = recMarshaller.marshall(recordDTO, session);


        EJValue decoded = JSONDecoder.decode(marshalled);
        RecordDTO rec = recMarshaller.demarshall(decoded, session);
        Assert.assertTrue(rec.getProperties().size() == 8);

        Assert.assertEquals(new Date(100000L),rec.getValue("datumovepole"));
        Assert.assertEquals("string", rec.getValue("stringovepole"));
        Assert.assertEquals(2, rec.getValue("integerovepole"));
        Assert.assertEquals(3.3, rec.getValue("doublepole"));
        Assert.assertEquals(true, rec.getValue("booleanpole"));
        Assert.assertEquals(new BigDecimal("33.2223445"), rec.getValue("bigdecimal"));


        Assert.assertNull(rec.getValue("booleanABC"));

        Assert.assertEquals(Access.NONE.name(), rec.getAnnotation(AnnotationKeys.ACCESS_ANNOTATION_KEY));
        Assert.assertEquals("black", rec.getAnnotation("color"));
        Assert.assertEquals(Access.NONE.name(), rec.getPropertyAnnotation("stringovepole", AnnotationKeys.ACCESS_ANNOTATION_KEY));
        Assert.assertEquals("red", rec.getPropertyAnnotation("stringovepole","color"));
        Assert.assertEquals("blb", rec.getPropertyAnnotation("doublepole","creator"));
    }

    public void testMarshallWithNull() {
        RecordDTO recordDTO = new RecordDTO();
        recordDTO.setPrimaryKey(new PrimaryKey("modul", 2));

        recordDTO.setValue("datumovepole", null);
        recordDTO.setValue("stringovepole", null);
        recordDTO.setValue("integerovepole", null);
        recordDTO.setValue("doublepole", null);
        recordDTO.setValue("booleanpole", true);

        MarshallingSession session = ctx();

        RecordDTOMarshaller recMarshaller = new RecordDTOMarshaller();
        String marshalled = recMarshaller.marshall(recordDTO, session);
        RecordDTO demarshalledRecordDTO = recMarshaller.demarshall(JSONDecoder.decode(marshalled), session);
        Assert.assertNull(demarshalledRecordDTO.getValue("datumovepole"));
        Assert.assertNull(demarshalledRecordDTO.getValue("stringovepole"));
        Assert.assertNull(demarshalledRecordDTO.getValue("integerovepole"));
        Assert.assertNull(demarshalledRecordDTO.getValue("doublepole"));
        Assert.assertNotNull(demarshalledRecordDTO.getValue("booleanpole"));

    }

    @SuppressWarnings("unchecked")
    private MarshallingSession ctx() {
        MarshallingSession session = EasyMock.createMock(MarshallingSession.class);
        PrimaryKeyMarshaller pkmarsh = new PrimaryKeyMarshaller();
        Object marsh = pkmarsh;
        EasyMock.expect(session.getMarshallerInstance(PrimaryKey.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        StringMarshaller smarsh = new StringMarshaller();
        marsh = smarsh;
        EasyMock.expect(session.getMarshallerInstance(String.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        RecordDTOMarshaller recMarshaller = new RecordDTOMarshaller();
        marsh = recMarshaller;
        EasyMock.expect(session.getMarshallerInstance(RecordDTO.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        EasyMock.replay(session);
        return session;
    }

}
