package org.aplikator.client.local.widgets;

import org.aplikator.client.shared.descriptor.PropertyDTO;
import org.aplikator.client.shared.descriptor.ViewDTO;

public class TableFactory {

    public static TableInterface create(ViewDTO view, PropertyDTO ownerProperty, HasFields ownerForm, int height) {
        TableInterface retval = new TableListerWidget(view, ownerProperty, ownerForm, height);
        return retval;
    }

}
