package org.aplikator.client.shared.rpc;

/**
 *
 */
public interface AfterErrorExecutor {
    public void execute();
}
