package org.aplikator.server;


import org.aplikator.server.descriptor.Application;
import org.aplikator.server.persistence.PersisterFactory;
import org.aplikator.server.security.SecurityModule;
import org.slf4j.bridge.SLF4JBridgeHandler;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 */

@WebListener
public class AplikatorContextListener implements ServletContextListener {


    @Override
    public void contextInitialized(ServletContextEvent sce) {
        LogbackConfigLoader.init();

        SLF4JBridgeHandler.removeHandlersForRootLogger();
        SLF4JBridgeHandler.install();
        Logger.getLogger("").setLevel(Level.FINEST);

        ServletContext sc = sce.getServletContext();
        sc.setInitParameter("resteasy.scan", "true");
        sc.setInitParameter("resteasy.servlet.mapping.prefix", "/api");
        //sc.setInitParameter("resteasy.providers","org.jboss.errai.jaxrs.ErraiProvider");  //unless the bug in ErraiProvider readFrom is fixed, we must use our patched AplikatorJaxrsProvider
        sc.setInitParameter("resteasy.providers", "org.aplikator.server.AplikatorJaxrsProvider");

        Application.get();
        SecurityModule.get();


    }


    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        Application.get().shutdown();
        Configurator.get().shutdown();
        PersisterFactory.shutdownPersister();
    }


}