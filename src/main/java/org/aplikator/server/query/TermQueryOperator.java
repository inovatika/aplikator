package org.aplikator.server.query;

public enum TermQueryOperator {
    EQUAL,
    EQUAL_IGNORECASE,
    NOTEQUAL,
    NOTEQUAL_IGNORECASE,
    LESSTHAN,
    LESSTHAN_IGNORECASE,
    MOREOREQUAL,
    MOREOREQUAL_IGNORECASE,
    GREATERTHAN,
    GREATERTHAN_IGNORECASE,
    LESSOREQUAL,
    LESSOREQUAL_IGNORECASE,
    STARTSWITH,
    STARTSWITH_IGNORECASE,
    STARTSWITH_UNACCENT,
    NOTSTARTSWITH,
    NOTSTARTSWITH_IGNORECASE,
    NOTSTARTSWITH_UNACCENT,
    LIKE,
    LIKE_IGNORECASE,
    LIKE_UNACCENT,
    NOTLIKE,
    NOTLIKE_IGNORECASE,
    NOTLIKE_UNACCENT,
    NULL,
    NOTNULL,
    BETWEEN,
    NOTBETWEEN,
    IN,
    NOTIN;
}
