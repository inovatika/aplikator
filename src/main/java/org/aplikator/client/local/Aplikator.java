package org.aplikator.client.local;


import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.SimplePanel;
import org.aplikator.client.local.command.Login;
import org.aplikator.client.local.command.WelcomeScreen;
import org.aplikator.client.local.processes.ProcessInformations;
import org.aplikator.client.local.widgets.Messages;
import org.aplikator.client.shared.descriptor.ActionDTO;
import org.aplikator.client.shared.descriptor.ApplicationDTO;
import org.aplikator.client.shared.descriptor.MenuDTO;
import org.aplikator.client.shared.rpc.AplikatorErrorCallback;
import org.aplikator.client.shared.rpc.AplikatorService;
import org.aplikator.client.shared.rpc.AuthenticationService;
import org.gwtbootstrap3.client.ui.*;
import org.gwtbootstrap3.client.ui.constants.Toggle;
import org.gwtbootstrap3.client.ui.html.Div;
import org.jboss.errai.common.client.api.RemoteCallback;
import org.jboss.errai.enterprise.client.jaxrs.api.RestClient;
import org.jboss.errai.ioc.client.api.EntryPoint;
import org.jboss.errai.ui.nav.client.local.Navigation;
import org.jboss.errai.ui.nav.client.local.api.NavigationControl;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;


/**
 * Entry point class of Aplikator application (defines <code>onModuleLoad()</code> ).
 */
@EntryPoint
public class Aplikator {

    public static final int MAINMENUBAR_HEIGHT = 52;
    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(Aplikator.class);
    public static ApplicationDTO application = new ApplicationDTO();

    private Timer sessionRefreshTimer = new Timer() {
        @Override
        public void run() {
            RestClient.create(AplikatorService.class, new RemoteCallback<String>() {
                @Override
                public void callback(String s) {
                    LOG.debug("REFRESH SESSION CALLBACK: " + s);
                }
            }).getStatus();
        }
    };

    private static AplikatorUiBinder ourUiBinder = GWT.create(AplikatorUiBinder.class);
    @UiField
    Div mainPanel;
    @UiField
    NavbarNav mainMenuHolder;
    @UiField
    NavbarBrand homeButton;
    @UiField
    DropDownMenu servicemenu;
    @UiField
    AnchorListItem processes;
    //@UiField
    //AnchorListItem profile;

    @UiField
    Label username;

    @UiField
    Button login;

    Login loginModal;

    @UiField
    SimplePanel contents;
    //@Inject
    //TransitionTo<WelcomeScreen> homeButtonClicked;
    @Inject
    private WelcomeScreen welcomeScreen;

    @Inject
    private Navigation navigation;

    public static String getBaseURL() {
//        String moduleBase = GWT.getModuleBaseURL();
//        String moduleName = GWT.getModuleName();
//        return moduleBase.substring(0, moduleBase.lastIndexOf(moduleName));
        return GWT.getHostPageBaseURL() + "/";
    }

    public static String getBasePath() {
        String baseUrl = GWT.getHostPageBaseURL();
        String urlWithoutProtocol = baseUrl.substring(baseUrl.indexOf("//") + 2);
        return urlWithoutProtocol.substring(urlWithoutProtocol.indexOf("/"));
    }

    /**
     * Aplikator client entry point method.
     */

    @PostConstruct
    public final void onModuleLoad() {
//        Window.alert("applicationRoot:" + RestClient.getApplicationRoot()
//                + "\nmoduleBaseURL:" + GWT.getModuleBaseURL()
//                + "\nmoduleName:" + GWT.getModuleName()
//                + "\nhostPageBaseURL" + GWT.getHostPageBaseURL()
//                + "\nmoduleBaseForStaticFiles" + GWT.getModuleBaseForStaticFiles());

        // PushStateUtil.enablePushState(true);
        //Navigation.setAppContext("");
        //Window.alert("BASE PATH:"+getBasePath());
        Window.addWindowClosingHandler(closingHandler);
        History.addValueChangeHandler(closingHandler);
        RestClient.setApplicationRoot(getBasePath() + "api");
        //RestClient.setApplicationRoot("/" + GWT.getModuleName() + "/api");

        //RestClient.setJacksonMarshallingActive(true);
        GWT.setUncaughtExceptionHandler(new GWT.UncaughtExceptionHandler() {
            public void onUncaughtException(Throwable e) {
                LOG.error(e.getMessage(), e);
            }
        });


        mainPanel = ourUiBinder.createAndBindUi(this);
        contents.add(navigation.getContentPanel());

        RootPanel.get().add(mainPanel);
        reloadApplication(History.getToken());
    }

    public void reloadApplication(final String redirectToken) {
        RestClient.create(AplikatorService.class, new RemoteCallback<ApplicationDTO>() {
                    @Override
                    public void callback(ApplicationDTO resp) {
                        application = resp;
                        initPanels();
                        Messages.cleanAll();
                        if (application.isShowNavigation()) {
                            setupMainMenu(application.getMenus());
                        } else {
                            setupMainMenu(new ArrayList<>());
                        }
                        login.setEnabled(true);
                        setupServiceMenu(application.getServiceMenu());

                        int timeout = Integer.parseInt(resp.getConfigString("aplikator.client.sessiontimeout"));
                        sessionRefreshTimer.scheduleRepeating(timeout * 60000);
                        Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
                            @Override
                            public void execute() {
                                //Window.alert("RedirectToken:"+redirectToken+" DefaultToken:"+application.getDefaultActionToken()+ " Auth:"+authenticated());
                                if (redirectToken == null || redirectToken.equals("home") || redirectToken.equals("")) {

                                    if ("login".equals(application.getDefaultActionToken()) && !authenticated()) {

                                        openLoginDialog(redirectToken);


                                    } else {
                                        History.newItem("", true);
                                        History.newItem(application.getDefaultActionToken(), true);
                                        //homeButtonClicked.go();
                                    }
                                } else {
                                    History.newItem("", true);
                                    History.newItem(redirectToken, true);
                                }
                            }
                        });
                    }
                }, new AplikatorErrorCallback("aplikator.applicationreload.error")
        ).getApplication();

    }


    private void openLoginDialog(String redirectToken) {
        loginModal.setRedirectToken(redirectToken);
        loginModal.show();
        login.setEnabled(false);
    }

    private void initPanels() {

        homeButton.setText(application.getConfigString("aplikator.brand"));
        if (application.isShowNavigation()) {
            homeButton.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    Window.setTitle(Aplikator.application.getConfigString("aplikator.brand"));
                    //homeButtonClicked.go();
                    History.newItem(application.getDefaultActionToken(), true);
                }
            });
        }
        if (loginModal == null) {
            loginModal = new Login(this);

        }
        displayLoginButton(true);
        processes.setText(application.getConfigString("aplikator.menu.processes"));
        if (!authenticated()) {
            username.setText("");
            login.setText(application.getConfigString("aplikator.menu.login"));
        } else {
            username.setText(application.getUsername());
            login.setText(application.getConfigString("aplikator.menu.logout"));
        }

    }

    private boolean authenticated() {
        return (application.getUsername() != null);
    }

    @UiHandler("processes")
    public void displayProcesses(ClickEvent event) {
        ProcessInformations pi = new ProcessInformations();
        pi.display();
    }

    @UiHandler("login")
    public void displayLogin(ClickEvent event) {
        if (!authenticated()) {
            openLoginDialog("home");

        } else {
            login.setEnabled(false);
            RestClient.create(AuthenticationService.class, Login.SECURITY_REST_API_BASE, new RemoteCallback<Void>() {
                        @Override
                        public void callback(Void response) {
                            History.newItem(application.getDefaultActionToken(), true);
                            reloadApplication("home");
                        }
                    },
                    new AplikatorErrorCallback("aplikator.logout.error")
            ).logout();
        }
    }

    private void setupMainMenu(List<MenuDTO> menus) {
        mainMenuHolder.clear();
        for (MenuDTO menu : menus) {
            ListDropDown menuDropdown = new ListDropDown();

            AnchorButton menuButton = new AnchorButton();
            menuButton.setText(menu.getLocalizedName());
            menuButton.setDataToggle(Toggle.DROPDOWN);
            DropDownMenu menuItems = new DropDownMenu();
            menuDropdown.add(menuButton);
            menuDropdown.add(menuItems);
            mainMenuHolder.add(menuDropdown);
            for (final ActionDTO action : menu.getActions()) {
                AnchorListItem menuItem = new AnchorListItem();
                menuItem.setText(action.getLocalizedName());
                menuItem.setHref("#" + action.getToken());
                menuItem.addClickHandler(new ClickHandler() {
                    @Override
                    public void onClick(ClickEvent event) {
                        History.newItem(action.getToken(), true);
                    }
                });
                menuItems.add(menuItem);
            }
        }
    }

    private void setupServiceMenu(List<ActionDTO> serviceMenu) {
        while (servicemenu.getWidgetCount() > 1) {
            servicemenu.remove(0);
        }
        for (final ActionDTO action : serviceMenu) {
            AnchorListItem menuItem = new AnchorListItem();
            menuItem.setText(action.getLocalizedName());
            menuItem.setHref("#" + action.getToken());
            menuItem.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    History.newItem(action.getToken(), true);
                }
            });
            servicemenu.insert(menuItem, servicemenu.getWidgetCount() - 1);
        }
    }

    public void displayLoginButton(boolean displayLoginButton) {
        login.setEnabled(displayLoginButton);
    }

    interface AplikatorUiBinder extends UiBinder<Div, Aplikator> {
    }

    public static ClosingHandler closingHandler = new ClosingHandler();

    public static class ClosingHandler implements Window.ClosingHandler, ValueChangeHandler<String> {

        private boolean dirty = false;

        public boolean isDirty() {
            return dirty;
        }

        public void setDirty(boolean dirty) {
            this.dirty = dirty;
        }

        @Override
        public void onWindowClosing(Window.ClosingEvent closingEvent) {
            if (dirty) {
                closingEvent.setMessage(application.getConfigString("aplikator.confirmClose"));
            }

        }

        @Override
        public void onValueChange(ValueChangeEvent<String> valueChangeEvent) {


        }
    }

    public static void confirmCLose(NavigationControl control) {
        if (closingHandler.isDirty()) {
            if (Window.confirm(application.getConfigString("aplikator.confirmClose"))) {
                closingHandler.setDirty(false);
                control.proceed();
            }
        } else {
            control.proceed();
        }
    }

}
