package org.aplikator.client.shared.descriptor;

import org.aplikator.client.local.widgets.DateFieldWidget;
import org.aplikator.client.local.widgets.HasFields;

import com.google.gwt.user.client.ui.Widget;

@SuppressWarnings("serial")
//@JsonTypeName("date")
//@Portable
public class DateFieldDTO extends WidgetPropertyDTOBase {

    public static final String TYPE_KEY = "date";

    @SuppressWarnings("unused")
    public DateFieldDTO() { //TODO custom marshaller
    }

    public DateFieldDTO(PropertyDTO property) {
        super(property);
    }

    @Override
    public Widget getWidget(HasFields form) {
        DateFieldWidget field = new DateFieldWidget(getLocalizedName(), getLocalizedTooltip(), property, getSize(), getFormatPattern(), isHorizontal(), getElementId(), getCustomLabelStyle(), form);
        field.setEnabled(isEnabled());
        field.setVisible(isVisible());
        field.setDefaultEnabled(isEnabled());
        if (form != null) {
            form.registerDataField(field);
        }
        return field;
    }

}
