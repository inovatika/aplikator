package org.aplikator.server.data;

import java.io.Serializable;
import java.util.UUID;

import org.aplikator.client.shared.data.PrimaryKey;
import org.aplikator.client.shared.data.RecordDTO;
import org.aplikator.server.descriptor.Collection;
import org.aplikator.server.descriptor.Entity;
import org.aplikator.server.descriptor.Property;

import com.google.common.base.Preconditions;


public class RecordUtils {

    @SuppressWarnings("unchecked")
    public static <T extends Serializable> T getValue(RecordDTO record, Property<T> prop) {
        return ((T) record.getValue(prop.getId()));
    }

    public static <T extends Serializable> void setValue(RecordDTO record, Property<T> prop, T value) {
        prop.setValue(record, value);
    }


    public static Record newRecord(Entity entity) {
        return new Record(new RecordDTO(new PrimaryKey(entity.getId(), UUID.randomUUID().toString())));

    }

    public static Record newSubrecord(PrimaryKey ownerPrimaryKey, Collection<?> ownerProperty) {
        Preconditions.checkArgument(ownerProperty.getEntity().getId().equals(ownerPrimaryKey.getEntityId()), "Nonequal owner entity:" + ownerProperty.getEntity().getId() + " " + ownerPrimaryKey.getEntityId());
        Record retval = newRecord(ownerProperty.referredEntity);
        retval.setOwnerProperty(ownerProperty);
        retval.setOwnerPrimaryKey(ownerPrimaryKey);
        return retval;
    }

}
