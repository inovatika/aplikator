package org.aplikator.client.shared.rpc;

import com.google.gwt.http.client.Response;
import org.jboss.errai.enterprise.client.jaxrs.ClientExceptionMapper;

import javax.ws.rs.ext.Provider;

@Provider
public class AplikatorExceptionMapper implements ClientExceptionMapper {

    public static final String APLIKATOR_MESSAGE_START = "APLIKATOR_MESSAGE_START";
    public static final String APLIKATOR_MESSAGE_END = "APLIKATOR_MESSAGE_END";

    @Override
    public Throwable fromResponse(Response response) {
        String rawText = response.getText();
        if (rawText.contains(APLIKATOR_MESSAGE_START)){
            int msgStart = rawText.indexOf(APLIKATOR_MESSAGE_START)+APLIKATOR_MESSAGE_START.length();
            int msgEnd = rawText.indexOf(APLIKATOR_MESSAGE_END);
            rawText = rawText.substring(msgStart, msgEnd);
        }
        return new AplikatorException(rawText);
    }
}
