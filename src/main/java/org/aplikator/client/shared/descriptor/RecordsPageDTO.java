package org.aplikator.client.shared.descriptor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.aplikator.client.shared.data.RecordDTO;
import org.jboss.errai.common.client.api.annotations.Portable;

@Portable
public class RecordsPageDTO implements Serializable {

    private List<RecordDTO> recordDTOs = new ArrayList<RecordDTO>();

    private int offset = 0;

    public void setRecords(List<RecordDTO> recordDTOs) {
        this.recordDTOs = recordDTOs;
    }

    public List<RecordDTO> getRecords() {
        return this.recordDTOs;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }
}
