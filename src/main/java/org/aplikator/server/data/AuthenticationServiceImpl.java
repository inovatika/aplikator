package org.aplikator.server.data;

import org.aplikator.client.shared.rpc.AuthenticationService;
import org.aplikator.server.descriptor.Application;
import org.aplikator.server.security.SecurityModule;
import org.jboss.errai.bus.server.annotations.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The server side implementation of the RPC service.
 */
@Service
public class AuthenticationServiceImpl implements AuthenticationService {
    private static final Logger LOG = Logger.getLogger(AuthenticationServiceImpl.class.getName());
    @javax.ws.rs.core.Context
    HttpServletRequest httpServletRequest;
    @javax.ws.rs.core.Context
    HttpServletResponse httpServletResponse;
    private AplikatorServiceBackend aplikatorServiceBackend = new AplikatorServiceBackend();


    private Context ctx(RecordContainer rc) {
        return new Context(httpServletRequest, httpServletResponse, aplikatorServiceBackend, rc);
    }


    private Context ctx() {
        return new Context(httpServletRequest, httpServletResponse, aplikatorServiceBackend, null);
    }

    @Override
    public void login(String principal, String password) {
        try (Context ctx = ctx()){
            SecurityModule.get().ensureDatabaseRealmLoaded(ctx);
            SecurityModule.get().login(principal, password, ctx.getHttpServletRequest());
            Application.get().onLogin(ctx);
        } catch (Throwable t) {
            LOG.log(Level.SEVERE, "Error in login: " + t.getMessage(), t);
            throw new RuntimeException("Error in login: " + t.getMessage(), t);
        }
    }

    @Override
    public void logout() {
        try (Context ctx = ctx()){
            SecurityModule.get().logout(ctx.getHttpServletRequest());
            Application.get().onLogout( ctx);
        } catch (Throwable t) {
            LOG.log(Level.SEVERE, "Error in logout: " + t.getMessage(), t);
            throw new RuntimeException("Error in logout: " + t.getMessage(), t);
        }
    }

    @Override
    public void loginSSO() {
        try (Context ctx = ctx()){
            SecurityModule.get().ensureDatabaseRealmLoaded(ctx);
            Application.get().onLogin( ctx);
        } catch (Throwable t) {
            LOG.log(Level.SEVERE, "Error in loginSSO: " + t.getMessage(), t);
            throw new RuntimeException("Error in loginSSO: " + t.getMessage(), t);
        }
    }


}
