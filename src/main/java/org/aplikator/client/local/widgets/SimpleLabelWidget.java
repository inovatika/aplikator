/*
 * Copyright 2013 Pavel Stastny <pavel.stastny at gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aplikator.client.local.widgets;

import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import org.aplikator.client.shared.descriptor.PropertyDTO;
import org.gwtbootstrap3.client.ui.Label;
import org.gwtbootstrap3.client.ui.constants.LabelType;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Pavel Stastny <pavel.stastny at gmail.com>
 */
public class SimpleLabelWidget<T extends Serializable> extends DataFieldBase<T> implements DataField<T> {

    private Label bootstrapLabel = new Label();
    private LabelType labelType = null;

    public SimpleLabelWidget(String labelType, String localizedTooltip, PropertyDTO property, int size, boolean horizontal, String elementId, String customLabelStyle, HasFields form) {
        super(null, localizedTooltip, property, horizontal, elementId, customLabelStyle, form);
        this.labelType = LabelType.valueOf(labelType);
        bootstrapLabel.setType(this.labelType);
        controlHolder.add(bootstrapLabel);
        this.setGridSize(size);
    }

    public void setValue(T value) {
        bootstrapLabel.setText(value != null ? value.toString() : null);
    }

    @Override
    public void setEnabled(boolean enabled) {
    }

    @Override
    public HandlerRegistration addValueChangeHandler(ValueChangeHandler<T> handler) {
        return null;
    }


    @SuppressWarnings({"unchecked", "deprecation"})
    public T getValue() {
        String strVal = bootstrapLabel.getText();
        if (strVal == null)
            return null;
        String type = getProperty().getType();
        if ("java.lang.String".equals(type)) {
            return (T) strVal;
        } else if ("java.lang.Integer".equals(type)) {
            try {
                return (T) Integer.decode(strVal);
            } catch (NumberFormatException ex) {
                return null; //TODO notify user
            }
        } else if ("java.math.BigDecimal".equals(type)) {
            try {
                return (T) new BigDecimal(strVal);
            } catch (NumberFormatException ex) {
                return null; //TODO notify user
            }
        } else if ("java.util.Date".equals(type)) {
            try {
                return (T) new Date(strVal);
            } catch (IllegalArgumentException ex) {
                return null; //TODO notify user
            }
        } else {
            throw new IllegalStateException("SimpleLabelWidget does not support the data type " + type);
        }
    }


}
