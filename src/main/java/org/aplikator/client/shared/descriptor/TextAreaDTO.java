package org.aplikator.client.shared.descriptor;

import org.aplikator.client.local.widgets.HasFields;
import org.aplikator.client.local.widgets.TextAreaWidget;

import com.google.gwt.user.client.ui.Widget;

@SuppressWarnings("serial")
//@JsonTypeName("textarea")
//@Portable
public class TextAreaDTO extends WidgetPropertyDTOBase {

    public static final String TYPE_KEY = "textarea";

    private int rows;

    private int height;

    @SuppressWarnings("unused")
    public TextAreaDTO() {//TODO custom marshaller
    }

    public TextAreaDTO(PropertyDTO property) {
        super(property);
        //this.rows=rows;
    }

    @Override
    public Widget getWidget(HasFields form) {
        TextAreaWidget field = new TextAreaWidget(getLocalizedName(), getLocalizedTooltip(), property, getSize(), rows, height, isHorizontal(), getElementId(), getCustomLabelStyle(), form);
        field.setEnabled(isEnabled());
        field.setVisible(isVisible());
        field.setDefaultEnabled(isEnabled());
        if (form != null) {
            form.registerDataField(field);
        }
        return field;
    }

    public void setRows(int height) {
        this.rows = height;
    }

    public int getRows() {
        return rows;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
