package org.aplikator.server.query;

/**
 * @author vlahoda
 */
public class QueryParameterReference {
    private final int parameterIndex;


    private QueryParameterReference(int parameterIndex) {
        this.parameterIndex = parameterIndex;
    }

    public static QueryParameterReference param(int parameterIndex) {
        return new QueryParameterReference(parameterIndex);
    }

    public int getParameterIndex() {
        return parameterIndex;
    }
}
