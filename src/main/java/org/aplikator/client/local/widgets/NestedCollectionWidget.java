package org.aplikator.client.local.widgets;

import org.aplikator.client.shared.data.PrimaryKey;
import org.aplikator.client.shared.data.RecordContainerDTO;
import org.aplikator.client.shared.data.RecordDTO;
import org.aplikator.client.shared.descriptor.Access;
import org.aplikator.client.shared.descriptor.AnnotationKeys;
import org.aplikator.client.shared.descriptor.PropertyDTO;

public interface NestedCollectionWidget{

    PropertyDTO getOwnerProperty();

    void setOwnerPrimaryKey(PrimaryKey value);

    PrimaryKey getOwnerPrimaryKey();

    void setRecordContainerDTO(RecordContainerDTO recordContainerDTO);

    void save();

    void setDirty(boolean dirty);

    void reload();

    void initFromContainer(RecordContainerDTO initializingRecords);

    void setDefaultAccess(Access access);

    Access getDefaultAccess();

    void setAccess (Access access);

    Access getAccess();

    void clear();

    //void processAnnotations(RecordDTO sourceRecord);

    default void processAnnotations(RecordDTO sourceRecord){
        String recordAccessStr = sourceRecord.getAnnotation(AnnotationKeys.ACCESS_ANNOTATION_KEY);
        String fieldAccessStr = sourceRecord.getPropertyAnnotation(this.getOwnerProperty().getId(), AnnotationKeys.ACCESS_ANNOTATION_KEY);
        if (fieldAccessStr != null) {
            Access fieldAccess = Access.valueOf(fieldAccessStr);
            this.setAccess(fieldAccess);
        } else if (recordAccessStr != null) {
            Access recordAccess = Access.valueOf(recordAccessStr);
            this.setAccess(recordAccess);
        } else {
            this.setAccess(this.getDefaultAccess());
        }
    }
}
