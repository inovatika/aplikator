/*
 * Copyright 2013 Pavel Stastny <pavel.stastny at gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aplikator.server.descriptor;

import org.aplikator.client.shared.descriptor.SimpleBadgeDTO;
import org.aplikator.client.shared.descriptor.WidgetDTO;
import org.aplikator.server.data.Context;

import java.io.Serializable;

/**
 * http://twitter.github.io/bootstrap/components.html#labels-badges
 *
 * @author Pavel Stastny <pavel.stastny at gmail.com>
 */
public class SimpleBadge<T extends Serializable> extends WidgetPropertyDescriptorBase<T> {

    public static final String DEFAULT_LABEL_TYPE = "DEFAULT";
    public static final String SUCCESS_LABEL_TYPE = "SUCCESS";
    public static final String WARNING_LABEL_TYPE = "WARNING";
    public static final String IMPORTANT_LABEL_TYPE = "IMPORTANT";
    public static final String INFO_LABEL_TYPE = "INFO";
    public static final String INVERSE_LABEL_TYPE = "INVERSE";

    private String badgeType = DEFAULT_LABEL_TYPE;

    public SimpleBadge(Property<T> property) {
        super(property);
    }


    public void setBadgeType(String btype) {
        this.badgeType = btype;
    }

    public String getBadgeType() {
        return this.badgeType;
    }

    @Override
    public WidgetDTO getWidgetDescriptor(Context ctx) {
        SimpleBadgeDTO simpleLabelDTO = new SimpleBadgeDTO(this.getProperty().getPropertyDTO(ctx));
        simpleLabelDTO.setBadgeType(this.badgeType);
        simpleLabelDTO.setLocalizedName(getLocalizedName(ctx));
        simpleLabelDTO.setLocalizedTooltip(getLocalizedTooltip(ctx));
        simpleLabelDTO.setAccess(getProperty().getAccess(ctx));
        simpleLabelDTO.setVisible(isVisible(ctx));
        simpleLabelDTO.setHorizontal(isHorizontal());
        simpleLabelDTO.setElementId(getElementId());
        simpleLabelDTO.setCustomLabelStyle(getCustomLabelStyle());
        return simpleLabelDTO;
    }
}
