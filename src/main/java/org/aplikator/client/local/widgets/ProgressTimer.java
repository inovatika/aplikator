package org.aplikator.client.local.widgets;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.aplikator.client.local.Aplikator;

import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.HTML;

public class ProgressTimer extends Timer {

    private static final Logger LOG = Logger.getLogger(ProgressTimer.class.getName());

    private HTML statusLabel;
    private boolean running = false;

    public ProgressTimer(HTML statusLabel) {
        this.statusLabel = statusLabel;

    }

    public void reset() {
        // make one call to the server to make sure we have a session
        retrieveCurrentProgressFromServer(true);
    }

    public void run() {
        if (!running) {
            return;
        }

        retrieveCurrentProgressFromServer(false);
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    protected void retrieveCurrentProgressFromServer(boolean reset) {
        RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, Aplikator.getBaseURL() + "upload" + (reset ? "?reset" : ""));

        builder.setCallback(new RequestCallback() {
            public void onResponseReceived(Request request, Response response) {

                statusLabel.setText(response.getText());
            }

            public void onError(Request request, Throwable t) {
                LOG.log(Level.SEVERE, "Unable to retrieve the current status from the server", t);
            }
        });

        try {
            builder.send();
        } catch (RequestException e) {
            LOG.log(Level.SEVERE, "An error occurred while connecting to the file upload monitor", e);
        }
    }

}
