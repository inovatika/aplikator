package org.aplikator.client.local.widgets;

import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import org.aplikator.client.shared.data.ListItem;
import org.aplikator.client.shared.descriptor.PropertyDTO;
import org.gwtbootstrap3.client.ui.ButtonGroup;
import org.gwtbootstrap3.client.ui.RadioButton;
import org.gwtbootstrap3.client.ui.VerticalButtonGroup;
import org.gwtbootstrap3.client.ui.base.AbstractButtonGroup;
import org.gwtbootstrap3.client.ui.constants.Toggle;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RadioButtonWidget<T extends Serializable> extends DataFieldBase<T> implements DataField<T> {
    private List<RadioButton> buttons;

    public RadioButtonWidget(String caption, String localizedTooltip, PropertyDTO property, int size, boolean horizontal, String elementId, String switcherInit, String customLabelStyle, HasFields form) {
        super(caption, localizedTooltip, property, false, elementId, customLabelStyle, form);
        buttons = new ArrayList<RadioButton>(property.getListValues().size());
        AbstractButtonGroup buttonGroup;
        if (horizontal) {
            buttonGroup =
                    new ButtonGroup();
        } else {
            buttonGroup = new VerticalButtonGroup();
        }
        buttonGroup.getElement().getStyle().setDisplay(Style.Display.BLOCK);
        buttonGroup.setDataToggle(Toggle.BUTTONS);
        controlHolder.add(buttonGroup);
        for (Object obj : property.getListValues()) {
            ListItem item = (ListItem) obj;
            RadioButton button = new RadioButton(property.getId(), item.getName());
            button.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
                public void onValueChange(ValueChangeEvent<Boolean> event) {
                    setDirty(true);
                }
            });
            if (switcherInit != null) {
                switcher = new VisibilitySwitcher(switcherInit, property.getType(), form);
                //Window.alert("SWITCHER:"+switcher);
                button.addChangeHandler(new ChangeHandler() {
                    public void onChange(ChangeEvent event) {
                        switcher.apply(getValue());
                    }
                });
            }
            //button.addStyleName(PaneWidgetResources.INSTANCE.css().formData());
            buttons.add(button);
            buttonGroup.add(button);
        }
        this.setGridSize(size);
    }
    private VisibilitySwitcher<T> switcher;


    public void setValue(T value) {
        if (switcher != null){
            switcher.apply((T)value);
        }
        for (int i = 0; i < getProperty().getListValues().size(); i++) {
            if (((ListItem) getProperty().getListValues().get(i)).getValue().equals(value)) {
                buttons.get(i).setActive(true);
            } else {
                buttons.get(i).setActive(false);
            }
        }
    }

    @SuppressWarnings("unchecked")
    public T getValue() {
        int index = -1;
        for (int i = 0; i < buttons.size(); i++) {
            if (buttons.get(i).getValue()) {
                index = i;
                break;
            }
        }
        if (index == -1) {
            return null;
        }
        return (T) ((ListItem) getProperty().getListValues().get(index)).getValue();
    }

    public HandlerRegistration addValueChangeHandler(ValueChangeHandler<T> handler) {
        for (RadioButton button : buttons) {
            button.addHandler(handler, ValueChangeEvent.getType());
        }
        return null;
    }

    public void setEnabled(boolean enabled) {
        if (property.getRefferedThrough() != null) {
            enabled = false;
        }
        for (RadioButton button : buttons) {
            button.setEnabled(enabled);
        }
    }


}
