package org.aplikator.server.persistence.search;

import java.util.logging.Logger;

import org.aplikator.server.Configurator;

import com.typesafe.config.Config;

/**
 * @author eskymo
 *         <p/>
 *         SearchFactory provides a single instance of search client
 */
public class SearchFactory {

    private static final Logger LOG = Logger.getLogger(SearchFactory.class
            .getName());


    private static class LazyHolder {
        private static Search search = init();
    }


    /**
     * returns the state of search - if it's enabled or disabled for an application
     * it's defined in config file
     *
     * @return
     */
    public static boolean isSearchEnabled() {
        Config config = Configurator.get().getConfig();
        boolean isSearchEnabled = config.getBoolean(Search.SEARCH_ENABLED);
        return isSearchEnabled;
    }

    /**
     * inits the Search - initializes the driver from configuration
     *
     * @return
     */

    private static Search init() {
        String searchEngine = Configurator.get().getConfig()
                .getString(Search.SEARCH_ENGINE);
        String searchEngineClass = Configurator.get().getConfig().getConfig(searchEngine).getString("class");
        LOG.info("SearchEngineClass:" + searchEngineClass);
        Search search = null;
        try {
            search = (Search) Class.forName(searchEngineClass).newInstance();
        } catch (InstantiationException e) {
            LOG.severe(e.getMessage());
        } catch (IllegalAccessException e) {
            LOG.severe(e.getMessage());
        } catch (ClassNotFoundException e) {
            LOG.severe(e.getMessage());
        }
        if (search == null) {
            throw new IllegalStateException(
                    "Unable to instantiate searchengine");
        }
        return search;
    }

    /**
     * gets the Search instance
     *
     * @return
     */
    public static Search get() {
        return LazyHolder.search;
    }

}
