package org.aplikator.client.shared.descriptor;

import org.jboss.errai.common.client.api.annotations.Portable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@SuppressWarnings("serial")
@Portable
public final class ApplicationDTO implements Serializable {

    private List<MenuDTO> menus = new ArrayList<>();

    private List<ActionDTO> serviceMenu = new ArrayList<>();

    private String brand;

    private boolean showNavigation;

    private boolean showLoginSSOButton = true;

    private String defaultActionToken = "home";

    private String username;

    private String locale;

    private HashMap<String, String> config = new HashMap<>();

    public ApplicationDTO() {
    }

    public String getConfigString(String key) {
        if (config.containsKey(key)) {
            return config.get(key);
        } else {
            return key;
        }
    }

    public void setConfigString(String key, String value) {
        this.config.put(key, value);
    }

    public HashMap<String, String> getConfig() {
        return config;
    }

    public ApplicationDTO(String username, String locale) {
        this.username = username;
        this.locale = locale;
    }

    public ApplicationDTO addMenu(MenuDTO service) {
        if (service != null) {
            menus.add(service);
        }
        return this;
    }

    public List<MenuDTO> getMenus() {
        return menus;
    }

    public ApplicationDTO addServiceMenuItem(ActionDTO service) {
        if (service != null) {
            serviceMenu.add(service);
        }
        return this;
    }


    public List<ActionDTO> getServiceMenu() {
        return serviceMenu;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public boolean isShowNavigation() {
        return showNavigation;
    }

    public void setShowNavigation(boolean showNavigation) {
        this.showNavigation = showNavigation;
    }

    public String getDefaultActionToken() {
        return defaultActionToken;
    }

    public void setDefaultAction(String defaultAction) {
        this.defaultActionToken = defaultAction;
    }

    public String getUsername() {
        return username;
    }

    public String getLocale() {
        return locale;
    }

    public boolean isShowLoginSSOButton() {
        return showLoginSSOButton;
    }

    public void setShowLoginSSOButton(boolean showLoginButton) {
        this.showLoginSSOButton = showLoginButton;
    }
}
