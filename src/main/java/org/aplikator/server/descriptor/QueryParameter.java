package org.aplikator.server.descriptor;

import java.util.Locale;

import org.aplikator.client.shared.descriptor.QueryParameterDTO;
import org.aplikator.server.Configurator;
import org.aplikator.server.DescriptorRegistry;
import org.aplikator.server.data.Context;

public class QueryParameter {

    private String name;
    private String value;
    private Property property;
    private Property refferedProperty;

    public QueryParameter(Property property) {
        this.name = property.getLocalizationKey();
        this.property = property;
    }

    public QueryParameter(String name, Property property) {
        this.name = name;
        this.property = property;
    }

    public QueryParameter(String name, Property property, Property refferedProperty) {
        this.name = name;
        this.property = property;
        this.refferedProperty = refferedProperty;
    }

    public QueryParameter(QueryParameterDTO qpdto) {
        this.name = qpdto.getName();
        this.property = (Property) DescriptorRegistry.get().getDescriptionItem(qpdto.getProperty().getId());
        this.value = qpdto.getValue();
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public Property getProperty() {
        return property;
    }

    public Property getRefferedProperty() {
        return refferedProperty;
    }

    public void setRefferedProperty(Property refferedProperty) {
        this.refferedProperty = refferedProperty;
    }

    public QueryParameterDTO getQueryParameterDTO(Context ctx) {
        Locale locale = ctx != null ? ctx.getUserLocale() : Locale.getDefault();
        QueryParameterDTO retval = new QueryParameterDTO(Configurator.get().getLocalizedString(getName(), locale), property.getPropertyDTO(ctx));
        retval.setValue(getValue());
        return retval;
    }
}
