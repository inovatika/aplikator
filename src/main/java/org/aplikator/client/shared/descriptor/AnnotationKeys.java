package org.aplikator.client.shared.descriptor;

public class AnnotationKeys {
    public static final String STYLE_ANNOTATION_KEY = "style";
    public static final String LIST_ANNOTATION_KEY = "list";
    public static final String ACCESS_ANNOTATION_KEY = "access";

    public static final String STYLE_CSS_ATTRIBUTE = "styleannotation";
}
