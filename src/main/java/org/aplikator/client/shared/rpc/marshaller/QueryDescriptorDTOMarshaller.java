package org.aplikator.client.shared.rpc.marshaller;

import java.util.List;

import org.aplikator.client.shared.descriptor.QueryDescriptorDTO;
import org.aplikator.client.shared.descriptor.QueryParameterDTO;
import org.jboss.errai.marshalling.client.api.MarshallingSession;
import org.jboss.errai.marshalling.client.api.annotations.ClientMarshaller;
import org.jboss.errai.marshalling.client.api.annotations.ServerMarshaller;
import org.jboss.errai.marshalling.client.api.json.EJObject;
import org.jboss.errai.marshalling.client.api.json.EJValue;
import org.jboss.errai.marshalling.client.marshallers.AbstractNullableMarshaller;

@ClientMarshaller(QueryDescriptorDTO.class)
@ServerMarshaller(QueryDescriptorDTO.class)
@SuppressWarnings("unchecked")
public class QueryDescriptorDTOMarshaller extends AbstractNullableMarshaller<QueryDescriptorDTO> {

	/*@Override
    public Class<QueryDescriptorDTO> getTypeHandled() {
		return QueryDescriptorDTO.class;
	}*/

    @Override
    public QueryDescriptorDTO[] getEmptyArray() {
        return new QueryDescriptorDTO[0];
    }

    @Override
    public QueryDescriptorDTO doNotNullDemarshall(EJValue o,
                                                  MarshallingSession ctx) {
        if (o.isObject() != null) {
            EJObject ejObject = o.isObject();
            QueryDescriptorDTO qdto = new QueryDescriptorDTO();
            DemarshallingUtils.clientDescriptorBaseUnMarshall(ejObject, qdto);
            List l = DemarshallingUtils.listUnMarshall(ejObject.get("queryParameters"), ctx, QueryParameterDTO.class.getName());
            qdto.setQueryParameters(l);
            return qdto;

        } else
            return null;

    }

    @Override
    public String doNotNullMarshall(QueryDescriptorDTO o, MarshallingSession ctx) {
        StringBuilder builder = new StringBuilder().append('{');
        MarshallingUtils.clientDescriptorBase(builder, o).append(',');
        MarshallingUtils.key("queryParameters", builder).append(':');
        MarshallingUtils.listMarshall(builder, o.getQueryParameters(), ctx, QueryParameterDTO.class.getName());
        builder.append('}');
        return builder.toString();
    }


}
