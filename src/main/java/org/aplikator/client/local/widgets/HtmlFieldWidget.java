/*
 * Copyright 2013 Pavel Stastny <pavel.stastny at gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aplikator.client.local.widgets;

import org.aplikator.client.shared.descriptor.PropertyDTO;
import org.gwtbootstrap3.client.ui.gwt.HTMLPanel;

import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.SimplePanel;

/**
 *
 */
public class HtmlFieldWidget extends DataFieldBase<String> implements DataField<String> {

    private SimplePanel wrapper = new SimplePanel();

    public HtmlFieldWidget(String label, String localizedTooltip, PropertyDTO property, int size, boolean horizontal, String elementId, String customLabelStyle, HasFields form) {
        super(label, localizedTooltip, property, horizontal, elementId, customLabelStyle, form);
        this.setGridSize(size);
        controlHolder.add(wrapper);
        wrapper.setWidget(new HTMLPanel(""));
    }

    public void setValue(String value) {
        wrapper.setWidget(new HTMLPanel(value));
    }

    @Override
    public void setEnabled(boolean enabled) {
    }

    @Override
    public HandlerRegistration addValueChangeHandler(ValueChangeHandler<String> handler) {
        return null;
    }


    @SuppressWarnings({"unchecked", "deprecation"})
    public String getValue() {
        return wrapper.getWidget().getElement().getInnerHTML();
    }


}
