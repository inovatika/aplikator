package org.aplikator.client.shared.rpc.marshaller;

import org.aplikator.client.shared.data.ClientContext;
import org.aplikator.client.shared.data.FunctionParameters;
import org.aplikator.client.shared.data.RecordDTO;
import org.jboss.errai.marshalling.client.api.Marshaller;
import org.jboss.errai.marshalling.client.api.MarshallingSession;
import org.jboss.errai.marshalling.client.api.annotations.ClientMarshaller;
import org.jboss.errai.marshalling.client.api.annotations.ServerMarshaller;
import org.jboss.errai.marshalling.client.api.json.EJObject;
import org.jboss.errai.marshalling.client.api.json.EJValue;
import org.jboss.errai.marshalling.client.marshallers.AbstractNullableMarshaller;

@ClientMarshaller(FunctionParameters.class)
@ServerMarshaller(FunctionParameters.class)
public class FunctionParametersMarshaller extends AbstractNullableMarshaller<FunctionParameters> {

    @Override
    public FunctionParameters[] getEmptyArray() {
        return new FunctionParameters[0];
    }

    @Override
    public FunctionParameters doNotNullDemarshall(EJValue o,
                                                  MarshallingSession ctx) {
        if (o.isObject() != null) {
            EJObject ejObject = o.isObject();
            String functionId = DemarshallingUtils.safeString(ejObject, "functionId");
            ClientContext clientContext = null;
            if (DemarshallingUtils.containsAndNotNull(ejObject, "clientContext")) {
                Marshaller<Object> marshInstance = ctx.getMarshallerInstance(ClientContext.class.getName());
                clientContext = (ClientContext) marshInstance.demarshall(ejObject.get("clientContext"), ctx);
            }
            RecordDTO clientParameters = null;
            if (DemarshallingUtils.containsAndNotNull(ejObject, "clientParameters")) {
                Marshaller<Object> marshInstance = ctx.getMarshallerInstance(RecordDTO.class.getName());
                clientParameters = (RecordDTO) marshInstance.demarshall(ejObject.get("clientParameters"), ctx);
            }
            return new FunctionParameters(functionId, clientContext, clientParameters);
        } else
            return null;
    }

    @Override
    public String doNotNullMarshall(FunctionParameters o,
                                    MarshallingSession ctx) {
        StringBuilder builder = new StringBuilder().append('{');

        MarshallingUtils.string("functionId", o.getFunctionId(), builder);
        builder.append(',');

        MarshallingUtils.key("clientContext", builder).append(':');
        Marshaller<Object> marshInstance = ctx.getMarshallerInstance(ClientContext.class.getName());
        builder.append(marshInstance.marshall(o.getClientContext(), ctx)).append(',');

        MarshallingUtils.key("clientParameters", builder).append(':');
        marshInstance = ctx.getMarshallerInstance(RecordDTO.class.getName());
        builder.append(marshInstance.marshall(o.getClientParameters(), ctx));

        builder.append('}');
        return builder.toString();
    }
}
