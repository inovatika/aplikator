/*
 * Copyright 2013 Pavel Stastny <pavel.stastny at gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aplikator.server.processes;

import java.io.Serializable;

/**
 * Represents one part of chain of work
 *
 * @author Pavel Stastny <pavel.stastny at gmail.com>
 */
public interface Work extends Serializable {

    /**
     * Sets result from previous part
     *
     * @param val
     */
    public void setValue(Object val);

    /**
     * Returns result of calculation
     *
     * @return
     */
    public Object getValue();

    public void doWork();
}
