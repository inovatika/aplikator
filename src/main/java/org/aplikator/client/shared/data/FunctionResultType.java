package org.aplikator.client.shared.data;

/**
 *
 */
public enum FunctionResultType {
    MESSAGE, REPORT, DOWNLOAD, WINDOW
}
