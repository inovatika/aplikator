package org.aplikator.server.data;

import org.aplikator.client.shared.data.ContainerNodeDTO;
import org.aplikator.client.shared.data.Operation;
import org.aplikator.server.DescriptorRegistry;
import org.aplikator.server.descriptor.View;

import java.io.Serializable;

@SuppressWarnings("serial")
public class ContainerNode implements Serializable {
    private View view;
    private Record original;
    private Record edited;
    private Operation operation;

    public ContainerNode(View view, Record original, Record edited, Operation operation) {
        this.view = view;
        this.original = original;
        this.edited = edited;
        this.operation = operation;
    }

    /**
     * Conversion from ContainerNodeDTO
     *
     * @param containerNodeDTO
     */
    public ContainerNode(ContainerNodeDTO containerNodeDTO) {
        this((View) DescriptorRegistry.get().getDescriptionItem(containerNodeDTO.getViewId()),
                containerNodeDTO.getOriginal() != null ? new Record(containerNodeDTO.getOriginal()) : null,
                containerNodeDTO.getEdited() != null ? new Record(containerNodeDTO.getEdited()) : null,
                containerNodeDTO.getOperation());
    }

    /**
     * Conversion to ContainerNodeDTO
     *
     * @return
     */
    public ContainerNodeDTO getContainerNodeDTO() {
        return new ContainerNodeDTO(this.getView().getId(),
                this.getOriginal() != null ? this.getOriginal().getRecordDTO() : null,
                this.getEdited() != null ? this.getEdited().getRecordDTO() : null,
                this.getOperation());
    }

    public View getView() {
        return view;
    }

    public Record getOriginal() {
        return original;
    }

    public void setOriginal(Record original) {
        this.original = original;
    }

    public Record getEdited() {
        return edited;
    }

    public void setEdited(Record edited) {
        this.edited = edited;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    /**
     * Returns a record, which contains summary of properties from edited and original.
     * If property exists in both, the merged record contains the edited one
     *
     * @return
     */
    public Record getMerged() {
        // if edited and original is null then return null
        if ((this.getOriginal() == null) && (this.getEdited() == null)) {
            return null;
        }

        // if edited is null then returns cloned original
        if (this.getEdited() == null) {
            return this.getOriginal().clone();
        }

        // if original is null then returns cloned edited
        if (this.getOriginal() == null) {
            return this.getEdited().clone();
        }

        // initially creates a merged record as a clone of edited
        Record mergedRec = this.getEdited().clone();
        for (String property : this.getOriginal().getRecordDTO().getProperties()) {
            // adds properties from original if property is not yet present
            if (!mergedRec.getRecordDTO().getProperties().contains(property)) {
                mergedRec.getRecordDTO().setValue(property, this.getOriginal().getRecordDTO().getValue(property));
            }
        }

        // adds annotations from original
        for (String annotKey : this.getOriginal().getRecordDTO().getAnnotations()) {
            if (!mergedRec.hasAnnotation(annotKey)) {
                mergedRec.putAnnotation(annotKey, this.getOriginal().getAnnotation(annotKey));
            }
        }
        return mergedRec;
    }


}
