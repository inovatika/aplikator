package org.aplikator.server;

import org.aplikator.server.data.AplikatorExceptionHandler;
import org.aplikator.server.data.AuthenticationServiceImpl;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

/**
 *
 */
@ApplicationPath("/security")
public class RestApplicationSecurity extends Application {
    HashSet<Object> singletons = new HashSet<Object>();

    public RestApplicationSecurity() {
        singletons.add(new AuthenticationServiceImpl());
        singletons.add(new AplikatorExceptionHandler());
    }

    @Override
    public Set<Class<?>> getClasses() {
        HashSet<Class<?>> set = new HashSet<Class<?>>();
        return set;
    }

    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }

}

