package org.aplikator.client.shared.descriptor;

import org.jboss.errai.common.client.api.annotations.Portable;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
@Portable
public class ViewDTO extends ClientDescriptorBase implements Cloneable {

    private List<PropertyDTO> properties = new ArrayList<PropertyDTO>();

    private List<QueryDescriptorDTO> filters = new ArrayList<QueryDescriptorDTO>();

    private List<SortDescriptorDTO> sorts = new ArrayList<SortDescriptorDTO>();

    private List<FunctionDTO> functions = new ArrayList<FunctionDTO>();

    private String activeFilter = "";

    private String activeSort = "";

    private String searchString = null;

    private FormDTO formDescriptor;

    private EntityDTO entity;

    private int pageSize;

    private int listPanelWidth;

    private boolean openAsTable = false;

    private QueryDescriptorDTO defaultQueryDescriptor = new QueryDescriptorDTO();


    @SuppressWarnings("unused")
    public ViewDTO() {          //TODO custom marshaller, make private
    }

    public ViewDTO(String id, String localizedName, int pageSize) {
        super(id, localizedName);
        this.pageSize = pageSize;
    }

    public List<PropertyDTO> getProperties() {
        return properties;
    }

    public void setProperties(List<PropertyDTO> properties) {
        this.properties = properties;
    }

    public ViewDTO addProperty(PropertyDTO property) {
        properties.add(property);
        return this;
    }

    public FormDTO getFormDescriptor() {
        return formDescriptor;
    }

    public void setFormDescriptor(FormDTO formDescriptor) {
        this.formDescriptor = formDescriptor;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getListPanelWidth() {
        return listPanelWidth;
    }

    public void setListPanelWidth(int listWidth) {
        this.listPanelWidth = listWidth;
    }


    public boolean isOpenAsTable() {
        return openAsTable;
    }

    public void setOpenAsTable(boolean openAsTable) {
        this.openAsTable = openAsTable;
    }

    public EntityDTO getEntity() {
        return entity;
    }

    public void setEntity(EntityDTO entity) {
        this.entity = entity;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public ViewDTO clone() {
        ViewDTO retval = new ViewDTO(this.getId(), this.getLocalizedName(), this.getPageSize());
        retval.setLocalizedName(this.getLocalizedName());
        retval.setAccess(this.getAccess());
        retval.setListPanelWidth(this.getListPanelWidth());
        retval.setOpenAsTable(this.isOpenAsTable());
        retval.formDescriptor = this.formDescriptor;
        retval.entity = this.entity;
        retval.properties = this.properties;
        for (QueryDescriptorDTO qd : filters) {
            retval.addQueryDescriptor(qd.clone());
        }
        retval.sorts = this.sorts;
        retval.functions = this.functions;
        retval.searchString = this.searchString;
        retval.activeSort = this.activeSort;
        retval.activeFilter = this.activeFilter;
        return retval;
    }


    public ViewDTO addQueryDescriptor(QueryDescriptorDTO queryDescriptorDTO) {
        filters.add(queryDescriptorDTO);
        return this;
    }

    public List<QueryDescriptorDTO> getQueryDescriptors() {
        return filters;
    }

    public void setQueryDescriptors(List<QueryDescriptorDTO> descs) {
        this.filters = descs;
    }


    public QueryDescriptorDTO getActiveQueryDescriptor() {
        if (activeFilter != null && !activeFilter.equals("")) {
            for (QueryDescriptorDTO qd : filters) {
                if (qd.getId().equals(activeFilter)) {
                    return qd;
                }
            }
            throw new IllegalStateException("QueryDescriptor " + activeFilter + " not found");
        } else if (filters.size() > 0) {
            activeFilter = filters.get(0).getId();
            return filters.get(0);
        }
        return defaultQueryDescriptor;
    }


    public ViewDTO addSortDescriptor(SortDescriptorDTO sortDescriptorDTO) {
        sorts.add(sortDescriptorDTO);
        return this;
    }

    public List<SortDescriptorDTO> getSortDescriptors() {
        return sorts;
    }

    public void setSortDescriptors(List<SortDescriptorDTO> sorts) {
        this.sorts = sorts;
    }

    //@JsonIgnore
    public PropertyDTO getActivePrimarySortProperty() {
        if (activeSort != null && !activeSort.equals("")) {
            for (SortDescriptorDTO sd : sorts) {
                if (sd.getId().equals(activeSort)) {
                    return sd.getPrimarySortProperty();
                }
            }
            throw new IllegalStateException("SortDescriptor " + activeSort + " not found");
        } else if (sorts.size() > 0) {
            activeSort = sorts.get(0).getId();
            return sorts.get(0).getPrimarySortProperty();
        }
        return null;
    }

    public int getSortIndex(String sortId) {
        for (int i = 0; i < sorts.size(); i++) {
            if (sorts.get(i).getId().equals(sortId)) {
                return i;
            }
        }
        return 0;
    }

    public int getQueryIndex(String queryId) {
        for (int i = 0; i < filters.size(); i++) {
            if (filters.get(i).getId().equals(queryId)) {
                return i;
            }
        }
        return 0;
    }


    public void setFunctions(List<FunctionDTO> functions) {
        this.functions = functions;
    }

    public ViewDTO addFunction(FunctionDTO functionDTO) {
        if (functionDTO != null) {
            functions.add(functionDTO);
        }
        return this;
    }

    public List<FunctionDTO> getFunctions() {
        return functions;
    }

    public String getActiveFilter() {
        return activeFilter;
    }

    public void setActiveFilter(String activeFilter) {
        this.activeFilter = activeFilter;
    }

    public String getActiveSort() {
        return activeSort;
    }

    public void setActiveSort(String activeSort) {
        this.activeSort = activeSort;
    }


}
