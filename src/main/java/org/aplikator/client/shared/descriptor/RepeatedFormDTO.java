package org.aplikator.client.shared.descriptor;

import org.aplikator.client.local.widgets.HasFields;
import org.aplikator.client.local.widgets.RepeatedFormWidget;

import com.google.gwt.user.client.ui.Widget;

@SuppressWarnings("serial")
//@Portable
public class RepeatedFormDTO extends WidgetPropertyDTOBase {

    private ViewDTO view;

    private boolean defaultSortId = false;

    @SuppressWarnings("unused")
    public RepeatedFormDTO() {//TODO custom marshaller
    }

    public RepeatedFormDTO(PropertyDTO property, ViewDTO view) {
        super(property);
        this.view = view;
    }

    @Override
    public Widget getWidget(HasFields form) {
        RepeatedFormWidget repeatedForm = new RepeatedFormWidget(view, property, getLocalizedName(), form, getAccess(), getSize(), defaultSortId, getCustomLabelStyle());
        if (form != null) {
            form.registerNestedCollection(repeatedForm);
        }
        repeatedForm.setVisible(isVisible());
        return repeatedForm;

    }

    public ViewDTO getView() {
        return this.view;
    }

    public void setView(ViewDTO view) {
        this.view = view;
    }

    public boolean getDefaultSortId() {
        return defaultSortId;
    }

    public void setDefaultSortId(boolean defaultSortId) {
        this.defaultSortId = defaultSortId;
    }
}
