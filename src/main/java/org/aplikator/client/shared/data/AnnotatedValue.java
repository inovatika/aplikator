package org.aplikator.client.shared.data;

import java.io.Serializable;

import org.jboss.errai.common.client.api.annotations.Portable;

@SuppressWarnings("serial")
@Portable
public class AnnotatedValue<T> implements Serializable {
    private T value;

    private String annotation;

    public AnnotatedValue() {

    }

    public void setValue(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }

    public String getAnnotation() {
        return annotation;
    }

    public void setAnnotation(String annotation) {
        this.annotation = annotation;
    }

}
