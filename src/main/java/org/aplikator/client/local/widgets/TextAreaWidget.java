package org.aplikator.client.local.widgets;

import org.aplikator.client.shared.descriptor.PropertyDTO;
import org.gwtbootstrap3.client.ui.TextArea;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;

public class TextAreaWidget extends DataFieldBase<String> implements DataField<String> {
    private TextArea area = new TextArea();
    private String oldValue = "";

    public TextAreaWidget(String caption, String localizedTooltip, PropertyDTO property, int size, int rows, int height, boolean horizontal, String elementId, String customLabelStyle, HasFields form) {
        super(caption, localizedTooltip, property, horizontal, elementId, customLabelStyle, form);
        controlHolder.add(area);
        //area.addStyleName(PaneWidgetResources.INSTANCE.css().formData());
        area.addValueChangeHandler(new ValueChangeHandler<String>() {
            @Override
            public void onValueChange(ValueChangeEvent<String> event) {
                setDirty(true);
            }
        });
        area.addKeyUpHandler(new KeyUpHandler() {
            @Override
            public void onKeyUp(KeyUpEvent event) {
                if (!oldValue.equals(area.getValue())) {
                    oldValue = area.getValue();
                    ValueChangeEvent.fire(area, area.getValue());
                }
            }
        });
        // if (property.getRefferedThrough() != null) {  //handled in the setEnabled method
        //     this.setEnabled(false);
        // }

        this.setGridSize(size);

        if (rows > 0) {
            area.setVisibleLines(rows);
        } else if (height > 0) {
            area.setHeight("" + height + "px");
        } else {
            area.setVisibleLines(4);
        }
    }

    public String getValue() {
        return new String(area.getValue()).trim();
    }

    public void setValue(String value) {
        if (value == null) {
            oldValue = "";
        } else {
            oldValue = value;
        }
        area.setValue(value);
    }

    public HandlerRegistration addValueChangeHandler(ValueChangeHandler<String> handler) {
        return area.addValueChangeHandler(handler);
    }

    public void setEnabled(boolean enabled) {
        if (property.getRefferedThrough() != null) {
            enabled = false;
        }
        area.setEnabled(enabled);
    }

    @Override
    public void grabFocus() {
        Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
            @Override
            public void execute() {
                area.setFocus(true);
            }
        });
    }

}
