package org.aplikator.client.local.widgets;

import org.aplikator.client.local.Aplikator;
import org.gwtbootstrap3.client.ui.Label;

import com.google.gwt.user.client.ui.Composite;

/**
 * Created with IntelliJ IDEA.
 * User: vlahoda
 * Date: 21.03.13
 * Time: 15:41
 * To change this template use File | Settings | File Templates.
 */
public class EmptyLabel extends Composite {
    Label loadingLabel = new Label(Aplikator.application.getConfigString("aplikator.table.empty"));


    public EmptyLabel() {
        initWidget(loadingLabel);
        //loadingLabel.setType(LabelType.INFO);
    }
}
