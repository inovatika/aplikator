package org.aplikator.utils;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import org.aplikator.client.shared.descriptor.ApplicationDTO;
import org.aplikator.client.shared.rpc.AplikatorService;
import org.aplikator.server.AplikatorJaxrsProvider;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

public class ExampleErraiClient {

    public static final String URL = "http://127.0.0.1:8888";

    public static void main(String[] args) {

        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(URL);
        ResteasyWebTarget rtarget
                = (ResteasyWebTarget) target;

        rtarget.register(AplikatorJaxrsProvider.class);//unless the bug in ErraiProvider readFrom is fixed, we must use our patched AplikatorJaxrsProvider

        AplikatorService proxyObject = rtarget.proxy(AplikatorService.class);

        // Proxy object call
        ApplicationDTO application = proxyObject.getApplication();

        System.out.println("Application - brand: " + application.getBrand());


    }
}
