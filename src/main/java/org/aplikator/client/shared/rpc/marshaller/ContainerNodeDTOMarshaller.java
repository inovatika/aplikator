package org.aplikator.client.shared.rpc.marshaller;

import org.aplikator.client.shared.data.ContainerNodeDTO;
import org.aplikator.client.shared.data.Operation;
import org.aplikator.client.shared.data.RecordDTO;
import org.jboss.errai.marshalling.client.api.Marshaller;
import org.jboss.errai.marshalling.client.api.MarshallingSession;
import org.jboss.errai.marshalling.client.api.annotations.ClientMarshaller;
import org.jboss.errai.marshalling.client.api.annotations.ServerMarshaller;
import org.jboss.errai.marshalling.client.api.json.EJObject;
import org.jboss.errai.marshalling.client.api.json.EJValue;
import org.jboss.errai.marshalling.client.marshallers.AbstractNullableMarshaller;

@ClientMarshaller(ContainerNodeDTO.class)
@ServerMarshaller(ContainerNodeDTO.class)
public class ContainerNodeDTOMarshaller extends AbstractNullableMarshaller<ContainerNodeDTO> {

	/*@Override
    public Class<ContainerNode> getTypeHandled() {
		return ContainerNode.class;
	}*/

    @Override
    public ContainerNodeDTO[] getEmptyArray() {
        return new ContainerNodeDTO[0];
    }

    @Override
    public ContainerNodeDTO doNotNullDemarshall(EJValue o, MarshallingSession ctx) {
        if (o.isObject() != null) {
            ContainerNodeDTO cnode = new ContainerNodeDTO();
            EJObject ejObject = o.isObject();
            if (DemarshallingUtils.containsAndNotNull(ejObject, "viewId")) {
                cnode.setViewId(ejObject.get("viewId").isString().stringValue());
            }
            if (DemarshallingUtils.containsAndNotNull(ejObject, "operation")) {
                cnode.setOperation(Operation.valueOf(ejObject.get("operation").isString().stringValue()));
            }

            Marshaller<Object> recMarshaller = ctx.getMarshallerInstance(RecordDTO.class.getName());
            cnode.setOriginal((RecordDTO) recMarshaller.demarshall(ejObject.get("original"), ctx));
            cnode.setEdited((RecordDTO) recMarshaller.demarshall(ejObject.get("edited"), ctx));

            return cnode;

        } else
            return null;
    }

    @Override
    public String doNotNullMarshall(ContainerNodeDTO o, MarshallingSession ctx) {
        StringBuilder builder = new StringBuilder().append('{');
        MarshallingUtils.string("viewId", o.getViewId(), builder).append(',');
        MarshallingUtils.string("operation", o.getOperation().name(), builder).append(',');

        Marshaller<Object> recMarshaller = ctx.getMarshallerInstance(RecordDTO.class.getName());
        MarshallingUtils.key("original", builder).append(':');
        builder.append(recMarshaller.marshall(o.getOriginal(), ctx)).append(',');

        MarshallingUtils.key("edited", builder).append(':');
        builder.append(recMarshaller.marshall(o.getEdited(), ctx));

        builder.append('}');
        return builder.toString();
    }
}
