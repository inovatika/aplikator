package org.aplikator.client.shared.descriptor;

import org.jboss.errai.common.client.api.annotations.Portable;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
@Portable
public class FunctionDTO extends ClientDescriptorBase implements Cloneable {

    private List<PropertyDTO> properties = new ArrayList<PropertyDTO>();
    //private Map<String, WizardPageDTO> wizardPages = new HashMap<String, WizardPageDTO>();
    private boolean visible = true;
    private String elementId = null;



    @SuppressWarnings("unused")
    public FunctionDTO() { //TODO custom marshaller
    }

    public FunctionDTO(String id, String localizedName) {
        super(id, localizedName);
    }

    public List<PropertyDTO> getProperties() {
        return properties;
    }

    public FunctionDTO addProperty(PropertyDTO property) {
        properties.add(property);
        return this;
    }

    public void setProperties(List<PropertyDTO> properties) {
        this.properties = properties;
    }

//    public FunctionDTO registerWizardPageDTO(String key, WizardPageDTO wdto) {
//        this.wizardPages.put(key, wdto);
//        return this;
//    }
//            
//    public FunctionDTO deregisterWizardPageDTO(String key) {
//        this.wizardPages.remove(key);
//        return this;
//    }
//    
//    public WizardPageDTO getRegisteredWizardPageDTO(String key) {
//        return this.wizardPages.get(key);
//    }


    private int size = 1;

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean isVisible() {
        return this.visible;
    }

    public String getElementId() {
        return elementId;
    }

    public void setElementId(String elementId) {
        this.elementId = elementId;
    }


//    public WidgetDTO setSize(int size){
//        this.size = size;
//        return this;
//    }
//
//    @Override
//    public WidgetDTO setFormatPattern(String formatPattern) {
//        return this;
//    }
//
//    @Override
//    public String getFormatPattern() {
//        return null;
//    }
//
//    private boolean enabled = true;
//    @Override
//    public boolean isEnabled() {
//        return enabled;
//    }
//
//    @Override
//    public WidgetDTO setEnabled(boolean enabled) {
//        this.enabled = enabled;
//        return this;
//    }
}
