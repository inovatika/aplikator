package org.aplikator.server;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;


public class LogbackConfigLoader {
    private static Logger logger = LoggerFactory.getLogger(LogbackConfigLoader.class);

    private LogbackConfigLoader() {
    }

    public static void init() {
        String externalConfigFileLocation = Configurator.get().getConfig().getString("aplikator.log.config");
        LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
        File externalConfigFile = new File(externalConfigFileLocation);
        if (externalConfigFile.exists() && externalConfigFile.isFile() && externalConfigFile.canRead()) {
            try {
                JoranConfigurator configurator = new JoranConfigurator();
                configurator.setContext(lc);
                lc.reset();
                configurator.doConfigure(externalConfigFileLocation);
                logger.info("Configured Logback with config file from: " + externalConfigFileLocation);
                return;
            } catch (Exception ex) {
                logger.error("Error reading Logback extrenal configuration file " + externalConfigFileLocation, ex);
            }
        }
        logger.info("Configured Logback with internal configuration file.");
    }
}

