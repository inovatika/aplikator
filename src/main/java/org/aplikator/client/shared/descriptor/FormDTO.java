package org.aplikator.client.shared.descriptor;

import java.util.ArrayList;
import java.util.List;

import org.jboss.errai.common.client.api.annotations.Portable;


@SuppressWarnings("serial")
@Portable
public class FormDTO extends ClientDescriptorBase {


    private List<PropertyDTO> properties = new ArrayList<PropertyDTO>();

    private WidgetDTO layout;


    private boolean horizontal;

    @SuppressWarnings("unused")
    public FormDTO() { //TODO custom marshaller
    }

    public FormDTO(String id, String localizedName, boolean horizontal) {
        super(id, localizedName);
        this.horizontal = horizontal;
    }

    public List<PropertyDTO> getProperties() {
        return properties;
    }

    public void setProperties(List<PropertyDTO> properties) {
        this.properties = properties;
    }

    public FormDTO addProperty(PropertyDTO property) {
        properties.add(property);
        return this;
    }

    public WidgetDTO getLayout() {
        return layout;
    }

    public void setLayout(WidgetDTO layout) {
        this.layout = layout;
    }

    public boolean isHorizontal() {
        return horizontal;
    }

    public void setHorizontal(boolean horizontal) {
        this.horizontal = horizontal;
    }
}
