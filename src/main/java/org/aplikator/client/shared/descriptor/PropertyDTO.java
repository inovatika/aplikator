package org.aplikator.client.shared.descriptor;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.NumberFormat;
import org.aplikator.client.shared.data.ListItem;
import org.aplikator.client.shared.data.RecordDTO;
import org.jboss.errai.common.client.api.annotations.Portable;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@SuppressWarnings("serial")
@Portable
public class PropertyDTO extends ClientDescriptorBase implements Serializable {

    private String type;
    private double size;
    private PropertyDTO refferedThrough;
    private List<ListItem> listValues;
    private boolean editable;
    private boolean required;
    private String formatPattern;


    @SuppressWarnings("unused")
    public PropertyDTO() { //TODO custom marshaller
    }

    public PropertyDTO(String id, String localizedName, String type, double size, PropertyDTO refferedThrough, boolean editable, boolean required, String formatPattern) {
        super(id, localizedName);
        this.type = type;
        this.size = size;
        this.refferedThrough = refferedThrough;
        this.editable = editable;
        this.required = required;
        this.formatPattern = formatPattern;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public PropertyDTO getRefferedThrough() {
        return refferedThrough;
    }

    public void setRefferedThrough(PropertyDTO refferedThrough) {
        this.refferedThrough = refferedThrough;
    }

    public void setFormatPattern(String formatPattern) {
        this.formatPattern = formatPattern;
    }

    public String getFormatPattern() {
        return formatPattern;
    }

    public String getRecordId() {
        if (getRefferedThrough() != null) {
            return getRefferedThrough().getRecordId() + PATH_DELIMITER + getId();
        } else {
            return getId();
        }
    }

    @SuppressWarnings("unchecked")
    public Serializable getValue(RecordDTO record) {
        return (Serializable) record.getValue(getRecordId());
    }

    public void setValue(RecordDTO record, Serializable value) {
        record.setValue(getRecordId(), value);
    }

    public String getStringValue(RecordDTO record) {
        Object val = record.getValue(getRecordId());
        if (val == null) {
            return "";
        }
        if (listValues != null) {
            for (ListItem listValue : listValues) {
                if (val.equals(listValue.getValue())) {
                    return listValue.getName();
                }
            }
        }
        if ("java.math.BigDecimal".equals(type)) {
            return NumberFormat.getFormat(formatPattern).format((BigDecimal) val);
        }
        if ("java.util.Date".equals(type)) {
            return DateTimeFormat.getFormat(formatPattern).format((Date) val);
        }
        return val.toString();
    }

    @Override
    public String toString() {
        return getId();
    }

    public void setListValues(List<ListItem> listValues) {
        this.listValues = listValues;
    }

    public List<ListItem> getListValues() {
        return listValues;
    }


    public PropertyDTO cloneUnreferenced() {

        PropertyDTO remainingReference = null;
        if (getRefferedThrough() != null && getRefferedThrough().getRefferedThrough()!= null){
            remainingReference = getRefferedThrough().cloneUnreferenced();
        }
        PropertyDTO retval = new PropertyDTO(getId(), getLocalizedName(), getType(), getSize(), remainingReference, isEditable(), isRequired(), getFormatPattern());
        retval.setListValues(getListValues());
        return retval;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((refferedThrough == null) ? 0 : refferedThrough.hashCode());
        return result;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (!(obj instanceof PropertyDTO))
            return false;
        PropertyDTO other = (PropertyDTO) obj;
        if (refferedThrough == null) {
            if (other.refferedThrough != null)
                return false;
        } else if (!refferedThrough.equals(other.refferedThrough))
            return false;
        return true;
    }

}
