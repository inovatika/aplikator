package org.aplikator.client.local.widgets;

import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;
import org.gwtbootstrap3.client.ui.constants.ColumnSize;
import org.gwtbootstrap3.client.ui.html.Div;

import java.util.logging.Logger;

/**
 * @author vlahoda
 */
public class LayoutUtils {

    private static Logger LOG = Logger.getLogger(LayoutUtils.class.getName());

    static class DivWrapper extends Div {
        public void attach() {
            onAttach();//Widget.onAttach() is protected
            RootPanel.detachOnWindowClose(this);//mandatory for all widgets without parent widget
        }

    }

    private LayoutUtils() {
    }

    public static Widget addTooltip(Widget w, String message) {
        w.getElement().setAttribute("title", message);
        return w;
    }


    /*public static Widget tooltip(Widget w, String message) {
        Div wr1 = new Div();
        wr1.getElement().getStyle().setDisplay(Style.Display.INLINE_BLOCK);
        wr1.add(w);
        Tooltip tooltip = new Tooltip();
        tooltip.setWidget(wr1);
        tooltip.setTitle(message);
        tooltip.setPlacement(Placement.AUTO);
        tooltip.reconfigure();
        return wr1;
    }*/


    public static ColumnSize size(int size) {
        switch (size) {
            case 1:
                return ColumnSize.XS_1;
            case 2:
                return ColumnSize.XS_2;
            case 3:
                return ColumnSize.XS_3;
            case 4:
                return ColumnSize.XS_4;
            case 5:
                return ColumnSize.XS_5;
            case 6:
                return ColumnSize.XS_6;
            case 7:
                return ColumnSize.XS_7;
            case 8:
                return ColumnSize.XS_8;
            case 9:
                return ColumnSize.XS_9;
            case 10:
                return ColumnSize.XS_10;
            case 11:
                return ColumnSize.XS_11;
            case 12:
                return ColumnSize.XS_12;
        }
        return ColumnSize.XS_1;
    }
}
