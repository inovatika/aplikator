package org.aplikator.client.shared.data;

import org.jboss.errai.common.client.api.annotations.Portable;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@SuppressWarnings("serial")
@Portable
public class RecordContainerDTO {

    private transient static Logger LOG = Logger.getLogger(RecordContainerDTO.class.getName());

    private List<ContainerNodeDTO> nodes = new ArrayList<ContainerNodeDTO>();

    private FunctionResult functionResult;

    public RecordContainerDTO() {
        functionResult = new FunctionResult("", FunctionResultStatus.SUCCESS);
    }


    public void addRecord(String viewId, RecordDTO original, RecordDTO edited, Operation operation) {
        if (viewId == null || "".equals(viewId)) {
            throw new IllegalArgumentException("RecordContainer cannot accept nodes withou ViewId.");
        }
        if (original == null && !(Operation.CREATE.equals(operation) || Operation.PREPARE.equals(operation))) {
            throw new IllegalArgumentException("RecordContainer cannot accept null original for operation " + operation);
        }
        boolean foundExisting = false;
        if (Operation.UPDATE.equals(operation) || Operation.DELETE.equals(operation)) {
            for (ContainerNodeDTO node : nodes) {
                if (original.getPrimaryKey().equals(node.getOriginal().getPrimaryKey())) {
                    if (Operation.UPDATE.equals(node.getOperation()) || Operation.CREATE.equals(node.getOperation())) {
                        node.setEdited(edited);
                        foundExisting = true;
                        break;
                    }
                }
            }
        }
        if (!foundExisting) {
            nodes.add(new ContainerNodeDTO(viewId, original, edited, operation));
        } else {
            if (Operation.DELETE.equals(operation)) {
                for (int i = 0; i < nodes.size(); i++) {
                    if (original.getPrimaryKey().equals(nodes.get(i).getOriginal().getPrimaryKey())) {
                        nodes.remove(i);
                    }
                }
            }
        }
    }

    public List<ContainerNodeDTO> getRecords() {
        return nodes;
    }

    public void setContainerNodes(List<ContainerNodeDTO> nodes) {
        this.nodes = nodes;
    }

    public void clear() {
        nodes.clear();
        functionResult = new FunctionResult("", FunctionResultStatus.SUCCESS);
    }


    public RecordDTO findLastUpdated(PrimaryKey primaryKey) {
        RecordDTO retval = null;
        for (ContainerNodeDTO node : nodes) {
            //if (Operation.UPDATE.equals(node.getOperation())) {
            if (primaryKey.equals(node.getEdited().getPrimaryKey())) {
                retval = node.getEdited();
            }
            //}
        }
        return retval;
    }

    public void updateTemporaryOwnerPK(PrimaryKey rootPK) {
        for (ContainerNodeDTO node : nodes) {
            if (node.getEdited() != null && node.getEdited().getOwnerPrimaryKey() != null && node.getEdited().getOwnerPrimaryKey().getId() == -1) {
                if (rootPK.getTempId().equals(node.getEdited().getOwnerPrimaryKey().getTempId())) {
                    node.getEdited().getOwnerPrimaryKey().setId(rootPK.getId());
                }
            }
        }
    }

    public FunctionResult getFunctionResult() {
        return functionResult;
    }

    public void setFunctionResult(FunctionResult functionResult) {
        this.functionResult = functionResult;
    }
}
