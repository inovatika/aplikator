package org.aplikator.server.persistence.tempstore;


import com.google.common.io.ByteStreams;
import org.aplikator.client.local.widgets.BinaryFieldWidget;
import org.aplikator.server.Configurator;

import java.io.*;
import java.nio.channels.FileChannel;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author vlahoda
 */
public class FileTempstore implements Tempstore {

    private static final Logger LOG = Logger.getLogger(FileTempstore.class.getName());

    private File root;

    public FileTempstore() {
        String rootPath = Configurator.get().getConfig().getString(Configurator.TEMPSTORE_HOME);
        root = new File(rootPath);
        root.mkdirs();
    }

    @Override
    public String store(String filename, InputStream data, boolean closeInput) {
        int index = filename.lastIndexOf(File.separatorChar);//on some systems the filename contains full path
        if (index > -1) {
            filename = filename.substring(index + 1);
        }

        String uuid = UUID.randomUUID().toString();
        File tempDir = new File(root, uuid);
        if (tempDir.mkdir()) {
            File targetFile = new File(tempDir, filename);
            try {
                if (targetFile.createNewFile()) {
                    FileOutputStream target = new FileOutputStream(targetFile);
                    try {
                        ByteStreams.copy(data, target);
                        return uuid;
                    } finally {
                        if (data != null && closeInput) {
                            data.close();
                        }
                        if (target != null) {
                            target.close();
                        }
                    }
                }
            } catch (IOException e) {
                LOG.log(Level.SEVERE, e.toString() + " FILENAME:" + filename + " TEMPDIR:" + tempDir.getPath() + " TARGETFILE:" + targetFile.getPath(), e);
                return BinaryFieldWidget.ERROR_PREFIX + e.getMessage();
            }
        }
        return "";
    }

    @Override
    public String storeString(String filename, String data, String charset) {
        int index = filename.lastIndexOf(File.separatorChar);//on some systems the filename contains full path
        if (index > -1) {
            filename = filename.substring(index + 1);
        }

        String uuid = UUID.randomUUID().toString();
        File tempDir = new File(root, uuid);
        if (tempDir.mkdir()) {
            File targetFile = new File(tempDir, filename);
            try {
                if (targetFile.createNewFile()) {
                    FileOutputStream target = new FileOutputStream(targetFile);
                    try {

                        byte[] bytes = data.getBytes(charset);
                        if ("UTF-8".equals(charset)) {
                            target.write(239);
                            target.write(187);
                            target.write(191);

                        }
                        target.write(bytes);

                        return uuid;
                    } finally {

                        if (target != null) {
                            target.flush();
                            target.close();
                        }
                    }
                }
            } catch (IOException e) {
                LOG.log(Level.SEVERE, e.toString() + " FILENAME:" + filename + " TEMPDIR:" + tempDir.getPath() + " TARGETFILE:" + targetFile.getPath(), e);
                return BinaryFieldWidget.ERROR_PREFIX + e.getMessage();
            }
        }
        return "";
    }

    @Override
    public String getFilename(String id) {
        File tf = getFile(id);
        if (tf != null) {
            return tf.getName();
        }
        return "";
    }

    @Override
    public long getFileLength(String id) {
        File tf = getFile(id);
        if (tf != null) {
            return tf.length();
        }
        return 0;
    }

    private File getFile(String id) {
        File tempDir = new File(root, id);
        if (tempDir.exists()) {
            File[] tempFiles = tempDir.listFiles();
            if (tempFiles.length == 1) {
                return tempFiles[0];
            }
        }
        return null;
    }

    @Override
    public InputStream load(String id) {
        File tf = getFile(id);

        if (tf != null) {
            try {
                return new  MarkableFileInputStream(new FileInputStream(tf));
            } catch (FileNotFoundException e) {
                LOG.severe(e.toString());
            }

        }
        return null;
    }

    @Override
    public void remove(String id) {
        if (id == null || id.isEmpty()){
            return;
        }
        File tf = getFile(id);
        File folder = null;
        if (tf != null) {
            folder = tf.getParentFile();
            tf.delete();
        }
        if (folder != null) {
            folder.delete();
        }
    }

    public static class MarkableFileInputStream extends FilterInputStream {
        private FileChannel myFileChannel;
        private long mark = 0;

        public MarkableFileInputStream(FileInputStream fis) {
            super(fis);
            myFileChannel = fis.getChannel();
        }

        @Override
        public boolean markSupported() {
            return true;
        }

        @Override
        public synchronized void mark(int readlimit) {
            try {
                mark = myFileChannel.position();
            } catch (IOException ex) {
                mark = -1;
            }
        }

        @Override
        public synchronized void reset() throws IOException {
            if (mark == -1) {
                throw new IOException("not marked");
            }
            myFileChannel.position(mark);
        }
    }
}
