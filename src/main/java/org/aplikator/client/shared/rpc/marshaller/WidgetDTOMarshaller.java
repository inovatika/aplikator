package org.aplikator.client.shared.rpc.marshaller;

import org.aplikator.client.shared.descriptor.WidgetDTO;
import org.aplikator.client.shared.rpc.marshaller.WidgetMarshallingUtils.WidgetType;
import org.jboss.errai.marshalling.client.api.MarshallingSession;
import org.jboss.errai.marshalling.client.api.annotations.ClientMarshaller;
import org.jboss.errai.marshalling.client.api.annotations.ServerMarshaller;
import org.jboss.errai.marshalling.client.api.json.EJValue;
import org.jboss.errai.marshalling.client.marshallers.AbstractNullableMarshaller;


@ClientMarshaller(WidgetDTO.class)
@ServerMarshaller(WidgetDTO.class)
public class WidgetDTOMarshaller extends AbstractNullableMarshaller<WidgetDTO> {

	/*@Override
    public Class<WidgetDTO> getTypeHandled() {
		return WidgetDTO.class;
	}*/

    @Override
    public WidgetDTO[] getEmptyArray() {
        return new WidgetDTO[0];
    }

    @Override
    public WidgetDTO doNotNullDemarshall(EJValue o, MarshallingSession ctx) {
        WidgetType lookup = WidgetMarshallingUtils.WidgetType.lookup(o);
        if (lookup != null) {
            return lookup.demarshall(o, ctx);
        } else
            return null;
    }

    @Override
    public String doNotNullMarshall(WidgetDTO o, MarshallingSession ctx) {
        WidgetType lookup = WidgetMarshallingUtils.WidgetType.lookup(o);
        if (lookup != null) {
            String marshalled = lookup.marshall(new StringBuilder(), o, ctx).toString();
            return marshalled;
        } else
            return null;
    }

}
