/*
 * Copyright 2013 Pavel Stastny <pavel.stastny at gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aplikator.server.processes;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Process properties
 *
 * @author Pavel Stastny <pavel.stastny at gmail.com>
 */
public class ProcessPropeties implements Serializable {

//    /** start date key */
//    public static final String START_DATE_KEY = "startDate";
//    
//    /** stop date key */
//    public static final String STOP_DATE_KEY ="stopDate";

    /**
     * user key
     */
    public static final String USER_KEY = "user";

    private Map<String, Object> values = new HashMap<String, Object>();

    /**
     * Put property
     *
     * @param key   Property key
     * @param value Property value
     */
    public void putProperty(String key, Object value) {
        this.values.put(key, value);
    }

    /**
     * Get property
     *
     * @param key
     * @return
     */
    public Object getProperty(String key) {
        return this.values.get(key);
    }


    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<String, Object>();
        for (String k : this.values.keySet()) {
            map.put(k, this.values.get(k));
        }
        return map;
    }
}
