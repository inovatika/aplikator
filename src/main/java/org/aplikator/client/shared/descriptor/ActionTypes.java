package org.aplikator.client.shared.descriptor;

public interface ActionTypes {
    static final String LIST = "list";
    static final String FORM = "form";
    static final String WIZZARD = "wizzard";
}
