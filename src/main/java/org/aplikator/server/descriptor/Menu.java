package org.aplikator.server.descriptor;

import java.util.ArrayList;
import java.util.List;

import org.aplikator.client.shared.descriptor.Access;
import org.aplikator.client.shared.descriptor.MenuDTO;
import org.aplikator.server.data.Context;

public class Menu extends LocalizedServerDescriptorBase {

    private List<Action> actions = new ArrayList<Action>();

    public Menu(String name) {
        super(name);
        this.setLocalizationKey(name);
    }

    public Menu addAction(Action action) {
        actions.add(action);
        return this;
    }

    public List<Action> getActions() {
        return actions;
    }

    public MenuDTO getMenuDTO(Context ctx) {
        if (getAccess(ctx) == Access.NONE) {
            return null;
        }
        MenuDTO retval = new MenuDTO(this.getId(), this.getLocalizedName(ctx));
        for (Action a : actions) {
            retval.addAction(a.getActionDTO(ctx));
        }
        if (retval.getActions().isEmpty()) {
            return null;
        }
        return retval;
    }

    public Menu addView(View view) {
        return addAction(new Action(view.getId(), view.getLocalizationKey(), "list/" + view.getId()));
    }

    public Menu addFunction(Function function) {
        return addAction(new Action(function.getId(), function.getLocalizationKey(), "execute/" + function.getId()));
    }

}
