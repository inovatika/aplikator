package org.aplikator.server.persistence;

import java.sql.Connection;

public interface Transaction {

    Connection getConnection();

}
