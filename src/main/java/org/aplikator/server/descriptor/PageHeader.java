/*
 * Copyright 2013 Pavel Stastny <pavel.stastny at gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aplikator.server.descriptor;

import org.aplikator.client.shared.descriptor.PageHeaderDTO;
import org.aplikator.client.shared.descriptor.WidgetDTO;
import org.aplikator.server.data.Context;

import java.io.Serializable;

/**
 * //?? Subheader
 *
 * @author Pavel Stastny <pavel.stastny at gmail.com>
 */
public class PageHeader<T extends Serializable> extends WidgetPropertyDescriptorBase<T> {


    public PageHeader(Property<T> property) {
        super(property);
    }


    @Override
    public WidgetDTO getWidgetDescriptor(Context ctx) {
        PageHeaderDTO pageHeaderDTO = new PageHeaderDTO(this.getProperty().getPropertyDTO(ctx));
        pageHeaderDTO.setSize(this.getSize());
        pageHeaderDTO.setAccess(getProperty().getAccess(ctx));
        pageHeaderDTO.setEnabled(this.isEnabled(ctx));
        pageHeaderDTO.setVisible(isVisible(ctx));
        pageHeaderDTO.setLocalizedName(getLocalizedName(ctx));
        pageHeaderDTO.setLocalizedTooltip(getLocalizedTooltip(ctx));
        pageHeaderDTO.setHorizontal(isHorizontal());
        pageHeaderDTO.setElementId(getElementId());
        pageHeaderDTO.setCustomLabelStyle(getCustomLabelStyle());
        return pageHeaderDTO;
    }
}
