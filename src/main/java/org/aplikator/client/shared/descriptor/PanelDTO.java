package org.aplikator.client.shared.descriptor;

import com.google.gwt.user.client.ui.Widget;
import org.aplikator.client.local.widgets.HasFields;
import org.aplikator.client.local.widgets.PaneWidget;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("serial")
//@JsonTypeName("panel")
//@Portable
public class PanelDTO extends WidgetDTO {

    public static final String TYPE_KEY = "panel";

    private ArrayList children = new ArrayList();
    private boolean frame = false;

    @SuppressWarnings("unused")
    public PanelDTO() {
    } //TODO custom marshaller

    public PanelDTO(boolean horizontal) {
        setHorizontal(horizontal);
    }

    @Override
    public Widget getWidget(HasFields form) {
        PaneWidget pane = new PaneWidget(getLocalizedName(), getLocalizedTooltip(), frame, isHorizontal(), getSize(), getElementId(), getCustomLabelStyle(), form);
        for (Object chobj : children) {
            WidgetDTO childDescriptor = (WidgetDTO) chobj;
            pane.add(childDescriptor.getWidget(form));
        }
        pane.setVisible(isVisible());
        return pane;
    }

    @SuppressWarnings("unchecked")
    public PanelDTO addChild(WidgetDTO child) {
        children.add(child);
        return this;
    }

    public List getChildren() {
        return this.children;
    }

    public void setChildren(ArrayList children) {
        this.children = children;
    }

    public void setFrame(boolean frame) {
        this.frame = frame;
    }

    public boolean isFrame() {
        return frame;
    }

}
