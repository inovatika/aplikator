/*
 * Copyright 2013 Pavel Stastny <pavel.stastny at gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aplikator.client.local.widgets;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.Widget;
import org.aplikator.client.shared.data.RecordDTO;
import org.aplikator.client.shared.descriptor.FormDTO;
import org.aplikator.client.shared.descriptor.PropertyDTO;
import org.aplikator.client.shared.descriptor.WidgetDTO;
import org.aplikator.client.shared.descriptor.WizardPageDTO;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Pavel Stastny <pavel.stastny at gmail.com>
 */
public class WizardFormWidget extends AbstractFormContainerWidget implements ValueChangeHandler, HasFields {

    private List<DataField<?>> fields = new ArrayList<DataField<?>>();
    private WizardPageDTO view;
    private RecordDTO originalRecordDTO;


    public WizardFormWidget(WizardPageDTO view, FormDTO formDTO) {
        this.view = view;

        WidgetDTO layout = formDTO.getLayout();
        Widget form = layout.getWidget(this);

        initWidget(form);
    }


    @SuppressWarnings("unchecked")
    public void registerDataField(DataField<? extends Object> field) {
//        for (DataField<?> df : fields) {
//            if (df.getProperty().getId().equals(field.getProperty().getId()))
//                throw new IllegalArgumentException("property "+field.getProperty().getId()+" already defined");
//        }
        fields.add(field);
        field.addValueChangeHandler(this);
    }

    @Override
    public void registerNestedCollection(NestedCollectionWidget collection) {
        throw new UnsupportedOperationException("registerNestedCollection not supported");
    }

    @Override
    public void registerBinaryField(BinaryFieldWidget binaryField) {
        throw new UnsupportedOperationException("registerBinaryField not supported");
    }

    @Override
    public void registerFunctionButton(FunctionButtonWidget functionButton) {
        throw new UnsupportedOperationException("registerFunctionButton not supported");
    }

    @SuppressWarnings("unchecked")
    public RecordDTO getWizardRecord() {
        RecordDTO recordDTO = new RecordDTO();
        for (DataField field : fields) {
            PropertyDTO prop = field.getProperty();
            Serializable val = field.getValue();
            prop.setValue(recordDTO, val);
        }
        return recordDTO;
    }

    @SuppressWarnings({"unchecked"})
    public RecordDTO addFieldsToExistingRecord(RecordDTO recordDTO) {
        for (DataField field : fields) {
            PropertyDTO prop = field.getProperty();
            Serializable val = field.getValue();
            prop.setValue(recordDTO, val);
        }
        return recordDTO;
    }

    @Override
    public RecordDTO getPopulateRecord() {
        return this.originalRecordDTO;
    }


    @Override
    public void setDirty(boolean dirty) {

    }

    public void setOriginal(RecordDTO rec) {
        this.originalRecordDTO = rec;
    }

    @Override
    public RecordDTO getOriginal() {
        return this.originalRecordDTO;
    }

    @Override
    public String getPrefix() {
        return "";
    }

    @Override
    public void setPrefix(String prefix) {

    }

    @Override
    public List<DataField<?>> getDataFields() {
        return this.fields;
    }


    @SuppressWarnings({"unchecked"})
    public void onValueChange(ValueChangeEvent event) {
        setDirty(true);
        if (event.getSource().getClass().equals(ReferenceFieldWidget.class)) {
            ReferenceFieldWidget source = (ReferenceFieldWidget) event.getSource();
            RecordDTO reference = source.getSelectedRecord();
            if (reference != null) {
                for (DataField field : fields) {
                    PropertyDTO prop = field.getProperty();
                    if (prop.getRefferedThrough() != null) {// TODO kontrolovat
                        // pres co je
                        // referovano...
                        PropertyDTO unreferencedProperty = prop.cloneUnreferenced();
                        Serializable val = (Serializable) unreferencedProperty.getValue(reference);
                        if (val != null) {
                            field.setValue(val);
                        }
                    }
                }
            } else {
                for (DataField field : fields) {
                    PropertyDTO prop = field.getProperty();
                    if (prop.getRefferedThrough() != null) {
                        field.setValue(null);
                    }
                }
            }
        }

    }

    public void focusFirstField() {
        if (fields.size() > 0) {
            fields.get(0).grabFocus();
        }
    }

    public void addEnterHandler(Command command) {
        for (DataField<?> field : fields) {
            field.addEnterHandler(command);
        }
    }
}
