package org.aplikator.shared.rpc.marshaller;

import java.io.IOException;

import org.aplikator.utils.IOUtils;
import org.aplikator.client.shared.data.ListItem;
import org.aplikator.client.shared.data.PrimaryKey;
import org.aplikator.client.shared.data.RecordDTO;
import org.aplikator.client.shared.descriptor.*;
import org.aplikator.client.shared.rpc.marshaller.*;
import org.easymock.EasyMock;
import org.jboss.errai.marshalling.client.api.Marshaller;
import org.jboss.errai.marshalling.client.api.MarshallingSession;
import org.jboss.errai.marshalling.client.api.json.EJValue;
import org.jboss.errai.marshalling.client.marshallers.StringMarshaller;
import org.jboss.errai.marshalling.server.JSONDecoder;
import org.junit.Assert;

import junit.framework.TestCase;

public class WizardPageDTOMarshallerTest extends TestCase {

    public void testMarshalled() throws IOException {
        byte[] bytes = IOUtils.readBytes(ViewDTOMarshallerTest.class.getResourceAsStream("wizardpagedto.real.txt"), true);
        EJValue decoded = JSONDecoder.decode(new String(bytes));
        WizardPageDTOMarshaller dtomarsh = new WizardPageDTOMarshaller();
        MarshallingSession ctx = ctx();
        WizardPageDTO demarshall = dtomarsh.demarshall(decoded, ctx);
        Assert.assertNotNull(demarshall);
        Assert.assertNotNull(demarshall.getId());
        Assert.assertTrue(demarshall.getId().equals("WizardPage:test_first"));
        Assert.assertNotNull(demarshall.getPageId());
    }

    public void testMarshalled2() throws IOException {
        byte[] bytes = IOUtils.readBytes(ViewDTOMarshallerTest.class.getResourceAsStream("wizardpagedto.real2.txt"), true);
        EJValue decoded = JSONDecoder.decode(new String(bytes));
        WizardPageDTOMarshaller dtomarsh = new WizardPageDTOMarshaller();
        MarshallingSession ctx = ctx();
        WizardPageDTO demarshall = dtomarsh.demarshall(decoded, ctx);
        Assert.assertNotNull(demarshall);
        Assert.assertNotNull(demarshall.getId());
        Assert.assertTrue(demarshall.getId().equals("WizardPage:test_second"));
        Assert.assertNotNull(demarshall.getPageId());
    }

    public void testMarshalled3() throws IOException {
        byte[] bytes = IOUtils.readBytes(ViewDTOMarshallerTest.class.getResourceAsStream("wizardpagedto.real3.txt"), true);
        EJValue decoded = JSONDecoder.decode(new String(bytes));
        WizardPageDTOMarshaller dtomarsh = new WizardPageDTOMarshaller();
        MarshallingSession ctx = ctx();
        WizardPageDTO demarshall = dtomarsh.demarshall(decoded, ctx);
        Assert.assertNotNull(demarshall);
        Assert.assertNotNull(demarshall.getId());
        Assert.assertTrue(demarshall.getId().equals("WizardPage:test_third"));
        Assert.assertNotNull(demarshall.getPageId());
    }

    @SuppressWarnings("unchecked")
    private MarshallingSession ctx() {
        MarshallingSession session = EasyMock.createMock(MarshallingSession.class);

        PrimaryKeyMarshaller pkmarsh = new PrimaryKeyMarshaller();
        Object marsh = pkmarsh;
        EasyMock.expect(session.getMarshallerInstance(PrimaryKey.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        StringMarshaller smarsh = new StringMarshaller();
        marsh = smarsh;
        EasyMock.expect(session.getMarshallerInstance(String.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        QueryDescriptorDTOMarshaller m = new QueryDescriptorDTOMarshaller();
        marsh = m;
        EasyMock.expect(session.getMarshallerInstance(QueryDescriptorDTO.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        SortDescriptorDTOMarshaller sort = new SortDescriptorDTOMarshaller();
        marsh = sort;
        EasyMock.expect(session.getMarshallerInstance(SortDescriptorDTO.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        PropertyDTOMarshaller pmarsh = new PropertyDTOMarshaller();
        marsh = pmarsh;
        EasyMock.expect(session.getMarshallerInstance(PropertyDTO.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        ListItemMarshaller lmarsh = new ListItemMarshaller();
        marsh = lmarsh;
        EasyMock.expect(session.getMarshallerInstance(ListItem.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        FunctionDTOMarshaller fmarshaller = new FunctionDTOMarshaller();
        marsh = fmarshaller;
        EasyMock.expect(session.getMarshallerInstance(FunctionDTO.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        FormDTOMarshaller formmarshaller = new FormDTOMarshaller();
        marsh = formmarshaller;
        EasyMock.expect(session.getMarshallerInstance(FormDTO.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        WidgetDTOMarshaller wmarshaller = new WidgetDTOMarshaller();
        marsh = wmarshaller;
        EasyMock.expect(session.getMarshallerInstance(WidgetDTO.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        QueryParameterDTOMarshaller qmarshaller = new QueryParameterDTOMarshaller();
        marsh = qmarshaller;
        EasyMock.expect(session.getMarshallerInstance(QueryParameterDTO.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        EntityDTOMarshaller emarshaller = new EntityDTOMarshaller();
        marsh = emarshaller;
        EasyMock.expect(session.getMarshallerInstance(EntityDTO.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        ViewDTOMarshaller vmarshaller = new ViewDTOMarshaller();
        marsh = vmarshaller;
        EasyMock.expect(session.getMarshallerInstance(ViewDTO.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        RecordDTOMarshaller recmarsh = new RecordDTOMarshaller();
        marsh = recmarsh;
        EasyMock.expect(session.getMarshallerInstance(RecordDTO.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        EasyMock.replay(session);
        return session;
    }

}
