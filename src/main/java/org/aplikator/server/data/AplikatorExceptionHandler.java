package org.aplikator.server.data;

import org.aplikator.client.shared.rpc.AplikatorExceptionMapper;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

@Provider
public class AplikatorExceptionHandler implements ExceptionMapper<Exception> {
    private static final Logger LOG = Logger.getLogger(AplikatorExceptionHandler.class.getName());

    @Override
    public Response toResponse(Exception exception) {
        String errorID = UUID.randomUUID().toString();
        LOG.log(Level.SEVERE, "REST exception (ERROR_ID: " + errorID + "): " + exception.getMessage(), exception);
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(AplikatorExceptionMapper.APLIKATOR_MESSAGE_START + exception.getMessage() + "\nError ID: " + errorID + AplikatorExceptionMapper.APLIKATOR_MESSAGE_END)
                .build();
    }
}
