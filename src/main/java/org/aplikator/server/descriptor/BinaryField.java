package org.aplikator.server.descriptor;

import org.aplikator.client.shared.descriptor.BinaryFieldDTO;
import org.aplikator.client.shared.descriptor.EntityDTO;
import org.aplikator.client.shared.descriptor.PropertyDTO;
import org.aplikator.client.shared.descriptor.WidgetDTO;
import org.aplikator.server.data.BinaryData;
import org.aplikator.server.data.Context;

public class BinaryField extends WidgetPropertyDescriptorBase<BinaryData> {

    private int height = -1;

    private boolean useThumbnail = true;

    private boolean showTempFile = false;

    public BinaryField(BinaryProperty property) {
        super(property);
    }

    @Override
    public WidgetDTO getWidgetDescriptor(Context ctx) {
        BinaryFieldDTO desc = new BinaryFieldDTO(untypedClientClone(ctx), new EntityDTO(getProperty().getEntity().getId(), getProperty().getEntity().getLocalizedName(ctx)), height, useThumbnail, showTempFile);
        desc.setSize(getSize());
        desc.setAccess(getProperty().getAccess(ctx));
        desc.setEnabled(isEnabled(ctx));
        desc.setVisible(isVisible(ctx));
        desc.setHorizontal(isHorizontal());
        desc.setElementId(getElementId());
        desc.setCustomLabelStyle(getCustomLabelStyle());
        desc.setLocalizedName(getLocalizedName(ctx));
        desc.setLocalizedTooltip(getLocalizedTooltip(ctx));
        return desc;
    }

    private PropertyDTO untypedClientClone(Context ctx) {
        Property<BinaryData> property = getProperty();
        PropertyDTO clientRefThrough = null;
        if (property.getRefferedThrough() != null) {
            clientRefThrough = property.getRefferedThrough().getPropertyDTO(ctx);
        }
        PropertyDTO retval = new PropertyDTO(property.getId(), property.getLocalizedName(ctx), property.getType().getName(), property.getSize(), clientRefThrough, property.isEditable(), property.isRequired(), property.getFormatPattern());
        return retval;
    }

    @Override
    public void registerProperties(Form form) {
        //form.addProperty(property);
    }


    public BinaryField setHeight(int height) {
        this.height = height;
        return this;
    }

    public BinaryField useThumbnail(boolean useThumbnail) {
        this.useThumbnail = useThumbnail;
        return this;
    }

    public BinaryField setShowTempFile(boolean showTempFile) {
        this.showTempFile = showTempFile;
        return this;
    }
}
