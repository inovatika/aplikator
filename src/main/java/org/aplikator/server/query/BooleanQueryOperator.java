package org.aplikator.server.query;

public enum BooleanQueryOperator {
    AND, OR
}
