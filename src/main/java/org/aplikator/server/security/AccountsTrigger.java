package org.aplikator.server.security;

import org.aplikator.server.data.Context;
import org.aplikator.server.data.Record;

/**
 * This interface should be implemented by PersisterTriggers of dedicated entity used for Accounts management
 */
public interface AccountsTrigger {
    /**
     * Obtain Account from given Record
     * @param record
     * @return
     */
    public Account getAccount(Record record, Context ctx);
}
