/*
 * Copyright 2013 Pavel Stastny <pavel.stastny at gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aplikator.server.processes;

import static org.aplikator.server.processes.ProcessConfiguration.threadConf;

import java.io.File;
import java.io.IOException;
import java.util.List;


/**
 * @author Pavel Stastny <pavel.stastny at gmail.com>
 */
public class ProcessTest {
    private static File outFile;
    private static File file;


    @SuppressWarnings("unchecked")
    public static void main(String[] args) throws CannotCallStartException, IOException, IOException, InterruptedException {

        ThreadProcessConfiguration conf = threadConf();//.classpathlib(System.getProperty("user.dir") + File.separator + "libs");

        Process process = ProcessFactory.get(ProcessType.THREAD).create(conf, new RunnableSerializationAware() {

            @Override
            public void run() {
                System.out.println("... testik ... ");
            }
        });


        /*outFile = new File(System.getProperty("user.dir") + File.separator + "err.txt");
        outFile.createNewFile();

        process.redirectErrorStream(outFile);
        file = new File(System.getProperty("user.dir") + File.separator + "out.txt");
        file.createNewFile();
        process.redirectOutputStream(file);
*/

        List<Process> processes = ProcessManager.getManager().getProcesses();
        for (Process p : processes) {
            System.out.println(p.getIdentifier() + " " + p.getProcessState().name());
        }

        process.startMe();
        System.out.println("After start....");

        processes = ProcessManager.getManager().getProcesses();
        for (Process p : processes) {
            System.out.println(p.getIdentifier() + " " + p.getProcessState().name() + " instance id 0x" + Integer.toHexString(System.identityHashCode(p)));
        }
        Thread.sleep(8000);

        processes = ProcessManager.getManager().getProcesses();
        for (Process p : processes) {
            System.out.println(p.getIdentifier() + " " + p.getProcessState().name() + " instance id 0x" + Integer.toHexString(System.identityHashCode(p)));
        }

        ProcessManager.getManager().shutDown();

    }
}
