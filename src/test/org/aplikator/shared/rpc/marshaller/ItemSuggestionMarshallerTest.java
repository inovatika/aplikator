package org.aplikator.shared.rpc.marshaller;

import org.aplikator.client.shared.data.ItemSuggestion;
import org.aplikator.client.shared.data.PrimaryKey;
import org.aplikator.client.shared.data.RecordDTO;
import org.aplikator.client.shared.rpc.marshaller.ItemSuggestionMarshaller;
import org.aplikator.client.shared.rpc.marshaller.PrimaryKeyMarshaller;
import org.aplikator.client.shared.rpc.marshaller.RecordDTOMarshaller;
import org.easymock.EasyMock;
import org.jboss.errai.marshalling.client.api.Marshaller;
import org.jboss.errai.marshalling.client.api.MarshallingSession;
import org.jboss.errai.marshalling.client.marshallers.StringMarshaller;
import org.jboss.errai.marshalling.server.JSONDecoder;
import org.junit.Assert;

import junit.framework.TestCase;

public class ItemSuggestionMarshallerTest extends TestCase {

    public void testMarshall() {

        ItemSuggestion itm = new ItemSuggestion("Napoveda", 1, "replace", RecordsPageDTOMarshallerTest.createRecord());
        ItemSuggestionMarshaller marshaller = new ItemSuggestionMarshaller();


        MarshallingSession session = ctx();
        String m = marshaller.marshall(itm, session);
        Assert.assertNotNull(m);

        ItemSuggestion itmSug = marshaller.demarshall(JSONDecoder.decode(m), session);
        Assert.assertTrue(itmSug.getId() == 1);
        Assert.assertTrue(itmSug.getDisplayString().equals(itm.getDisplayString()));
        Assert.assertTrue(itmSug.getReplacementString().equals(itm.getReplacementString()));
        Assert.assertTrue(itmSug.getRecord() != null);
        System.out.println(m);

    }

    @SuppressWarnings("unchecked")
    private MarshallingSession ctx() {
        MarshallingSession session = EasyMock.createMock(MarshallingSession.class);
        PrimaryKeyMarshaller pkmarsh = new PrimaryKeyMarshaller();
        Object marsh = pkmarsh;
        EasyMock.expect(session.getMarshallerInstance(PrimaryKey.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        StringMarshaller smarsh = new StringMarshaller();
        marsh = smarsh;
        EasyMock.expect(session.getMarshallerInstance(String.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        RecordDTOMarshaller recMarsh = new RecordDTOMarshaller();
        marsh = recMarsh;
        EasyMock.expect(session.getMarshallerInstance(RecordDTO.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        EasyMock.replay(session);
        return session;
    }
}
