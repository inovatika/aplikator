package org.aplikator.client.shared.descriptor;

import com.google.gwt.user.client.ui.Widget;
import org.aplikator.client.local.widgets.HasFields;
import org.jboss.errai.common.client.api.annotations.Portable;

import java.io.Serializable;

@Portable
public abstract class WidgetDTO implements Serializable {

    private boolean horizontal = false;
    private int size = 1;
    private String formatPattern;
    private boolean enabled;
    private boolean visible = true;
    private String elementId = null;
    private String customLabelStyle = null;
    private String icon = null;
    private Access access = Access.READ_WRITE_CREATE_DELETE;



    private String localizedName;
    private String localizedTooltip;

    public WidgetDTO setLocalizedName(String localizedName) {
        this.localizedName = localizedName;
        return this;
    }

    public String getLocalizedName() {
        return localizedName;
    }

    public WidgetDTO setLocalizedTooltip(String localizedTooltip) {
        this.localizedTooltip = localizedTooltip;
        return this;
    }

    public String getLocalizedTooltip() {
        return localizedTooltip;
    }

    public int getSize() {
        return this.size;
    }

    public WidgetDTO setSize(int size) {
        this.size = size;
        return this;
    }

    public WidgetDTO setFormatPattern(String formatPattern) {
        this.formatPattern = formatPattern;
        return this;
    }

    public String getFormatPattern() {
        return this.formatPattern;
    }

    public WidgetDTO setEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    public boolean isEnabled() {
        return this.enabled;
    }

    public WidgetDTO setVisible(boolean visible) {
        this.visible = visible;
        return this;
    }

    public boolean isVisible() {
        return this.visible;
    }

    public boolean isHorizontal() {
        return horizontal;
    }

    public void setHorizontal(boolean horizontal) {
        this.horizontal = horizontal;
    }

    public String getElementId() {
        return elementId;
    }

    public void setElementId(String elementId) {
        this.elementId = elementId;
    }

    public String getCustomLabelStyle() {
        return customLabelStyle;
    }

    public void setCustomLabelStyle(String customLabelStyle) {
        this.customLabelStyle = customLabelStyle;
    }

    public WidgetDTO setAccess(Access access) {
        this.access = access;
        return this;
    }

    public Access getAccess() {
        return this.access;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public abstract Widget getWidget(HasFields form);
}
