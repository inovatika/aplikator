package org.aplikator.server.query;

import java.util.List;

import org.aplikator.server.descriptor.QueryParameter;

public class BooleanQueryExpression extends QueryExpression {

    private final QueryExpression left;
    private final BooleanQueryOperator operator;
    private final QueryExpression right;

    public BooleanQueryExpression(QueryExpression left, BooleanQueryOperator operator, QueryExpression right) {
        super();
        this.left = left;
        this.operator = operator;
        this.right = right;
    }

    public QueryExpression getLeft() {
        return left;
    }

    public BooleanQueryOperator getOperator() {
        return operator;
    }

    public QueryExpression getRight() {
        return right;
    }

    @Override
    public void bindParameters(List<QueryParameter> parameters) {
        left.bindParameters(parameters);
        right.bindParameters(parameters);
    }

}
