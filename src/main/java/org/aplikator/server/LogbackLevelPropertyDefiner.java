package org.aplikator.server;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.core.PropertyDefinerBase;
import ch.qos.logback.core.spi.PropertyDefiner;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigObject;
import com.typesafe.config.ConfigValue;

import java.util.Map;

public class LogbackLevelPropertyDefiner extends PropertyDefinerBase implements PropertyDefiner {
    @Override
    public String getPropertyValue() {
        Config config = Configurator.get().getConfig();
        LoggerContext loggerContext = (LoggerContext) getContext();
        if (config.hasPath("aplikator.log.class")) {
            setLoggingLevels(config.getObject("aplikator.log.class"), "", loggerContext);
        }
        if (config.hasPath("aplikator.log.level")) {
            return config.getString("aplikator.log.level");
        } else {
            return "INFO";
        }
    }

    private void setLoggingLevels(ConfigObject configObject, String prefix, LoggerContext loggerContext) {
        for (Map.Entry<String, ConfigValue> entry : configObject.entrySet()) {
            String currentPath = prefix.isEmpty() ? entry.getKey() : prefix + "." + entry.getKey();

            if (entry.getValue() instanceof ConfigObject) {
                setLoggingLevels((ConfigObject) entry.getValue(), currentPath, loggerContext);
            } else {
                String level = entry.getValue().unwrapped().toString();
                loggerContext.getLogger(currentPath).setLevel(Level.valueOf(level));
            }
        }
    }
}
