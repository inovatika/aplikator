package org.aplikator.shared.rpc.marshaller;

import java.util.ArrayList;
import java.util.List;

import org.aplikator.client.shared.descriptor.ActionDTO;
import org.aplikator.client.shared.descriptor.MenuDTO;
import org.aplikator.client.shared.rpc.marshaller.ActionDTOMarshaller;
import org.aplikator.client.shared.rpc.marshaller.MenuDTOMarshaller;
import org.easymock.EasyMock;
import org.jboss.errai.marshalling.client.api.Marshaller;
import org.jboss.errai.marshalling.client.api.MarshallingSession;
import org.jboss.errai.marshalling.client.api.json.EJValue;
import org.jboss.errai.marshalling.server.JSONDecoder;
import org.junit.Assert;

import junit.framework.TestCase;

public class MenuDTOMarshallerTest extends TestCase {

    public static List<ActionDTO> list() {
        List<ActionDTO> list = new ArrayList<ActionDTO>();
        list.add(new ActionDTO("1", "locname-1", "token-1"));
        list.add(new ActionDTO("2", "locname-2", "token-2"));
        list.add(new ActionDTO("3", "locname-3", "token-3"));
        list.add(new ActionDTO("4", "locname-4", "token-4"));
        return list;
    }


    public void testMenuDTO() {
        MarshallingSession session = ctx();

        MenuDTO mdto = new MenuDTO("mdto1", "locname-1");
        for (ActionDTO adto : list()) {
            mdto.addAction(adto);
        }
        MenuDTOMarshaller marshTest = new MenuDTOMarshaller();
        String marshalled = marshTest.marshall(mdto, session);

        EJValue decoded = JSONDecoder.decode(marshalled);
        MenuDTO mdtoDem = marshTest.demarshall(decoded, session);

        Assert.assertTrue(mdtoDem.getId().equals("mdto1"));
        Assert.assertTrue(mdtoDem.getLocalizedName().equals("locname-1"));

        Assert.assertTrue(mdto.getActions().size() == 4);
        Assert.assertTrue(mdto.getActions().get(0).getId().equals("1"));
        Assert.assertTrue(mdto.getActions().get(0).getLocalizedName().equals("locname-1"));
        Assert.assertTrue(mdto.getActions().get(0).getToken().equals("token-1"));

        Assert.assertTrue(mdto.getActions().get(1).getId().equals("2"));
        Assert.assertTrue(mdto.getActions().get(1).getLocalizedName().equals("locname-2"));
        Assert.assertTrue(mdto.getActions().get(1).getToken().equals("token-2"));

        Assert.assertTrue(mdto.getActions().get(2).getId().equals("3"));
        Assert.assertTrue(mdto.getActions().get(2).getLocalizedName().equals("locname-3"));
        Assert.assertTrue(mdto.getActions().get(2).getToken().equals("token-3"));

        Assert.assertTrue(mdto.getActions().get(3).getId().equals("4"));
        Assert.assertTrue(mdto.getActions().get(3).getLocalizedName().equals("locname-4"));
        Assert.assertTrue(mdto.getActions().get(3).getToken().equals("token-4"));

        Assert.assertTrue(mdtoDem.getLocalizedName().equals("locname-1"));
    }


    @SuppressWarnings("unchecked")
    private MarshallingSession ctx() {
        MarshallingSession session = EasyMock.createMock(MarshallingSession.class);
        ActionDTOMarshaller adtoMarsh = new ActionDTOMarshaller();
        Object marsh = adtoMarsh;
        EasyMock.expect(session.getMarshallerInstance(ActionDTO.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        EasyMock.replay(session);
        return session;
    }
}
