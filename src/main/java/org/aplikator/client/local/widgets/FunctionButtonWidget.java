package org.aplikator.client.local.widgets;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import org.aplikator.client.local.Aplikator;
import org.aplikator.client.local.wizards.WizardSupport;
import org.aplikator.client.local.wizards.WizardSupportCallbacks;
import org.aplikator.client.shared.data.ClientContext;
import org.aplikator.client.shared.descriptor.FunctionDTO;
import org.aplikator.client.shared.descriptor.WizardPageDTO;
import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.constants.IconType;

public class FunctionButtonWidget extends Button implements WizardSupportCallbacks {

    private WizardSupport wizardSupport;
    final FormWidget containingForm;


    public FunctionButtonWidget(final FunctionDTO function, final FormWidget containingForm, String elementId, String customLabelStyle, String icon, HasFields form) {
        this.containingForm = containingForm;
        ClientContext clientContext = new ClientContext();
        clientContext.setUser(Aplikator.application.getUsername());
        clientContext.setLocale(Aplikator.application.getLocale());
        clientContext.setCurrentRecord(containingForm.getOriginal());
        clientContext.setView(containingForm.getView());
        this.wizardSupport = new WizardSupport(function, clientContext, this);
        if (containingForm != null) {
            containingForm.registerFunctionButton(this);
        }

        if (icon != null) {
            this.setIcon(IconType.fromStyleName(icon));
            this.setText(""); // pouziva se ikona misto textu
        } else {
            this.setText(function.getLocalizedName());
        }
        if (function.getLocalizedTooltip() != null && !function.getLocalizedTooltip().isEmpty()
                && !"null".equals(function.getLocalizedTooltip())) {
            this.getElement().setAttribute("title", function.getLocalizedTooltip());
        }
        this.addStyleName("functionButton");

        this.addClickHandler(new ClickHandler() {
            public void onClick(ClickEvent event) {
                if (containingForm != null) {
                    wizardSupport.getClientContext().setCurrentRecord(containingForm.getOriginal());
                }
                wizardSupport.changePageRequest(true);
            }
        });
        if (elementId!= null && form != null){
            this.getElement().setId(form.getPrefix()+"-"+elementId);
        }
        if (customLabelStyle!= null){
            this.getElement().addClassName(customLabelStyle);
        }
    }


    @Override
    public void wizardPageCallback(WizardPageDTO response, boolean forw) {

    }

    @Override
    public void runBeforeFunctionCallback() {
        this.setEnabled(false);
    }

    @Override
    public void runAfterFunctionCallback() {
        this.setEnabled(true);
        if (containingForm != null) {
            this.containingForm.refreshRecord();
        }
    }
}
