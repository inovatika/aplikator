package org.aplikator.shared.rpc.marshaller;

import org.aplikator.client.shared.data.PrimaryKey;
import org.aplikator.client.shared.rpc.marshaller.PrimaryKeyMarshaller;
import org.jboss.errai.marshalling.client.api.json.EJValue;
import org.jboss.errai.marshalling.server.JSONDecoder;
import org.junit.Assert;

import junit.framework.TestCase;

public class PrimaryKeyMarshallerTest extends TestCase {

    public void testMarshall() {
        PrimaryKeyMarshaller pkmarsh = new PrimaryKeyMarshaller();
        PrimaryKey pk = new PrimaryKey("entity-1", 1);
        String marshall = pkmarsh.marshall(pk, null);

        EJValue decoded = JSONDecoder.decode(marshall);
        PrimaryKey pkDem = pkmarsh.demarshall(decoded, null);


        Assert.assertTrue(pkDem.getId() == pk.getId());
        Assert.assertTrue(pkDem.getEntityId().equals(pk.getEntityId()));
        Assert.assertNull(pkDem.getTempId());
    }
}
