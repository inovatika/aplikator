package org.aplikator.server.data;

import org.aplikator.server.descriptor.View;
import org.aplikator.server.query.QueryExpression;

public interface PersisterTriggers {

    void onPrepare(ContainerNode node, boolean isCopy, Context ctx);

    void onCreate(ContainerNode node, Context ctx);

    void onUpdate(ContainerNode node, Context ctx);

    void onDelete(ContainerNode node, Context ctx);

    void onLoad(Record record, View view, Context ctx);

    void afterCommit(ContainerNode node, Context ctx);

    default void onPageLoad(Record record, View view, Context ctx){
        onLoad(record, view, ctx);
    }

    QueryExpression  addToQueryExpression(QueryExpression queryExpression, View view, Context ctx);

    View changeView(View view, Context ctx);

    public static class Default implements PersisterTriggers {

        @Override
        public void onPrepare(ContainerNode node, boolean isCopy, Context ctx) {
        }

        @Override
        public void onCreate(ContainerNode node, Context ctx) {
        }

        @Override
        public void onUpdate(ContainerNode node, Context ctx) {
        }

        @Override
        public void onDelete(ContainerNode node, Context ctx) {
        }

        @Override
        public void onLoad(Record record, View view, Context ctx) {
            record.setPreview(record.toSafeHTML(view, ctx));
        }

        @Override
        public void afterCommit(ContainerNode node, Context ctx) {
        }

        @Override
        public QueryExpression addToQueryExpression(QueryExpression queryExpression, View view, Context ctx) {
            return queryExpression;
        }

        @Override
        public View changeView(View view, Context ctx) {
            return view;
        }

    }

}
