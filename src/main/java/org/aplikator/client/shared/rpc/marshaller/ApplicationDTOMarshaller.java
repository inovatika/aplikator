package org.aplikator.client.shared.rpc.marshaller;

import org.aplikator.client.shared.descriptor.ActionDTO;
import org.aplikator.client.shared.descriptor.ApplicationDTO;
import org.aplikator.client.shared.descriptor.MenuDTO;
import org.jboss.errai.marshalling.client.api.MarshallingSession;
import org.jboss.errai.marshalling.client.api.annotations.ClientMarshaller;
import org.jboss.errai.marshalling.client.api.annotations.ServerMarshaller;
import org.jboss.errai.marshalling.client.api.json.EJObject;
import org.jboss.errai.marshalling.client.api.json.EJValue;
import org.jboss.errai.marshalling.client.marshallers.AbstractNullableMarshaller;

import java.util.List;
import java.util.Map;
import java.util.Set;


@ClientMarshaller(ApplicationDTO.class)
@ServerMarshaller(ApplicationDTO.class)
public class ApplicationDTOMarshaller extends
        AbstractNullableMarshaller<ApplicationDTO> {

    public ApplicationDTOMarshaller() {
        super();
    }

	/*@Override
    public Class<ApplicationDTO> getTypeHandled() {
		return ApplicationDTO.class;
	}*/

    @Override
    public ApplicationDTO[] getEmptyArray() {
        return new ApplicationDTO[0];
    }

    @Override
    public ApplicationDTO doNotNullDemarshall(EJValue o, MarshallingSession ctx) {
        if (o.isObject() != null) {
            EJObject object = o.isObject();
            String username = null;
            if (object.containsKey("username") && object.get("username").isString() != null) {
                username = object.get("username").isString().stringValue();
            }
            String locale = null;
            if (object.containsKey("locale") && object.get("locale").isString() != null) {
                locale = object.get("locale").isString().stringValue();
            }
            ApplicationDTO retval = new ApplicationDTO(username, locale);

            if (object.get("brand").isString() != null) {
                retval.setBrand(object.get("brand").isString().stringValue());
            }
            if (object.get("showNavigation").isBoolean() != null) {
                retval.setShowNavigation(object.get("showNavigation").isBoolean().booleanValue());
            }
            if (object.get("showLoginSSOButton").isBoolean() != null) {
                retval.setShowLoginSSOButton(object.get("showLoginSSOButton").isBoolean().booleanValue());
            }
            if (object.containsKey("defaultActionToken") && object.get("defaultActionToken").isString() != null) {
                retval.setDefaultAction(object.get("defaultActionToken").isString().stringValue());
            }


            if (object.get("config").isObject() != null) {
                Map map = DemarshallingUtils.mapUnMarshall(object.get("config"), ctx, String.class.getName());
                Set keySet = map.keySet();
                for (Object ob : keySet) {
                    retval.setConfigString(ob.toString(), map.get(ob).toString());
                }
            }

            if (object.get("menus").isArray() != null) {
                List list = DemarshallingUtils.listUnMarshall(object.get("menus"), ctx, MenuDTO.class.getName());
                for (Object ob : list) {
                    MenuDTO mdto = (MenuDTO) ob;
                    retval.addMenu(mdto);
                }
            }

            if (object.get("serviceMenu").isArray() != null) {
                List list = DemarshallingUtils.listUnMarshall(object.get("serviceMenu"), ctx, ActionDTO.class.getName());
                for (Object ob : list) {
                    ActionDTO mdto = (ActionDTO) ob;
                    retval.addServiceMenuItem(mdto);
                }
            }
            return retval;
        } else
            return null;
    }

    @Override
    public String doNotNullMarshall(ApplicationDTO o, MarshallingSession ctx) {
        StringBuilder builder = new StringBuilder("{");
        MarshallingUtils.string("brand", o.getBrand(), builder).append(',');
        MarshallingUtils.bool("showNavigation", o.isShowNavigation(), builder).append(',');
        MarshallingUtils.bool("showLoginSSOButton", o.isShowLoginSSOButton(), builder).append(',');

        MarshallingUtils.key("menus", builder).append(':');
        MarshallingUtils.listMarshall(builder, o.getMenus(), ctx, MenuDTO.class.getName()).append(',');


        MarshallingUtils.key("serviceMenu", builder).append(':');
        MarshallingUtils.listMarshall(builder, o.getServiceMenu(), ctx, ActionDTO.class.getName()).append(',');

        MarshallingUtils.key("config", builder).append(':');
        MarshallingUtils.mapMarshall(builder, o.getConfig(), ctx, String.class.getName());

        //defaultActionToken
        if (o.getDefaultActionToken() != null) {
            builder.append(',');
            MarshallingUtils.string("defaultActionToken", o.getDefaultActionToken(), builder);
        }
        if (o.getUsername() != null) {
            builder.append(',');
            MarshallingUtils.string("username", o.getUsername(), builder);
        }
        if (o.getLocale() != null) {
            builder.append(',');
            MarshallingUtils.string("locale", o.getLocale(), builder);
        }

        builder.append("}");
        return builder.toString();
    }
}
