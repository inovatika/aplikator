package org.aplikator.server.descriptor;

import com.google.common.collect.Lists;
import org.aplikator.client.shared.data.ListItem;
import org.aplikator.server.Configurator;
import org.aplikator.server.ListRegistry;
import org.aplikator.server.data.Context;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Classes implementing this interface are used as sources of items in {@link ComboBox} pop-up list
 */
public interface ListProvider {

    /**
     * @return the unique identifier of the provided list
     */
    String getId();

    /**
     * @param ctx {@link Context} with information for generating properly localized and secured list
     * @return the List of {@link ListItem} for this list, localization should correspond to the Locale in provided {@link Context}
     */
    List<ListItem> getListValues(Context ctx);

    /**
     * Default implementation of the {@link ListProvider}
     * Handles the registration of the list in the {@link ListRegistry}
     * Contains just the list identifier and static list of ListItems.
     */
    public static class Default implements ListProvider {

        private String id;

        protected List<ListItem> values;

        protected  boolean sortByName = false;

        protected  boolean leaveUnsorted = false;

        /**
         * Constructs new empty list with the given identifier.
         *
         * @param id unique identifier of the list. If the value is null, it is replaced by the value this.getClass().getName()
         */
        protected Default(String id) {
            if (id == null) {
                id = getClass().getName();
            }
            this.id = id;
            ListRegistry.get().registerListProvider(this);
            values = Lists.newArrayList();
        }

        /**
         * Constructs new  list with the given identifier and items.
         *
         * @param id    unique identifier of the list. If the value is null, it is replaced by the value this.getClass().getName()
         * @param items array of List items
         */
        public Default(String id, ListItem... items) {
            this(id);
            values = Arrays.asList(items);
        }

        /**
         * Constructs new  list with the given identifier and items
         *
         * @param id   unique identifier of the list. If the value is null, it is replaced by the value this.getClass().getName()
         * @param vals List of List items
         */
        public Default(String id, List<ListItem> vals) {
            this(id);
            values = vals;
        }


        public Default sortByName() {
            this.sortByName = true;
            return this;
        }

        public Default leaveUnsorted() {
            this.leaveUnsorted = true;
            return this;
        }

        @Override
        public String getId() {
            return id;
        }

        @Override
        public List<ListItem> getListValues(Context ctx) {

            List<ListItem> clientListValues = new ArrayList<ListItem>(
                    values.size());
            for (ListItem item : values) {
                ListItem clientItem = new ListItem.Default(
                        item.getValue(), Configurator.get().getLocalizedString(
                        item.getName(), ctx.getUserLocale()));
                clientListValues.add(clientItem);
            }
            if (sortByName) {
                Collections.sort(clientListValues, new ListItem.Comparator());
            } else if (!leaveUnsorted){
                Collections.sort(clientListValues, new ListItem.KeyComparator());
            }
            return clientListValues;
        }
    }
}
