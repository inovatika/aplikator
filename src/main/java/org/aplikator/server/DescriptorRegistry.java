package org.aplikator.server;

import org.aplikator.server.descriptor.ServerDescriptorBase;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public final class DescriptorRegistry {
    // singleton
    private static class LazyHolder {
        private static DescriptorRegistry instance = new DescriptorRegistry();
    }

    private static final Logger LOG = Logger.getLogger(DescriptorRegistry.class.getName());


    private DescriptorRegistry() {
    }

    public static DescriptorRegistry get() {
        return LazyHolder.instance;
    }

    private static Map<String, ServerDescriptorBase> descriptionItems = new HashMap<String, ServerDescriptorBase>();

    public synchronized void registerDescriptionItem(ServerDescriptorBase item) {
        if (descriptionItems.containsKey(item.getId())) {
            throw new IllegalArgumentException("Description item with ID '" + item.getId() + "' is already registered");
        }
        descriptionItems.put(item.getId(), item);
    }

    public ServerDescriptorBase getDescriptionItem(String id) {
        ServerDescriptorBase retval = descriptionItems.get(id);
        if (retval == null && !id.startsWith("Reference")) {
            LOG.warning("Could not find DescriptionItem " + id);
        }
        return retval;
    }

    public synchronized boolean containsItem(String id) {
        return descriptionItems.containsKey(id);
    }

}
