package org.aplikator.server;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.aplikator.server.descriptor.ListProvider;

public final class ListRegistry {
    private static class LazyHolder {
        private static ListRegistry instance = new ListRegistry();
    }

    private static final Logger LOG = Logger.getLogger(ListRegistry.class.getName());


    private ListRegistry() {
    }

    public static ListRegistry get() {
        return LazyHolder.instance;
    }

    private static Map<String, ListProvider> listProviders = new HashMap<String, ListProvider>();

    public synchronized void registerListProvider(ListProvider item) {
        if (listProviders.containsKey(item.getId())) {
            throw new IllegalArgumentException("ListProvider with ID '" + item.getId() + "' is already registered");
        }
        listProviders.put(item.getId(), item);
    }

    public ListProvider getListProvider(String id) {
        ListProvider retval = listProviders.get(id);
        if (retval == null) {
            LOG.warning("Could not find ListProvider " + id);
        }
        return retval;
    }

    public synchronized boolean containsItem(String id) {
        return listProviders.containsKey(id);
    }

}
