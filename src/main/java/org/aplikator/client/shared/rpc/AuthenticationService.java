package org.aplikator.client.shared.rpc;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import java.util.List;

import org.aplikator.client.shared.data.FunctionParameters;
import org.aplikator.client.shared.data.FunctionResult;
import org.aplikator.client.shared.data.ListItem;
import org.aplikator.client.shared.data.RecordContainerDTO;
import org.aplikator.client.shared.data.RecordDTO;
import org.aplikator.client.shared.data.SearchResult;
import org.aplikator.client.shared.descriptor.ApplicationDTO;
import org.aplikator.client.shared.descriptor.FunctionDTO;
import org.aplikator.client.shared.descriptor.ProcessDTO;
import org.aplikator.client.shared.descriptor.QueryDescriptorDTO;
import org.aplikator.client.shared.descriptor.RecordsPageDTO;
import org.aplikator.client.shared.descriptor.SuggestionsDTO;
import org.aplikator.client.shared.descriptor.ViewDTO;
import org.aplikator.client.shared.descriptor.WizardPageDTO;
import org.jboss.resteasy.annotations.cache.NoCache;

/**
 * The client side stub for the RPC service.
 */
@Path("/auth")
public interface AuthenticationService {

    @POST
    @Path("/login")
    public void login(@QueryParam("principal") String principal, /*@QueryParam("password")*/ String password);

    @GET
    @Path("/logout")
    public void logout();

    @GET
    @Path("/loginSSO")
    public void loginSSO();


}


