package org.aplikator.client.shared.descriptor;

import java.io.Serializable;

import org.jboss.errai.common.client.api.annotations.Portable;

@SuppressWarnings("serial")
@Portable
public class QueryParameterDTO implements Serializable {

    private String name;
    private String value;
    private PropertyDTO property;

    @SuppressWarnings("unused")
    public QueryParameterDTO() {//TODO custom marshaller
    }


    public QueryParameterDTO(PropertyDTO property) {
        this.name = property.getLocalizedName();
        this.property = property;
    }

    public QueryParameterDTO(String name, PropertyDTO property) {
        this.name = name;
        this.property = property;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public PropertyDTO getProperty() {
        return property;
    }
}
