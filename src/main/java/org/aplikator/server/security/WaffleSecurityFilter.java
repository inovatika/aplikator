package org.aplikator.server.security;

import javax.servlet.DispatcherType;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import java.io.IOException;

import org.aplikator.server.security.SSOGroupMap;

import waffle.servlet.NegotiateSecurityFilter;

/**
 *
 */


@WebFilter(
        filterName = "WaffleSecurityFilter",
        urlPatterns = {"/api/auth/loginSSO"},
        dispatcherTypes = {DispatcherType.REQUEST, DispatcherType.FORWARD, DispatcherType.INCLUDE, DispatcherType.ERROR},
        initParams = {@WebInitParam(name = "securityFilterProviders", value = "waffle.servlet.spi.NegotiateSecurityFilterProvider")}

)

public class WaffleSecurityFilter extends NegotiateSecurityFilter {


    public WaffleSecurityFilter() {
        super();
    }

    @Override
    public void doFilter(ServletRequest sreq, ServletResponse sres, FilterChain chain) throws IOException, ServletException {
        if (SSOGroupMap.useSSO()) {
            super.doFilter(sreq, sres, chain);
        } else {
            chain.doFilter(sreq, sres);
        }
    }
}
