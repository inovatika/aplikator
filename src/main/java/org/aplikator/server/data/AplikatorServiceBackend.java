package org.aplikator.server.data;

import org.aplikator.client.shared.data.*;
import org.aplikator.server.DescriptorRegistry;
import org.aplikator.server.descriptor.*;
import org.aplikator.server.persistence.Persister;
import org.aplikator.server.persistence.PersisterFactory;
import org.aplikator.server.persistence.RecordIterator;
import org.aplikator.server.persistence.search.ReindexDescriptor;
import org.aplikator.server.persistence.search.SearchFactory;
import org.aplikator.server.persistence.tempstore.Tempstore;
import org.aplikator.server.persistence.tempstore.TempstoreFactory;
import org.aplikator.server.query.QueryExpression;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The server side implementation of the RPC service.
 */
public class AplikatorServiceBackend {

    private static final Logger LOG = Logger.getLogger(AplikatorServiceBackend.class.getName());


    Persister persister = PersisterFactory.getPersister();

    public AplikatorServiceBackend() {
    }


    private void processAfterCommit(RecordContainer rc, Context ctx) {
        for (ContainerNode containerNode : rc.getRecords()) {
            PersisterTriggers trigger = containerNode.getView().getPersisterTriggers();
            try{
                trigger.afterCommit(containerNode, ctx);
            } catch (Throwable t) {
                String errorID = UUID.randomUUID().toString();
                LOG.log(Level.WARNING, "Error in afterCommit (ERROR_ID: " + errorID + "):" + t.getMessage(), t);
                FunctionResult result = rc.getFunctionResult();
                if (result == null ) {
                    result = new FunctionResult("aplikator.table.savewarning", FunctionResultStatus.WARNING);
                    result.setDetail(t.getMessage() + "\nError ID: " + errorID);
                } else if ( !result.getStatus().equals(FunctionResultStatus.WARNING)){
                    result.setStatus(FunctionResultStatus.WARNING);
                    result.setMessage("aplikator.table.savewarning");
                    result.addDetail(t.getMessage() + "\nError ID: " + errorID);
                }
                rc.setFunctionResult(result);
            }
        }
    }

    public RecordContainer processRecords(RecordContainer rc, Context ctx, boolean runTriggers) {
        if (rc.getRecords().isEmpty()){
            return rc;
        }
        LOG.fine("processing container");
        checkSuspendedState();
        ReindexDescriptor reindexDescriptor = new ReindexDescriptor();
        List<ContainerNode> recursivelyDeletedNodes = new ArrayList<>();

        try {
            for (int progressIter = 0; progressIter < rc.getRecords().size(); progressIter++) {
                ContainerNode node = rc.getRecords().get(progressIter);
                PersisterTriggers trigger = node.getView().getPersisterTriggers();
                loadBinaryData(node.getEdited());

                switch (node.getOperation()) {
                    case CREATE:
                        if (runTriggers) {
                            trigger.onCreate(node, ctx);
                        }
                        if (canSave(ctx)) {
                            node.setEdited(persister.updateRecord(node.getEdited(), ctx));
                            if (node.getView().getEntity().isIndexed()) {
                                reindexDescriptor.addDescriptor(node.getEdited().getPrimaryKey(), Operation.CREATE);
                            }
                            if (runTriggers) {
                                trigger.onLoad(node.getEdited(), node.getView(), ctx);
                            }
                        }
                        break;
                    case UPDATE:
                        if (runTriggers) {
                            trigger.onUpdate(node, ctx);
                        }
                        if (canSave(ctx)) {
                            persister.updateRecord(node.getEdited(), ctx);
                            reindexDescriptor.addDescriptor(node.getEdited().getPrimaryKey(), Operation.UPDATE);
                            if (runTriggers) {
                                trigger.onLoad(node.getEdited(), node.getView(), ctx);
                            }
                        }
                        break;
                    case DELETE:
                        deleteRecordRecursive(node, runTriggers, trigger, reindexDescriptor, recursivelyDeletedNodes, ctx);
                        break;
                }

            }
            if (canSave(ctx)) {
                if (SearchFactory.isSearchEnabled()) {
                    reindexDescriptor.processReindex(ctx);
                }
                persister.commitTransaction(ctx.getTransaction());
                cleanupComittedBinaryDataTempFiles(rc);
                if (runTriggers) {
                    rc.getRecords().addAll(recursivelyDeletedNodes);
                    processAfterCommit(rc, ctx);
                }
            } else {
                if (ctx.getTransaction() != null) {
                    persister.rollbackTransaction(ctx.getTransaction());
                    if ((ctx.getRecordContainer().getFunctionResult() != null) && (ctx.getRecordContainer().getFunctionResult().getStatus().equals(FunctionResultStatus.ERROR))) {
                        LOG.log(Level.SEVERE, "Error in processing records:"+ctx.getRecordContainer().getFunctionResult());
                    }
                }
            }
        } catch (Throwable th) {
            if (ctx.getTransaction() != null) {
                persister.rollbackTransaction(ctx.getTransaction());
            }
            LOG.log(Level.SEVERE, "Error in processing records:", th);
            resetBinData(rc);
            Application.get().onError(th, ctx);
            throw th;
        }
        if (canSave(ctx)) {
            for (ContainerNode cn : rc.getRecords()) {
                if (cn.getOperation().equals(Operation.CREATE) || cn.getOperation().equals(Operation.UPDATE)) {
                    PrimaryKey pk = cn.getEdited().getPrimaryKey();
                    View view = cn.getView();
                    Record updated = persister.getRecord(view, pk, ctx, false);
                    if (cn.getOperation().equals(Operation.CREATE)) {
                        updated.getPrimaryKey().setTempId(pk.getTempId());
                    }
                    cn.setOriginal(updated);
                } else {
                    cn.setOriginal(null);
                }
                cn.setEdited(null);
            }
        }
        resetBinData(rc);
        return rc;
    }

    private void deleteRecordRecursive(ContainerNode node, boolean runTriggers, PersisterTriggers trigger, ReindexDescriptor reindexDescriptor, List<ContainerNode> recursivelyDeletedNodes, Context ctx){
        if (runTriggers) {
            trigger.onDelete(node, ctx);
        }

        //find and delete any nested collections
        for (Property p : node.getView().getEntity().getProperties()) {
            if (p instanceof Collection) {
                Collection nestedCollection = (Collection) p;
                Entity nestedEntity = nestedCollection.referredEntity;
                PersisterTriggers nestedTrigger = nestedEntity.getPersisterTriggers();
                List<Record> nestedRecords = node.getOriginal().getCollectionRecords(nestedCollection,ctx);
                for (Record nestedRecord : nestedRecords) {
                    ContainerNode nestedNode = new ContainerNode(nestedEntity.view(), nestedRecord,nestedRecord,Operation.DELETE);
                    recursivelyDeletedNodes.add(nestedNode);
                    deleteRecordRecursive(nestedNode, runTriggers, nestedTrigger, reindexDescriptor, recursivelyDeletedNodes, ctx);
                }
            }
        }

        if (canSave(ctx)) {
            persister.deleteRecord(node.getOriginal(), ctx);
            reindexDescriptor.addDescriptor(node.getOriginal().getPrimaryKey(), Operation.DELETE);
        }
    }

    private void resetBinData(RecordContainer rc) {
        for (ContainerNode cn : rc.getRecords()) {
            if (cn.getEdited() != null) {
                cn.getEdited().resetBinaryPropertiesToFileIDs();
            }
            if (cn.getOriginal() != null) {
                cn.getOriginal().resetBinaryPropertiesToFileIDs();
            }
        }
    }

    private void cleanupComittedBinaryDataTempFiles(RecordContainer rc) {
        for (ContainerNode cn : rc.getRecords()) {
            for (String propertyId : cn.getEdited().getRecordDTO().getProperties()) {
                if (cn.getEdited().getRecordDTO().getValue(propertyId) instanceof BinaryData) {
                    BinaryData value = (BinaryData) cn.getEdited().getRecordDTO().getValue(propertyId);
                    TempstoreFactory.getTempstore().remove(value.getTempFileId());
                }
            }
        }
    }

    private boolean canSave(Context ctx) {
        if ((ctx.getRecordContainer().getFunctionResult() != null) && (ctx.getRecordContainer().getFunctionResult().getStatus().equals(FunctionResultStatus.ERROR))) {
            return false;
        }
        return true;
    }

    private void loadBinaryData(Record record) {
        if (record == null)
            return;
        Tempstore ts = TempstoreFactory.getTempstore();
        for (String id : record.getRecordDTO().getProperties()) {
            Property<? extends Serializable> p = (Property<? extends Serializable>) DescriptorRegistry.get().getDescriptionItem(id);
            if (p == null || p.getRefferedThrough() != null || !p.getType().equals(BinaryData.class)) {
                continue;
            }
            BinaryData binaryData = null;
            if (record.getValue(p) instanceof BinaryData) {
                binaryData = (BinaryData) record.getValue(p);
            } else {
                String fileId = (String) record.getValue(p);
                if (fileId == null || "".equals(fileId)) {
                    continue;
                }
                String filename = ts.getFilename(fileId);
                binaryData = new BinaryData((BinaryProperty) p, filename, ts.load(fileId), ts.getFileLength(fileId), fileId);

            }
            String[] s = p.getFormatPattern().split(",");
            int thumbnailSize = Integer.parseInt(s[0]);
            int previewSize = Integer.parseInt(s[1]);
            binaryData.preprocess(thumbnailSize, previewSize);
            @SuppressWarnings("unchecked")
            BinaryProperty prop = (BinaryProperty) p;
            record.setValue(prop, binaryData);
        }
    }


    public Record getRecord(PrimaryKey primaryKey, View view, Context ctx, boolean limitToView) {
        try {
            return persister.getRecord(view, primaryKey, ctx, limitToView);
        } catch (Throwable t) {
            LOG.log(Level.SEVERE, "Error in getRecord: " + t.getMessage(), t);
            throw new RuntimeException("Error in getRecord: " + t.getMessage(), t);
        }
    }


    public int getRecordCount(View view, QueryExpression queryExpression, SortItem[] sortItems, Property[] groupBy, QueryExpression havingExpression, String searchString, Collection ownerProperty, PrimaryKey ownerPrimaryKey, Context ctx) {
        try {
            return persister.getRecordCount(view, queryExpression, sortItems, groupBy, havingExpression, searchString, ownerProperty, ownerPrimaryKey, ctx);
        } catch (Throwable t) {
            LOG.log(Level.SEVERE, "Error in getRecordCount: " + t.getMessage(), t);
            throw new RuntimeException("Error in getRecordCount: " + t.getMessage(), t);
        }
    }


    public RecordDTO getCompleteRecord(String primaryKey, int traverseLevel, boolean includeCollections, Context ctx) {
        try {
            RecordDTO completeRecord = persister.getCompleteRecord(new PrimaryKey(primaryKey), traverseLevel, includeCollections, ctx);
            return completeRecord;
        } catch (Throwable t) {
            LOG.log(Level.SEVERE, "Error in getCompleteRecord: " + t.getMessage(), t);
            throw new RuntimeException("Error in getCompleteRecord: " + t.getMessage(), t);
        }
    }


    public RecordContainer prepareRecord(PrimaryKey primaryKey, View view, Collection ownerProperty, PrimaryKey ownerPrimaryKey, Context ctx) {
        checkSuspendedState();
        try {
            RecordContainer retval = ctx.getRecordContainer();
            Record original = null;
            Record edited = null;
            if (primaryKey != null) {
                original = persister.getRecord(view, primaryKey, ctx, false);
                edited = original.clone();
                edited.setPrimaryKey(new PrimaryKey(view.getEntity().getId(), UUID.randomUUID().toString()));
            } else {
                edited = RecordUtils.newRecord(view.getEntity());
            }
            if (ownerProperty != null) {
                edited.setOwnerPrimaryKey(ownerPrimaryKey);
                edited.setOwnerProperty(ownerProperty);
            }
            retval.addRecord(view, original, edited, Operation.PREPARE);
            ContainerNode node = retval.getRecords().get(0);
            PersisterTriggers trigger = view.getPersisterTriggers();
            trigger.onPrepare(node, primaryKey != null, ctx);
            return retval;
        } catch (Throwable t) {
            LOG.log(Level.SEVERE, "Error in prepareRecord: " + t.getMessage(), t);
            throw new RuntimeException("Error in prepareRecord: " + t.getMessage(), t);
        }
    }

    public RecordIterator getRecordIterator(View view, QueryExpression queryExpression, SortItem[] sortItems, Property[] groupBy, QueryExpression havingExpression, String searchString, Collection ownerProperty, PrimaryKey ownerPrimaryKey, int pageOffset, int pageSize, Context ctx) {
        return persister.getRecordIterator(view, queryExpression, sortItems, groupBy, havingExpression, searchString, ownerProperty, ownerPrimaryKey, pageOffset, pageSize, ctx);
    }

    public List<Record> getRecords(View view, QueryExpression queryExpression, SortItem[] sortItems, Property[] groupBy, QueryExpression havingExpression, String searchString, Collection ownerProperty, PrimaryKey ownerPrimaryKey, int pageOffset, int pageSize, Context ctx) {
        return persister.getRecords(view, queryExpression, sortItems, groupBy, havingExpression, searchString, ownerProperty, ownerPrimaryKey, pageOffset, pageSize, ctx);
    }


    void checkSuspendedState() {
        if (persister.isSuspended()) {
            throw new RuntimeException("Application is in the suspended state.");
        }
    }

}
