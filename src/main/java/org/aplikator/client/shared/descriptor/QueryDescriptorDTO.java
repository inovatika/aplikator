package org.aplikator.client.shared.descriptor;

import java.util.ArrayList;
import java.util.List;

import org.jboss.errai.common.client.api.annotations.Portable;

/**
 * @author vlahoda
 */

@Portable
public class QueryDescriptorDTO extends ClientDescriptorBase implements Cloneable {

    private List<QueryParameterDTO> queryParameters = new ArrayList<QueryParameterDTO>();

    @SuppressWarnings("unused")
    public QueryDescriptorDTO() { //TODO custom marshaller
    }

    public QueryDescriptorDTO(String id, String localizedName) {
        super(id, localizedName);
    }


    public QueryDescriptorDTO addQueryParameter(QueryParameterDTO param) {
        queryParameters.add(param);
        return this;
    }

    public List<QueryParameterDTO> getQueryParameters() {
        return queryParameters;
    }

    public void setQueryParameters(List<QueryParameterDTO> queryParameters) {
        this.queryParameters = queryParameters;
    }

    public QueryDescriptorDTO clone() {
        QueryDescriptorDTO retval = new QueryDescriptorDTO(getId(), getLocalizedName());
        for (QueryParameterDTO par : queryParameters) {
            QueryParameterDTO qpdto = new QueryParameterDTO(par.getName(), par.getProperty());
            //qpdto.setValue(par.getValue());
            retval.addQueryParameter(qpdto);
        }
        return retval;
    }
}
