package org.aplikator.client.local.widgets;

public interface DisableableWidget {

    void setEnabled(boolean enabled);
}
