/*
 * Copyright 2013 Pavel Stastny <pavel.stastny at gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aplikator.client.local.processes;

import java.util.List;

import org.aplikator.client.shared.descriptor.ProcessDTO;
import org.aplikator.client.shared.rpc.AplikatorErrorCallback;
import org.aplikator.client.shared.rpc.AplikatorService;
import org.gwtbootstrap3.client.ui.Button;
import org.gwtbootstrap3.client.ui.Modal;
import org.gwtbootstrap3.client.ui.ModalBody;
import org.gwtbootstrap3.client.ui.ModalFooter;
import org.gwtbootstrap3.client.ui.constants.ModalBackdrop;
import org.gwtbootstrap3.client.ui.gwt.CellTable;
import org.jboss.errai.common.client.api.RemoteCallback;
import org.jboss.errai.enterprise.client.jaxrs.api.RestClient;

import com.google.gwt.cell.client.ButtonCell;
import com.google.gwt.cell.client.FieldUpdater;
import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.cellview.client.Column;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.view.client.ListDataProvider;

/**
 * TODO: Prvni priblizeni dialogu procesu..
 *
 * @author Pavel Stastny <pavel.stastny at gmail.com>
 */
public class ProcessInformations {

    private ListDataProvider<ProcessDTO> provider;

    public void changeData(List<ProcessDTO> list) {
        if (this.provider != null)
            this.provider.setList(list);
    }

    public void requestForNewData() {
        RestClient.create(AplikatorService.class, new RemoteCallback<List<ProcessDTO>>() {
                    @Override
                    public void callback(List<ProcessDTO> response) {
                        changeData(response);
                    }
                },
                new AplikatorErrorCallback()
        ).getProcesses();
    }

    public final void showWindow() {
        final Modal informBox = new Modal();
        informBox.getElement().getStyle().setOverflowY(Style.Overflow.AUTO);//fix against hidding scrollbar in modal stack
        informBox.setTitle(" Procesy ");
        informBox.setDataBackdrop(ModalBackdrop.STATIC);
        informBox.setRemoveOnHide(true);
        informBox.setClosable(true);

        ModalBody contents = new ModalBody();
        informBox.add(contents);
        CellTable<ProcessDTO> table = new CellTable<ProcessDTO>();
        TextColumn<ProcessDTO> ident = new TextColumn<ProcessDTO>() {

            @Override
            public String getValue(ProcessDTO object) {
                return object.getIdentifier();
            }
        };
        table.addColumn(ident, "Identification");

        TextColumn<ProcessDTO> state = new TextColumn<ProcessDTO>() {

            @Override
            public String getValue(ProcessDTO object) {
                return object.getProcessState();
            }
        };
        table.addColumn(state, "State");

        TextColumn<ProcessDTO> startedDate = new TextColumn<ProcessDTO>() {

            @Override
            public String getValue(ProcessDTO arg0) {
                return arg0.getStartedDate();
            }


        };
        table.addColumn(startedDate, "Started");

        TextColumn<ProcessDTO> stoppedDate = new TextColumn<ProcessDTO>() {

            @Override
            public String getValue(ProcessDTO arg0) {
                return arg0.getStoppedDate();
            }

        };
        table.addColumn(stoppedDate, "Stopped");

        TextColumn<ProcessDTO> type = new TextColumn<ProcessDTO>() {
            @Override
            public String getValue(ProcessDTO object) {
                return object.getProcessType();
            }
        };
        table.addColumn(type, "Type");


        ButtonCell stopButtonCell = new ButtonCell();
        Column<ProcessDTO, String> buttonColumn = new Column<ProcessDTO, String>(stopButtonCell) {

            @Override
            public String getValue(ProcessDTO object) {
                return "Stop";
            }
        };
        buttonColumn.setFieldUpdater(new FieldUpdater<ProcessDTO, String>() {

            @Override
            public void update(int index, ProcessDTO object, String value) {
                RestClient.create(AplikatorService.class, new RemoteCallback<ProcessDTO>() {
                            @Override
                            public void callback(ProcessDTO response) {
                                requestForNewData();
                            }
                        },
                        new AplikatorErrorCallback()
                ).stopProcess(object.getIdentifier(), "true");
            }
        });

        table.addColumn(buttonColumn, "Stop");

        this.provider = new ListDataProvider<ProcessDTO>();
        this.provider.addDataDisplay(table);

        contents.add(table);


        ModalFooter buttonPanel = new ModalFooter();
        Button OKButton = new Button("OK", new ClickHandler() {
            public void onClick(ClickEvent event) {
                informBox.hide();
            }
        });
        buttonPanel.add(OKButton);
        informBox.add(buttonPanel);
        informBox.setWidth("800px");
        informBox.show();
    }


    public void display() {
        RestClient.create(AplikatorService.class,
                new RemoteCallback<List<ProcessDTO>>() {
                    @Override
                    public void callback(List<ProcessDTO> response) {
                        showWindow();
                        changeData(response);
                    }
                },
                new AplikatorErrorCallback()
        ).getProcesses();
    }

}
