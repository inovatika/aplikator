/*
 * Copyright 2013 Pavel Stastny <pavel.stastny at gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aplikator.server.processes;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.aplikator.server.processes.impl.processes.OSProcessFactory;
import org.aplikator.server.processes.impl.thread.ProcessThreadFactory;

/**
 * Creates new process
 *
 * @author Pavel Stastny <pavel.stastny at gmail.com>
 */
public abstract class ProcessFactory<T extends ProcessConfiguration> {

    private final static Map<ProcessType, ProcessFactory> factories = new HashMap<ProcessType, ProcessFactory>();

    static {
        factories.put(ProcessType.THREAD, new ProcessThreadFactory(ProcessManager.GENERATED_NAME));
        factories.put(ProcessType.PROCESS, new OSProcessFactory(ProcessManager.GENERATED_NAME));
    }


    public ProcessFactory() {
    }

    /**
     * Creates new process
     *
     * @param runnable Process code
     */
    public abstract Process create(RunnableSerializationAware runnable);

    /**
     * Creates new process
     *
     * @param conf     Process configuration
     * @param runnable Process code
     */
    public abstract Process create(T conf, RunnableSerializationAware runnable);

    /**
     * Create new process from given works
     *
     * @param works
     * @return
     */
    public abstract Process create(Work... works);

    /**
     * Create new process from given configuration, params and works
     *
     * @param conf   Process configurations
     * @param params Parameters
     * @param works  Works
     * @return
     */
    public abstract Process create(T conf, String[] params, Work... works);

    public static ProcessFactory get(ProcessType processType) {
        return factories.get(processType);
    }

    protected String generateProcessId() {
        return UUID.randomUUID().toString();
    }

}
