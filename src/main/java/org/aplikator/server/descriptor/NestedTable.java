package org.aplikator.server.descriptor;

import org.aplikator.client.shared.descriptor.NestedTableDTO;
import org.aplikator.client.shared.descriptor.WidgetDTO;
import org.aplikator.server.data.Context;

public class NestedTable extends WidgetPropertyDescriptorBase<Integer> {

    private View view;
    private boolean defaultSortId = false;

    public NestedTable(Collection<? extends Entity> property, View view) {
        super(property);
        this.view = view;
    }

    @Override
    public WidgetDTO getWidgetDescriptor(Context ctx) {
        NestedTableDTO desc = new NestedTableDTO(getProperty().getPropertyDTO(ctx), view.getViewDTO(ctx));
        desc.setSize(getSize());
        desc.setAccess(getProperty().getAccess(ctx));
        desc.setEnabled(isEnabled(ctx));
        desc.setVisible(isVisible(ctx));
        desc.setLocalizedName(getLocalizedName(ctx));
        desc.setLocalizedTooltip(getLocalizedTooltip(ctx));
        desc.setDefaultSortId(defaultSortId);
        desc.setHorizontal(isHorizontal());
        desc.setElementId(getElementId());
        desc.setCustomLabelStyle(getCustomLabelStyle());
        return desc;
    }

    @Override
    public void registerProperties(Form form) {
        // form.addProperty(property);
    }

    public static NestedTable nestedTable(Collection<? extends Entity> property) {
        return new NestedTable(property, property.referredEntity.view());
    }

    public static NestedTable nestedTable(Collection<? extends Entity> property, View view) {
        return new NestedTable(property, view);
    }

    public NestedTable useDefaultSortId() {
        this.defaultSortId = true;
        return this;
    }
}
