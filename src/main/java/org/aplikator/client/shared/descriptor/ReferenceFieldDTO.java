package org.aplikator.client.shared.descriptor;

import org.aplikator.client.local.widgets.HasFields;
import org.aplikator.client.local.widgets.ReferenceFieldWidget;

import com.google.gwt.user.client.ui.Widget;

@SuppressWarnings("serial")
//@Portable
public class ReferenceFieldDTO extends WidgetPropertyDTOBase {

    public static final String TYPE_KEY = "reference";

    private ViewDTO view;
    private WidgetDTO child;


    @SuppressWarnings("unused")
    public ReferenceFieldDTO() {//TODO custom marshaller
    }

    public ReferenceFieldDTO(PropertyDTO property, ViewDTO view, WidgetDTO child) {
        super(property);
        this.view = view;
        this.child = child;
    }

    public ViewDTO getView() {
        return view;
    }

    public void setView(ViewDTO view) {
        this.view = view;
    }

    public WidgetDTO getChild() {
        return child;
    }

    public void setChild(WidgetDTO child) {
        this.child = child;
    }


    @Override
    public Widget getWidget(HasFields form) {
        ReferenceFieldWidget field = new ReferenceFieldWidget(getLocalizedName(), view, child.getWidget(form), getSize(), getCustomLabelStyle(), form);
        field.setEnabled(isEnabled());
        field.setVisible(isVisible());
        field.setDefaultEnabled(isEnabled());
        field.setProperty(property);
        if (property.getRefferedThrough()!= null){
            field.setEnabled(false);
            field.setDefaultEnabled(false);
        }
        if (form != null) {
            form.registerDataField(field);
        }
        return field;
    }

}
