package org.aplikator.client.local.widgets;

import org.aplikator.client.local.Aplikator;
import org.gwtbootstrap3.client.ui.Label;
import org.gwtbootstrap3.client.ui.constants.LabelType;

import com.google.gwt.user.client.ui.Composite;

/**
 * Created with IntelliJ IDEA.
 * User: vlahoda
 * Date: 21.03.13
 * Time: 15:41
 * To change this template use File | Settings | File Templates.
 */
public class LoadingLabel extends Composite {
    Label loadingLabel;


    public LoadingLabel() {
        this(Aplikator.application.getConfigString("aplikator.table.loading"));
    }

    public LoadingLabel(String text) {
        loadingLabel = new Label(text);
        initWidget(loadingLabel);
        loadingLabel.setType(LabelType.INFO);
    }
}
