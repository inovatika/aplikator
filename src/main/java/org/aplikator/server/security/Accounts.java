package org.aplikator.server.security;

import org.aplikator.client.shared.data.Operation;
import org.aplikator.server.data.ContainerNode;
import org.aplikator.server.data.Context;
import org.aplikator.server.data.PersisterTriggers;
import org.aplikator.server.data.Record;
import org.aplikator.server.descriptor.Entity;
import org.aplikator.server.descriptor.Form;
import org.aplikator.server.descriptor.Panel;
import org.aplikator.server.descriptor.Property;
import org.aplikator.server.descriptor.View;

import com.google.gwt.safehtml.shared.SafeHtmlUtils;

public class Accounts extends Entity {
    public Property<String> principal;
    public Property<String> hashedPassword;
    public Property<String> fullName;
    public Property<String> email;
    public Property<String> roles;

    public Accounts() {
        super("Accounts", "Accounts", "Account_ID");
        initFields();
    }

    public void initFields() {
        principal = stringProperty("principal", 128);
        hashedPassword = stringProperty("hashedPassword");
        fullName = stringProperty("fullName");
        email = stringProperty("email");
        roles = stringProperty("roles");
        addIndex("principal_IDX", true, principal);
        setPersistersTriggers(new AccountsTriggers());
    }

    @Override
    protected View initDefaultView() {
        View retval = new View(this);
        retval.addProperty(principal).addProperty(hashedPassword).addProperty(fullName).addProperty(email).addProperty(roles);
        retval.setDefaultSortProperty(principal);
        Form form = new Form(false);
        form.setLayout(Panel.column(Panel.row(principal, hashedPassword), Panel.row(fullName, email), Panel.row(roles)));
        retval.setForm(form);
        return retval;
    }

    class AccountsTriggers extends PersisterTriggers.Default implements AccountsTrigger{
        @Override
        public void onLoad(Record record, View view, Context ctx) {
            record.setPreview(SafeHtmlUtils.fromString(record.getStringValue(principal, ctx)));
        }

        @Override
        public void onCreate(ContainerNode node, Context ctx) {
            super.onCreate(node, ctx);
            onCreateUpdate(node, ctx);
        }

        @Override
        public void onUpdate(ContainerNode node, Context ctx) {
            super.onUpdate(node, ctx);
            onCreateUpdate(node, ctx);
        }


        @Override
        public void afterCommit(ContainerNode node, Context ctx) {
            if (node.getOperation().equals(Operation.DELETE)) {
                String userNameString = node.getMerged().getValue(principal);
                SecurityModule.get().removeAccount(userNameString);
            } else {
                Record merged = node.getMerged();
                Account account = getAccount(merged, ctx);
                SecurityModule.get().addAccount(account);
            }
        }

        private void onCreateUpdate(ContainerNode node, Context ctx) {
            Record mergedRecord = node.getMerged();
            String userNameString = mergedRecord.getValue(principal);
            if (node.getEdited().getValue(hashedPassword) != null) {
                String passwordString = node.getEdited().getValue(hashedPassword);
                node.getEdited().setValue(hashedPassword, SecurityModule.hashPassword(passwordString, userNameString));
            }

        }

        @Override
        public Account getAccount(Record record, Context ctx) {
            Account account = new Account(record.getValue(principal));
            account.setPasswordHash(record.getValue(hashedPassword));
            String rolesString = record.getValue(roles);
            if (rolesString != null) {
                account.addRoles(rolesString.split(","));
            }
            account.setFullName(record.getValue(fullName));
            account.setEmail(record.getValue(email));
            return account;
        }
    }
}
