package org.aplikator.client.local.widgets;

import com.google.gwt.dom.client.Style;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.ui.RadioButton;
import org.aplikator.client.shared.data.ListItem;
import org.aplikator.client.shared.descriptor.PropertyDTO;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RadioSwitchWidget<T extends Serializable> extends DataFieldBase<T> implements DataField<T> {
    private List<RadioButton> buttons;

    public RadioSwitchWidget(String caption, String localizedTooltip, PropertyDTO property, int size, boolean horizontal, String elementId, String switcherInit, String customLabelStyle, HasFields form) {
        super(caption, localizedTooltip, property, horizontal, elementId, customLabelStyle, form);
        buttons = new ArrayList<RadioButton>(property.getListValues().size());

        for (Object obj : property.getListValues()) {
            ListItem item = (ListItem) obj;
            RadioButton button = new RadioButton(property.getId() + (elementId != null ? elementId : ""), item.getName());
            button.addValueChangeHandler(new ValueChangeHandler() {
                public void onValueChange(ValueChangeEvent event) {
                    setDirty(true);
                }
            });
            if (switcherInit != null) {
                switcher = new VisibilitySwitcher(switcherInit, property.getType(), form);
                //Window.alert("SWITCHER:"+switcher);
                button.addValueChangeHandler(new ValueChangeHandler() {
                    public void onValueChange(ValueChangeEvent event) {
                        switcher.apply(getValue());
                    }
                });
            }
            //button.addStyleName(PaneWidgetResources.INSTANCE.css().formData());
            if (!horizontal) {
                button.getElement().getStyle().setDisplay(Style.Display.BLOCK);
                button.getElement().getStyle().setHeight(16, Style.Unit.PX);
            } else {
                button.getElement().getStyle().setDisplay(Style.Display.INLINE_BLOCK);
                button.getElement().getStyle().setMarginLeft(8, Style.Unit.PX);
            }
            button.getElement().getFirstChildElement().getNextSiblingElement().getStyle().setFontWeight(Style.FontWeight.NORMAL);
            button.getElement().getFirstChildElement().getNextSiblingElement().getStyle().setMarginLeft(5, Style.Unit.PX);
            buttons.add(button);
            controlHolder.add(button);
        }
        this.setGridSize(size);
    }

    private VisibilitySwitcher<T> switcher;


    public void setValue(T value) {
        if (switcher != null){
            switcher.apply((T)value);
        }
//        StringBuilder sb = new StringBuilder("Value:");
//        sb.append(value).append("-");
        for (int i = 0; i < getProperty().getListValues().size(); i++) {
//            sb.append(i).append(":").append(((ListItem) getProperty().getListValues().get(i)).getValue());
            if (((ListItem) getProperty().getListValues().get(i)).getValue().equals(value)) {
//                sb.append("-true;");
                buttons.get(i).setValue(true, false);
            } else {
//                sb.append("-false;");
                buttons.get(i).setValue(false, false);
            }
        }
//        Window.alert(sb.toString());
    }

    @SuppressWarnings("unchecked")
    public T getValue() {
        int index = -1;
        for (int i = 0; i < buttons.size(); i++) {
            if (buttons.get(i).getValue()) {
                index = i;
                break;
            }
        }
        if (index == -1) {
            return null;
        }
        return (T) ((ListItem) getProperty().getListValues().get(index)).getValue();
    }

    public HandlerRegistration addValueChangeHandler(ValueChangeHandler<T> handler) {
        for (RadioButton button : buttons) {
            button.addHandler(handler, ValueChangeEvent.getType());
        }
        return null;
    }

    public void setEnabled(boolean enabled) {
        if (property.getRefferedThrough() != null) {
            enabled = false;
        }
        for (RadioButton button : buttons) {
            button.setEnabled(enabled);
        }
    }


}
