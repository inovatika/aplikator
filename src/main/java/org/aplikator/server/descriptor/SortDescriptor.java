package org.aplikator.server.descriptor;

import java.util.ArrayList;
import java.util.List;

import org.aplikator.client.shared.descriptor.SortDescriptorDTO;
import org.aplikator.server.data.Context;

/**
 * @author vlahoda
 */
public class SortDescriptor extends LocalizedServerDescriptorBase {
    private List<SortItem> items = new ArrayList<SortItem>();


    public SortDescriptor(String id, String label) {
        super(id);
        setLocalizationKey(label);
    }

    public SortDescriptor addItem(SortItem item) {
        items.add(item);
        return this;
    }

    public List<SortItem> getItems() {
        return items;
    }

    public Property getPrimarySortProperty() {
        return items.get(0).getSortProperty();
    }

    public SortDescriptorDTO getSortDescriptorDTO(Context ctx) {
        SortDescriptorDTO sortDescriptorDTO = new SortDescriptorDTO(this.getId(), this.getLocalizedName(ctx));
        if (items.size() > 0) {
            sortDescriptorDTO.setPrimarySortProperty(items.get(0).getSortProperty().getPropertyDTO(ctx));
            sortDescriptorDTO.setPrimaryAscending(items.get(0).isSortAscending());
        } else {
            throw new IllegalStateException("SortDescriptor " + getId() + " has no SortItems.");
        }
        return sortDescriptorDTO;
    }
}
