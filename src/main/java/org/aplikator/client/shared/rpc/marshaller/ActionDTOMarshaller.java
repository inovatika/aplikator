package org.aplikator.client.shared.rpc.marshaller;

import static org.aplikator.client.shared.rpc.marshaller.DemarshallingUtils.clientDescriptorBaseUnMarshall;

import org.aplikator.client.shared.descriptor.ActionDTO;
import org.jboss.errai.marshalling.client.api.MarshallingSession;
import org.jboss.errai.marshalling.client.api.annotations.ClientMarshaller;
import org.jboss.errai.marshalling.client.api.annotations.ServerMarshaller;
import org.jboss.errai.marshalling.client.api.json.EJObject;
import org.jboss.errai.marshalling.client.api.json.EJValue;
import org.jboss.errai.marshalling.client.marshallers.AbstractNullableMarshaller;

/**
 * ActionDTO marshaller
 *
 * @author pavels
 */
@ClientMarshaller(ActionDTO.class)
@ServerMarshaller(ActionDTO.class)
public class ActionDTOMarshaller extends AbstractNullableMarshaller<ActionDTO> {

	/*@Override
    public Class<ActionDTO> getTypeHandled() {
		return ActionDTO.class;
	}*/

    @Override
    public ActionDTO[] getEmptyArray() {
        return new ActionDTO[0];
    }

    @Override
    public ActionDTO doNotNullDemarshall(EJValue o, MarshallingSession ctx) {
        if (o.isObject() != null) {
            EJObject object = o.isObject();
            ActionDTO adto = new ActionDTO();
            clientDescriptorBaseUnMarshall(object, adto);
            adto.setToken(object.get("token").isString().stringValue());
            return adto;

        } else
            return null;
    }

    @Override
    public String doNotNullMarshall(ActionDTO o, MarshallingSession ctx) {
        StringBuilder builder = new StringBuilder("{");
        MarshallingUtils.clientDescriptorBase(builder, o).append(',');
        MarshallingUtils.string("token", o.getToken(), builder);
        builder.append("}");
        return builder.toString();
    }

}
