package org.aplikator.client.shared.descriptor;

import java.util.ArrayList;
import java.util.List;

import org.jboss.errai.common.client.api.annotations.Portable;

@SuppressWarnings("serial")
@Portable
public class MenuDTO extends ClientDescriptorBase {

    private List<ActionDTO> actions = new ArrayList<ActionDTO>();

    @SuppressWarnings("unused")
    public MenuDTO() {   //TODO custom marshaller
    }

    public MenuDTO(String id, String localizedName) {
        super(id, localizedName);
    }

    public MenuDTO addAction(ActionDTO action) {
        if (action != null) {
            actions.add(action);
        }
        return this;
    }

    public List<ActionDTO> getActions() {
        return actions;
    }

}
