package org.aplikator.client.shared.data;

import org.aplikator.client.shared.descriptor.ViewDTO;
import org.jboss.errai.common.client.api.annotations.Portable;

import java.io.Serializable;
import java.util.Set;

@SuppressWarnings("serial")
@Portable
public class ClientContext implements Serializable {

    private String user;
    private String locale;
    private ViewDTO view;
    private RecordDTO currentRecord;

    private Set<RecordDTO> selectedRecords;

    public ClientContext() {
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public ViewDTO getView() {
        return view;
    }

    public void setView(ViewDTO view) {
        this.view = view;
    }

    public RecordDTO getCurrentRecord() {
        return currentRecord;
    }

    public void setCurrentRecord(RecordDTO currentRecord) {
        this.currentRecord = currentRecord;
    }

    public Set<RecordDTO> getSelectedRecords() {
        return selectedRecords;
    }

    public void setSelectedRecords(Set<RecordDTO> selectedRecords) {
        this.selectedRecords = selectedRecords;
    }
}
