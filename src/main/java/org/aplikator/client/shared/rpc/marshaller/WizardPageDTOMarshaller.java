package org.aplikator.client.shared.rpc.marshaller;

import java.util.List;

import org.aplikator.client.shared.data.RecordDTO;
import org.aplikator.client.shared.descriptor.FormDTO;
import org.aplikator.client.shared.descriptor.PropertyDTO;
import org.aplikator.client.shared.descriptor.WizardPageDTO;
import org.jboss.errai.marshalling.client.api.Marshaller;
import org.jboss.errai.marshalling.client.api.MarshallingSession;
import org.jboss.errai.marshalling.client.api.annotations.ClientMarshaller;
import org.jboss.errai.marshalling.client.api.annotations.ServerMarshaller;
import org.jboss.errai.marshalling.client.api.json.EJObject;
import org.jboss.errai.marshalling.client.api.json.EJValue;
import org.jboss.errai.marshalling.client.marshallers.AbstractNullableMarshaller;

@ClientMarshaller(WizardPageDTO.class)
@ServerMarshaller(WizardPageDTO.class)
@SuppressWarnings("unchecked")
public class WizardPageDTOMarshaller extends AbstractNullableMarshaller<WizardPageDTO> {

	/*@Override
    public Class<WizardPageDTO> getTypeHandled() {
		return WizardPageDTO.class;
	}*/

    @Override
    public WizardPageDTO[] getEmptyArray() {
        return new WizardPageDTO[0];
    }

    @Override
    public WizardPageDTO doNotNullDemarshall(EJValue o, MarshallingSession ctx) {
        if (o.isObject() != null) {
            EJObject ejObject = o.isObject();
            WizardPageDTO pageDTO = new WizardPageDTO();
            DemarshallingUtils.clientDescriptorBaseUnMarshall(ejObject, pageDTO);


            List l = DemarshallingUtils.listUnMarshall(ejObject.get("properties"), ctx, PropertyDTO.class.getName());
            pageDTO.setProperties(l);

            Marshaller<Object> dem = ctx.getMarshallerInstance(FormDTO.class.getName());
            Object demarshall = dem.demarshall(ejObject.get("formDescriptor"), ctx);
            pageDTO.setFormDescriptor((FormDTO) demarshall);

            dem = ctx.getMarshallerInstance(RecordDTO.class.getName());
            demarshall = dem.demarshall(ejObject.get("clientRecord"), ctx);
            pageDTO.setClientRecord((RecordDTO) demarshall);

            if (DemarshallingUtils.containsAndNotNull(ejObject, "hasNext")) {
                pageDTO.setHasNext(ejObject.get("hasNext").isBoolean().booleanValue());
            }

            if (DemarshallingUtils.containsAndNotNull(ejObject, "hasPrevious")) {
                pageDTO.setHasPrevious(ejObject.get("hasPrevious").isBoolean().booleanValue());
            }

            if (DemarshallingUtils.containsAndNotNull(ejObject, "hasExecute")) {
                pageDTO.setHasExecute(ejObject.get("hasExecute").isBoolean().booleanValue());
            }

            if (DemarshallingUtils.containsAndNotNull(ejObject, "handleEnter")) {
                pageDTO.setHandleEnter(ejObject.get("handleEnter").isBoolean().booleanValue());
            }

            if (DemarshallingUtils.containsAndNotNull(ejObject, "pageId")) {
                pageDTO.setPageId(ejObject.get("pageId").isString().stringValue());
            }

            return pageDTO;
        } else
            return null;
    }

    @Override
    public String doNotNullMarshall(WizardPageDTO o, MarshallingSession ctx) {
        StringBuilder builder = new StringBuilder().append('{');
        MarshallingUtils.clientDescriptorBase(builder, o).append(',');

        MarshallingUtils.string("pageId", o.getPageId(), builder).append(',');

        MarshallingUtils.key("properties", builder).append(':');
        MarshallingUtils.listMarshall(builder, o.getProperties(), ctx, PropertyDTO.class.getName()).append(',');

        MarshallingUtils.key("formDescriptor", builder).append(':');
        Marshaller<Object> dem = ctx.getMarshallerInstance(FormDTO.class.getName());
        builder.append(dem.marshall(o.getFormDescriptor(), ctx)).append(',');

        MarshallingUtils.key("clientRecord", builder).append(':');
        dem = ctx.getMarshallerInstance(RecordDTO.class.getName());
        builder.append(dem.marshall(o.getClientRecord(), ctx)).append(',');

        MarshallingUtils.bool("hasNext", o.isHasNext(), builder).append(',');
        MarshallingUtils.bool("hasPrevious", o.isHasPrevious(), builder).append(',');
        MarshallingUtils.bool("hasExecute", o.isHasExecute(), builder).append(',');
        MarshallingUtils.bool("handleEnter", o.isHandleEnter(), builder);

        builder.append('}');
        return builder.toString();
    }


}
