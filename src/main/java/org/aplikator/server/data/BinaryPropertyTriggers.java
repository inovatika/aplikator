package org.aplikator.server.data;

import org.aplikator.client.shared.data.PrimaryKey;
import org.aplikator.server.descriptor.BinaryProperty;

/**
 *
 */
public interface BinaryPropertyTriggers {

    String processFileName(String originalFilename, int maxsize, PrimaryKey primaryKey, BinaryProperty property, Context ctx);

}
