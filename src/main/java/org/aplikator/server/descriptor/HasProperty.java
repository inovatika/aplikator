package org.aplikator.server.descriptor;

import java.io.Serializable;

public interface HasProperty<T extends Serializable> {
    Property<T> getProperty();
}
