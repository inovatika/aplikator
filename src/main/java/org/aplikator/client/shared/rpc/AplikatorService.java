package org.aplikator.client.shared.rpc;

import org.aplikator.client.shared.data.*;
import org.aplikator.client.shared.descriptor.*;
import org.jboss.resteasy.annotations.cache.NoCache;

import javax.ws.rs.*;
import java.util.List;

/**
 * The client side stub for the RPC service.
 */
@Path("/all")
public interface AplikatorService {


    @GET
    @Path("/application")
    @Produces("application/json")
    @NoCache
    public ApplicationDTO getApplication();


    @GET
    @Path("/list/{listName}")
    @Produces("application/json")
    @NoCache
    public List<ListItem> getList(@PathParam("listName") String listName);

    @GET
    @Path("/getView/{viewId}")
    @Produces("application/json")
    @NoCache
    public ViewDTO getView(@PathParam("viewId") String viewId);


    @GET
    @Path("/getSuggestions/{viewId}")
    @Produces("application/json")
    @NoCache
    public SuggestionsDTO getSuggestions(@PathParam("viewId") String viewId, @QueryParam("activeSort") String activeSort, @QueryParam("query") String query);

    @GET
    @Path("/getRecord/{viewId}/{primaryKey}")
    @Produces("application/json")
    @NoCache
    public RecordDTO getRecord(@PathParam("primaryKey") String primaryKey, @PathParam("viewId") String viewId);

    @GET
    @Path("/getCompleteRecord/{primaryKey}")
    @Produces("application/json")
    @NoCache
    public RecordDTO getCompleteRecord(@PathParam("primaryKey") String primaryKey, @QueryParam("traverseLevel") int traverseLevel, @QueryParam("includecollections") boolean includeCollections);

    @POST
    @Path("/getRecords/{viewId}")
    @Consumes("application/json")
    @Produces("application/json")
    public RecordsPageDTO getRecords(@PathParam("viewId") String viewId, @QueryParam("activeFilter") String activeFilter, @QueryParam("activeSort") String activeSort, QueryDescriptorDTO queryDescriptor, @QueryParam("searchString") String searchString, @QueryParam("ownerPropertyId") String ownerProperty, @QueryParam("ownerPrimaryKey") String ownerPrimaryKey, @QueryParam("pageOffset") int pageOffset, @QueryParam("pageSize") int pageSize);

    @GET
    @Path("/getPageSearch/{viewId}")
    @Produces("application/json")
    @NoCache
    public SearchResult getPageSearch(@PathParam("viewId") String viewId, @QueryParam("searchArgument") String searchArgument, @QueryParam("pageOffset") int pageOffset, @QueryParam("pageSize") int pageSize);


    @POST
    @Path("/getRecordCount/{viewId}")
    @Consumes("application/json")
    @Produces("text/plain")
    public int getRecordCount(@PathParam("viewId") String viewId, @QueryParam("activeFilter") String activeFilter, @QueryParam("activeSort") String activeSort, QueryDescriptorDTO queryDescriptor, @QueryParam("searchString") String searchString, @QueryParam("ownerPropertyId") String ownerProperty, @QueryParam("ownerPrimaryKey") String ownerPrimaryKey);

    @POST
    @Path("/runFunction")
    @Consumes("application/json")
    @Produces("application/json")
    public FunctionResult runFunction(FunctionParameters functionParameters);

    @GET
    @Path("/getFunction/{id}")
    @Produces("application/json")
    @NoCache
    public FunctionDTO getFunction(@PathParam("id") String id);

    @POST
    @Path("/getFunction/nextWizardPage")
    @Consumes("application/json")
    @Produces("application/json")
    public WizardPageDTO getWizardPage(@QueryParam("previouspage") String prevPage, @QueryParam("forward") boolean forwardFlag, FunctionParameters functionParameters);

    @GET
    @Path("/prepareRecord/{viewId}")
    @Produces("application/json")
    @NoCache
    public RecordContainerDTO prepareRecord(@QueryParam("primaryKey") String primaryKey, @PathParam("viewId") String viewId, @QueryParam("ownerProperty") String ownerProperty, @QueryParam("ownerPrimaryKey") String ownerPrimaryKey);

    @POST
    @Path("/processRecords")
    @Consumes("application/json")
    @Produces("application/json")
    public RecordContainerDTO processRecords(RecordContainerDTO rc);


    // processes part
    @GET
    @Path("/processes")
    @Produces("application/json")
    @NoCache
    public List<ProcessDTO> getProcesses();


    @GET
    @Path("/processes/{procid}")
    @Produces("application/json")
    @NoCache
    public ProcessDTO getProcess(@PathParam("procid") String procId);

    @POST
    @Path("/processes/{procid}")
    @Produces("application/json")
    public ProcessDTO stopProcess(@PathParam("procid") String procId, @QueryParam("stop") String stop);

    @DELETE
    @Path("/processes/{procid}")
    @Produces("application/json")
    public ProcessDTO deleteProcess(@PathParam("procid") String procId);


    /**
     * Return true if the application is in the suspended (read - only) state, which is used for system maintenance and backup
     *
     * @return
     */
    @GET
    @Path("/suspended")
    @Produces("text/plain")
    @NoCache
    public boolean isSuspended();

    /**
     * Attempt to set the suspended (read-only) application state flag
     *
     * @param suspended true tu set the application in the suspended state
     * @return true if the state was changed successfully, false if the state flag already was in the desired state
     */
    @POST
    @Path("/suspended/{suspended}")
    @Produces("text/plain")
    public boolean setSuspended(@PathParam("suspended") boolean suspended);


    @GET
    @Path("/status")
    @Produces("text/plain")
    @NoCache
    public String getStatus();
}


