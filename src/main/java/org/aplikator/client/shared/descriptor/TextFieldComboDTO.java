package org.aplikator.client.shared.descriptor;

import com.google.gwt.user.client.ui.Widget;
import org.aplikator.client.local.widgets.HasFields;
import org.aplikator.client.local.widgets.TextFieldComboWidget;

@SuppressWarnings("serial")
//@JsonTypeName("textfield")
//@Portable
public class TextFieldComboDTO extends WidgetPropertyDTOBase {

    public static final String TYPE_KEY = "textfieldcombo";

    @SuppressWarnings("unused")
    public TextFieldComboDTO() {//TODO custom marshaller
    }

    public TextFieldComboDTO(PropertyDTO property) {
        super(property);
    }

    @Override
    public Widget getWidget(HasFields form) {
        TextFieldComboWidget field = new TextFieldComboWidget(getLocalizedName(), getLocalizedTooltip(), property, getSize(), getFormatPattern(), isHorizontal(), getElementId(), getCustomLabelStyle(), form);
        field.setEnabled(isEnabled());
        field.setVisible(isVisible());
        field.setDefaultEnabled(isEnabled());
        if (form != null) {
            form.registerDataField(field);
        }
        return field;
    }

}
