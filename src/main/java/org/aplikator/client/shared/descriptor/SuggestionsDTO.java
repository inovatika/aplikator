package org.aplikator.client.shared.descriptor;

import java.util.List;

import org.aplikator.client.shared.data.ItemSuggestion;
import org.jboss.errai.common.client.api.annotations.Portable;

@Portable
public class SuggestionsDTO {

    private List<ItemSuggestion> suggestions;

    public List<ItemSuggestion> getSuggestions() {
        return suggestions;
    }

    public void setSuggestions(List<ItemSuggestion> suggestions) {
        this.suggestions = suggestions;
    }
}
