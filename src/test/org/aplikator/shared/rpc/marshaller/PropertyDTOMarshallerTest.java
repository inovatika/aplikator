package org.aplikator.shared.rpc.marshaller;

import junit.framework.TestCase;
import org.aplikator.client.shared.data.ListItem;
import org.aplikator.client.shared.descriptor.Access;
import org.aplikator.client.shared.descriptor.PropertyDTO;
import org.aplikator.client.shared.rpc.marshaller.ListItemMarshaller;
import org.aplikator.client.shared.rpc.marshaller.PropertyDTOMarshaller;
import org.easymock.EasyMock;
import org.jboss.errai.marshalling.client.api.Marshaller;
import org.jboss.errai.marshalling.client.api.MarshallingSession;

import java.util.ArrayList;
import java.util.List;

public class PropertyDTOMarshallerTest extends TestCase {

    public static List<ListItem> items() {
        List<ListItem> items = new ArrayList<ListItem>();
        items.add(new ListItem.Default("first-val", "first-name"));
        items.add(new ListItem.Default("second-val", "second-name"));
        items.add(new ListItem.Default("third-val", "third-name"));
        return items;
    }

    public void testMarshall() {
        PropertyDTO prop = new PropertyDTO();
        prop.marshallInitialization("id", "property", Access.READ_WRITE_CREATE_DELETE.name());

        prop.setType("string");
        prop.setSize(33.3);
        prop.setEditable(true);
        prop.setRequired(false);

        prop.setListValues(items());
        PropertyDTOMarshaller propMarsh = new PropertyDTOMarshaller();

        MarshallingSession ctx = ctx();
        String marshall = propMarsh.marshall(prop, ctx);
        System.out.println(marshall);

    }

    @SuppressWarnings("unchecked")
    private MarshallingSession ctx() {
        MarshallingSession session = EasyMock.createMock(MarshallingSession.class);
        ListItemMarshaller lMarsh = new ListItemMarshaller();
        Object marsh = lMarsh;
        EasyMock.expect(session.getMarshallerInstance(ListItem.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();
        EasyMock.replay(session);
        return session;
    }
}
