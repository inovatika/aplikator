package org.aplikator.shared.rpc.marshaller;

public class QueryDescriptorDTOMarshallerTest /*extends TestCase*/ {
    /*

    public static List<QueryParameterDTO> qpars() {
        List<QueryParameterDTO> l = new ArrayList<QueryParameterDTO>();
        QueryParameterDTO qp = new QueryParameterDTO("1");
        qp.setValue("1v");
        l.add(qp);

        qp = new QueryParameterDTO("2");
        qp.setValue("2v");
        l.add(qp);

        qp = new QueryParameterDTO("3");
        qp.setValue("3v");
        l.add(qp);

        qp = new QueryParameterDTO("4");
        qp.setValue("4v");
        l.add(qp);

        qp = new QueryParameterDTO("5");
        qp.setValue("5v");
        l.add(qp);

        return l;
    }


    public void testMarshall() {
        MarshallingSession session = ctx();

        QueryDescriptorDTOMarshaller marshaller = new QueryDescriptorDTOMarshaller();
        QueryDescriptorDTO qdto = new QueryDescriptorDTO("idone", "palos");
        qdto.setQueryParameters(qpars());

        String marshalled = marshaller.marshall(qdto, session);

        EJValue decoded = JSONDecoder.decode(marshalled);

        QueryDescriptorDTO demarshalled = marshaller.demarshall(decoded, session);
        Assert.assertTrue(demarshalled.getId().equals(qdto.getId()));
        Assert.assertTrue(demarshalled.getLocalizedName().equals(qdto.getLocalizedName()));

        Assert.assertTrue(demarshalled.getQueryParameters().size() == 5);
    }


    @SuppressWarnings("unchecked")
    private MarshallingSession ctx() {
        MarshallingSession session = EasyMock.createMock(MarshallingSession.class);
        QueryParameterDTOMarshaller adtoMarsh = new QueryParameterDTOMarshaller();
        Object marsh = adtoMarsh;
        EasyMock.expect(session.getMarshallerInstance(QueryParameterDTO.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        EasyMock.replay(session);
        return session;
    }

*/
}
