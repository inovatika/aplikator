package org.aplikator.shared.rpc.marshaller;

import junit.framework.TestCase;
import org.aplikator.client.shared.data.ListItem;
import org.aplikator.client.shared.descriptor.Access;
import org.aplikator.client.shared.descriptor.FunctionDTO;
import org.aplikator.client.shared.descriptor.PropertyDTO;
import org.aplikator.client.shared.rpc.marshaller.FunctionDTOMarshaller;
import org.aplikator.client.shared.rpc.marshaller.ListItemMarshaller;
import org.aplikator.client.shared.rpc.marshaller.PropertyDTOMarshaller;
import org.easymock.EasyMock;
import org.jboss.errai.marshalling.client.api.Marshaller;
import org.jboss.errai.marshalling.client.api.MarshallingSession;
import org.jboss.errai.marshalling.server.JSONDecoder;
import org.junit.Assert;

import java.util.ArrayList;
import java.util.List;

public class FunctionDTOMarshallerTest extends TestCase {

    public static List<ListItem> items() {
        List<ListItem> items = new ArrayList<ListItem>();
        items.add(new ListItem.Default("first-val", "first-name"));
        items.add(new ListItem.Default("second-val", "second-name"));
        items.add(new ListItem.Default("third-val", "third-name"));
        return items;
    }

    public static PropertyDTO prop() {
        PropertyDTO prop = new PropertyDTO();
        prop.marshallInitialization("id", "property", Access.READ_WRITE_CREATE_DELETE.name());

        prop.setType("string");
        prop.setSize(33.3);
        prop.setEditable(true);
        prop.setRequired(false);

        prop.setListValues(items());
        return prop;
    }

    public void testMarshall() {
        FunctionDTO fdto = new FunctionDTO("idfunc", "func-locname");
        fdto.addProperty(prop());

        MarshallingSession session = ctx();

        FunctionDTOMarshaller fmarsh = new FunctionDTOMarshaller();
        String marshall = fmarsh.marshall(fdto, session);
        System.out.println(marshall);
        FunctionDTO demarshall = fmarsh.demarshall(JSONDecoder.decode(marshall), session);

        Assert.assertTrue(fdto.getId().equals(demarshall.getId()));
        Assert.assertTrue(fdto.getLocalizedName().equals(demarshall.getLocalizedName()));
        Assert.assertTrue(fdto.getSize() == demarshall.getSize());

        Assert.assertTrue(fdto.getProperties().size() == demarshall.getProperties().size());
        List<PropertyDTO> list = fdto.getProperties();
        List<PropertyDTO> dlist = demarshall.getProperties();
        for (int i = 0, ll = list.size(); i < ll; i++) {
            Assert.assertTrue(list.get(i).equals(dlist.get(i)));
        }
    }

    @SuppressWarnings("unchecked")
    private MarshallingSession ctx() {
        MarshallingSession session = EasyMock.createMock(MarshallingSession.class);
        ListItemMarshaller lMarsh = new ListItemMarshaller();
        Object marsh = lMarsh;
        EasyMock.expect(session.getMarshallerInstance(ListItem.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        PropertyDTOMarshaller pMarsh = new PropertyDTOMarshaller();
        marsh = pMarsh;
        EasyMock.expect(session.getMarshallerInstance(PropertyDTO.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        EasyMock.replay(session);
        return session;
    }

}
