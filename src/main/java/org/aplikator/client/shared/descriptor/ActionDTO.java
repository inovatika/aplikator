package org.aplikator.client.shared.descriptor;


import org.jboss.errai.common.client.api.annotations.Portable;

@SuppressWarnings("serial")
@Portable
public class ActionDTO extends ClientDescriptorBase {

    private String token;

    @SuppressWarnings("unused")
    public ActionDTO() {   //TODO custom marshaller
    }


    public ActionDTO(String id, String localizedName, String token) {
        super(id, localizedName);
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
