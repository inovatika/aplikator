/*
 * Copyright 2013 Pavel Stastny <pavel.stastny at gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aplikator.server.processes.impl.thread;

import org.aplikator.server.processes.Process;
import org.aplikator.server.processes.ProcessFactory;
import org.aplikator.server.processes.ProcessManager;
import org.aplikator.server.processes.RunnableSerializationAware;
import org.aplikator.server.processes.ThreadProcessConfiguration;
import org.aplikator.server.processes.Work;

/**
 * @author Pavel Stastny <pavel.stastny at gmail.com>
 */
public class ProcessThreadFactory extends ProcessFactory<ThreadProcessConfiguration> {

    private String appname;

    public ProcessThreadFactory(String appname) {
        this.appname = appname;
    }


    @Override
    public Process create(RunnableSerializationAware runnable) {
        ProcessSimpleThread psth = new ProcessSimpleThread(generateProcessId(), appname, runnable);
        ProcessManager.getManager().registerProcess(psth);
        return psth;
    }

    @Override
    public Process create(ThreadProcessConfiguration conf, RunnableSerializationAware runnable) {
        ProcessSimpleThread psth = new ProcessSimpleThread(generateProcessId(), appname, runnable);
        ProcessManager.getManager().registerProcess(psth);
        return psth;
    }

    @Override
    public Process create(Work... works) {
        ProcessWorkersThread psth = new ProcessWorkersThread(works, generateProcessId(), appname);
        ProcessManager.getManager().registerProcess(psth);
        return psth;
    }

    @Override
    public Process create(ThreadProcessConfiguration conf, String[] params, Work... works) {
        ProcessWorkersThread psth = new ProcessWorkersThread(works, generateProcessId(), appname);
        ProcessManager.getManager().registerProcess(psth);
        return psth;
    }
}
