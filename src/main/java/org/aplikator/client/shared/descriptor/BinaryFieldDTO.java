package org.aplikator.client.shared.descriptor;

import org.aplikator.client.local.widgets.BinaryFieldWidget;
import org.aplikator.client.local.widgets.HasFields;

import com.google.gwt.user.client.ui.Widget;

@SuppressWarnings("serial")
//@JsonTypeName("binary")
//@Portable
public class BinaryFieldDTO extends WidgetPropertyDTOBase {

    public static final String TYPE_KEY = "binary";

    private String entityId;


    private int height;


    private boolean useThumbnail;

    private boolean showTempFile;


    @SuppressWarnings("unused")
    public BinaryFieldDTO() {      //TODO custom marshaller
    }

    public BinaryFieldDTO(PropertyDTO property, EntityDTO entity, int height, boolean useThumbnail, boolean showTempFile) {
        super(property);
        this.entityId = entity.getId();
        this.height = height;
        this.useThumbnail = useThumbnail;
        this.showTempFile = showTempFile;
    }


    public String getEntityId() {
        return this.entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public boolean isUseThumbnail() {
        return useThumbnail;
    }

    public void setUseThumbnail(boolean useThumbnail) {
        this.useThumbnail = useThumbnail;
    }

    public boolean isShowTempFile() {
        return showTempFile;
    }

    public void setShowTempFile(boolean showTempFile) {
        this.showTempFile = showTempFile;
    }

    @Override
    public Widget getWidget(HasFields form) {
        BinaryFieldWidget field = new BinaryFieldWidget(property, getLocalizedName(), getLocalizedTooltip(), this.entityId, getSize(), isEnabled(), height, useThumbnail, showTempFile, isHorizontal(), getElementId(), getCustomLabelStyle(), form);
        if (form != null) {
            form.registerBinaryField(field);
        }
        return field;
    }

}
