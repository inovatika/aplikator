package org.aplikator.server.descriptor;

import org.aplikator.client.shared.descriptor.TableDTO;
import org.aplikator.client.shared.descriptor.WidgetDTO;
import org.aplikator.server.data.Context;

public class Table extends WidgetPropertyDescriptorBase<Integer> {

    private View view;
    private int height = 0;

    public Table(Collection<? extends Entity> property, View view) {
        super(property);
        this.view = view;
        setSize(12);
    }

    public Table setHeight(int height) {
        this.height = height;
        return this;
    }

    @Override
    public WidgetDTO getWidgetDescriptor(Context ctx) {
        TableDTO desc = new TableDTO(getProperty().getPropertyDTO(ctx), view.getViewDTO(ctx));
        desc.setSize(getSize());
        desc.setHeight(height);
        desc.setAccess(getProperty().getAccess(ctx));
        desc.setEnabled(isEnabled(ctx));
        desc.setVisible(isVisible(ctx));
        desc.setLocalizedName(getLocalizedName(ctx));
        desc.setLocalizedTooltip(getLocalizedTooltip(ctx));
        desc.setHorizontal(isHorizontal());
        desc.setElementId(getElementId());
        return desc;
    }

    @Override
    public void registerProperties(Form form) {
        // form.addProperty(property);
    }

    public static Table table(Collection<? extends Entity> property) {
        return new Table(property, property.referredEntity.view());
    }

    public static Table table(Collection<? extends Entity> property, View view) {
        return new Table(property, view);
    }

}
