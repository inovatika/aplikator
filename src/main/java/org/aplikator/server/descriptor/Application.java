package org.aplikator.server.descriptor;

import com.typesafe.config.ConfigValue;
import org.aplikator.client.shared.descriptor.ActionDTO;
import org.aplikator.client.shared.descriptor.ApplicationDTO;
import org.aplikator.server.Configurator;
import org.aplikator.server.data.Context;
import org.aplikator.server.persistence.PersisterFactory;
import org.aplikator.server.persistence.search.SearchFactory;
import org.aplikator.server.security.Account;
import org.aplikator.server.security.SSOGroupMap;
import org.aplikator.server.security.SecurityModule;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;


public abstract class Application {
    protected static final Logger LOG = Logger.getLogger(Application.class.getName());

    protected List<Menu> menus = new ArrayList<>();
    protected List<Action> serviceMenu = new ArrayList<>();
    protected boolean showNavigation = true;
    protected String defaultActionToken = "home";
    protected Entity accountsEntity = null;


    protected Application() {
    }

    private static Application instance() {

        if (SearchFactory.isSearchEnabled()) {
            SearchFactory.get();
        }

        @SuppressWarnings("rawtypes")
        Class clazz;
        try {
            String cname = Configurator.get().getConfig().getString(Configurator.STRUCTURE);
            clazz = Class.forName(cname);
        } catch (ClassNotFoundException e) {
            throw new IllegalStateException(e);
        }
        try {
            Application app = (Application) clazz.newInstance();
            app.initialize();
            return app;
        } catch (InstantiationException e) {
            throw new IllegalStateException(e);
        } catch (IllegalAccessException e) {
            throw new IllegalStateException(e);
        }

    }

    public static Application get() {
        return LazyHolder.instance;
    }

    public abstract void initialize();

    public Application addMenu(Menu menu) {
        menus.add(menu);
        return this;
    }

    public Application addServiceMenuFunction(Function function) {
        return addServiceMenuAction(new Action(function.getId(), function.getLocalizationKey(), "execute/" + function.getId()));
    }
    public Application addServiceMenuAction(Action action) {
        serviceMenu.add(action);
        return this;
    }

    public void setShowNavigation(boolean showNavigation) {
        this.showNavigation = showNavigation;
    }

    public void setDefaultAction(String defaultAction) {
        this.defaultActionToken = defaultAction;
    }

    public ApplicationDTO getApplicationDTO(Context ctx) {
        SecurityModule.get().ensureDatabaseRealmLoaded(ctx);
        LOG.info("REMOTE USER:" + ctx.getHttpServletRequest().getRemoteUser());
        LOG.info("SESSION SUBJECT:" + ctx.getHttpServletRequest().getSession().getAttribute("javax.security.auth.subject"));
        ApplicationDTO retval = new ApplicationDTO(ctx.getUserLogin(), ctx.getUserLocale().getLanguage());
        for (Menu m : menus) {
            retval.addMenu(m.getMenuDTO(ctx));
        }
        for (Action a : serviceMenu) {
            ActionDTO adto = a.getActionDTO(ctx);
            if (adto != null) {
                retval.addServiceMenuItem(adto);
            }
        }

        retval.setBrand(Configurator.get().getLocalizedString(Configurator.BRAND, ctx.getUserLocale()));
        retval.setShowNavigation(showNavigation);
        if (!retval.getMenus().isEmpty()) {
            retval.setDefaultAction(defaultActionToken);
        } else {
            retval.setDefaultAction("login");
        }
        Set<Map.Entry<String, ConfigValue>> configSet = Configurator.get().getConfig().entrySet();
        for (Map.Entry<String, ConfigValue> entry : configSet) {
            if (entry.getKey().startsWith("aplikator.client")) {
                retval.setConfigString(entry.getKey(), entry.getValue().render());
            }
        }
        retval.getConfig().putAll(Configurator.get().getSystemLabels(ctx.getUserLocale()));

        retval.setShowLoginSSOButton(SSOGroupMap.useSSO());

        return retval;
    }

    public void onLogin( Context ctx) {
    }

    public void onLogout( Context ctx) {
    }

    public void onError(Throwable th, Context ctx) {
    }

    public Entity getAccountsEntity() {
        return accountsEntity;
    }

    public void setAccountsEntity(Entity accountsEntity) {
        this.accountsEntity = accountsEntity;
    }

    public Account getCustomAccount(String principal, HttpServletRequest request){
        return null;
    }

    public String createLocalizedIndex(boolean updateDB) {
        return PersisterFactory.getPersister().createLocalizedIndex(updateDB);
    }

    public String generateDDL(boolean updateDB, boolean checkDB) {
        return PersisterFactory.getPersister().generateDDL(updateDB, checkDB);
    }

    public void shutdown(){

    }

    // singleton
    private static class LazyHolder {
        private static Application instance = instance();
    }
}
