package org.aplikator.client.shared.rpc.marshaller;

import org.aplikator.client.shared.data.RecordDTO;
import org.aplikator.client.shared.descriptor.RecordsPageDTO;
import org.jboss.errai.marshalling.client.api.MarshallingSession;
import org.jboss.errai.marshalling.client.api.annotations.ClientMarshaller;
import org.jboss.errai.marshalling.client.api.annotations.ServerMarshaller;
import org.jboss.errai.marshalling.client.api.json.EJObject;
import org.jboss.errai.marshalling.client.api.json.EJValue;
import org.jboss.errai.marshalling.client.marshallers.AbstractNullableMarshaller;

@ClientMarshaller(RecordsPageDTO.class)
@ServerMarshaller(RecordsPageDTO.class)
@SuppressWarnings("unchecked")
public class RecordsPageDTOMarshaller extends AbstractNullableMarshaller<RecordsPageDTO> {

	/*@Override
    public Class<RecordsPageDTO> getTypeHandled() {
		return RecordsPageDTO.class;
	}*/

    @Override
    public RecordsPageDTO[] getEmptyArray() {
        return new RecordsPageDTO[0];
    }

    @Override
    public RecordsPageDTO doNotNullDemarshall(EJValue o, MarshallingSession ctx) {
        if (o.isObject() != null) {
            EJObject eObject = o.isObject();
            RecordsPageDTO rp = new RecordsPageDTO();
            rp.setOffset(DemarshallingUtils.safeInteger(eObject, "offset"));
            rp.setRecords(DemarshallingUtils.listUnMarshall(eObject.get("records"), ctx, RecordDTO.class.getName()));
            return rp;
        } else
            return null;
    }

    @Override
    public String doNotNullMarshall(RecordsPageDTO o, MarshallingSession ctx) {
        StringBuilder builder = new StringBuilder("{");
        MarshallingUtils.number("offset", o.getOffset(), builder).append(',');
        MarshallingUtils.key("records", builder).append(':');
        MarshallingUtils.listMarshall(builder, o.getRecords(), ctx, RecordDTO.class.getName());
        builder.append('}');
        return builder.toString();
    }
}
