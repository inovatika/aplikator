package org.aplikator.client.local.widgets;

import org.aplikator.client.shared.data.RecordDTO;

/**
 *
 */
public interface AddRecordCallback {
    void recordAdded(RecordDTO recordDTO);
}
