package org.aplikator.client.shared.rpc;

import java.io.Serializable;

/**
 * @author vlahoda
 */
public class AplikatorException extends RuntimeException implements Serializable {
    private static final long serialVersionUID = 1L;

    public AplikatorException() {
        super();
    }

    public AplikatorException(String msg) {
        super(msg);
    }

    public AplikatorException(String msg, Throwable e) {
        super(msg, e);
    }
}