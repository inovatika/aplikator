package org.aplikator.server.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.aplikator.server.Configurator;

import java.io.ByteArrayInputStream;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

public class JWTUtils {
    private static final Logger LOG = Logger.getLogger(JWTUtils.class.getName());

    public static Map<String, Object> getClaims(String token){
        Map<String, Object> claims = "".equals(JWT_CERTIFICATE) ? parseTokenUnsafe(token) : parseToken(token);
        return claims;
    }

    public static String getUserLogin(String token) {
        Map<String, Object> claims = getClaims(token);
        if (claims != null && claims.get(JWT_PRINCIPAL_FIELD) != null) {
            return (String) claims.get(JWT_PRINCIPAL_FIELD);
        }else {
            return null;
        }
    }


    private static final String JWT_CERTIFICATE = Configurator.get().getConfig().getString("aplikator.jwt.certificate");
    private static final String JWT_PRINCIPAL_FIELD = Configurator.get().getConfig().getString("aplikator.jwt.principalField");


    private static PublicKey getPublicKeyFromCertificateString(String certificateString) throws CertificateException {
        // Normalize and decode the certificate string
        String normalizedCertString = certificateString
                .replace("-----BEGIN CERTIFICATE-----", "")
                .replace("-----END CERTIFICATE-----", "")
                .replaceAll("\\s", "");
        byte[] decoded = Base64.getDecoder().decode(normalizedCertString);

        // Create a certificate factory for X.509 certificates
        CertificateFactory factory = CertificateFactory.getInstance("X.509");

        // Generate a certificate from the decoded bytes
        Certificate certificate = factory.generateCertificate(new ByteArrayInputStream(decoded));

        // Extract the public key from the certificate
        return certificate.getPublicKey();
    }


    private static Map<String, Object> parseToken(String token) {
        try {
            PublicKey publicKey = getPublicKeyFromCertificateString(JWT_CERTIFICATE);
            Claims claims = Jwts.parser().verifyWith(publicKey)
                    .build().parseSignedClaims(token)
                    .getPayload();
            return claims;
        } catch (Exception e) {
            LOG.severe("Invalid JWT Token: " + e.getMessage());
        }
        return new HashMap<>();
    }

    private static Map<String, Object> parseTokenUnsafe(String token) {
        try {
            String[] parts = token.split("\\.");

            if (parts.length != 3) {
                LOG.severe("Invalid JWT token: ");// Not a JWT format
                return new HashMap<>();
            }
            String json = new String(Base64.getDecoder().decode(parts[1]));
            ObjectMapper mapper = new ObjectMapper();
            Map<String, Object> map = mapper.readValue(json, Map.class);
            return map;
        } catch (Exception e) {
            LOG.severe("Error parsing payload: " + e.getMessage());
            return new HashMap<>();
        }
    }

}
