package org.aplikator.shared.rpc.marshaller;

import org.aplikator.client.shared.data.ListItem;
import org.aplikator.client.shared.descriptor.Access;
import org.aplikator.client.shared.descriptor.PropertyDTO;
import org.aplikator.client.shared.descriptor.SortDescriptorDTO;
import org.aplikator.client.shared.rpc.marshaller.ListItemMarshaller;
import org.aplikator.client.shared.rpc.marshaller.PropertyDTOMarshaller;
import org.aplikator.client.shared.rpc.marshaller.SortDescriptorDTOMarshaller;
import org.easymock.EasyMock;
import org.jboss.errai.marshalling.client.api.Marshaller;
import org.jboss.errai.marshalling.client.api.MarshallingSession;
import org.jboss.errai.marshalling.server.JSONDecoder;
import org.junit.Assert;

import junit.framework.TestCase;

public class SortDescriptorDTOMarshallerTest extends TestCase {

    public void testMarshall() {
        SortDescriptorDTO sdto = new SortDescriptorDTO("ab", "localized-name");

        PropertyDTO pdto = new PropertyDTO();
        pdto.marshallInitialization("id", "locname", Access.READ_WRITE_CREATE_DELETE.name());
        pdto.setEditable(true);
        pdto.setRequired(true);
        pdto.setType("string");

        sdto.setPrimarySortProperty(pdto);

        MarshallingSession session = ctx();

        SortDescriptorDTOMarshaller sdtoMarsh = new SortDescriptorDTOMarshaller();
        SortDescriptorDTO demarshalled = sdtoMarsh.demarshall(JSONDecoder.decode(sdtoMarsh.marshall(sdto, session)), session);

        Assert.assertTrue(sdto.getId().equals(demarshalled.getId()));
        Assert.assertTrue(sdto.getLocalizedName().equals(demarshalled.getLocalizedName()));
        Assert.assertTrue(sdto.getPrimarySortProperty().equals(demarshalled.getPrimarySortProperty()));
    }

    @SuppressWarnings("unchecked")
    private MarshallingSession ctx() {
        MarshallingSession session = EasyMock.createMock(MarshallingSession.class);
        ListItemMarshaller lMarsh = new ListItemMarshaller();
        Object marsh = lMarsh;
        EasyMock.expect(session.getMarshallerInstance(ListItem.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();


        PropertyDTOMarshaller pMarsh = new PropertyDTOMarshaller();
        marsh = pMarsh;
        EasyMock.expect(session.getMarshallerInstance(PropertyDTO.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        EasyMock.replay(session);
        return session;
    }

}
