package org.aplikator.server.descriptor;

import org.aplikator.client.shared.descriptor.TextFieldDTO;
import org.aplikator.client.shared.descriptor.WidgetDTO;
import org.aplikator.server.data.Context;

import java.io.Serializable;

public class TextField<T extends Serializable> extends WidgetPropertyDescriptorBase<T> {


    public TextField(Property<T> property) {
        super(property);
    }

    @Override
    public WidgetDTO getWidgetDescriptor(Context ctx) {
        TextFieldDTO desc = new TextFieldDTO(getProperty().getPropertyDTO(ctx));
        desc.setSize(getSize());
        desc.setFormatPattern(getFormatPattern());
        desc.setLocalizedName(getLocalizedName(ctx));
        desc.setLocalizedTooltip(getLocalizedTooltip(ctx));
        desc.setAccess(getProperty().getAccess(ctx));
        desc.setEnabled(isEnabled(ctx));
        desc.setVisible(isVisible(ctx));
        desc.setHorizontal(isHorizontal());
        desc.setElementId(getElementId());
        desc.setCustomLabelStyle(getCustomLabelStyle());
        return desc;
    }


    @Override
    public String toString() {
        return "TextField{" + "property=" + getProperty() + ", size=" + getSize() + '}';
    }


}
