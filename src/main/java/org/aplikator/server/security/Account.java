package org.aplikator.server.security;

import java.util.HashSet;
import java.util.Set;

/**
 *
 */
public class Account {

    private final String principal;
    private String passwordHash;
    private String fullName;
    private String email;
    private Set<String> roles = new HashSet<>();


    public Account(String principal) {
        this.principal = principal;
    }

    public String getPrincipal() {
        return principal;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public Account setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
        return this;
    }

    public String getFullName() {
        return fullName;
    }

    public Account setFullName(String fullName) {
        this.fullName = fullName;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Account setEmail(String email) {
        this.email = email;
        return this;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public Account addRoles(String... role) {
        for (String s : role) {
            roles.add(s);
        }
        return this;
    }

    @Override
    public String toString() {
        return "Account{" +
                "principal='" + principal + '\'' +
                ", passwordHash='" + passwordHash + '\'' +
                ", fullName='" + fullName + '\'' +
                ", email='" + email + '\'' +
                ", roles=" + roles +
                '}';
    }
}
