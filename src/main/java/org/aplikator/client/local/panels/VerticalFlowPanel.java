package org.aplikator.client.local.panels;

import com.google.gwt.dom.client.Style;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

public class VerticalFlowPanel extends VerticalPanel {

    public VerticalFlowPanel() {
        super();
        getElement().getStyle().setBorderStyle(Style.BorderStyle.SOLID);
        // addCleaner();

    }

    @Override
    public void add(Widget widget) {
        /*
         * widget.getElement().getStyle().setProperty("cssFloat", "left");
         * widget.getElement().getStyle().setProperty("clear", "left");
         * //widget.getElement().getStyle().setProperty("whiteSpace", "nowrap");
         * 
         * insert(widget,getWidgetCount()-1);
         */
        super.add(widget);
    }

    /*
     * private void addCleaner(){ HTML cleaner = new HTML("<hr>");
     * cleaner.getElement().getStyle().setProperty("clear", "both");
     * cleaner.getElement().getStyle().setProperty("visibility", "hidden");
     * cleaner.setHeight("0px"); super.add(cleaner); }
     */
}
