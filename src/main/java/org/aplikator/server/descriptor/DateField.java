package org.aplikator.server.descriptor;

import org.aplikator.client.shared.descriptor.DateFieldDTO;
import org.aplikator.client.shared.descriptor.WidgetDTO;
import org.aplikator.server.data.Context;

import java.util.Date;

public class DateField extends WidgetPropertyDescriptorBase<Date> {


    public DateField(Property<Date> property) {
        super(property);
    }

    @Override
    public WidgetDTO getWidgetDescriptor(Context ctx) {
        DateFieldDTO desc = new DateFieldDTO(getProperty().getPropertyDTO(ctx));
        desc.setSize(getSize());
        desc.setFormatPattern(getFormatPattern());
        desc.setLocalizedName(getLocalizedName(ctx));
        desc.setLocalizedTooltip(getLocalizedTooltip(ctx));
        desc.setAccess(getProperty().getAccess(ctx));
        desc.setEnabled(isEnabled(ctx));
        desc.setVisible(isVisible(ctx));
        desc.setHorizontal(isHorizontal());
        desc.setElementId(getElementId());
        desc.setCustomLabelStyle(getCustomLabelStyle());
        return desc;
    }


}
