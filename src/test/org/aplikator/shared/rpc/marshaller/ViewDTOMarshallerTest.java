package org.aplikator.shared.rpc.marshaller;

import junit.framework.TestCase;
import org.aplikator.client.shared.data.ListItem;
import org.aplikator.client.shared.data.PrimaryKey;
import org.aplikator.client.shared.descriptor.*;
import org.aplikator.client.shared.rpc.marshaller.*;
import org.aplikator.utils.IOUtils;
import org.easymock.EasyMock;
import org.jboss.errai.marshalling.client.api.Marshaller;
import org.jboss.errai.marshalling.client.api.MarshallingSession;
import org.jboss.errai.marshalling.client.api.json.EJValue;
import org.jboss.errai.marshalling.client.marshallers.StringMarshaller;
import org.jboss.errai.marshalling.server.JSONDecoder;
import org.junit.Assert;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class ViewDTOMarshallerTest extends TestCase {

    public static List<ListItem> items() {
        List<ListItem> items = new ArrayList<ListItem>();
        items.add(new ListItem.Default("first-val", "first-name"));
        items.add(new ListItem.Default("second-val", "second-name"));
        items.add(new ListItem.Default("third-val", "third-name"));
        return items;
    }

    public static PropertyDTO prop() {
        PropertyDTO prop = new PropertyDTO();
        prop.marshallInitialization("id", "property", Access.READ_WRITE_CREATE_DELETE.name());

        prop.setType("string");
        prop.setSize(33.3);
        prop.setEditable(true);
        prop.setRequired(false);

        prop.setListValues(items());
        return prop;
    }

    public void testMarshall() {

        MarshallingSession ctx = ctx();

        ViewDTO vdto = new ViewDTO();
        vdto.marshallInitialization("testik", "locname", Access.READ_WRITE_CREATE_DELETE.name());

        FunctionDTO fdto = new FunctionDTO("idfunc", "func-locname");
        fdto.addProperty(prop());

        vdto.addFunction(fdto);

        QueryDescriptorDTO qdto = new QueryDescriptorDTO("qid", "qloc");
        qdto.addQueryParameter(new QueryParameterDTO("name", prop()));
        vdto.addQueryDescriptor(qdto);

        EntityDTO edto = new EntityDTO("entityID", "locaname");
        edto.setIndexed(true);
        edto.setPrimaryKey(prop());
        vdto.setEntity(edto);

        FormDTO formdto = new FormDTO("id", "form-loc-name", true);
        formdto.setLayout(WidgetDTOMarshallerTest.panel());
        vdto.setFormDescriptor(formdto);

        ViewDTOMarshaller marshaller = new ViewDTOMarshaller();
        String marshall = marshaller.marshall(vdto, ctx);

        ViewDTO demarshalled = marshaller.demarshall(JSONDecoder.decode(marshall), ctx);
        Assert.assertNotNull(demarshalled);
        Assert.assertTrue(demarshalled.getActiveFilter().equals(vdto.getActiveFilter()));
        Assert.assertTrue(demarshalled.getActiveSort().equals(vdto.getActiveSort()));
    }

    public void testMarshalled() throws IOException {
        byte[] bytes = IOUtils.readBytes(ViewDTOMarshallerTest.class.getResourceAsStream("viewdto.real.txt"), true);
        EJValue decoded = JSONDecoder.decode(new String(bytes));

        MarshallingSession ctx = ctx();

        ViewDTOMarshaller marshaller = new ViewDTOMarshaller();
        ViewDTO demarshalled = marshaller.demarshall(decoded, ctx);
        Assert.assertNotNull(demarshalled);
        Assert.assertNotNull(demarshalled.getFormDescriptor());

        FormDTO fdto = demarshalled.getFormDescriptor();
        Stack<WidgetDTO> stack = new Stack<WidgetDTO>();
        stack.push(fdto.getLayout());
        while (!stack.isEmpty()) {
            WidgetDTO top = stack.pop();
            if (top instanceof PanelDTO) {
                PanelDTO p = (PanelDTO) top;
                List children = p.getChildren();
                for (Object o : children) {
                    stack.push((WidgetDTO) o);
                }
            } else if (top instanceof WidgetPropertyDTOBase) {
                WidgetPropertyDTOBase wpd = (WidgetPropertyDTOBase) top;
                if (wpd.getProperty() == null) {
                    System.out.println(wpd.getClass().getName());
                    System.out.println("Size:" + wpd.getSize());
                    System.out.println("formatpattern:" + wpd.getFormatPattern());
                }
                Assert.assertNotNull(((WidgetPropertyDTOBase) top).getProperty());
            }
        }
    }

    @SuppressWarnings("unchecked")
    private MarshallingSession ctx() {
        MarshallingSession session = EasyMock.createMock(MarshallingSession.class);

        PrimaryKeyMarshaller pkmarsh = new PrimaryKeyMarshaller();
        Object marsh = pkmarsh;
        EasyMock.expect(session.getMarshallerInstance(PrimaryKey.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        StringMarshaller smarsh = new StringMarshaller();
        marsh = smarsh;
        EasyMock.expect(session.getMarshallerInstance(String.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        QueryDescriptorDTOMarshaller m = new QueryDescriptorDTOMarshaller();
        marsh = m;
        EasyMock.expect(session.getMarshallerInstance(QueryDescriptorDTO.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        SortDescriptorDTOMarshaller sort = new SortDescriptorDTOMarshaller();
        marsh = sort;
        EasyMock.expect(session.getMarshallerInstance(SortDescriptorDTO.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        PropertyDTOMarshaller pmarsh = new PropertyDTOMarshaller();
        marsh = pmarsh;
        EasyMock.expect(session.getMarshallerInstance(PropertyDTO.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        ListItemMarshaller lmarsh = new ListItemMarshaller();
        marsh = lmarsh;
        EasyMock.expect(session.getMarshallerInstance(ListItem.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        FunctionDTOMarshaller fmarshaller = new FunctionDTOMarshaller();
        marsh = fmarshaller;
        EasyMock.expect(session.getMarshallerInstance(FunctionDTO.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        FormDTOMarshaller formmarshaller = new FormDTOMarshaller();
        marsh = formmarshaller;
        EasyMock.expect(session.getMarshallerInstance(FormDTO.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        WidgetDTOMarshaller wmarshaller = new WidgetDTOMarshaller();
        marsh = wmarshaller;
        EasyMock.expect(session.getMarshallerInstance(WidgetDTO.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        QueryParameterDTOMarshaller qmarshaller = new QueryParameterDTOMarshaller();
        marsh = qmarshaller;
        EasyMock.expect(session.getMarshallerInstance(QueryParameterDTO.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        EntityDTOMarshaller emarshaller = new EntityDTOMarshaller();
        marsh = emarshaller;
        EasyMock.expect(session.getMarshallerInstance(EntityDTO.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        ViewDTOMarshaller vmarshaller = new ViewDTOMarshaller();
        marsh = vmarshaller;
        EasyMock.expect(session.getMarshallerInstance(ViewDTO.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();


        EasyMock.replay(session);
        return session;
    }

}
