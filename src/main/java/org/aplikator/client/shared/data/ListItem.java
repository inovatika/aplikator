package org.aplikator.client.shared.data;

import java.io.Serializable;
import java.util.Date;

import org.jboss.errai.common.client.api.annotations.Portable;

/**
 * Item in a list of values used in {@link org.aplikator.server.descriptor.ComboBox}
 */
@SuppressWarnings("serial")
public interface ListItem extends Serializable {

    /**
     * @return The value which represents the selected ListItem in the ComboBox and which is stored in the property assigned to the ComboBox
     */
    public Serializable getValue();

    /**
     * @return The (localized) label representing the ListItem, which is displayed in the ComboBox popup list
     */
    public String getName();

    /**
     * Default implementation if the {@link ListItem} interface
     */
    @Portable
    public static class Default implements ListItem {
        private Serializable value;
        private String name;

        public Default() {
        }

        /**
         * Constructs a ListItem with property value and corresponding localized label (name)
         *
         * @param value the value returned by {@link #getValue()} method
         * @param name  the name returned by the {@link #getName()} method
         */
        public Default(Serializable value, String name) {
            super();
            this.value = value;
            this.name = name;
        }

        /**
         * Constructs a ListItem with the property value that is also used as the localized label
         *
         * @param value the value returned by {@link #getValue()} and {@link #getName()} methods
         */
        public Default(String value) {
            this(value, value);
        }

        @Override
        public Serializable getValue() {
            return value;
        }

        @Override
        public String getName() {
            return name;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o)
                return true;
            if (o == null || getClass() != o.getClass())
                return false;

            Default aDefault = (Default) o;

            return value.equals(aDefault.value);

        }

        @Override
        public int hashCode() {
            return value.hashCode();
        }
    }

    public static class Comparator implements java.util.Comparator<ListItem> {


        @Override
        public int compare(ListItem o1, ListItem o2) {
            return o1.getName().compareTo(o2.getName());
        }
    }

    public static class KeyComparator implements java.util.Comparator<ListItem> {

        @Override
        public int compare(ListItem o1, ListItem o2) {
            if (o1.getValue() instanceof Integer) {
                return ((Integer) o1.getValue()).compareTo((Integer) o2.getValue());
            } else if (o1.getValue() instanceof Date) {
                return ((Date) o1.getValue()).compareTo((Date) o2.getValue());
            } else if (o1.getValue() instanceof Boolean) {
                return ((Boolean) o1.getValue()).toString().compareTo(((Boolean) o2.getValue()).toString());
            } else {
                return ((String) o1.getValue()).compareTo((String) o2.getValue());
            }
        }
    }
}
