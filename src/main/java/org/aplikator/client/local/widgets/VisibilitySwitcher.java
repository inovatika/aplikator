package org.aplikator.client.local.widgets;

import com.google.gwt.dom.client.Style;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Window;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class VisibilitySwitcher<T> {

    Map<T, DivStates> statesMap = new HashMap<>();
    DivStates  defaultStates = null;

    VisibilitySwitcher(String init, String type, HasFields form){
        String[] lines = init.split("\\$");
        for (String line : lines) {
            String[] items = line.split("\\|");
            if ("null".equalsIgnoreCase(items[0])){
                defaultStates = new DivStates(items[1], items[2], form);
            } else {
                Object key = null;
                if ("java.lang.String".equals(type)) {
                    key = items[0];
                } else if ("java.lang.Integer".equals(type)) {
                    key = Integer.valueOf(items[0]);
                } else if ("java.lang.Boolean".equals(type)) {
                    key = Boolean.valueOf(items[0]);
                } else {
                    Window.alert("Unsupported VisibilitySwitcher type: " + type);
                    return;
                }
                //Window.alert("KEY:"+key+" ITEMS1:"+items[1]+ " ITEMS2:"+items[2]);
                statesMap.put((T) key, new DivStates(items[1], items[2], form));
            }
        }
    }

    public void apply(T value){
        DivStates states = statesMap.get(value);
        //Window.alert("APPLY:"+value+ " STATES:"+states);
        if (states != null){
            states.apply();
        } else {
            if (defaultStates != null){
                defaultStates.apply();
            }
        }
    }

    @Override
    public String toString() {
        return "VisibilitySwitcher{" +
                "statesMap=" + statesMap +
                '}';
    }

    static class DivStates {
        List<String> visible;
        List<String> hidden;

        DivStates(String visibleString, String hiddenString, HasFields form) {
            visible = Arrays.stream(visibleString.split(",")).map(id-> (form!= null?form.getPrefix():"")+"-"+id).collect(Collectors.toList());
            hidden = Arrays.stream(hiddenString.split(",")).map(id-> (form!=null?form.getPrefix():"")+"-"+id).collect(Collectors.toList());
            //Window.alert("CREATED STATES:"+this.toString());
        }

        void apply() {
            visible.forEach(div -> {
                Element element = DOM.getElementById(div);
                if (element != null) {
                    element.getStyle().setDisplay(Style.Display.BLOCK);
                } else {
                    //Window.alert("Visible element not found: "+div);
                }
            });
            hidden.forEach(div -> {
                Element element = DOM.getElementById(div);
                if (element != null) {
                    element.getStyle().setDisplay(Style.Display.NONE);
                } else {
                    //Window.alert("Hidden element not found: "+div);
                }
            });
        }

        @Override
        public String toString() {
            return "DivStates{" +
                    "visible=" + visible +
                    ", hidden=" + hidden +
                    '}';
        }
    }
}
