package org.aplikator.shared.rpc.marshaller;

import org.aplikator.client.shared.descriptor.Access;
import org.aplikator.client.shared.descriptor.ActionDTO;
import org.aplikator.client.shared.rpc.marshaller.ActionDTOMarshaller;
import org.jboss.errai.marshalling.client.api.json.EJValue;
import org.jboss.errai.marshalling.server.JSONDecoder;
import org.junit.Assert;

import junit.framework.TestCase;

public class ActionDTOMarshallerTest extends TestCase {


    public void testMarshall() {
        ActionDTO adto = new ActionDTO();
        adto.setToken("token");
        adto.marshallInitialization("id", "testik",  Access.READ_WRITE_CREATE_DELETE.name());
        ActionDTOMarshaller marshTest = new ActionDTOMarshaller();
        String marshalled = marshTest.marshall(adto, null);

        EJValue encoded = JSONDecoder.decode(marshalled);
        ActionDTO demarshalled = marshTest.demarshall(encoded, null);
        Assert.assertTrue(demarshalled.getId().equals("id"));
        Assert.assertTrue(demarshalled.getToken().equals("token"));
        Assert.assertTrue(demarshalled.getLocalizedName().equals("testik"));

    }
}
