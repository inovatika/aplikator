package org.aplikator.client.shared.data;

public class FunctionParameters {

    private final String functionId;
    private final ClientContext clientContext;
    private final RecordDTO clientParameters;

    public FunctionParameters(String functionId, ClientContext clientContext, RecordDTO clientParameters) {
        this.functionId = functionId;
        this.clientContext = clientContext;
        this.clientParameters = clientParameters;
    }

    public String getFunctionId() {
        return functionId;
    }

    public ClientContext getClientContext() {
        return clientContext;
    }


    public RecordDTO getClientParameters() {
        return clientParameters;
    }

}
