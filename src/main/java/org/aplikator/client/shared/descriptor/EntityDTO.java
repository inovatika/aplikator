package org.aplikator.client.shared.descriptor;

import java.io.Serializable;

import org.jboss.errai.common.client.api.annotations.Portable;

@SuppressWarnings("serial")
@Portable
public class EntityDTO extends ClientDescriptorBase implements Serializable {


    private PropertyDTO primaryKey;
    private boolean indexed = false;


    public boolean isIndexed() {
        return indexed;
    }

    public void setIndexed(boolean indexed) {
        this.indexed = indexed;
    }

    @SuppressWarnings("unused")
    public EntityDTO() { //TODO custom marshaller

    }

    public EntityDTO(String id, String localizedName) {
        super(id, localizedName);
    }

    public PropertyDTO getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(PropertyDTO primaryKey) {
        this.primaryKey = primaryKey;
    }


    @Override
    public String toString() {
        return "EntityDTO [" + getId() + "]";
    }

}
