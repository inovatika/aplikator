package org.aplikator.server;

import net.coobird.thumbnailator.Thumbnails;
import org.aplikator.server.descriptor.BinaryProperty;
import org.imgscalr.Scalr;

import javax.activation.FileTypeMap;
import javax.activation.MimeType;
import javax.activation.MimeTypeParseException;
import javax.activation.MimetypesFileTypeMap;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ImageResizer {

    private static final Logger LOG = Logger.getLogger(ImageResizer.class.getName());


    private static MimetypesFileTypeMap mimeTypes;

    /**
     * Resize the input picture (byte array/ImageIcon) to fit the given maximum
     * size.
     * Supports JPEG, PNG, GIF and TIFF. For other file types returns null.
     *
     * @param maxSize
     * @return JPEG encoded byte array of resized picture
     */
    public static byte[] resize(BufferedImage srcImage, int maxSize) {
        //BufferedImage imageWithoutAlpha = new BufferedImage(srcImage.getWidth(), srcImage.getHeight(), BufferedImage.TYPE_INT_RGB);
        //Graphics g = imageWithoutAlpha.getGraphics();
        //try {
        //   g.drawImage(srcImage, 0, 0, null);
        String resizer = Configurator.get().getConfig().getString(Configurator.IMAGE_RESIZER);
        if ("scalr".equals(resizer)) {
            return resizeWithScalr(srcImage, maxSize);
        } else if ("aplikator".equals(resizer)) {
            return resizeWithOldAplikator(srcImage, maxSize);
        } else {
            return resizeWithThumbnailator(srcImage, maxSize);
        }
        //}finally{
        //   g.dispose();
        //   imageWithoutAlpha.flush();
        // }
    }

    public static BufferedImage removeAlphaChannel(BufferedImage srcImage) {
        BufferedImage imageWithoutAlpha = new BufferedImage(srcImage.getWidth(), srcImage.getHeight(), BufferedImage.TYPE_INT_RGB);
        Graphics g = imageWithoutAlpha.getGraphics();
        try {
            g.drawImage(srcImage, 0, 0, null);
            return imageWithoutAlpha;
        } finally {
            g.dispose();
            srcImage.flush();
        }

    }


    public static byte[] resizeWithScalr(BufferedImage srcImage, int maxSize) {
        byte[] retval = null;
        try {
            int originalX = srcImage.getWidth();
            int originalY = srcImage.getHeight();
            if (originalX <= maxSize && originalY <= maxSize) {
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                retval = outputStream.toByteArray();
                ImageIO.write(srcImage, "jpg", outputStream);
                retval = outputStream.toByteArray();
            } else {
                BufferedImage scaledImage = Scalr.resize(srcImage, maxSize);
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                ImageIO.write(scaledImage, "jpg", os);
                retval = os.toByteArray();
                os.close();
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Error in image resizer:", ex);
        }
        return retval;
    }


    public static byte[] resizeWithThumbnailator(BufferedImage srcImage, int maxSize) {
        byte[] retval = null;
        try {
            int originalX = srcImage.getWidth();
            int originalY = srcImage.getHeight();
            if (originalX <= maxSize && originalY <= maxSize) {
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                retval = outputStream.toByteArray();
                ImageIO.write(srcImage, "jpg", outputStream);
                retval = outputStream.toByteArray();
            } else {
                ByteArrayOutputStream os = new ByteArrayOutputStream();

                Thumbnails.of(srcImage)
                        .size(maxSize, maxSize)
                        .outputFormat("jpg")
                        .toOutputStream(os);
                retval = os.toByteArray();
                os.close();
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Error in image resizer:", ex);
        }
        return retval;
    }


    /**
     * Resize the input picture (byte array/ImageIcon) to fit the given maximum
     * size.
     * Supports JPEG, PNG, GIF and TIFF. For other file types returns null.
     *
     * @param maxSize
     * @return JPEG encoded byte array of resized picture
     */
    public static byte[] resizeWithOldAplikator(BufferedImage sourceImage, int maxSize) {
        byte[] retval = null;
        try {
            int originalX = sourceImage.getWidth();
            int originalY = sourceImage.getHeight();
            if (originalX <= maxSize && originalY <= maxSize) {
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                retval = outputStream.toByteArray();
                ImageIO.write(sourceImage, "jpg", outputStream);
                retval = outputStream.toByteArray();
            } else {
                int calculatedX;
                int calculatedY;
                if (originalX >= originalY) {
                    calculatedX = maxSize;
                    float ratio = originalX / (float) maxSize;
                    calculatedY = Math.round(originalY / ratio);
                } else {
                    calculatedY = maxSize;
                    float ratio = originalY / (float) maxSize;
                    calculatedX = Math.round(originalX / ratio);
                }
                // scale the image
                Image targetImage = sourceImage.getScaledInstance(calculatedX, calculatedY, Image.SCALE_AREA_AVERAGING);
                MediaTracker mediaTracker = new MediaTracker(new JLabel());
                mediaTracker.addImage(targetImage, 0);
                mediaTracker.waitForAll();
                // encode output as JPEG
                BufferedImage outputImage = new BufferedImage(calculatedX, calculatedY, BufferedImage.TYPE_INT_RGB);
                Graphics graphics = outputImage.createGraphics();
                graphics.drawImage(targetImage, 0, 0, null);
                graphics.dispose();
                ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                retval = outputStream.toByteArray();
                ImageIO.write(outputImage, "jpg", outputStream);
                retval = outputStream.toByteArray();
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Error in image resizer:", ex);
        }
        return retval;
    }

    public static boolean isResizeable(BinaryProperty binaryProperty, String fileName) {
        if (binaryProperty != null && !binaryProperty.isGenerateThumbnails()) {
            return false;
        }
        MimeType mimeType = null;
        try {
            mimeType = getMimeType(fileName);
        } catch (MimeTypeParseException e) {
            return false;
        }
        return "image".equals(mimeType.getPrimaryType()) || isPdf(fileName);
    }

    public static boolean isPdf(String fileName) {
        MimeType mimeType = null;
        try {
            mimeType = getMimeType(fileName);
        } catch (MimeTypeParseException e) {
            return false;
        }
        return "application".equals(mimeType.getPrimaryType()) && "pdf".equalsIgnoreCase(mimeType.getSubType());
    }

    /**
     * Determine the Mime type based on file extension
     *
     * @param fileName
     * @return
     * @throws MimeTypeParseException
     */
    public static MimeType getMimeType(String fileName) throws MimeTypeParseException {

        if (fileName == null) {
            return new MimeType("application", "octet");
        }
        if (mimeTypes == null) {
            mimeTypes = (MimetypesFileTypeMap) FileTypeMap.getDefaultFileTypeMap();
            mimeTypes.addMimeTypes("application/msword doc DOC");
            mimeTypes.addMimeTypes("application/vnd.ms-excel xls XLS");
            mimeTypes.addMimeTypes("application/pdf pdf PDF");
            mimeTypes.addMimeTypes("text/xml xml XML");
            mimeTypes.addMimeTypes("text/html html htm HTML HTM");
            mimeTypes.addMimeTypes("text/plain txt text TXT TEXT");
            mimeTypes.addMimeTypes("image/gif gif GIF");
            mimeTypes.addMimeTypes("image/ief ief");
            mimeTypes.addMimeTypes("image/jpeg jpeg jpg jpe JPG");
            mimeTypes.addMimeTypes("image/tiff tiff tif TIFF TIF");
            mimeTypes.addMimeTypes("image/png png PNG");
            mimeTypes.addMimeTypes("image/x-xwindowdump xwd");
            mimeTypes.addMimeTypes("application/postscript ai eps ps");
            mimeTypes.addMimeTypes("application/rtf rtf");
            mimeTypes.addMimeTypes("application/x-tex tex");
            mimeTypes.addMimeTypes("application/x-texinfo texinfo texi");
            mimeTypes.addMimeTypes("application/x-troff t tr roff");
            mimeTypes.addMimeTypes("audio/basic au");
            mimeTypes.addMimeTypes("audio/midi midi mid");
            mimeTypes.addMimeTypes("audio/x-aifc aifc");
            mimeTypes.addMimeTypes("audio/x-aiff aif aiff");
            mimeTypes.addMimeTypes("audio/x-mpeg mpeg mpg");
            mimeTypes.addMimeTypes("audio/x-wav wav");
            mimeTypes.addMimeTypes("video/mpeg mpeg mpg mpe");
            mimeTypes.addMimeTypes("video/quicktime qt mov");
            mimeTypes.addMimeTypes("video/x-msvideo avi");
            mimeTypes.addMimeTypes("image/vnd.adobe.photoshop psd PSD");
            mimeTypes.addMimeTypes("video/x-ms-wmv wmv WMV");
            mimeTypes.addMimeTypes("application/zip zip ZIP");
            mimeTypes.addMimeTypes("video/mp4 mp4 MP4");
            mimeTypes.addMimeTypes("image/bmp bmp BMP");

        }
        return new MimeType(mimeTypes.getContentType(fileName.toLowerCase()));

    }

}

/*
 *
 * BufferedImage image = ImageIO.read(new File("c:\picture.jpg"));
 *
 *
 * private static BufferedImage resize(BufferedImage image, int width, int
 * height) { BufferedImage resizedImage = new BufferedImage(width, height,
 * BufferedImage.TYPE_INT_ARGB); Graphics2D g = resizedImage.createGraphics();
 * g.drawImage(image, 0, 0, width, height, null); g.dispose(); return
 * resizedImage; }
 *
 * ImageIO.write(resized, "png", new File("c:\picture1.png"));
 *
 *
 * private static BufferedImage resize(BufferedImage image, int width, int
 * height) { int type = image.getType() == 0? BufferedImage.TYPE_INT_ARGB :
 * image.getType(); BufferedImage resizedImage = new BufferedImage(width,
 * height, type); Graphics2D g = resizedImage.createGraphics();
 * g.setComposite(AlphaComposite.Src);
 *
 * g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
 * RenderingHints.VALUE_INTERPOLATION_BILINEAR);
 *
 * g.setRenderingHint(RenderingHints.KEY_RENDERING,
 * RenderingHints.VALUE_RENDER_QUALITY);
 *
 * g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
 * RenderingHints.VALUE_ANTIALIAS_ON);
 *
 * g.drawImage(image, 0, 0, width, height, null); g.dispose(); return
 * resizedImage; }
 *
 *
 * PDF: https://pdf-renderer.dev.java.net/examples.html
 */
