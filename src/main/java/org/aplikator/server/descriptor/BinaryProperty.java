package org.aplikator.server.descriptor;

import org.aplikator.server.data.BinaryData;
import org.aplikator.server.data.BinaryPropertyTriggers;

/**
 *
 */
public class BinaryProperty extends Property<BinaryData> {
    private boolean generateThumbnails = true;
    private boolean displayAsIcon = false;
    private BinaryPropertyTriggers triggers;

    public BinaryProperty(String id, Entity entity) {
        super(id, BinaryData.class, 0, false, entity, false, null);
    }

    public BinaryPropertyTriggers getTriggers() {
        return triggers;
    }

    public void setTriggers(BinaryPropertyTriggers triggers) {
        this.triggers = triggers;
    }

    public boolean isGenerateThumbnails() {
        return generateThumbnails;
    }

    public BinaryProperty setGenerateThumbnails(boolean generateThumbnails) {
        this.generateThumbnails = generateThumbnails;
        return this;
    }

    public boolean isDisplayAsIcon() {
        return displayAsIcon;
    }

    public BinaryProperty setDisplayAsIcon(boolean displayAsIcon) {
        this.displayAsIcon = displayAsIcon;
        return this;
    }
}
