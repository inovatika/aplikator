package org.aplikator.server.descriptor;

import org.aplikator.client.shared.descriptor.Access;
import org.aplikator.client.shared.descriptor.FunctionDTO;
import org.aplikator.client.shared.descriptor.FunctionWidgetDTO;
import org.aplikator.client.shared.descriptor.WidgetDTO;
import org.aplikator.server.Configurator;
import org.aplikator.server.data.Context;
import org.aplikator.server.data.Executable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Function extends ServerDescriptorBase implements Widget {


    /**
     * selected wizards key
     */
    public static final String ANNOTATION_SELECTED_WIZARDS_KEY = "selected-wizard";

    private final Executable executable;

    private List<Property<? extends Serializable>> properties = new ArrayList<Property<? extends Serializable>>();

    private List<WizardPage> wizards = new ArrayList<WizardPage>();


    private boolean horizontal = false;
    private String elementId = null;
    private String customLabelStyle = null;
    private boolean async = false;

    public Function(String id, String readableName, Executable executable) {
        this(id, readableName, null, executable);
    }

    public Function(String id, String readableName, Executable executable, boolean async) {
        this(id, readableName, null, executable);
        this.async = async;
    }

    public Function(String id, String readableName, String readableTooltip, Executable executable) {
        super(id);
        setLocalizationKey(readableName);
        setLocalizationTooltipKey(readableTooltip);
        this.executable = executable;
        this.executable.setFunction(this);
    }

    public Function(String id, String readableName, String readableTooltip, String icon, Executable executable) {
        super(id);
        setLocalizationKey(readableName);
        setLocalizationTooltipKey(readableTooltip);
        setIcon(icon);
        this.executable = executable;
        this.executable.setFunction(this);
    }

    protected void setAsync(boolean async) {
        this.async = async;
    }

    public boolean isAsync() {
        return async;
    }

    @Override
    public boolean isHorizontal() {
        return horizontal;
    }

    @Override
    public Widget setHorizontal(boolean horizontal) {
        this.horizontal = horizontal;
        return this;
    }

    @Override
    public String getElementId() {
        return elementId;
    }

    @Override
    public Widget setElementId(String elementId) {
        this.elementId = elementId;
        return this;
    }

    @Override
    public String getCustomLabelStyle() {
        return customLabelStyle;
    }

    @Override
    public Widget setCustomLabelStyle(String customLabelStyle) {
        this.customLabelStyle = customLabelStyle;
        return this;
    }

    public Executable getExecutable() {
        return executable;
    }

    public List<Property<? extends Serializable>> getProperties() {
        return properties;
    }

    public Function addProperty(Property<? extends Serializable> property) {
        properties.add(property);
        return this;
    }

    public FunctionDTO getFunctionDTO(Context ctx) {
        if (getAccess(ctx) == Access.NONE) {
            return null;
        }
        FunctionDTO functionDTO = new FunctionDTO(this.getId(), this.getLocalizedName(ctx));
        //functionDTO.setEnabled(isEnabled(ctx));
        functionDTO.setAccess(getAccess(ctx));
        functionDTO.setLocalizedName(this.getLocalizedName(ctx));
        functionDTO.setLocalizedTooltip(this.getLocalizedTooltip(ctx));
        functionDTO.setIcon(this.getIcon());
        functionDTO.setElementId(getElementId());
        functionDTO.setVisible(isVisible(ctx));
        for (Property<? extends Serializable> p : this.getProperties()) {
            functionDTO.addProperty(p.getPropertyDTO(ctx));
        }
//        Set<String> keySet = this.wizards.keySet();
//        for (String key : keySet) {
//            WizardPage wizard = this.wizards.get(key);
//            functionDTO.registerWizardPageDTO(key, wizard.getViewDTO(ctx));
//        }


        return functionDTO;
    }

    @Override
    public WidgetDTO getWidgetDescriptor(Context ctx) {
        FunctionDTO functionDTO = getFunctionDTO(ctx);
        if (functionDTO != null) {
            FunctionWidgetDTO retval =  new FunctionWidgetDTO(functionDTO);
            retval.setAccess(getAccess(ctx));
            retval.setElementId(getElementId());
            retval.setVisible(isVisible(ctx));
            retval.setElementId(getElementId());
            retval.setCustomLabelStyle(getCustomLabelStyle());
            retval.setLocalizedTooltip(getLocalizedTooltip(ctx));
            retval.setIcon(getIcon());
            return retval;
        }else{
            return null;
        }
    }

    public void registerProperties(Form form) {
    }

    public void registerWizardPage(WizardPage wview) {
        this.wizards.add(wview);
    }


    public List<WizardPage> getRegisteredViews() {
        return this.wizards;
    }

    private int size = -1;

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public Widget setSize(int size) {
        this.size = size;
        return this;
    }


    @Override
    public Widget setFormatPattern(String formatPattern) {
        return this;
    }

    @Override
    public String getFormatPattern() {
        return null;
    }


    private boolean enabled = true;

    @Override
    public boolean isEnabled(Context ctx) {
        return enabled;
    }

    @Override
    public Widget setEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    private boolean visible = true;

    @Override
    public boolean isVisible(Context ctx) {
        return visible;
    }

    @Override
    public Widget setVisible(boolean visible) {
        this.visible = visible;
        return this;
    }

    private String localizationKey;

    @Override
    public Widget setLocalizationKey(String localizationKey) {
        this.localizationKey = localizationKey;
        return this;
    }

    @Override
    public String getLocalizationKey() {
        return localizationKey;
    }

    @Override
    public String getLocalizedName(Context ctx) {
        return Configurator.get().getLocalizedString(localizationKey, ctx != null ? ctx.getUserLocale() : Locale.getDefault());
    }

    private String localizationTooltipKey;

    @Override
    public String getLocalizationTooltipKey() {
        return localizationTooltipKey;
    }

    @Override
    public Widget setLocalizationTooltipKey(String localizationTooltipKey) {
        this.localizationTooltipKey = localizationTooltipKey;
        return this;
    }

    @Override
    public String getLocalizedTooltip(Context ctx) {
        return Configurator.get().getLocalizedString(localizationTooltipKey, ctx != null ? ctx.getUserLocale() : Locale.getDefault());
    }

    private String icon;

    @Override
    public String getIcon() {
        return icon;
    }

    @Override
    public Widget setIcon(String icon) {
        this.icon = icon;
        return this;
    }

    @Override
    public Widget cloneWithReference(Reference<? extends Entity> referencingProperty) {
        Function retval;
        try {
            retval = (Function) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new UnsupportedOperationException(e);
        }
        if (referencingProperty != null) {
            retval.setEnabled(false);
        }
        return retval;
    }
}
