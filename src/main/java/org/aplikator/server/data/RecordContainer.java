package org.aplikator.server.data;

import org.aplikator.client.shared.data.*;
import org.aplikator.server.DescriptorRegistry;
import org.aplikator.server.descriptor.Entity;
import org.aplikator.server.descriptor.View;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@SuppressWarnings("serial")
public class RecordContainer implements Serializable {

    private transient static Logger LOG = Logger.getLogger(RecordContainer.class.getName());

    private List<ContainerNode> nodes = new ArrayList<ContainerNode>();

    private FunctionResult functionResult;


    public RecordContainer() {
    }

    public RecordContainer(RecordContainerDTO rcDTO) {
        List<ContainerNode> nodes = new ArrayList<ContainerNode>(rcDTO.getRecords().size());
        for (ContainerNodeDTO containerNodeDTO : rcDTO.getRecords()) {
            nodes.add(new ContainerNode(containerNodeDTO));
        }
        this.setContainerNodes(nodes);
        this.functionResult = rcDTO.getFunctionResult();
    }

    public RecordContainerDTO getRecordContainerDTO() {
        List<ContainerNodeDTO> nodes = new ArrayList<ContainerNodeDTO>(this.getRecords().size());
        for (ContainerNode containerNode : this.getRecords()) {
            nodes.add(containerNode.getContainerNodeDTO());
        }
        RecordContainerDTO retval = new RecordContainerDTO();
        retval.setContainerNodes(nodes);
        retval.setFunctionResult(functionResult);
        return retval;
    }


    public void addRecord(View view, Record original, Record edited, Operation operation) {
        if (view == null) {
            if (original != null) {
                view = ((Entity) DescriptorRegistry.get().getDescriptionItem(original.getPrimaryKey().getEntityId())).view();
            } else if (edited != null){
                view = ((Entity) DescriptorRegistry.get().getDescriptionItem(edited.getPrimaryKey().getEntityId())).view();
            } else {
                throw new IllegalArgumentException("RecordContainer cannot accept null original and edited ");
            }
        }
        if (original == null && !(Operation.CREATE.equals(operation) || Operation.PREPARE.equals(operation))) {
            throw new IllegalArgumentException("RecordContainer cannot accept null original for operation " + operation);
        }
        boolean foundExisting = false;
        if (Operation.UPDATE.equals(operation) || Operation.DELETE.equals(operation)) {
            for (ContainerNode node : nodes) {
                if (node.getOriginal() != null && original.getPrimaryKey().equals(node.getOriginal().getPrimaryKey())) {
                    if (Operation.UPDATE.equals(node.getOperation()) || Operation.CREATE.equals(node.getOperation())) {
                        node.setEdited(edited);
                        foundExisting = true;
                        break;
                    }
                }
            }
        }
        if (!foundExisting) {
            nodes.add(new ContainerNode(view, original, edited, operation));
        } else {
            if (Operation.DELETE.equals(operation)) {
                for (int i = 0; i < nodes.size(); i++) {
                    if (original.getPrimaryKey().equals(nodes.get(i).getOriginal().getPrimaryKey())) {
                        nodes.remove(i);
                    }
                }
            }
        }
    }

    public List<ContainerNode> getRecords() {
        return nodes;
    }

    public void setContainerNodes(List<ContainerNode> nodes) {
        this.nodes = nodes;
    }

    public void clear() {
        nodes.clear();
        functionResult = null;
    }


    public Record findLastUpdated(PrimaryKey primaryKey) {
        Record retval = null;
        for (ContainerNode node : nodes) {
            //if (Operation.UPDATE.equals(node.getOperation())) {
            if (node.getEdited() != null && primaryKey.equals(node.getEdited().getPrimaryKey())) {
                retval = node.getEdited();
            }
            //}
        }
        return retval;
    }

    public void updateTemporaryOwnerPK(PrimaryKey rootPK) {
        for (ContainerNode node : nodes) {
            if (node.getEdited() != null && node.getEdited().getOwnerPrimaryKey() != null && node.getEdited().getOwnerPrimaryKey().getId() == -1) {
                if (rootPK.getTempId().equals(node.getEdited().getOwnerPrimaryKey().getTempId())) {
                    node.getEdited().getOwnerPrimaryKey().setId(rootPK.getId());
                }
            }
        }
    }

    public FunctionResult getFunctionResult() {
        return functionResult;
    }

    public void setFunctionResult(FunctionResult functionResult) {
        this.functionResult = functionResult;
    }
}
