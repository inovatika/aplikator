package org.aplikator.client.local.widgets;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.event.dom.client.*;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.i18n.client.NumberFormat;
import com.google.gwt.user.client.Command;
import org.aplikator.client.local.Aplikator;
import org.aplikator.client.shared.data.ListItem;
import org.aplikator.client.shared.descriptor.PropertyDTO;
import org.gwtbootstrap3.client.ui.*;
import org.gwtbootstrap3.client.ui.constants.Pull;
import org.gwtbootstrap3.client.ui.constants.Toggle;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.logging.Logger;

public class TextFieldComboWidget<T extends Serializable> extends DataFieldBase<T> implements DataField<T> {
    private static Logger LOG = Logger.getLogger(TextFieldComboWidget.class.getName());
    private TextBox box = new TextBox();
    private Button button = new Button();
    private DropDown dropdown = new DropDown();
    private InputGroup inputGroup = new InputGroup();

    private String format = null;
    String oldValue = "";

    private void initDropDown(){

        //button.setIcon(IconType.ARROW_DOWN);
        button.setDataToggle(Toggle.DROPDOWN);
        dropdown.add(button);
        DropDownMenu dm = new DropDownMenu();
        dm.setPull(Pull.LEFT);
        for (ListItem item : getProperty().getListValues()) {
            AnchorListItem it = new AnchorListItem(item.getName());
            it.addClickHandler(new ClickHandler() {
                @Override
                public void onClick(ClickEvent event) {
                    int cpos = box.getCursorPos();
                    box.setText(box.getText().substring(0,cpos)+it.getText()+box.getText().substring(cpos));
                    setDirty(true);
                    ValueChangeEvent.fire(box, box.getValue());
                }
            });
            dm.add(it);
        }
        dropdown.add(dm);
    }

    private void initInputGroup(){
        inputGroup.add(box);
        InputGroupButton igb = new InputGroupButton();
        igb.add(dropdown);
        inputGroup.add(igb);
    }

    public TextFieldComboWidget(String caption, String localizedTooltip, PropertyDTO property, int size, String format, boolean horizontal, String elementId, String customLabelStyle, HasFields form) {
        super(caption, localizedTooltip, property, horizontal, elementId, customLabelStyle, form);
        initDropDown();
        initInputGroup();
        controlHolder.add(inputGroup);
        //box.addStyleName(PaneWidgetResources.INSTANCE.css().formData());
        box.addValueChangeHandler(new ValueChangeHandler<String>() {
            public void onValueChange(ValueChangeEvent<String> event) {
                //event.getValue()
                setDirty(true);
            }
        });
        box.addKeyUpHandler(new KeyUpHandler() {
            @Override
            public void onKeyUp(KeyUpEvent event) {
                if (!oldValue.equals(box.getValue())) {
                    oldValue = box.getValue();
                    ValueChangeEvent.fire(box, box.getValue());
                }
            }
        });
//        if (property.getRefferedThrough() != null) { //resolved in the setEnabled method
//            this.setEnabled(false);
//        }

        this.setGridSize(size);

        this.format = format;
    }

    @SuppressWarnings({"unchecked", "deprecation"})
    public T getValue() {
        String strVal = box.getValue();
        //LOG.fine("TextField getValue:"+strVal);
        if (strVal == null || "".equals(strVal.trim()))
            return null;
        String type = getProperty().getType();
        if ("java.lang.String".equals(type)) {
            return (T) strVal;
        } else if ("java.lang.Integer".equals(type)) {
            try {
                return (T) Integer.decode(strVal);
            } catch (NumberFormatException ex) {
                Messages.error(Aplikator.application.getConfigString("aplikator.field.invalidValue") + " " + label.getText() + " - " + strVal);
                throw ex;
            }
        } else if ("java.math.BigDecimal".equals(type)) {
            try {
                BigDecimal rv = new BigDecimal(strVal);
                //LOG.fine("TextField getValue return:"+rv);
                return (T) rv;
            } catch (NumberFormatException ex) {
                Messages.error(Aplikator.application.getConfigString("aplikator.field.invalidValue") + " " + label.getText() + " - " + strVal);
                throw ex;
            }
        } else if ("java.util.Date".equals(type)) {
            try {
                return (T) new Date(strVal);
            } catch (IllegalArgumentException ex) {
                Messages.error(Aplikator.application.getConfigString("aplikator.field.invalidValue") + " " + label.getText() + " - " + strVal);
                throw ex;
            }
        } else {
            throw new IllegalStateException("TextFieldWidget does not support the data type " + type);
        }
    }

    public void setValue(Serializable value) {
        if (format == null) {
            box.setValue(value != null ? value.toString() : null);
            oldValue = value != null ? value.toString() : "";
        } else {
            if (value == null) {
                box.setValue(null);
                oldValue = "";
            } else {
                String formattedValue = value.toString();
                String type = getProperty().getType();
                if ("java.math.BigDecimal".equals(type)) {
                    formattedValue = NumberFormat.getFormat(format).format((BigDecimal) value);
                }
                box.setValue(formattedValue);
                oldValue = formattedValue;
            }
        }
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public HandlerRegistration addValueChangeHandler(ValueChangeHandler handler) {
        return box.addValueChangeHandler(handler);
    }

    public void setEnabled(boolean enabled) {
        if (property.getRefferedThrough() != null) {
            enabled = false;
        }
        box.setEnabled(enabled);
        button.setEnabled(enabled);
    }

    @Override
    public void grabFocus() {
        Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
            @Override
            public void execute() {
                box.setFocus(true);
            }
        });
    }

    @Override
    public void addEnterHandler(final Command command) {
        box.addKeyUpHandler(new KeyUpHandler() {
            @Override
            public void onKeyUp(KeyUpEvent event) {
                event.stopPropagation();
                if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
                    command.execute();
                }
            }
        });
    }
}
