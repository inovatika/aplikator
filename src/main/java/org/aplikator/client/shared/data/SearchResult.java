package org.aplikator.client.shared.data;

import java.util.List;

import org.jboss.errai.common.client.api.annotations.Portable;

/**
 * SearchResult class for storing the result from search - contains records and total count
 *
 * @author eskymo
 */
@Portable
public class SearchResult {
    private List<RecordDTO> records;
    private Long count;

    public List<RecordDTO> getRecords() {
        return records;
    }

    public void setRecords(List<RecordDTO> records) {
        this.records = records;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

    public SearchResult() {
    }

    public SearchResult(List<RecordDTO> records, Long count) {
        super();
        this.records = records;
        this.count = count;
    }


}
