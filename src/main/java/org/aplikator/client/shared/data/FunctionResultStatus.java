package org.aplikator.client.shared.data;

/**
 *
 */
public enum FunctionResultStatus {
    SUCCESS, ERROR, WARNING
}
