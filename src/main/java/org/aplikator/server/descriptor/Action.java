package org.aplikator.server.descriptor;

import org.aplikator.client.shared.descriptor.Access;
import org.aplikator.client.shared.descriptor.ActionDTO;
import org.aplikator.server.DescriptorRegistry;
import org.aplikator.server.data.Context;

public class Action extends LocalizedServerDescriptorBase {

    private String token;

    public Action(String id, String readableName, String token) {
        super(id);
        this.setLocalizationKey(readableName);
        this.token = token;
        ServerDescriptorBase actionSource = DescriptorRegistry.get().getDescriptionItem(id);
        if (actionSource != null) {
            this.accessControl = actionSource.getAccessControl();
        }
    }

    public Action(String name, String token) {
        this(name, name, token);
    }

    public ActionDTO getActionDTO(Context ctx) {
        if (getAccess(ctx).equals(Access.NONE)) {
            return null;
        }
        ActionDTO retval = new ActionDTO(this.getId(), this.getLocalizedName(ctx), token);
        retval.setAccess(getAccess(ctx));
        return retval;
    }

}
