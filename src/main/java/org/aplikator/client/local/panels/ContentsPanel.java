package org.aplikator.client.local.panels;

import org.gwtbootstrap3.client.ui.Column;
import org.gwtbootstrap3.client.ui.Row;
import org.gwtbootstrap3.client.ui.constants.ColumnSize;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.Label;

public class ContentsPanel extends Composite implements AcceptsOneWidget {
    public interface ContentsPanelResources extends ClientBundle {
        public static final ContentsPanelResources INSTANCE = GWT.create(ContentsPanelResources.class);

        @Source("ContentsPanel.css")
        public ContentsPanelCss css();
    }

    public interface ContentsPanelCss extends CssResource {
        public String contentsHeader();

        public String contentsFrame();

        public String contentsHolder();
    }

    static {
        ContentsPanelResources.INSTANCE.css().ensureInjected();
    }

    private Row headerPanel = new Row();
    private Row contents = new Row();
    private Label headerText = new Label();
    private Column holder = new Column(ColumnSize.XS_12);

    public ContentsPanel() {

        headerPanel.add(headerText);
        // headerPanel.setWidth("100%");
        // headerPanel.setHeight("2em");
        // headerPanel.getElement().getStyle().setPosition(Position.RELATIVE);
        // contents.getElement().getStyle().setPosition(Position.RELATIVE);
        // ScrollPanel scroller = new ScrollPanel(contents);
        // contents.setSize("100%", "100%");

        holder.add(headerPanel);
        holder.add(contents);

        initWidget(holder);

        //headerPanel.setStyleName(ContentsPanelResources.INSTANCE.css().contentsHeader());
        //holder.setStyleName(ContentsPanelResources.INSTANCE.css().contentsFrame());
        //contents.setStyleName(ContentsPanelResources.INSTANCE.css().contentsHolder());

    }

    public void setWidget(IsWidget wdgt) {
        contents.clear();
        if (wdgt != null) {
            contents.add(wdgt);
        }
    }

    public void setTitle(String title) {
        headerText.setText(title);
    }

    /**
     * Adjusts the widget's size such that it fits within the window's client
     * area.
     */
    /*
     * public void adjustSize(int windowWidth, int windowHeight) { int
     * scrollWidth = windowWidth - holder.getAbsoluteLeft() - 3; if (scrollWidth
     * < 1) { scrollWidth = 1; }
     *
     * int scrollHeight = windowHeight - holder.getAbsoluteTop() - 3; if
     * (scrollHeight < 1) { scrollHeight = 1; }
     *
     * holder.setPixelSize(scrollWidth, scrollHeight); }
     */
}
