package org.aplikator.client.shared.rpc.marshaller;

import org.aplikator.client.shared.data.ListItem;
import org.aplikator.client.shared.descriptor.ClientDescriptorBase;
import org.jboss.errai.marshalling.client.api.Marshaller;
import org.jboss.errai.marshalling.client.api.MarshallingSession;
import org.jboss.errai.marshalling.client.util.MarshallUtil;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class MarshallingUtils {

    /*public static StringBuilder marshallData(StringBuilder input, Serializable value) {
        DataType lookpup = DataMarshallingUtils.DataType.lookup(value);
		if (lookpup != null) {
			return lookpup.marshall(input, value);
		} else throw new UnsupportedOperationException("unsupported type !");
	}*/


    /**
     * marshall default map
     */
    public static StringBuilder mapMarshall(StringBuilder input, @SuppressWarnings("rawtypes") Map o, MarshallingSession ctx, String encName) {
        input.append('{');
        Marshaller<Object> marshaller = ctx.getMarshallerInstance(encName);
        Object[] keys = o.keySet().toArray();
        for (int i = 0, ll = keys.length; i < ll; i++) {
            if (i > 0)
                input.append(',');
            key(keys[i].toString(), input).append(':').append(marshaller.marshall(o.get(keys[i]), ctx));
        }
        input.append('}');
        return input;
    }

    /**
     * marshall default list
     */
    public static StringBuilder listMarshall(StringBuilder input, @SuppressWarnings("rawtypes") List o, MarshallingSession ctx, String encName) {
        input.append('[');
        Marshaller<Object> marshaller = ctx.getMarshallerInstance(encName);
        for (int i = 0, ll = o.size(); i < ll; i++) {
            if (i > 0)
                input.append(',');
            input.append(marshaller.marshall(o.get(i), ctx));
        }
        input.append(']');
        return input;
    }

    /**
     * marshall default set
     */
    public static StringBuilder setMarshall(StringBuilder input, @SuppressWarnings("rawtypes") Set o, MarshallingSession ctx, String encName) {
        input.append('[');
        Marshaller<Object> marshaller = ctx.getMarshallerInstance(encName);
        Iterator it = o.iterator();
        for (int i = 0, ll = o.size(); i < ll; i++) {
            if (i > 0)
                input.append(',');
            input.append(marshaller.marshall(it.next(), ctx));
        }
        input.append(']');
        return input;
    }

    public static StringBuilder clientDescriptorBase(StringBuilder input, ClientDescriptorBase cdb) {
        StringBuilder m = string("id", cdb.getId(), input).append(',');
        m = string("localizedName", cdb.getLocalizedName(), input).append(',');
        m = string("access", cdb.getAccess().name(), input);
        if (cdb.getLocalizedTooltip()!= null){
            m.append(",");
            m=string("localizedTooltip", cdb.getLocalizedTooltip(), input);
        }
        if (cdb.getIcon() != null) {
            m.append(",");
            m = string("icon", cdb.getIcon(), input);
        }
        return m;
    }

    public static StringBuilder key(String key, StringBuilder input) {
        return input.append('"').append(key).append('"');
    }

    public static StringBuilder number(String key, Number number, StringBuilder input) {
        return key(key, input).append(':').append(number);
    }

    public static String escape(String val) {
        return MarshallUtil.jsonStringEscape(val);
    }


    public static StringBuilder string(String key, String str, StringBuilder input) {
        if (str != null) {
            return key(key, input).append(':').append('"').append(escape(str)).append('"');
        } else {
            return key(key, input).append(':').append("null");
        }
    }

    public static StringBuilder bool(String key, boolean val, StringBuilder input) {
        return key(key, input).append(':').append(val);
    }


    public static StringBuilder nullval(String key, StringBuilder input) {
        return key(key, input).append(':').append("null");
    }

    public static void main(String[] args) {
        StringBuilder builder = new StringBuilder("prvniradek \n "
                + "druhyradek \"\n"
                + "treti radek \n");

        System.out.println(escape(builder.toString()));

    }

    /**
     * marshall default list
     */
    public static String listItemsMarshall(List<ListItem> o) {
        StringBuilder input = new StringBuilder();
        input.append('[');
        ListItemMarshaller marshaller = new ListItemMarshaller();
        for (int i = 0, ll = o.size(); i < ll; i++) {
            if (i > 0)
                input.append(',');
            input.append(marshaller.marshall(o.get(i), null));
        }
        input.append(']');
        return input.toString();
    }
}
