package org.aplikator.client.shared.descriptor;

import org.aplikator.client.local.widgets.CheckBoxWidget;
import org.aplikator.client.local.widgets.HasFields;

import com.google.gwt.user.client.ui.Widget;

@SuppressWarnings("serial")
//@Portable
public class CheckBoxDTO extends WidgetPropertyDTOBase {

    @SuppressWarnings("unused")
    public CheckBoxDTO() {  //TODO custom marshaller
    }

    public CheckBoxDTO(PropertyDTO property) {
        super(property);
    }

    private String switcher;

    public String getSwitcher() {
        return switcher;
    }

    public void setSwitcher(String switcher) {
        this.switcher = switcher;
    }

    @Override
    public Widget getWidget(HasFields form) {
        CheckBoxWidget field = new CheckBoxWidget(getLocalizedName(), getLocalizedTooltip(), property, getSize(), isHorizontal(), getElementId(), getSwitcher(), getCustomLabelStyle(), form);
        field.setEnabled(isEnabled());
        field.setVisible(isVisible());
        field.setDefaultEnabled(isEnabled());
        if (form != null) {
            form.registerDataField(field);
        }
        return field;
    }

}
