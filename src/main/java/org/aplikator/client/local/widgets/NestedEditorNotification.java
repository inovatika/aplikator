package org.aplikator.client.local.widgets;

/**
 *
 */
public interface NestedEditorNotification {
    void nestedEditorStarted();

    void nestedEditorFinished();
}
