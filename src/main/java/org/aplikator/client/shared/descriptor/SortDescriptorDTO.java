package org.aplikator.client.shared.descriptor;

import org.jboss.errai.common.client.api.annotations.Portable;

/**
 * @author vlahoda
 */

@Portable
public class SortDescriptorDTO extends ClientDescriptorBase {

    private PropertyDTO primarySortProperty;

    private boolean primaryAscending;

    @SuppressWarnings("unused")
    public SortDescriptorDTO() { //TODO custom marshaller
    }

    public SortDescriptorDTO(String id, String localizedName) {
        super(id, localizedName);
    }

    public PropertyDTO getPrimarySortProperty() {
        return primarySortProperty;
    }

    public void setPrimarySortProperty(PropertyDTO primarySortProperty) {
        this.primarySortProperty = primarySortProperty;
    }

    public boolean isPrimaryAscending() {
        return primaryAscending;
    }

    public void setPrimaryAscending(boolean primaryAscending) {
        this.primaryAscending = primaryAscending;
    }


}
