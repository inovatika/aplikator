package org.aplikator.server.descriptor;

import org.aplikator.client.shared.descriptor.TextFieldComboDTO;
import org.aplikator.client.shared.descriptor.WidgetDTO;
import org.aplikator.server.data.Context;

import java.io.Serializable;

public class TextFieldCombo<T extends Serializable> extends WidgetPropertyDescriptorBase<T> {


    public TextFieldCombo(Property<T> property) {
        super(property);
        if (property.getListProvider() == null) {
            throw new IllegalStateException("Property " + property.getId() + " must have assigned listProvider");
        }
    }

    @Override
    public WidgetDTO getWidgetDescriptor(Context ctx) {
        TextFieldComboDTO desc = new TextFieldComboDTO(getProperty().getPropertyDTO(ctx));
        desc.setSize(getSize());
        desc.setFormatPattern(getFormatPattern());
        desc.setLocalizedName(getLocalizedName(ctx));
        desc.setLocalizedTooltip(getLocalizedTooltip(ctx));
        desc.setAccess(getProperty().getAccess(ctx));
        desc.setEnabled(isEnabled(ctx));
        desc.setVisible(isVisible(ctx));
        desc.setHorizontal(isHorizontal());
        desc.setElementId(getElementId());
        desc.setCustomLabelStyle(getCustomLabelStyle());
        return desc;
    }


    @Override
    public String toString() {
        return "TextFieldCombo{" + "property=" + getProperty() + ", size=" + getSize() + '}';
    }


}
