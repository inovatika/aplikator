package org.aplikator.client.local.widgets;

import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.user.client.Command;
import org.aplikator.client.shared.data.RecordDTO;
import org.aplikator.client.shared.descriptor.PropertyDTO;

import java.io.Serializable;

public interface DataField<I extends Serializable> extends HasValueChangeHandlers<I>, DisableableWidget {

    PropertyDTO getProperty();

    void setValue(I value);

    I getValue();

    void setDirty(boolean dirty);

    boolean isDirty();

    void grabFocus();

    void addEnterHandler(Command command);

    void setDefaultEnabled(boolean enabled);

    boolean isDefaultEnabled();

    void processAnnotations(RecordDTO sourceRecord);

//    default void processAnnotations(RecordDTO sourceRecord){
//        String recordAccessStr = sourceRecord.getAnnotation(AnnotationKeys.ACCESS_ANNOTATION_KEY);
//        String fieldAccessStr = sourceRecord.getPropertyAnnotation(this.getProperty().getId(), AnnotationKeys.ACCESS_ANNOTATION_KEY);
//        if (fieldAccessStr != null) {
//            Access fieldAccess = Access.valueOf(fieldAccessStr);
//            this.setEnabled(!fieldAccess.isReadOnly());
//        } else if (recordAccessStr != null) {
//            Access recordAccess = Access.valueOf(recordAccessStr);
//            this.setEnabled(!recordAccess.isReadOnly());
//        } else {
//            this.setEnabled(this.isDefaultEnabled());
//        }
//    }
}
