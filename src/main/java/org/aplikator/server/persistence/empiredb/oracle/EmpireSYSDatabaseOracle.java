/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.aplikator.server.persistence.empiredb.oracle;

// java

import org.apache.empire.data.DataType;
import org.apache.empire.db.DBDatabase;
import org.apache.empire.db.DBTable;
import org.apache.empire.db.DBTableColumn;
import org.apache.empire.db.oracle.DBDatabaseDriverOracle;

/**
 * Represents the data model of the system tables. They are required to parse
 * the data models.
 * <p/>
 */
public class EmpireSYSDatabaseOracle extends DBDatabase {

    // Table for all columns of a schema with their comments
    public static class DBColComments extends DBTable {
        private final static long serialVersionUID = 1L;

        public DBTableColumn C_OWNER;
        public DBTableColumn C_TABLE_NAME;
        public DBTableColumn C_COLUMN_NAME;
        public DBTableColumn C_COMMENTS;

        // Constructor
        public DBColComments(DBDatabase db) {
            super("ALL_COL_COMMENTS", db);
            // add all Colums
            C_OWNER = addColumn("OWNER", DataType.TEXT, 50, false);
            C_TABLE_NAME = addColumn("TABLE_NAME", DataType.TEXT, 50, false);
            C_COLUMN_NAME = addColumn("COLUMN_NAME", DataType.TEXT, 50, false);
            C_COMMENTS = addColumn("COMMENTS", DataType.TEXT, 500, false);
        }
    }

    // Table for all columns of a schema with their comments
    public static class DBColInfo extends DBTable {
        private final static long serialVersionUID = 1L;

        public DBTableColumn C_OWNER;
        public DBTableColumn C_TABLE_NAME;
        public DBTableColumn C_COLUMN_NAME;
        public DBTableColumn C_DATA_TYPE;
        public DBTableColumn C_DATA_TYPE_MOD;
        public DBTableColumn C_DATA_TYPE_OWNER;
        public DBTableColumn C_NULLABLE;
        public DBTableColumn C_DATA_LENGTH;
        public DBTableColumn C_DATA_PRECISION;
        public DBTableColumn C_DATA_SCALE;
        public DBTableColumn C_CHAR_LENGTH;

        // Constructor
        public DBColInfo(DBDatabase db) {
            super("ALL_TAB_COLUMNS", db);
            // add all Colums
            C_OWNER = addColumn("OWNER", DataType.TEXT, 30, false);
            C_TABLE_NAME = addColumn("TABLE_NAME", DataType.TEXT, 30, false);
            C_COLUMN_NAME = addColumn("COLUMN_NAME", DataType.TEXT, 30, false);
            C_DATA_TYPE = addColumn("DATA_TYPE", DataType.TEXT, 50, false);
            C_DATA_TYPE_MOD = addColumn("DATA_TYPE_MOD", DataType.TEXT, 50,
                    false);
            C_DATA_TYPE_OWNER = addColumn("DATA_TYPE_OWNER", DataType.TEXT, 50,
                    false);
            C_NULLABLE = addColumn("NULLABLE", DataType.TEXT, 1, false);
            C_DATA_LENGTH = addColumn("DATA_LENGTH", DataType.DECIMAL, 0, false);
            C_DATA_PRECISION = addColumn("DATA_PRECISION", DataType.DECIMAL, 0,
                    false);
            C_DATA_SCALE = addColumn("DATA_SCALE", DataType.DECIMAL, 0, false);
            C_CHAR_LENGTH = addColumn("CHAR_LENGTH", DataType.DECIMAL, 0, false);
        }
    }

    // Table for all constraints
    public static class DBConstraints extends DBTable {
        private final static long serialVersionUID = 1L;

        public DBTableColumn C_CONSTRAINT_NAME;
        public DBTableColumn C_TABLE_NAME;
        public DBTableColumn C_R_CONSTRAINT_NAME;
        public DBTableColumn C_CONSTRAINT_TYPE;
        public DBTableColumn C_STATUS;

        // Constructor
        public DBConstraints(DBDatabase db) {
            super("USER_CONSTRAINTS", db);
            // add all Colums

            C_CONSTRAINT_NAME = addColumn("CONSTRAINT_NAME", DataType.TEXT,
                    100, false);
            C_TABLE_NAME = addColumn("TABLE_NAME", DataType.TEXT, 100, false);
            C_R_CONSTRAINT_NAME = addColumn("R_CONSTRAINT_NAME", DataType.TEXT,
                    100, false);
            C_CONSTRAINT_TYPE = addColumn("CONSTRAINT_TYPE", DataType.TEXT, 1,
                    false);
            C_STATUS = addColumn("STATUS", DataType.TEXT, 20, false);
        }
    }

    // Table for all tables of a schema with their comments
    public static class DBTabComments extends DBTable {
        private static final long serialVersionUID = 1L;

        public DBTableColumn C_OWNER;
        public DBTableColumn C_TABLE_NAME;
        public DBTableColumn C_TABLE_TYPE;
        public DBTableColumn C_COMMENTS;

        // Constructor
        public DBTabComments(DBDatabase db) {
            super("ALL_TAB_COMMENTS", db);
            // add all Colums
            C_OWNER = addColumn("OWNER", DataType.TEXT, 50, false);
            C_TABLE_NAME = addColumn("TABLE_NAME", DataType.TEXT, 50, false);
            C_TABLE_TYPE = addColumn("TABLE_TYPE", DataType.TEXT, 50, false);
            C_COMMENTS = addColumn("COMMENTS", DataType.TEXT, 500, false);
        }
    }

    public static class DBTabSequences extends DBTable {
        private static final long serialVersionUID = 1L;
        public DBTableColumn C_SEQUENCE_NAME;

        public DBTabSequences(DBDatabase db) {
            super("USER_SEQUENCES", db);
            C_SEQUENCE_NAME = addColumn("SEQUENCE_NAME", DataType.TEXT, 50,
                    true);
        }

    }

    //Table for Indexes
    public static class DBTabIndexes extends DBTable {
        private static final long serialVersionUID = 1L;
        public DBTableColumn C_INDEX_NAME;
        public DBTableColumn C_TABLE_NAME;
        public DBTableColumn C_INDEX_TYPE;
        public DBTableColumn C_UNIQUENESS;

        public DBTabIndexes(DBDatabase db) {
            super("USER_INDEXES", db);
            C_INDEX_NAME = addColumn("INDEX_NAME", DataType.TEXT, 100, false);
            C_TABLE_NAME = addColumn("TABLE_NAME", DataType.TEXT, 100, false);
            C_INDEX_TYPE = addColumn("INDEX_TYPE", DataType.TEXT, 100, false);
            C_UNIQUENESS = addColumn("UNIQUENESS", DataType.TEXT, 100, false);
            setPrimaryKey(C_INDEX_NAME);
        }

    }

    //Table for Index columns
    public static class DBIndexesCols extends DBTable {
        private static final long serialVersionUID = 1L;
        public DBTableColumn C_INDEX_NAME;
        public DBTableColumn C_TABLE_NAME;
        public DBTableColumn C_COLUMN_NAME;
        public DBTableColumn C_COLUMN_POSITION;
        public DBTableColumn C_COLUMN_LENGTH;
        public DBTableColumn C_CHAR_LENGTH;
        public DBTableColumn C_DESCEND;


        public DBIndexesCols(DBDatabase db) {
            super("USER_IND_COLUMNS", db);
            C_INDEX_NAME = addColumn("INDEX_NAME", DataType.TEXT, 100, false);
            C_TABLE_NAME = addColumn("TABLE_NAME", DataType.TEXT, 100, false);
            C_COLUMN_NAME = addColumn("COLUMN_NAME", DataType.TEXT, 100, false);
            C_COLUMN_POSITION = addColumn("COLUMN_POSITION", DataType.DECIMAL, 10, false);
            C_COLUMN_LENGTH = addColumn("COLUMN_LENGTH", DataType.DECIMAL, 10, false);
            C_CHAR_LENGTH = addColumn("CHAR_LENGTH", DataType.DECIMAL, 10, false);
            C_DESCEND = addColumn("DESCEND", DataType.TEXT, 4, false);
            setPrimaryKey(C_INDEX_NAME, C_COLUMN_NAME);
        }
    }

    // Table for Columns and Tables a constraint is associated to
    public static class DBUserConCol extends DBTable {
        private final static long serialVersionUID = 1L;

        public DBTableColumn C_CONSTRAINT_NAME;
        public DBTableColumn C_TABLE_NAME;
        public DBTableColumn C_COLUMN_NAME;
        public DBTableColumn C_OWNER;
        public DBTableColumn C_POSITION;

        // Constructor
        public DBUserConCol(DBDatabase db) {
            super("USER_CONS_COLUMNS", db);
            // add all Colums

            C_CONSTRAINT_NAME = addColumn("CONSTRAINT_NAME", DataType.TEXT,
                    100, false);
            C_TABLE_NAME = addColumn("TABLE_NAME", DataType.TEXT, 100, false);
            C_COLUMN_NAME = addColumn("COLUMN_NAME", DataType.TEXT, 100, false);
            C_OWNER = addColumn("OWNER", DataType.TEXT, 100, false);
            C_POSITION = addColumn("POSITION", DataType.INTEGER, 5, false);
            setPrimaryKey(C_CONSTRAINT_NAME);
        }
    }

    private final static long serialVersionUID = 1L;

    public DBTabComments TC = null;
    public DBColInfo CI = null;
    public DBColComments CC = null;
    public DBConstraints CO = null;
    public DBUserConCol UC = null;
    public DBTabSequences SQ = null;
    public DBTabIndexes IX = null;
    public DBIndexesCols IC = null;

    public EmpireSYSDatabaseOracle(DBDatabaseDriverOracle driver) {
        // System schema
        super("SYS");
        // Set Driver (do not use open(...)!)
        this.driver = driver;
        // Add Tables
        TC = new DBTabComments(this);
        CI = new DBColInfo(this);
        CC = new DBColComments(this);
        CO = new DBConstraints(this);
        UC = new DBUserConCol(this);
        SQ = new DBTabSequences(this);
        IX = new DBTabIndexes(this);
        IC = new DBIndexesCols(this);
    }
}