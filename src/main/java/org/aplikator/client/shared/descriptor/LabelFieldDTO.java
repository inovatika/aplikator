package org.aplikator.client.shared.descriptor;

import org.aplikator.client.local.widgets.HasFields;
import org.aplikator.client.local.widgets.LabelFieldWidget;

import com.google.gwt.user.client.ui.Widget;

@SuppressWarnings("serial")
//@Portable
public class LabelFieldDTO extends WidgetPropertyDTOBase {

    public static final String TYPE_KEY = "label";

    @SuppressWarnings("unused")
    public LabelFieldDTO() {   //TODO custom marshaller
    }

    public LabelFieldDTO(PropertyDTO property) {
        super(property);
    }

    @Override
    public Widget getWidget(HasFields form) {
        LabelFieldWidget field = new LabelFieldWidget(getLocalizedName(), getLocalizedTooltip(), property, getSize(), isHorizontal(), getElementId(), getCustomLabelStyle(), form);
        field.setVisible(isVisible());
        if (form != null) {
            form.registerDataField(field);
        }
        return field;
    }

}
