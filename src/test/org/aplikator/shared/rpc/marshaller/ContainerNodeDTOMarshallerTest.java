package org.aplikator.shared.rpc.marshaller;

import java.util.Date;

import org.aplikator.client.shared.data.ContainerNodeDTO;
import org.aplikator.client.shared.data.Operation;
import org.aplikator.client.shared.data.PrimaryKey;
import org.aplikator.client.shared.data.RecordDTO;
import org.aplikator.client.shared.rpc.marshaller.ContainerNodeDTOMarshaller;
import org.aplikator.client.shared.rpc.marshaller.PrimaryKeyMarshaller;
import org.aplikator.client.shared.rpc.marshaller.RecordDTOMarshaller;
import org.easymock.EasyMock;
import org.jboss.errai.marshalling.client.api.Marshaller;
import org.jboss.errai.marshalling.client.api.MarshallingSession;
import org.jboss.errai.marshalling.client.api.json.EJValue;
import org.jboss.errai.marshalling.client.marshallers.StringMarshaller;
import org.jboss.errai.marshalling.server.JSONDecoder;
import org.junit.Assert;

import junit.framework.TestCase;

public class ContainerNodeDTOMarshallerTest extends TestCase {

    public static RecordDTO createEditedRecord() {
        RecordDTO recordDTO = new RecordDTO();
        recordDTO.setPrimaryKey(new PrimaryKey("modul", 2));

        recordDTO.setValue("datumovepole", new Date());
        recordDTO.setValue("stringovepole", "string");
        recordDTO.setValue("integerovepole", 2);
        recordDTO.setValue("doublepole", 3.3);
        recordDTO.setValue("booleanpole", true);
        return recordDTO;
    }

    public static RecordDTO createOriginalRecord() {
        RecordDTO recordDTO = new RecordDTO();
        recordDTO.setPrimaryKey(new PrimaryKey("modul", 4));

        recordDTO.setValue("datumovepole", new Date());
        recordDTO.setValue("stringovepole", "string");
        recordDTO.setValue("integerovepole", 2);
        recordDTO.setValue("doublepole", 3.3);
        recordDTO.setValue("booleanpole", true);
        return recordDTO;
    }


    public void testMarshall() {
        ContainerNodeDTO cnode = new ContainerNodeDTO();
        cnode.setEdited(createEditedRecord());
        cnode.setOperation(Operation.UPDATE);
        cnode.setViewId("modul");
        cnode.setOriginal(createOriginalRecord());

        MarshallingSession ctx = ctx();

        ContainerNodeDTOMarshaller cmarsh = new ContainerNodeDTOMarshaller();
        String marshalled = cmarsh.marshall(cnode, ctx);
        EJValue ejValue = JSONDecoder.decode(marshalled);

        ContainerNodeDTO demarshalled = cmarsh.demarshall(ejValue, ctx);
        Assert.assertTrue(demarshalled.getViewId().equals(cnode.getViewId()));
        Assert.assertTrue(demarshalled.getEdited().getPrimaryKey().equals(cnode.getEdited().getPrimaryKey()));
        Assert.assertTrue(demarshalled.getOriginal().getPrimaryKey().equals(cnode.getOriginal().getPrimaryKey()));
        Assert.assertTrue(demarshalled.getOperation().equals(cnode.getOperation()));

    }

    @SuppressWarnings("unchecked")
    private MarshallingSession ctx() {
        MarshallingSession session = EasyMock.createMock(MarshallingSession.class);
        PrimaryKeyMarshaller pkmarsh = new PrimaryKeyMarshaller();
        Object marsh = pkmarsh;
        EasyMock.expect(session.getMarshallerInstance(PrimaryKey.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        StringMarshaller smarsh = new StringMarshaller();
        marsh = smarsh;
        EasyMock.expect(session.getMarshallerInstance(String.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        RecordDTOMarshaller recMarsh = new RecordDTOMarshaller();
        marsh = recMarsh;
        EasyMock.expect(session.getMarshallerInstance(RecordDTO.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        EasyMock.replay(session);
        return session;
    }

}
