package org.aplikator.server.descriptor;

import org.aplikator.client.shared.data.PrimaryKey;
import org.aplikator.client.shared.descriptor.Access;
import org.aplikator.server.DescriptorRegistry;
import org.aplikator.server.data.Context;

import java.util.UUID;

public abstract class ServerDescriptorBase {

    protected static final char TYPE_DELIMITER = ':';
    protected static final char SUFFIX_DELIMITER = '_';
    protected static final char PATH_DELIMITER = '.';
    /**
     * programmer - assigned globally unique string identifier
     */
    private final String id;
    protected AccessControl accessControl;

    protected ServerDescriptorBase(String id) {
        if (id == null) {
            this.id = getIdClassPrefix() + UUID.randomUUID().toString();
        } else {
            this.id = getIdClassPrefix() + id;
        }
        // throws Illegal Argument Exception for duplicate IDs
        DescriptorRegistry.get().registerDescriptionItem(this);
    }

    private String getIdClassPrefix() {
        if (this instanceof Entity) {
            return "";
        } else {
            return getClass().getSimpleName() + TYPE_DELIMITER;
        }
    }

    public String getId() {
        return id;
    }


    @Override
    public String toString() {
        return id;
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof ServerDescriptorBase))
            return false;
        ServerDescriptorBase other = (ServerDescriptorBase) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    public AccessControl getAccessControl() {
        return accessControl;
    }

    public void setAccessControl(AccessControl accessControl) {
        this.accessControl = accessControl;
    }

    public Access getAccess(Context ctx) {
        if (getAccessControl() == null || ctx == null) {
            return Access.READ_WRITE_CREATE_DELETE;
        } else {
            return getAccessControl().getAccess(ctx);
        }
    }

    public Access getRecordAccess(Context ctx, PrimaryKey primaryKey, int maxSize) {
        if (getAccessControl() == null || ctx == null) {
            return Access.READ_WRITE_CREATE_DELETE;
        } else {
            return getAccessControl().getRecordAccess(ctx, primaryKey, maxSize);
        }
    }

}
