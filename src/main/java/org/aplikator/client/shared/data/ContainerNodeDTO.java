package org.aplikator.client.shared.data;

import java.io.Serializable;

import org.jboss.errai.common.client.api.annotations.Portable;

@SuppressWarnings("serial")
@Portable
public class ContainerNodeDTO implements Serializable {
    //private ViewDTO view;
    private String viewId;
    private RecordDTO original;
    private RecordDTO edited;
    private Operation operation;

    @SuppressWarnings("unused")
    public ContainerNodeDTO() {//TODO custom marshaller
    }

    public ContainerNodeDTO(String viewId, RecordDTO original, RecordDTO edited, Operation operation) {
        this.viewId = viewId;
        this.original = original;
        this.edited = edited;
        this.operation = operation;
    }

//    public ViewDTO getView() {
//        return view;
//    }

    public String getViewId() {
        return viewId;
    }

    public void setViewId(String viewId) {
        this.viewId = viewId;
    }

    public RecordDTO getOriginal() {
        return original;
    }

    public RecordDTO getEdited() {
        return edited;
    }

    public Operation getOperation() {
        return operation;
    }

//    public void setView(ViewDTO view) {
//        this.view = view;
//    }

    public void setOriginal(RecordDTO original) {
        this.original = original;
    }

    public void setEdited(RecordDTO edited) {
        this.edited = edited;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }

    /**
     * Returns a record, which contains summary of properties from edited and original.
     * If property exists in both, the merged record contains the edited one
     *
     * @return
     */
    public RecordDTO getMerged() {
        // if edited and original is null then return null
        if ((this.getOriginal() == null) && (this.getEdited() == null)) {
            return null;
        }

        // if edited is null then returns cloned original
        if (this.getEdited() == null) {
            return this.getOriginal().clone();
        }

        // if original is null then returns cloned edited
        if (this.getOriginal() == null) {
            return this.getEdited().clone();
        }

        // initially creates a merged record as a clone of edited
        RecordDTO mergedRec = this.getEdited().clone();
        for (String property : this.getOriginal().data.keySet()) {
            // adds properties from original if property is not yet present
            if (!mergedRec.data.containsKey(property)) {
                mergedRec.data.put(property, this.getOriginal().data.get(property));
            }
        }

        // adds annotations from original
        for (String annotKey : this.getOriginal().recordAnnotations.keySet()) {
            if (!mergedRec.hasAnnotation(annotKey)) {
                mergedRec.putAnnotation(annotKey, this.getOriginal().recordAnnotations.get(annotKey));
            }
        }
        return mergedRec;
    }


}
