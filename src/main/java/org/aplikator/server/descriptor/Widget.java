package org.aplikator.server.descriptor;

import org.aplikator.client.shared.descriptor.WidgetDTO;
import org.aplikator.server.data.Context;

public interface Widget extends Cloneable {

    WidgetDTO getWidgetDescriptor(Context ctx);

    void registerProperties(Form form);

    int getSize();

    Widget setSize(int size);

    String getLocalizationKey();

    Widget setLocalizationKey(String localizationKey);

    String getLocalizedName(Context ctx);

    String getLocalizationTooltipKey();

    Widget setLocalizationTooltipKey(String localizationTooltipKey);

    String getLocalizedTooltip(Context ctx);

    Widget setFormatPattern(String formatPattern);

    String getFormatPattern();

    boolean isEnabled(Context ctx);

    Widget setEnabled(boolean enabled);

    boolean isVisible(Context ctx);

    Widget setVisible(boolean visible);

    boolean isHorizontal();

    Widget setHorizontal(boolean horizontal);

    String getElementId();

    Widget setElementId(String elementId);

    String getCustomLabelStyle();

    Widget setCustomLabelStyle(String customLabelStyle);

    Widget cloneWithReference(Reference<? extends Entity> referencingProperty);

    Widget setIcon(String icon);

    String getIcon();

}
