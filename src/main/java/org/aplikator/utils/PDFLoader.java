package org.aplikator.utils;

import org.apache.pdfbox.Loader;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Logger;

/**
 *
 */
public class PDFLoader {
    public static final Logger LOGGER = Logger.getLogger(PDFLoader.class.getName());

    private PDFLoader() {
    }

    public static BufferedImage load(InputStream stream) throws IOException {
        return load(IOUtils.readBytes(stream, false));
    }


    public static BufferedImage load(byte[] input) throws IOException {
        PDDocument document = null;
        try {
            document = Loader.loadPDF(input);

            int resolution = 160;
            int page = 0;
            PDFRenderer renderer = new PDFRenderer(document);
            BufferedImage renderImage = renderer.renderImageWithDPI(page, resolution, ImageType.RGB);
            return renderImage;

        } finally {
            if (document != null) {
                document.close();
            }
        }
    }
}
