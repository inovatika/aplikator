package org.aplikator.shared.rpc.marshaller;

import java.io.IOException;

import org.aplikator.client.shared.data.ClientContext;
import org.aplikator.client.shared.data.PrimaryKey;
import org.aplikator.client.shared.data.RecordDTO;
import org.aplikator.client.shared.descriptor.FunctionDTO;
import org.aplikator.client.shared.descriptor.PropertyDTO;
import org.aplikator.client.shared.rpc.marshaller.ClientContextMarshaller;
import org.aplikator.client.shared.rpc.marshaller.FunctionDTOMarshaller;
import org.aplikator.client.shared.rpc.marshaller.FunctionParametersMarshaller;
import org.aplikator.client.shared.rpc.marshaller.PrimaryKeyMarshaller;
import org.aplikator.client.shared.rpc.marshaller.PropertyDTOMarshaller;
import org.aplikator.client.shared.rpc.marshaller.RecordDTOMarshaller;
import org.aplikator.utils.IOUtils;
import org.easymock.EasyMock;
import org.jboss.errai.marshalling.client.api.Marshaller;
import org.jboss.errai.marshalling.client.api.MarshallingSession;
import org.jboss.errai.marshalling.client.api.json.EJValue;
import org.jboss.errai.marshalling.client.marshallers.StringMarshaller;
import org.jboss.errai.marshalling.server.JSONDecoder;

import junit.framework.TestCase;

public class ParameterWrapperRunFunctionMarshallerTest extends TestCase {

    public void testMarshall() throws IOException {
        byte[] bytes = IOUtils.readBytes(ParameterWrapperRunFunctionMarshallerTest.class.getResourceAsStream("parameterwrapperrunfunciton.real.txt"), true);
        String str = new String(bytes);
        EJValue decoded = JSONDecoder.decode(str);

        MarshallingSession ctx = ctx();
        FunctionParametersMarshaller rf = new FunctionParametersMarshaller();

    }

    @SuppressWarnings("unchecked")
    private MarshallingSession ctx() {
        MarshallingSession session = EasyMock.createMock(MarshallingSession.class);
        PrimaryKeyMarshaller pkmarsh = new PrimaryKeyMarshaller();
        Object marsh = pkmarsh;
        EasyMock.expect(session.getMarshallerInstance(PrimaryKey.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        StringMarshaller smarsh = new StringMarshaller();
        marsh = smarsh;
        EasyMock.expect(session.getMarshallerInstance(String.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        RecordDTOMarshaller rmarsh = new RecordDTOMarshaller();
        marsh = rmarsh;
        EasyMock.expect(session.getMarshallerInstance(RecordDTO.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        PropertyDTOMarshaller pmarsh = new PropertyDTOMarshaller();
        marsh = pmarsh;
        EasyMock.expect(session.getMarshallerInstance(PropertyDTO.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        FunctionDTOMarshaller fmarsh = new FunctionDTOMarshaller();
        marsh = fmarsh;
        EasyMock.expect(session.getMarshallerInstance(FunctionDTO.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();

        ClientContextMarshaller cmarsh = new ClientContextMarshaller();
        marsh = cmarsh;
        EasyMock.expect(session.getMarshallerInstance(ClientContext.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();


        EasyMock.replay(session);
        return session;
    }

}
