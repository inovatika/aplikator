package org.aplikator.server.data;

import java.util.List;

import org.aplikator.client.shared.data.ClientContext;
import org.aplikator.client.shared.data.FunctionResult;
import org.aplikator.server.descriptor.Function;
import org.aplikator.server.descriptor.WizardPage;

public abstract class Executable {

    protected Function function;

    public abstract FunctionResult execute(Record currentRecord, Record wizardParameters, ClientContext clientContext, Context context);


    public WizardPage getWizardPage(String currentPageId, boolean forwardDirection, Record currentRecord, Record wizardParameters, ClientContext clientContext, Context context) {
        List<WizardPage> pages = this.function.getRegisteredViews();
        int currentPageIndex = -1;
        for (int i = 0; i < pages.size(); i++) {
            if (pages.get(i).getPageId().equals(currentPageId)) {
                currentPageIndex = i;
            }
        }
        int nextPageIndex = forwardDirection ? Math.min(currentPageIndex + 1, pages.size() - 1) : Math.max(currentPageIndex - 1, 0);
        if (nextPageIndex < 0) {
            return null;
        }

        WizardPage p = pages.get(nextPageIndex);
        p.setHasExecute(nextPageIndex == pages.size() - 1);
        p.setHasNext(nextPageIndex < pages.size() - 1);
        p.setHasPrevious(nextPageIndex > 0);
        p.setClientRecord(wizardParameters);
        return p;
    }

    public void setFunction(Function func) {
        function = func;
    }

}
