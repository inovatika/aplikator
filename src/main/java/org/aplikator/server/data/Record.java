package org.aplikator.server.data;

import com.google.gwt.safehtml.shared.SafeHtml;
import org.aplikator.client.shared.data.ListItem;
import org.aplikator.client.shared.data.PrimaryKey;
import org.aplikator.client.shared.data.RecordDTO;
import org.aplikator.server.descriptor.Collection;
import org.aplikator.server.descriptor.Property;
import org.aplikator.server.descriptor.Reference;
import org.aplikator.server.descriptor.SortItem;
import org.aplikator.server.descriptor.View;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.aplikator.utils.AplikatorUtils.getDateAsString;

public class Record implements Serializable {

    private RecordDTO recordDTO;

    public Record(RecordDTO recordDTO) {
        this.recordDTO = recordDTO;
    }

    public RecordDTO getRecordDTO() {
        return recordDTO;
    }

    public <X extends Serializable> void setValue(Property<X> property, X value) {
        recordDTO.setValue(property.getRecordId(), value);
    }


    @SuppressWarnings("unchecked")
    public <X extends Serializable> X getValue(Property<X> property) {
        return (X) recordDTO.getValue(property.getRecordId());
    }


    public String getStringValue(Property property, Context ctx) {
        Object val = recordDTO.getValue(property.getRecordId());
        if (val == null) {
            return "";
        }
        if (property.getListProvider() != null) {
            for (ListItem listItem : property.getListProvider().getListValues(ctx)) {
                if (val.equals(listItem.getValue())) {
                    return listItem.getName();
                }
            }
            return val.toString();
        }
        if (java.util.Date.class.equals(property.getType())) {
            Date date = (Date) val;
            return getDateAsString(date, property.getFormatPattern());
        }
        return val.toString();

    }

    public List<Record> getCollectionRecords(Collection collection, Context ctx) {
        return getCollectionRecords(collection, null, ctx);
    }

    @SuppressWarnings("unchecked")
    public List<Record> getCollectionRecords(Collection collection, View view, Context ctx) {
        if (this.getPrimaryKey().getId() != -1) {
            if (view != null) {
                return ctx.getRecords(view).withOwner(collection, this.getPrimaryKey()).withDefaultSort().list();
            } else {
                return ctx.getRecords(collection.referredEntity).withOwner(collection, this.getPrimaryKey()).withSort(SortItem.ascending(collection.referredEntity.getPrimaryKey())).list();
            }
        } else {
            List<Record> retval = new ArrayList<Record>();
            for (ContainerNode containerNode : ctx.getRecordContainer().getRecords()) {
                if (collection.getId().equals(containerNode.getEdited().getOwnerPropertyId())
                        && this.getPrimaryKey().equals(containerNode.getEdited().getOwnerPrimaryKey())) {
                    retval.add(containerNode.getEdited());
                }
            }
            return retval;
        }
    }


    @SuppressWarnings("unchecked")
    public Record getReferredRecord(Reference reference, Context ctx) {
        return getReferredRecord(reference,reference.referredEntity.view(), ctx );
    }

    public Record getReferredRecord(Reference reference, Context ctx, boolean limitToView) {
        return getReferredRecord(reference,reference.referredEntity.view(), ctx , limitToView);
    }

    public Record getReferredRecord(Reference reference, View view, Context ctx, boolean limitToView) {
        Integer refPK = (Integer) this.getValue(reference);
        if (refPK == null || refPK == -1) {
            return null;
        } else {
            return ctx.getRecord(new PrimaryKey(reference.referredEntity.getId(), refPK), view, limitToView);
        }
    }

    public Record getReferredRecord(Reference reference, View view, Context ctx){
        return getReferredRecord(reference, view,ctx,false);
    }

    public PrimaryKey getPrimaryKey() {
        return recordDTO.getPrimaryKey();
    }

    public void setPrimaryKey(PrimaryKey pk) {
        recordDTO.setPrimaryKey(pk);
    }

    public PrimaryKey getOwnerPrimaryKey() {
        return recordDTO.getOwnerPrimaryKey();
    }

    public void setOwnerPrimaryKey(PrimaryKey ownerPrimaryKey) {
        recordDTO.setOwnerPrimaryKey(ownerPrimaryKey);
    }

    public String getOwnerPropertyId() {
        return recordDTO.getOwnerPropertyId();
    }

    public void setOwnerProperty(Property ownerProperty) {
        if (ownerProperty != null) {
            recordDTO.setOwnerPropertyId(ownerProperty.getId());
        }
    }

    public Record clone() {
        Record retval = new Record(recordDTO.clone());
        return retval;
    }

    @Override
    public String toString() {
        return recordDTO.toString();
    }

    public String toHTML(View view, Context ctx) {
        return recordDTO.toHTML(view.getViewDTO(ctx));
    }

    public SafeHtml toSafeHTML(View view, Context ctx) {
        return recordDTO.toSafeHTML(view.getViewDTO(ctx));
    }

    public String getPreview() {
        return recordDTO.getPreview();
    }

    public void setPreview(SafeHtml preview) {
        recordDTO.setPreview(preview);
    }

    public boolean isDirty(Property property) {
        return recordDTO.isDirty(property.getId());
    }


    public void resetDirtyFlags() {
        recordDTO.resetDirtyFlags();
    }

    public void putAnnotation(String key, String value) {
        recordDTO.putAnnotation(key, value);
    }

    public void removeAnnotation(String key) {
        recordDTO.removeAnnotation(key);
    }

    public String getAnnotation(String key) {
        return recordDTO.getAnnotation(key);
    }

    public boolean hasAnnotation(String key) {
        return recordDTO.hasAnnotation(key);
    }

    public void putPropertyAnnotation(Property property, String key, String value) {
        recordDTO.putPropertyAnnotation(property.getRecordId(), key, value);
    }

    public void removePropertyAnnotation(Property property, String key) {
        recordDTO.removePropertyAnnotation(property.getRecordId(), key);
    }

    public String getPropertyAnnotation(Property property, String key) {
        return recordDTO.getPropertyAnnotation(property.getRecordId(), key);
    }

    public boolean hasPropertyAnnotation(Property property, String key) {
        return recordDTO.hasPropertyAnnotation(property.getRecordId(), key);
    }

    public void setDirty(String k) {
        recordDTO.setDirty(k);
    }

    public List<String> getDirtyFields() {
        return recordDTO.getDirtyFields();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Record record = (Record) o;
        return this.getPrimaryKey().equals(record.getPrimaryKey());
    }

    @Override
    public int hashCode() {
        return getPrimaryKey().hashCode();
    }

    /**
     * Replace the possible BinData values with their corrersponding tempFileIds
     * (safeguard method, used if something went wrong in processRecordContainer)
     */
    public void resetBinaryPropertiesToFileIDs() {
        for (String propertyId : recordDTO.getProperties()) {
            if (recordDTO.getValue(propertyId) instanceof BinaryData) {
                BinaryData bd = (BinaryData) recordDTO.getValue(propertyId);
                recordDTO.setValue(propertyId, bd.getTempFileId());
            }
        }
    }
}
