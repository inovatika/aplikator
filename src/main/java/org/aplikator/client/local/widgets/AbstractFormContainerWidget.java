/*
 * Copyright 2013 Pavel Stastny <pavel.stastny at gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.aplikator.client.local.widgets;

import com.google.gwt.user.client.ui.Composite;
import org.aplikator.client.shared.data.RecordDTO;
import org.aplikator.client.shared.descriptor.PropertyDTO;

import java.io.Serializable;

/**
 * Represents abstract FormWidget
 *
 * @author Pavel Stastny <pavel.stastny at gmail.com>
 */
public abstract class AbstractFormContainerWidget extends Composite implements HasFields {


    public abstract RecordDTO getPopulateRecord();

    @SuppressWarnings("unchecked")
    public void populateFormWithSimpleData(boolean setDirty) {
        RecordDTO sourceRecord = getPopulateRecord();
        setDirty(setDirty);
        for (DataField field : getDataFields()) {
            field.processAnnotations(sourceRecord);
            PropertyDTO prop = field.getProperty();
            Serializable val = prop.getValue(sourceRecord);
            field.setValue(val);
            if (val != null) {
                field.setDirty(setDirty);
            }
        }
    }
}
