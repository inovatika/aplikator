package org.aplikator.server.descriptor;

import org.aplikator.server.Configurator;
import org.aplikator.server.data.Context;

import java.util.Locale;

/**
 * Created with IntelliJ IDEA.
 * User: vlahoda
 * Date: 02.04.13
 * Time: 14:49
 * To change this template use File | Settings | File Templates.
 */
public abstract class WidgetDescriptorBase implements Widget {

    protected boolean horizontal = false;
    private String elementId = null;
    private String customLabelStyle = null;
    private int size = 0;


    public WidgetDescriptorBase() {
    }

    public WidgetDescriptorBase(boolean horizontal) {
        this.horizontal = horizontal;
    }

    @Override
    public boolean isHorizontal() {
        return horizontal;
    }

    @Override
    public Widget setHorizontal(boolean horizontal) {
        this.horizontal = horizontal;
        return this;
    }

    @Override
    public String getCustomLabelStyle() {
        return customLabelStyle;
    }

    @Override
    public Widget setCustomLabelStyle(String customLabelStyle) {
        this.customLabelStyle = customLabelStyle;
        return this;
    }

    @Override
    public String getElementId() {
        return elementId;
    }

    @Override
    public Widget setElementId(String elementId) {
        this.elementId = elementId;
        return this;
    }

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public Widget setSize(int size) {
        this.size = size;
        return this;
    }


    private String formatPattern;

    @Override
    public Widget setFormatPattern(String formatPattern) {
        this.formatPattern = formatPattern;
        return this;
    }

    @Override
    public String getFormatPattern() {
        return formatPattern;
    }

    private String localizationKey;

    @Override
    public Widget setLocalizationKey(String localizationKey) {
        this.localizationKey = localizationKey;
        return this;
    }

    @Override
    public String getLocalizationKey() {
        return localizationKey;
    }

    @Override
    public String getLocalizedName(Context ctx) {
        return Configurator.get().getLocalizedString(localizationKey, ctx != null ? ctx.getUserLocale() : Locale.getDefault());
    }

    private String localizationTooltipKey;

    @Override
    public String getLocalizationTooltipKey() {
        return localizationTooltipKey;
    }

    @Override
    public Widget setLocalizationTooltipKey(String localizationTooltipKey) {
        this.localizationTooltipKey = localizationTooltipKey;
        return this;
    }

    @Override
    public String getLocalizedTooltip(Context ctx) {
        return Configurator.get().getLocalizedString(localizationTooltipKey, ctx != null ? ctx.getUserLocale() : Locale.getDefault());
    }


    private boolean enabled = true;

    @Override
    public boolean isEnabled(Context ctx) {
        return enabled;
    }

    @Override
    public Widget setEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }

    private boolean visible = true;

    @Override
    public boolean isVisible(Context ctx) {
        return visible;
    }

    @Override
    public Widget setVisible(boolean visible) {
        this.visible = visible;
        return this;
    }

    private String icon = null;

    @Override
    public String getIcon() {
        return icon;
    }

    @Override
    public Widget setIcon(String icon) {
        this.icon = icon;
        return this;
    }
}
