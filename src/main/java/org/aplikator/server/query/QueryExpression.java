package org.aplikator.server.query;

import java.util.List;

import org.aplikator.server.descriptor.QueryParameter;

public abstract class QueryExpression {

    public abstract void bindParameters(List<QueryParameter> parameters);

    public BooleanQueryExpression AND(QueryExpression rightSide) {
        return new BooleanQueryExpression(this, BooleanQueryOperator.AND, rightSide);
    }

    public BooleanQueryExpression OR(QueryExpression rightSide) {
        return new BooleanQueryExpression(this, BooleanQueryOperator.OR, rightSide);
    }

}
