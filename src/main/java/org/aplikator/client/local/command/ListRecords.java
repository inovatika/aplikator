package org.aplikator.client.local.command;

import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.AcceptsOneWidget;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import org.aplikator.client.local.Aplikator;
import org.aplikator.client.local.widgets.LoadingLabel;
import org.aplikator.client.local.widgets.TableFactory;
import org.aplikator.client.local.widgets.TableInterface;
import org.aplikator.client.shared.descriptor.ViewDTO;
import org.aplikator.client.shared.rpc.AplikatorErrorCallback;
import org.aplikator.client.shared.rpc.AplikatorService;
import org.jboss.errai.common.client.api.RemoteCallback;
import org.jboss.errai.enterprise.client.jaxrs.api.RestClient;
import org.jboss.errai.ui.nav.client.local.Page;
import org.jboss.errai.ui.nav.client.local.PageHiding;
import org.jboss.errai.ui.nav.client.local.PageShown;
import org.jboss.errai.ui.nav.client.local.PageState;
import org.jboss.errai.ui.nav.client.local.api.NavigationControl;


@Page(path = "list/{viewID}")
public class ListRecords extends Composite {
    protected ViewDTO view;
    protected String searchArgument;
    protected HandlerRegistration submitHandlerRegistration;
    TableInterface table;
    SimplePanel holder;
    @PageState
    private String viewID;
    private Widget widget = null;

    public ListRecords() {
        holder = new SimplePanel();
        initWidget(holder);
    }

    @PageShown
    public void start() {
        holder.setWidget(new LoadingLabel(Aplikator.application.getConfigString("aplikator.menu.loading")));
        //Window.alert("ViewID:"+viewID+"-primaryKey:"+ primaryKey);
        if (view == null) {
            RestClient.create(AplikatorService.class, new RemoteCallback<ViewDTO>() {
                        @Override
                        public void callback(ViewDTO response) {
                            ListRecords.this.view = response;
                            drawPanel(holder);
                            Window.setTitle( Aplikator.application.getConfigString("aplikator.brand")+ " - "+ response.getLocalizedName());
                        }
                    },
                    new AplikatorErrorCallback()
            ).getView(viewID);
        } else {
            drawPanel(holder);
        }
    }

    private void drawPanel(AcceptsOneWidget panel) {

        if (widget == null) {
            widget = createTable();
        }
        panel.setWidget(widget);
    }

    private Widget createTable() {
        table = TableFactory.create(view.clone(), null, null, -1);
        table.reload();
        return table.asWidget();
    }

    @PageHiding
    public void onLeave(NavigationControl control){
        Aplikator.confirmCLose(control);
    }

}
