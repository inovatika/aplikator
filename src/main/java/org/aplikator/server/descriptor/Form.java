package org.aplikator.server.descriptor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Form extends LocalizedServerDescriptorBase {
    private Widget layout;

    private boolean horizontal;

    private List<Property<? extends Serializable>> properties = new ArrayList<Property<? extends Serializable>>();


    public Form() {
        this(true);
    }

    public Form(boolean horizontal) {
        super(null);
        this.horizontal = horizontal;
    }

    public List<Property<? extends Serializable>> getProperties() {
        return properties;
    }

    public Form addProperty(Property<? extends Serializable> property) {
        properties.add(property);
        return this;
    }

    public Widget getLayout() {
        return layout;
    }

    public Form setLayout(Widget layout) {
        this.layout = layout;
        this.layout.registerProperties(this);
        return this;
    }

    public static Form form(Widget layout, boolean horizontal) {
        Form retval = new Form(horizontal);
        return retval.setLayout(layout);
    }


    public boolean isHorizontal() {
        return horizontal;
    }

    public Form setHorizontal(boolean horizontal) {
        this.horizontal = horizontal;
        return this;
    }

    @Override
    public String toString() {
        return "Form{" + "layout=" + layout + ", properties=" + properties + '}';
    }


}
