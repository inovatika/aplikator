package org.aplikator.client.local.widgets;

import com.google.gwt.dom.client.Style;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import org.aplikator.client.shared.descriptor.PropertyDTO;
import org.gwtbootstrap3.client.ui.CheckBox;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;

public class CheckBoxWidget extends DataFieldBase<Boolean> implements DataField<Boolean> {
    private CheckBox box = new CheckBox();

    public CheckBoxWidget(String caption, String localizedTooltip, PropertyDTO property, int size, boolean horizontal, String elementId, String switcherInit, String customLabelStyle, HasFields form) {
        super(horizontal?null:caption, localizedTooltip, property, horizontal, elementId, customLabelStyle, form);

        if (horizontal) {
            box.setText(caption);
            //box.addStyleName("fieldLabel");
            box.getElement().getFirstChildElement().getFirstChildElement().getNextSiblingElement().getStyle().setMarginBottom(5, Style.Unit.PX);
            box.getElement().getFirstChildElement().getFirstChildElement().getNextSiblingElement().getStyle().setFontWeight(Style.FontWeight.BOLD);
            box.getElement().getFirstChildElement().getFirstChildElement().getNextSiblingElement().getStyle().setMarginTop(2, Style.Unit.PX);
            box.getElement().getFirstChildElement().getFirstChildElement().getNextSiblingElement().getStyle().setDisplay(Style.Display.INLINE_BLOCK);
        }
        controlHolder.add(box);

        //box.addStyleName(PaneWidgetResources.INSTANCE.css().formData());
        box.addValueChangeHandler(new ValueChangeHandler<Boolean>() {
            public void onValueChange(ValueChangeEvent<Boolean> event) {
                setDirty(true);
            }
        });
        if (switcherInit != null) {
            switcher = new VisibilitySwitcher(switcherInit, property.getType(), form);
            //Window.alert("SWITCHER:"+switcher);
            box.addChangeHandler(new ChangeHandler() {
                public void onChange(ChangeEvent event) {
                    switcher.apply(getValue());
                }
            });
        }

        this.setGridSize(size);

    }

    private VisibilitySwitcher<Boolean> switcher;


    public void setValue(Boolean value) {
        if (switcher != null){
            switcher.apply(value);
        }
        box.setValue(value, false);
    }

    public Boolean getValue() {
        return box.getValue();
    }

    public HandlerRegistration addValueChangeHandler(ValueChangeHandler<Boolean> handler) {
        return box.addValueChangeHandler(handler);
    }

    public void setEnabled(boolean enabled) {
        if (property.getRefferedThrough() != null) {
            enabled = false;
        }
        box.setEnabled(enabled);
    }

    @Override
    public void grabFocus() {
        Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand() {
            @Override
            public void execute() {
                box.setFocus(true);
            }
        });
    }

}
