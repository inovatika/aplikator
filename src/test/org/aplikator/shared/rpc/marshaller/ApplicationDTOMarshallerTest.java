package org.aplikator.shared.rpc.marshaller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.aplikator.client.shared.descriptor.ActionDTO;
import org.aplikator.client.shared.descriptor.ApplicationDTO;
import org.aplikator.client.shared.descriptor.MenuDTO;
import org.aplikator.client.shared.rpc.marshaller.ActionDTOMarshaller;
import org.aplikator.client.shared.rpc.marshaller.ApplicationDTOMarshaller;
import org.aplikator.client.shared.rpc.marshaller.MenuDTOMarshaller;
import org.easymock.EasyMock;
import org.jboss.errai.marshalling.client.api.Marshaller;
import org.jboss.errai.marshalling.client.api.MarshallingSession;
import org.jboss.errai.marshalling.client.api.json.EJValue;
import org.jboss.errai.marshalling.client.marshallers.StringMarshaller;
import org.jboss.errai.marshalling.server.JSONDecoder;
import org.junit.Assert;

import junit.framework.TestCase;

public class ApplicationDTOMarshallerTest extends TestCase {

    public static List<ActionDTO> aList1() {
        List<ActionDTO> list = new ArrayList<ActionDTO>();
        list.add(new ActionDTO("1", "locname-1", "token-1"));
        list.add(new ActionDTO("2", "locname-2", "token-2"));
        list.add(new ActionDTO("3", "locname-3", "token-3"));
        list.add(new ActionDTO("4", "locname-4", "token-4"));
        return list;
    }

    public static List<ActionDTO> aList2() {
        List<ActionDTO> list = new ArrayList<ActionDTO>();
        list.add(new ActionDTO("a", "locname-a", "token-a"));
        list.add(new ActionDTO("b", "locname-b", "token-b"));
        list.add(new ActionDTO("c", "locname-c", "token-c"));
        list.add(new ActionDTO("d", "locname-d", "token-d"));
        return list;
    }

    public static List<MenuDTO> mList() {
        List<MenuDTO> list = new ArrayList<MenuDTO>();
        MenuDTO mdto1 = new MenuDTO("one", "one-loc");
        for (ActionDTO a : aList1()) {
            mdto1.addAction(a);
        }
        list.add(mdto1);

        MenuDTO mdto2 = new MenuDTO("two", "two-loc");
        for (ActionDTO a : aList2()) {
            mdto2.addAction(a);
        }
        list.add(mdto2);

        return list;
    }


    public void testMarshall() {
        ApplicationDTO apdto = new ApplicationDTO();
        apdto.setBrand("Test-app-name");
        apdto.setShowNavigation(true);
        apdto.setDefaultAction("default-action");
        for (MenuDTO m : mList()) {
            apdto.addMenu(m);
        }
        apdto.setConfigString("config-key-1", "config-value-1");
        apdto.setConfigString("config-key-2", "config-value-2");
        apdto.setConfigString("config-key-3", "config-value-3");
        apdto.setConfigString("config-key-4", "config-value-4");


        MarshallingSession session = ctx();

        ApplicationDTOMarshaller appDTOMArshaller = new ApplicationDTOMarshaller();
        String marshalled = appDTOMArshaller.marshall(apdto, session);


        EJValue encoded = JSONDecoder.decode(marshalled);
        ApplicationDTO demarshalled = appDTOMArshaller.demarshall(encoded, session);
        Assert.assertTrue(demarshalled.getBrand().equals("Test-app-name"));
        Assert.assertTrue(demarshalled.isShowNavigation() == true);
        Assert.assertTrue(demarshalled.getDefaultActionToken().equals("default-action"));

        Assert.assertTrue(demarshalled.getMenus().size() == 2);
        Assert.assertTrue(demarshalled.getMenus().get(0).getActions().size() == 4);
        Assert.assertTrue(demarshalled.getMenus().get(1).getActions().size() == 4);

        Assert.assertTrue(demarshalled.getConfig().keySet().size() == 4);

        List<String> expectedKeys = new ArrayList<String>(Arrays.asList(
                "config-key-1",
                "config-key-2",
                "config-key-3",
                "config-key-4"
        ));
        HashSet<String> haset = new HashSet<String>(demarshalled.getConfig().keySet());
        while (!expectedKeys.isEmpty()) {
            String o = expectedKeys.get(0);
            Assert.assertTrue(haset.remove(o));
            expectedKeys.remove(o);
        }

        Assert.assertTrue(expectedKeys.size() == 0);
        Assert.assertTrue(haset.size() == 0);
    }

    @SuppressWarnings("unchecked")
    private MarshallingSession ctx() {
        MarshallingSession session = EasyMock.createMock(MarshallingSession.class);
        ActionDTOMarshaller adtoMarsh = new ActionDTOMarshaller();
        Object marsh = adtoMarsh;
        EasyMock.expect(session.getMarshallerInstance(ActionDTO.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();
        MenuDTOMarshaller mdtoMarsh = new MenuDTOMarshaller();
        marsh = mdtoMarsh;
        EasyMock.expect(session.getMarshallerInstance(MenuDTO.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();
        StringMarshaller smarshaller = new StringMarshaller();
        marsh = smarshaller;
        EasyMock.expect(session.getMarshallerInstance(String.class.getName())).andReturn((Marshaller<Object>) marsh).anyTimes();
        EasyMock.replay(session);
        return session;
    }
}
