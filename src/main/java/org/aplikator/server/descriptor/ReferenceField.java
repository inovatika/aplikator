package org.aplikator.server.descriptor;

import org.aplikator.client.shared.descriptor.ReferenceFieldDTO;
import org.aplikator.client.shared.descriptor.WidgetDTO;
import org.aplikator.server.data.Context;

import static org.aplikator.server.descriptor.Panel.row;

public class ReferenceField<T extends Entity> extends WidgetPropertyDescriptorBase<Integer> {

    private View view;
    private Widget child;

    public ReferenceField(Reference<T> property, View view, Widget child) {
        super(property);
        this.view = view;
        this.child = child.cloneWithReference(property);
    }

    @Override
    public WidgetDTO getWidgetDescriptor(Context ctx) {
        ReferenceFieldDTO desc = new ReferenceFieldDTO(getProperty().getPropertyDTO(ctx), view.getViewDTO(ctx), child.getWidgetDescriptor(ctx));
        desc.setSize(getSize());
        desc.setAccess(getProperty().getAccess(ctx));
        desc.setEnabled(isEnabled(ctx));
        desc.setVisible(isVisible(ctx));
        desc.setLocalizedName(getLocalizedName(ctx));
        desc.setLocalizedTooltip(getLocalizedTooltip(ctx));
        desc.setHorizontal(isHorizontal());
        desc.setElementId(getElementId());
        desc.setCustomLabelStyle(getCustomLabelStyle());
        return desc;
    }

    @Override
    public void registerProperties(Form form) {
        form.addProperty(getProperty());
        child.registerProperties(form);
    }

    public static <T extends Entity> ReferenceField<T> reference(Reference<T> property, View view, Widget child) {
        return new ReferenceField<T>(property, view, child);
    }

    public static <T extends Entity> ReferenceField<T> reference(Reference<T> property, Widget child) {
        return reference(property, property.referredEntity.view(), child);
    }

    public static <T extends Entity> ReferenceField<T> reference(Reference<T> property) {
        return reference(property, property.referredEntity.view(), property.referredEntity.view().getForm(null).getLayout());
    }

    public static <T extends Entity> ReferenceField<T> reference(Reference<T> property, View view) {
        return reference(property, view, view.getForm(null).getLayout());
    }


    @SuppressWarnings({"unchecked", "rawtypes"})
    public static <T extends Entity> ReferenceField<T> reference(Reference<T> property, View view, Property<?>... children) {
        Panel panel = row();
        for (Property<?> p : children) {
            panel.add(p);
        }
        return reference(property, view, panel);
    }

    public static <T extends Entity> ReferenceField<T> reference(Reference<T> property, Property<?>... children) {
        return reference(property, property.referredEntity.view(), children);
    }
}
