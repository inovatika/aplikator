package org.aplikator.server.persistence.empiredb;

import java.sql.Connection;

import org.apache.empire.db.DBCmdType;
import org.apache.empire.db.DBCommand;
import org.apache.empire.db.DBDatabase;
import org.apache.empire.db.DBDatabaseDriver;
import org.apache.empire.db.DBSQLScript;
import org.apache.empire.exceptions.MiscellaneousErrorException;
import org.aplikator.server.persistence.empiredb.oracle.EmpireDriverOracle;

public class EmpireDatabase extends DBDatabase {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Override
    public DBCommand createCommand() {
//		if (this.getDriver() instanceof DBDatabaseDriverOracle) {
//			return new EmpireCommandOracle(this);
//		} else {
        return super.createCommand();
//		}
    }

    public synchronized void getCreateDDLScript(DBCmdType type, DBDatabaseDriver driver, DBSQLScript script, Connection conn) {
        DBDatabaseDriver prevDriver = this.driver;
        try {
            // Set driver
            if (this.driver != null && this.driver != driver && driver != null) {   // The database belongs to a different driver
                throw new MiscellaneousErrorException("The database is attached to a different driver.");
            }
            // Temporarily change driver
            if (this.driver == null)
                this.driver = driver;
            // Get DDL Command
            if (driver instanceof EmpireDriverOracle) {
                ((EmpireDriverOracle) driver).getDDLScript(type, this, script, conn);
            } else {
                driver.getDDLScript(type, this, script);
            }

        } finally {
            this.driver = prevDriver;
        }
    }

}
