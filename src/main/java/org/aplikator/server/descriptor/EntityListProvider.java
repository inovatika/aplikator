package org.aplikator.server.descriptor;

import org.aplikator.client.shared.data.ListItem;
import org.aplikator.server.Configurator;
import org.aplikator.server.data.Context;
import org.aplikator.server.data.QueryBuilder;
import org.aplikator.server.data.Record;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * Implementation of the {@link ListProvider} interface dynamically backed by an Entity
 */
public class EntityListProvider extends ListProvider.Default {
    protected static final String DEFAULT_LANG_KEY = "default";
    protected View view;
    protected Property valueProperty;
    protected ConcurrentMap<String, Property> localeProperties = new ConcurrentSkipListMap<>();
    protected ConcurrentMap<String, Collection<ListItem>> listValues = new ConcurrentSkipListMap<>();
    protected boolean initialized;


    /**
     * @param listName      unique identifier of the list
     * @param view          {@link View} for the {@link Entity} that provides the values for this list.
     *                      The View must contain all Properties used as {@link #valueProperty}
     *                      and {@link #addLanguageProperty(String, Property)}
     * @param valueProperty The property (declared in the {@link #view} that contains the values
     *                      that should be returned by the ListItems {@link ListItem#getValue()} method
     */
    public EntityListProvider(String listName, View view, Property valueProperty) {
        this(listName, view, valueProperty, valueProperty);
    }

    public EntityListProvider(Enum listName, View view, Property valueProperty) {
        this(listName.name(), view, valueProperty);
    }

    public EntityListProvider(Enum listName, View view, Property valueProperty, Property defaultLocaleProperty) {
        this(listName.name(), view, valueProperty, defaultLocaleProperty);
    }

    public EntityListProvider(String listName, View view, Property valueProperty, Property defaultLocaleProperty) {
        super(listName);
        this.view = view;
        this.valueProperty = valueProperty;
        addLanguageProperty(DEFAULT_LANG_KEY, defaultLocaleProperty);
    }

    /**
     * Calling this method tells the EntityListProvider which property from the backing View should provide ListItem names
     * (localized labels) for the given locale
     *
     * @param lang     language code of the locale
     * @param property Property containing the names for the given locale
     * @return this object for fluent method call chaining
     */
    public EntityListProvider addLanguageProperty(String lang, Property property) {
        localeProperties.put(lang, property);
        listValues.put(lang, new ArrayList<ListItem>());
        return this;
    }

    /**
     * @param ctx
     */
    @SuppressWarnings("unchecked")
    public synchronized void refreshListValues(Context ctx) {
        for (String loc : listValues.keySet()) {
            if (sortByName) {
                listValues.replace(loc, new ConcurrentSkipListSet<>(new Comparator(loc)));
            } else {
                listValues.replace(loc, new ArrayList<>());
            }
        }
        QueryBuilder qb = ctx.getRecords(view);
        if (view.getQueryDescriptors().size() > 0) {
            String queryID = view.getQueryDescriptors().get(0).getId();
            qb.withQuery(view.getQueryDescriptor(queryID, ctx).getQueryExpression(null, ctx));
        }
        if (view.getSortDescriptors().size() > 0) {
            qb.withSort(view.getSortDescriptors().get(0).getItems().toArray(new SortItem[]{}));
        }
        List<Record> records = qb.list();
        for (Record record : records) {
            for (String loc : listValues.keySet()) {
                ListItem listItem = new ListItem.Default(record.getValue(valueProperty), record.getStringValue(localeProperties.get(loc), ctx));
                listValues.get(loc).add(listItem);
            }
        }
        initialized = true;
    }

    @Override
    public synchronized List<ListItem> getListValues(Context ctx) {
        if (!initialized){
            refreshListValues(ctx);
        }
        String lang = ctx.getUserLocale().getLanguage();

        Collection<ListItem> itemCollection = listValues.get(lang);
        if (itemCollection == null) {
            itemCollection = listValues.get(DEFAULT_LANG_KEY);
            lang = DEFAULT_LANG_KEY;
        }
        if (itemCollection.size() == 0) {
            itemCollection = listValues.get(lang);
        }
        if (sortByName) {
            return new ArrayList(itemCollection);
        } else {
            return (List) itemCollection;
        }
    }

    public static class Comparator implements java.util.Comparator<ListItem> {

        public Comparator() {
            this(Configurator.get().getConfig().getString(Configurator.DEFAULT_LOCALE));
        }

        public Comparator(String locale) {
            collator = Collator.getInstance(Locale.forLanguageTag(locale));
            if (collator == null) {
                collator = Collator.getInstance();
            }
        }

        private Collator collator;

        @Override
        public int compare(ListItem o1, ListItem o2) {
            return collator.compare(o1.getName(), o2.getName());
        }
    }
}
