package org.aplikator.server;

/**
 * User haven't permission for create new record
 *
 * @author Marek Kortus
 */
public class NoRightForNewRecordException extends RuntimeException {

    public NoRightForNewRecordException() {
    }

    public NoRightForNewRecordException(String message) {
        super(message);
    }

    public NoRightForNewRecordException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoRightForNewRecordException(Throwable cause) {
        super(cause);
    }
}
