package org.aplikator.server.descriptor;

import org.aplikator.client.shared.descriptor.RepeatedFormDTO;
import org.aplikator.client.shared.descriptor.WidgetDTO;
import org.aplikator.server.data.Context;

public class RepeatedForm extends WidgetPropertyDescriptorBase<Integer> {

    private View view;
    private boolean defaultSortId = false;

    public RepeatedForm(Collection<? extends Entity> property, View view) {
        super(property);
        this.view = view;
    }

    @Override
    public WidgetDTO getWidgetDescriptor(Context ctx) {
        RepeatedFormDTO desc = new RepeatedFormDTO(getProperty().getPropertyDTO(ctx), view.getViewDTO(ctx));
        desc.setSize(getSize());
        desc.setAccess(getProperty().getAccess(ctx));
        desc.setEnabled(isEnabled(ctx));
        desc.setVisible(isVisible(ctx));
        desc.setLocalizedName(getLocalizedName(ctx));
        desc.setLocalizedTooltip(getLocalizedTooltip(ctx));
        desc.setDefaultSortId(defaultSortId);
        desc.setHorizontal(isHorizontal());
        desc.setElementId(getElementId());
        desc.setCustomLabelStyle(getCustomLabelStyle());
        return desc;
    }

    @Override
    public void registerProperties(Form form) {
        // form.addProperty(property);
    }

    public static RepeatedForm repeated(Collection<? extends Entity> property) {
        return new RepeatedForm(property, property.referredEntity.view());
    }

    public static RepeatedForm repeated(Collection<? extends Entity> property, View view) {
        return new RepeatedForm(property, view);
    }

    public RepeatedForm useDefaultSortId() {
        this.defaultSortId = true;
        return this;
    }
}
