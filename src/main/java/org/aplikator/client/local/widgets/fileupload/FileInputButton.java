package org.aplikator.client.local.widgets.fileupload;

import org.gwtbootstrap3.client.ui.Icon;
import org.gwtbootstrap3.client.ui.constants.ButtonType;
import org.gwtbootstrap3.client.ui.constants.IconType;
import org.gwtbootstrap3.client.ui.constants.Styles;
import org.gwtbootstrap3.client.ui.html.Span;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JsArray;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FileUpload;

/*
 * Copied from https://groups.google.com/forum/#!msg/gwtbootstrap3/3Yd5fxfEHYA/RF2CpbwToXsJ
 * USAGE

 FileInputButton one = new FileInputButton(ButtonType.PRIMARY, false);
 one.setText("Select ONE");
 one.setIcon(IconType.UPLOAD);

 FileInputButton two = new FileInputButton(ButtonType.DANGER, true);
 two.setIcon(IconType.PLUS);
 two.setText("Select MANY");

 InputGroup ig = new InputGroup();

 final TextBox tb = new TextBox();

 tb.setEnabled(false);

 FileInputButton three = new FileInputButton(ButtonType.DEFAULT, false);
 three.setText("Browse...");
 three.addValueChangeHandler(new ValueChangeHandler<JsArray<UploadFile>>() {

@Override
public void onValueChange(ValueChangeEvent<JsArray<UploadFile>> event) {

tb.setText("");

if( event.getValue() != null && event.getValue().length() > 0 ){

tb.setText(event.getValue().get(0).getName());

}

}

});

 ig.add(new FileInputGroupButton(three));
 ig.add(tb);


 RootPanel.get().add(one);
 RootPanel.get().add(two);
 RootPanel.get().add(ig);
 */

public class FileInputButton extends Composite implements HasValueChangeHandlers<JsArray<UploadFile>> {

    public interface FileInputButtonResources extends ClientBundle {
        public static final FileInputButtonResources INSTANCE = GWT.create(FileInputButtonResources.class);

        @Source("FileInputButton.css")
        public FileInputButtonCss css();
    }

    public interface FileInputButtonCss extends CssResource {
        @ClassName("btn-file")
        public String btnFile();


    }

    static {
        FileInputButtonResources.INSTANCE.css().ensureInjected();
    }

    private Span wrapper;

    private Span textSpan;

    private FileUpload upload;

    private Icon icon;

    public FileInputButton(boolean isMultiple) {

        this(null, isMultiple);

    }


    public FileInputButton(ButtonType additionalStyle, boolean isMultiple) {

        wrapper = new Span();

        wrapper.addStyleName(Styles.BTN);

        if (additionalStyle != null) {

            wrapper.addStyleName(additionalStyle.getCssName());

        }

        //wrapper.addStyleName("btn-file");
        wrapper.addStyleName(FileInputButtonResources.INSTANCE.css().btnFile());

        initWidget(wrapper);

        upload = new FileUpload();

        if (isMultiple) {

            upload.getElement().setPropertyBoolean("multiple", true);

        }

        upload.addChangeHandler(new ChangeHandler() {


            @Override

            public void onChange(ChangeEvent event) {

                fireChanged();

            }

        });

        wrapper.add(upload);

    }


    public void setText(String text) {

        if (textSpan == null) {

            textSpan = new Span(text);

            wrapper.add(textSpan);

        } else {

            textSpan.setText(text);

        }

    }


    public void setIcon(IconType type) {

        if (icon == null) {

            icon = new Icon(type);

            wrapper.add(icon);

        } else {

            icon.setType(type);

        }

    }


    private void fireChanged() {

        ValueChangeEvent.fire(this, getFiles(upload.getElement()));

    }


    private native JsArray<UploadFile> getFiles(Element el) /*-{

        if (el.files) {

            return el.files;

        } else {

            return null;

        }

    }-*/;


    @Override

    public HandlerRegistration addValueChangeHandler(ValueChangeHandler<JsArray<UploadFile>> handler) {

        return super.addHandler(handler, ValueChangeEvent.getType());


    }
}

